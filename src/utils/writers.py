from datetime import timedelta, datetime, date
from typing import Tuple

from elasticsearch import helpers, Elasticsearch
from pymongo import MongoClient
from pymongo.cursor import Cursor
from sqlalchemy import Table, and_
from sqlalchemy.orm import close_all_sessions

from dwh_sync.dwh.entities import MatviewRefreshingProcess, IndexManager
from dwh_sync.dwh.mapping_writer_classes import RefreshingPlan
from utils.common import write_log, commit_session, write_exception, obtain_lock, flatten, create_config_dictionary, \
    create_session_new, enrich_year_month, remove_old_dates

from utils.constants import SynchronizationState, DbType, EngineType
from utils.readers import get_data, get_elastic_sync_size, get_reconciliation_length, get_data_for_fix, \
    create_mongo_filter_reconciliation, pre_fetch, get_data_from_string_query, create_order_rule_string

from utils.transformers import compare_metrics, create_cursor_mongo_reconciliation, modify_df_for_writing, \
    get_ids_for_months

from multiprocessing import Queue
from queue import Empty


def refresh_custom_reports(lock_record: str,
                           plpgsql_functions: set,
                           log_state: SynchronizationState) -> bool:
    """
    Refresh flat tables for some custom records - call special pgplsql functions
    :param lock_record: query for lock
    :param plpgsql_functions: pgplsql functions in DWH
    :param log_state: instance of SynchronizationState class which consists of log arguments
    :return: refresh status
    """
    log_state.message = f"start refreshing custom reports: {len(plpgsql_functions)} refreshers"
    write_log(log_state)
    if not obtain_lock(log_state.lock_session, lock_record, log_state):
        close_all_sessions()
        return False
    try:
        for func in plpgsql_functions:
            log_state.message = f"start refreshing - run {func}"
            write_log(log_state)
            log_state.dwh_session.execute(f'SELECT FROM {func}()')
            log_state.message = "report refreshed"
            write_log(log_state)
            commit_session(log_state.dwh_session, log_state)
        log_state.lock_session.rollback()
        return True
    # TODO add exception types
    except Exception as ex:
        log_state.message = f"Exception while refreshing report: {str(ex)}"
        write_exception(log_state)
        log_state.lock_session.rollback()
        close_all_sessions()
        return False


def refresh_mat_views(lock_record: str,
                      views: set,
                      log_state: SynchronizationState) -> bool:
    """
    Refresh materialized views after dwh sync
    :param lock_record: query for lock
    :param views: materialized views for update
    :param log_state: instance of SynchronizationState class which consists of log arguments
    :return: refresh status
    """
    log_state.message = f"start refreshing mat views: {len(views)} mat views"
    write_log(log_state)
    if not obtain_lock(log_state.lock_session, lock_record, log_state):
        close_all_sessions()
        return False
    try:
        for view in views:
            log_state.message = f"start refreshing {view}"
            write_log(log_state)
            log_state.dwh_session.execute(f'REFRESH MATERIALIZED VIEW CONCURRENTLY {view}')
            log_state.dwh_session.execute(f'ANALYZE {view}')
            log_state.message = "mat view refreshed"
            write_log(log_state)
            commit_session(log_state.dwh_session, log_state)
        log_state.lock_session.rollback()
        return True
    # TODO add exception types
    except Exception as ex:
        log_state.message = f"Exception while refreshing: {str(ex)}"
        write_exception(log_state)
        log_state.lock_session.rollback()
        close_all_sessions()
        return False


def refresh_mat_views_partitioned(lock_record: str,
                                  views: set,
                                  log_state: SynchronizationState,
                                  max_id: int) -> bool:
    """
    Refresh materialized views after dwh sync
    :param lock_record: query for lock
    :param views: materialized views for update
    :param log_state: instance of SynchronizationState class which consists of log arguments
    :param max_id: maximal id from RefreshingPlan
    :return: refresh status
    """
    log_state.message = f"start refreshing mat views: {len(views)} mat views"
    write_log(log_state)
    if not obtain_lock(log_state.lock_session, lock_record, log_state):
        close_all_sessions()
        return False

    for view in views:
        try:
            with IndexManager(log_state.refresh_mat_views_session, log_state, view):
                log_state.message = f"DANGER! start vacuum {view}"
                write_log(log_state)
                log_state.refresh_mat_views_session.execute(f'VACUUM FULL {view}')
                log_state.message = f"End vacuum: {view} unlocked"
                write_log(log_state)
                log_state.message = f"start refreshing {view}"
                write_log(log_state)
                log_state.refresh_mat_views_session.execute(f'REFRESH MATERIALIZED VIEW CONCURRENTLY {view}')
                log_state.message = "mat view refreshed"
                write_log(log_state)
            # log_state.refresh_mat_views_session.query(RefreshingPlan) \
            #     .filter(and_(RefreshingPlan.id <= max_id,
            #                  RefreshingPlan.matview_name == view.split('.')[1],
            #                  RefreshingPlan.refresh_time.is_(None))) \
            #     .update({'refresh_time': datetime.now()})
            commit_session(log_state.refresh_mat_views_session, log_state)
        # TODO add exception types
        except Exception as ex:
            if ex.__dict__.get('code') == 'f405':
                log_state.message = f"Exception while refreshing: {str(ex)} - can't find view"
                write_log(log_state)
            else:
                log_state.message = f"Exception while refreshing: {str(ex)}"
                write_exception(log_state)
            log_state.lock_session.rollback()
            close_all_sessions()
            continue
    log_state.lock_session.rollback()
    return True


def refresh_mat_views_partitioned_multiprocess(lock_record: str,
                                               views: set,
                                               log_state: SynchronizationState,
                                               max_id: int,
                                               config: dict) -> bool:
    """
    Refresh materialized views after dwh sync
    :param config: connection config dictionary
    :param lock_record: query for lock
    :param views: materialized views for update
    :param log_state: instance of SynchronizationState class which consists of log arguments
    :param max_id: maximal id from RefreshingPlan
    :return: refresh status
    """

    log_state.message = f"start refreshing mat views: {len(views)} mat views"
    write_log(log_state)
    queue = Queue()
    completed_processes = list()
    if not obtain_lock(log_state.lock_session, lock_record, log_state):
        close_all_sessions()
        return False
    for view_name in views:
        MatviewRefreshingProcess(queue, view_name, max_id, config, log_state).start()
    while True:
        try:
            view = queue.get(timeout=0)
            completed_processes.append(view)
        except Empty:
            continue
        else:
            if len(views) == len(completed_processes): break
    log_state.lock_session.rollback()
    return True


def write_rows(log_state: SynchronizationState, sync_length: range, filter_expression: str) -> bool:
    """
    Write rows from dataframe to dwh
    :param log_state: instance of SynchronizationState class which consists of log arguments
    :param sync_length: length of rows list for merge
    :param filter_expression: string filter expression for row
    :return: commit status as boolean
    """
    commit_status = False
    log_state.message = f'start sync for {log_state.table_name.__tablename__}'
    write_log(log_state)
    for offset in sync_length:
        df = get_data(log_state, filter_expression, offset)
        for row in df:
            # TODO  write in good way
            check_ticket_thread_body(row)
            log_state.dwh_session.merge(row)
        log_state.message = f"merged: {len(df)} rows, offset: {offset}"
        write_log(log_state)
        commit_status = commit_session(log_state.dwh_session, log_state)
    return commit_status


def check_ticket_thread_body(row):
    if hasattr(row, "body") and row.__tablename__ == "ticket_thread":
        try:
            row.body = row.body.decode('UTF-8')
        except Exception:
            row.body = 'decoding error'


def write_rows_from_mongo(cursor: Cursor, log_state: SynchronizationState) -> tuple:
    """
    Merges and write rows from mongo by specified query in cursor
    :param cursor: cursor query
    :param log_state: instance of SynchronizationState class which consists of log arguments
    :return: commit status as boolean
    """
    log_state.message = f'start mongo sync for {log_state.table_name.__tablename__}'
    write_log(log_state)
    batch = cursor.limit(log_state.batch_size)
    log_state.message = f"uploaded: {batch.count()} rows"
    write_log(log_state)
    rows_merged = 0
    mongo_months = set()
    mongo_ids = dict(zip(list(log_state.table_name.uuids()),
                         [set() for x in log_state.table_name.uuids()]))
    for c in batch:
        flattened_mongo_object = \
            {log_state.table_name.mongo_map[key]: value for key, value in flatten(c).items()
             if key in log_state.table_name.mongo_map.keys()}
        if 'prod_num' in log_state.table_name.__table__.c:
            flattened_mongo_object['prod_num'] = log_state.prod_number
        for key in log_state.table_name.uuids():
            if flattened_mongo_object.get(key) is None:
                flattened_mongo_object[key + '_compound'] = None
            else:
                flattened_mongo_object[key + '_compound'] = str(flattened_mongo_object.get(key, '')) \
                                                            + '-' + str(log_state.prod_number)
            mongo_ids[key].add(flattened_mongo_object.get(key, ''))
        row = log_state.table_name(**flattened_mongo_object)
        log_state.dwh_session.merge(row)
        rows_merged += 1
        mongo_months = enrich_year_month(log_state, flattened_mongo_object, mongo_months)
    log_state.message = f"merged: {rows_merged} rows"
    write_log(log_state)
    commit_session(log_state.dwh_session, log_state)
    return mongo_ids, mongo_months


def write_rows_from_elasticsearch(q: dict, log_state: SynchronizationState, index: str) -> Tuple:
    """
    Merges and write rows from elasticsearch by specified query in cursor
    :param q: elasticsearch query
    :param log_state: instance of SynchronizationState class which consists of log arguments
    :param index: name of index
    :return: commit status as boolean
    """
    log_state.message = f'start elastic sync for {log_state.table_name.__tablename__}'
    write_log(log_state)
    log_state.message = f"uploaded: {get_elastic_sync_size(log_state)} rows"
    write_log(log_state)
    rows_merged = 0
    elastic_months = set()
    elastic_ids = dict(zip(list(log_state.table_name.uuids()),
                           [set() for x in log_state.table_name.uuids()]))
    for hit in helpers.scan(log_state.source_session, q,
                            scroll=log_state.elasticsearch_scroll_size, size=int(log_state.batch_size / 10),
                            index=index, preserve_order=True, timeout='60s'):
        flattened_elasticsearch_object = \
            {log_state.table_name.elastic_map[key]: value for key, value in flatten(hit["_source"]).items() if
             key in log_state.table_name.elastic_map.keys()}
        if 'prod_num' in log_state.table_name.__table__.c:
            flattened_elasticsearch_object['prod_num'] = log_state.prod_number
        for key in log_state.table_name.uuids():
            if flattened_elasticsearch_object.get(key) is None:
                flattened_elasticsearch_object[key + '_compound'] = None
            else:
                flattened_elasticsearch_object[key + '_compound'] = str(flattened_elasticsearch_object.get(key, '')) \
                                                                    + '-' + str(log_state.prod_number)
                elastic_ids[key].add(flattened_elasticsearch_object.get(key, ''))
        row = log_state.table_name(**flattened_elasticsearch_object)
        log_state.dwh_session.merge(row)
        elastic_months = enrich_year_month(log_state, flattened_elasticsearch_object, elastic_months)
        commit_session(log_state.dwh_session, log_state)
        rows_merged += 1
    log_state.message = f"merged: {rows_merged} rows"
    write_log(log_state)
    # TODO add status for whole batch not a last one merged row
    return elastic_ids, elastic_months


def fix_data(log_state: SynchronizationState, bad_hours: list) -> bool:
    """
    Write data to dwh within bad hours list
    :param log_state: instance of SynchronizationState class which consists of log arguments
    :param bad_hours: list of datetime objects
    :return: status of final batch commit
    """
    reconciliation_state = True
    for bad_hour in bad_hours:
        log_state.message = f"Reconciliation for {str(bad_hour)}"
        write_log(log_state)
        for offset in get_reconciliation_length(log_state, bad_hour):
            for row in get_data_for_fix(log_state, bad_hour, offset):
                log_state.dwh_session.merge(row)
            log_state.message = f"merged: {len(get_data_for_fix(log_state, bad_hour, offset))} rows, offset: {offset}"
            write_log(log_state)
            reconciliation_state = commit_session(log_state.dwh_session, log_state)
    return reconciliation_state


def fix_data_mongo(log_state: SynchronizationState, bad_hours: list) -> datetime:
    """
    Write data to dwh within bad hours list
    :param log_state: instance of SynchronizationState class which consists of log arguments
    :param bad_hours: list of datetime objects
    :return: status of final batch commit
    """
    date_start = None
    for bad_hour in bad_hours:
        log_state.message = f"Reconciliation for {str(bad_hour)}"
        write_log(log_state)
        date_start = bad_hour
        cursor = create_cursor_mongo_reconciliation(log_state, create_mongo_filter_reconciliation(log_state, bad_hour))
        while cursor.count() > 1:
            date_start = write_rows_from_mongo(cursor, log_state)
            cursor = create_cursor_mongo_reconciliation(log_state, {log_state.table_name.mongo_filter_field:
                                                                        {'$gt': date_start,
                                                                         '$lt': bad_hour + timedelta(hours=1)}})
    return date_start


def fix_data_elasticsearch(log_state, bad_hours) -> bool:
    """
    Write data to dwh within bad hours list
    :param log_state: instance of SynchronizationState class which consists of log arguments
    :param bad_hours: list of datetime objects
    :return: status of last commit
    """
    commit_status = None
    for bad_hour in bad_hours:
        log_state.message = f"Reconciliation for {str(bad_hour)}"
        write_log(log_state)
        query = {"query": {"bool": {"must": {"exists": {"field": "uuid"}},
                                    "filter": {"range":
                                                   {log_state.table_name.elastic_filter_field:
                                                        {'gte': bad_hour, 'lt': bad_hour + timedelta(hours=1)}}}}},
                 "sort": [{"lastUpdatedDate": {"order": "asc"}}]}
        commit_status = write_rows_from_elasticsearch(query, log_state, 'profile')
    return commit_status


def reconcile_postgres(log_state, config_path, table, reconciliation) -> bool:
    """
    Reconciliation for postgres tables
    :param log_state: instance of SynchronizationState class which consists of log arguments
    :param config_path: path to .ini file
    :param table: table object
    :param reconciliation: reconciliation object
    :return: status of last commit
    """
    log_state.source_session = create_session_new(
        create_config_dictionary(config_path,
                                 DbType.SOURCE.value,
                                 dbname=table["dbname"],
                                 engine_type=EngineType.postgres.value))

    src_metrics = reconciliation.get_all(log_state, log_state.source_session,
                                         datetime.now() - timedelta(
                                             days=log_state.reconciliation_interval_days),
                                         datetime.now() - timedelta(
                                             days=1))
    dwh_metrics = reconciliation.get_all(log_state, log_state.dwh_session,
                                         datetime.now() - timedelta(
                                             days=log_state.reconciliation_interval_days),
                                         datetime.now() - timedelta(
                                             days=1))
    bad_hours = compare_metrics(log_state, src_metrics, dwh_metrics)
    if log_state.fix:
        return fix_data(log_state, bad_hours)


def reconcile_mongo(log_state, config_path, table, reconciliation) -> datetime:
    """
    Reconciliation for mongo tables
    :param log_state: instance of SynchronizationState class which consists of log arguments
    :param config_path: path to .ini file
    :param table: table object
    :param reconciliation: reconciliation object
    :return: last updated datetime
    """
    log_state.source_session = MongoClient(
        host=create_config_dictionary(config_path, DbType.SOURCE.value)['mongo_uri'],
        tz_aware=False)[table['dbname']]
    date_start = datetime.combine(date.today(), datetime.min.time()) - \
                 timedelta(days=log_state.reconciliation_interval_days)
    date_stop = datetime.combine(date.today(), datetime.min.time())
    src_metrics = reconciliation.get_all_mongo(log_state,
                                               date_start)
    dwh_metrics = reconciliation.get_all(log_state, log_state.dwh_session,
                                         date_start,
                                         date_stop)
    bad_hours = compare_metrics(log_state, src_metrics, dwh_metrics)
    if log_state.fix:
        return fix_data_mongo(log_state, bad_hours)


def reconcile_elasticsearch(log_state, config_path, reconciliation) -> bool:
    """
    Reconciliation for elasticsearch tables
    :param log_state: instance of SynchronizationState class which consists of log arguments
    :param config_path: path to .ini file
    :param reconciliation: reconciliation: reconciliation object
    :return: status of last commit
    """
    log_state.source_session = Elasticsearch(
        hosts=create_config_dictionary(config_path, DbType.SOURCE.value)
        ["elastic_host"])
    src_metrics = reconciliation.get_all_elasticsearch(log_state,
                                                       datetime.now() - timedelta(
                                                           days=log_state.reconciliation_interval_days),
                                                       datetime.now())
    dwh_metrics = reconciliation.get_all(log_state, log_state.dwh_session,
                                         datetime.now() - timedelta(
                                             days=log_state.reconciliation_interval_days),
                                         datetime.now())
    bad_hours = compare_metrics(log_state, src_metrics, dwh_metrics)
    if log_state.fix:
        return fix_data_elasticsearch(log_state, bad_hours)


def write_modified_rows(log_state: SynchronizationState, sync_length: range, filter_expression: str,
                        dwh_table: Table) -> tuple:
    """
    Write rows from dataframe to dwh with modifying of initial row set
    :param log_state: instance of SynchronizationState class which consists of log arguments
    :param sync_length: length of rows list for merge
    :param filter_expression: string filter expression for row
    :param dwh_table: table in dwh
    :return: commit status as boolean
    """
    log_state.message = f'start sync for {log_state.table_name.__tablename__}'
    write_log(log_state)
    postgres_ids = set()
    postgres_months = dict()
    for offset in sync_length:
        df, postgres_ids, postgres_months = modify_df_for_writing(
            get_data(log_state, filter_expression, offset), dwh_table, log_state)
        for row in df:
            log_state.dwh_session.merge(row)
        log_state.message = f"merged: {len(df)} rows, offset: {offset}"
        write_log(log_state)
        commit_session(log_state.dwh_session, log_state)
    return postgres_ids, postgres_months


def write_year_months(log_state: SynchronizationState,
                      matview_name: str,
                      year_months: set) -> bool:
    """
    Write year months into refresh plan table
    :param log_state: instance of SynchronizationState class which consists of log arguments
    :param matview_name: view of partitioned matview for reqfres
    :param year_months: list of year months in format 'yearmonth'
    :param sync_name: name of sync that generated this rows
    :return: commit status
    """
    for year_month in year_months:
        log_state.refresh_mat_views_session.add(
            RefreshingPlan(matview_name=matview_name + year_month,
                           sync_name=log_state.sync_name,
                           execution_time=datetime.now()
                           ))
    return log_state.refresh_mat_views_session.commit()


def updating_dates_for_es_profile_trading_payment(log_state: SynchronizationState,
                                                  months: set,
                                                  ids: dict,
                                                  entity_for_ids: Table,
                                                  ):
    """
    Write es_profile_trading_payment refreshing plan by existing months and ids of updating rows
    :param log_state: instance of SynchronizationState class which consists of log arguments
    :param months: years and months for mat views refreshing plan
    :param ids: ids for getting additional years and months for mat views refreshing plan
    :param sync_name: name of sync that generated this rows
    :param entity_for_ids: entity for ids
    :return:
    """
    log_state.message = f'start updating dates for es_profile_trading_payment'
    write_log(log_state)
    if log_state.table_name.__tablename__ == 'elastic_profile':
        write_year_months(log_state, 'es_profile_trading_payment', months)
        log_state.message = "dates for es_profile_trading_payment written"
        write_log(log_state)
        return
    get_ids_for_months(log_state, entity_for_ids(), ids)
    monthes = log_state \
        .dwh_session \
        .execute(f"select creation_time_profile from "
                 f"stg_tables_partitioned.es_profile_trading_payment_{log_state.table_name.__tablename__};")
    for month in monthes:
        months.add(remove_old_dates(month[0]))
    write_year_months(log_state, 'es_profile_trading_payment', months)
    log_state.message = "dates for es_profile_trading_payment written"
    write_log(log_state)


def updating_dates_for_trading_payment_all_status_v(log_state: SynchronizationState,
                                                    months: set,
                                                    ids: dict,
                                                    entity_for_ids: Table
                                                    ):
    """
    Write trading_payment_all_status_v refreshing plan by existing months and ids of updating rows
    :param log_state: instance of SynchronizationState class which consists of log arguments
    :param months: years and months for mat views refreshing plan
    :param ids: ids for getting additional years and months for mat views refreshing plan
    :param sync_name: name of sync that generated this rows
    :param entity_for_ids: entity for ids
    :return:
    """
    log_state.message = f'start updating dates for trading_payment_all_status_v'
    write_log(log_state)
    if log_state.table_name.__tablename__ == 'payment':
        write_year_months(log_state, 'trading_payment_all_status_v', months)
        log_state.message = "dates for trading_payment_all_status_v written"
        write_log(log_state)
        return
    get_ids_for_months(log_state, entity_for_ids(), ids)
    monthes = log_state \
        .dwh_session \
        .execute(f"select creation_time from "
                 f"stg_tables_partitioned.trading_payment_all_status_v_{log_state.table_name.__tablename__};")
    for month in monthes:
        months.add(remove_old_dates(month[0]))
    write_year_months(log_state, 'trading_payment_all_status_v', months)
    log_state.message = f"dates for trading_payment_all_status_v written"
    write_log(log_state)


def remove_prefetched_rows(log_state: SynchronizationState, source_table: Table, dwh_table: Table):
    """
    Removes rows which is not is in source but exists in dwh
    :param log_state: instance of SynchronizationState class which consists of log arguments
    :param source_table: entity for reading data
    :param dwh_table: entity for wirting data
    :return: status of last commit
    """
    log_state.message = 'We need to prefetch this table'
    write_log(log_state)
    deleted_rows = 0
    dwh_prefetch = pre_fetch(log_state, log_state.dwh_session, dwh_table)
    mismatch_bunch = set(dwh_prefetch.keys()) - \
                     set(pre_fetch(log_state, log_state.source_session, source_table).keys())
    log_state.message = f'Number of mismatch {len(mismatch_bunch)}'
    write_log(log_state)
    for ids in mismatch_bunch:
        deleted_rows += 1
        log_state.dwh_session.delete(dwh_prefetch.get(ids))
    log_state.message = f"Prefetched {deleted_rows} rows"
    write_log(log_state)
    return commit_session(log_state.dwh_session, log_state)


def write_rows_from_string_query(log_state: SynchronizationState, sync_length: range, query: str, filter: str) -> bool:
    """
    Write rows from dataframe to dwh
    :param log_state: instance of SynchronizationState class which consists of log arguments
    :param sync_length: length of rows list for merge
    :param query: string query for results
    :param filter: string expression for filtering
    :return: commit status as boolean
    """
    commit_status = False
    log_state.message = f'start sync for {log_state.table_name.__tablename__}'
    write_log(log_state)
    for offset in sync_length:
        options = f" OFFSET {offset * log_state.batch_size} ROWS FETCH NEXT {log_state.batch_size} ROWS ONLY"
        df = get_data_from_string_query(log_state, query + filter + create_order_rule_string(log_state) + options)
        log_state.message = f"uploaded {len(df)} rows, offset {offset}"
        write_log(log_state)
        for row in df:
            trx_customer = log_state.table_name(**dict(zip(log_state.table_name.__table__.columns.keys(), row)))
            log_state.dwh_session.merge(trx_customer)
        log_state.message = f"merged: {len(df)} rows, offset: {offset}"
        write_log(log_state)
        commit_status = commit_session(log_state.dwh_session, log_state)
    return commit_status


def save_dataframe_flat(config_dict, log_state, app_name, df, timestamp=None):
    if not timestamp:
        timestamp = datetime.now().strftime('%Y%m%d%H%M%S%f')
    schema = config_dict['schema']
    table_name = f"{app_name}{timestamp}"
    log_state.table_name.__tablename__ = f'{app_name}_prod0{log_state.prod_number}'
    log_state.message = f"Saving data to {config_dict['schema']}.{table_name}"
    write_log(log_state)
    df.write \
        .format("jdbc") \
        .option("url", f"jdbc:postgresql://{config_dict['host']}:5432/{config_dict['dbname']}") \
        .option("dbtable", f"{schema}.{table_name}_filling") \
        .option("user", config_dict['user']) \
        .option("password", config_dict['password']) \
        .save()
    rename_table(config_dict, log_state, f'{schema}.{table_name}_filling', table_name)
    log_state.message = f"{config_dict['schema']}.{table_name} saving complete"
    write_log(log_state)
    return timestamp


def save_dataframe(config_dict, log_state, df, table_name):
    schema = config_dict['schema']
    log_state.message = f"Saving data to {schema}.{table_name}"
    write_log(log_state)
    df.write \
        .mode('overwrite')\
        .format("jdbc") \
        .option("url", f"jdbc:postgresql://{config_dict['host']}:5432/{config_dict['dbname']}") \
        .option("dbtable", f"{schema}.{table_name}") \
        .option("user", config_dict['user']) \
        .option("password", config_dict['password']) \
        .save()
    log_state.message = f"{schema}.{table_name} saving complete"
    write_log(log_state)


def rename_table(config_dict: dict, log_state: SynchronizationState, old_table_path: str, new_name: str):
    dwh_session = create_session_new(config_dict)
    query = f"ALTER TABLE {old_table_path} RENAME TO {new_name}"
    dwh_session.execute(query)
    commit_session(dwh_session, log_state)


def drop_table(config_dict: dict, log_state: SynchronizationState, table_path: str):
    dwh_session = create_session_new(config_dict)
    query = f"DROP TABLE {table_path}"
    dwh_session.execute(query)
    commit_session(dwh_session, log_state)
