import collections
import uuid

from sqlalchemy import Table

from dwh_sync.dwh.mapping_auth2 import UserAuthority, Users, Authorities, AccessPhoneAuthorities
from dwh_sync.dwh.mapping_auth2_writer import UsersDWH, UserAuthorityDWH, AuthoritiesDWH, AccessPhoneAuthoritiesDWH
from utils.readers import get_max_id_by_prod_num
from utils.common import write_log, create_session_new, create_config_dictionary, write_exception, \
    obtain_lock, get_sync_length
from utils.constants import DbType, SynchronizationState, EngineType, ProdNumber, LOCK_AUTH2_QUERY, LOCK_QUERY
from utils.transformers import get_filter_or_backfill_by_prod_num
from utils.writers import write_modified_rows, remove_prefetched_rows, refresh_mat_views

tables = [
    {
        'table': Users,
        'dwh_table': UsersDWH,
        'dbname': 'auth2'
    },
    {
        'table': UserAuthority,
        'dwh_table': UserAuthorityDWH,
        'dbname': 'auth2'
    },
    {
        'table': Authorities,
        'dwh_table': AuthoritiesDWH,
        'dbname': 'auth2'
    },
    {
        'table': AccessPhoneAuthorities,
        'dwh_table': AccessPhoneAuthoritiesDWH,
        'dbname': 'auth2'
    }
]

matview_for_refreshing = {'public.operator_authority_v'}

config_path = "settings_auth2_prod01.ini"
log_session = create_session_new(create_config_dictionary(config_path,
                                                          DbType.DWH.value,
                                                          engine_type=EngineType.postgres.value))
dwh_session = create_session_new(create_config_dictionary(config_path,
                                                          DbType.DWH.value,
                                                          schema_translate_map=True,
                                                          engine_type=EngineType.postgres.value))
lock_session = create_session_new(create_config_dictionary(config_path,
                                                           DbType.DWH.value,
                                                           engine_type=EngineType.postgres.value))

log_state = SynchronizationState(log_session,
                                 uuid.uuid1(),
                                 dwh_session=dwh_session,
                                 lock_session=lock_session,
                                 source_engine_type=EngineType.postgres.value,
                                 prod_number=ProdNumber.prod01.value,
                                 sync_name=__file__)

for table in tables:
    try:
        log_state.table_name = table['table']
        log_state.source_session = create_session_new(
            create_config_dictionary(config_path,
                                     DbType.SOURCE.value,
                                     dbname=table["dbname"],
                                     engine_type=EngineType.postgres.value))
        if not obtain_lock(log_state.lock_session, LOCK_AUTH2_QUERY, log_state):
            continue
        f = get_filter_or_backfill_by_prod_num(
            log_state,
            table['dwh_table'],
            get_max_id_by_prod_num
        )
        if isinstance(log_state.table_name.filter_column, collections.Callable):
            remove_prefetched_rows(log_state, table['table'], table['dwh_table'])
        log_state.message = f"size of sync is no more than " \
                            f"{len(get_sync_length(log_state, f)) * log_state.batch_size} rows for " \
                            f"{log_state.table_name.__tablename__}"
        write_log(log_state)
        write_modified_rows(log_state,
                            get_sync_length(log_state, f),
                            f,
                            table['dwh_table'])
        log_state.message = f"table {table['table'].__tablename__} synchronized"
        write_log(log_state)
        log_state.lock_session.rollback()
    except Exception as ex:
        print(f'\n{"-" * 40}\nAuth2 Exception: {str(ex)}\n{"-" * 40}\n')
        log_state.message = f"DWH PROD01 Hierarchy Exception: {str(ex)}"
        write_exception(log_state)
        continue
try:
    log_state.table_name = Table()
    log_state.table_name.__tablename__ = 'operator_authority_v'
    refresh_mat_views(LOCK_QUERY, matview_for_refreshing, log_state)
except Exception as ex:
    print(f'\n{"-" * 40}\nException: {str(ex)}\n{"-" * 40}\n')
    log_state.message = f"DWH PROD01 Exception: {str(ex)}"
    write_exception(log_state)
