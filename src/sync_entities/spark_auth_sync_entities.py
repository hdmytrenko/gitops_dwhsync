import uuid
from typing import Optional

from pyspark.sql import DataFrame
from pyspark.sql.functions import lit, col, when
from sqlalchemy import Table

from utils.common import create_spark_session, add_prefix, create_config_dictionary, create_session_new
from utils.constants import SynchronizationState, DbType, EngineType
from utils.readers import generate_initial_query, read_postgresql_with_schema
from utils.transformers import add_compound_columns


class UsersSync:
    def __init__(self, app_name: str, log_state: SynchronizationState, config_dict: dict):
        self.spark_postgres_sql = create_spark_session(app_name)
        self.config_dict = config_dict
        self.uri = f"jdbc:postgresql://{config_dict['host']}:5432/auth2"
        self.log_state = log_state
        self.select_fields = ['id', 'uuid']
        self.custom_string_filter = str()

    def init_table_name(self, log_state: SynchronizationState) -> SynchronizationState:
        log_state.table_name = Table()
        log_state.table_name.__tablename__ = 'users'
        log_state.schema = self.config_dict.get("schema")
        return log_state

    def generate_query(self, select_fields: tuple = None, custom_string_filter: str = str()) -> str:
        return generate_initial_query(
            self.init_table_name(self.log_state),
            select_fields=select_fields,
            custom_string_filter=custom_string_filter
        )

    def read_df(self, query: str) -> DataFrame:
        df = add_prefix(read_postgresql_with_schema(self.spark_postgres_sql,
                                                    self.uri,
                                                    self.config_dict["user"],
                                                    self.config_dict["password"],
                                                    query
                                                    ), "users")
        return df

    def filter_df(self, custom_string_filter: str = str()) -> DataFrame:
        df = self.read_df(self.generate_query(self.select_fields, custom_string_filter))
        df_filtered = df.filter(~(col('users_uuid').like('%test%')))
        return df_filtered

    def get_operators_df(self):
        custom_string_filter = "uuid like 'OPERATOR%'"
        return self.filter_df(custom_string_filter)


class AuthoritiesSync:
    def __init__(self, app_name: str, log_state: SynchronizationState, config_dict: dict):
        self.spark_postgres_sql = create_spark_session(app_name)
        self.config_dict = config_dict
        self.uri = f"jdbc:postgresql://{config_dict['host']}:5432/auth2"
        self.log_state = log_state
        self.custom_string_filter = str()

    def init_table_name(self, log_state: SynchronizationState) -> SynchronizationState:
        log_state.table_name = Table()
        log_state.table_name.__tablename__ = 'authorities'
        log_state.schema = self.config_dict.get("schema")
        return log_state

    def generate_query(self, select_fields: tuple = None, custom_string_filter: str = str()) -> str:
        return generate_initial_query(
            self.init_table_name(self.log_state),
            select_fields=select_fields,
            custom_string_filter=custom_string_filter
        )

    def read_df(self, query: str) -> DataFrame:
        df = add_prefix(read_postgresql_with_schema(self.spark_postgres_sql,
                                                    self.uri,
                                                    self.config_dict["user"],
                                                    self.config_dict["password"],
                                                    query
                                                    ), "authorities")
        return df

    def filter_df(self) -> DataFrame:
        df = self.read_df(self.generate_query())
        df_filtered = df.filter(~(col('brand') == 'dobby'))
        return df_filtered


class UserAuthoritySync:
    def __init__(self, app_name: str, log_state: SynchronizationState, config_dict: dict):
        self.spark_postgres_sql = create_spark_session(app_name)
        self.config_dict = config_dict
        self.uri = f"jdbc:postgresql://{config_dict['host']}:5432/auth2"
        self.log_state = log_state
        self.custom_string_filter = str()

    def init_table_name(self, log_state: SynchronizationState) -> SynchronizationState:
        log_state.table_name = Table()
        log_state.table_name.__tablename__ = 'user_authority'
        log_state.schema = self.config_dict.get("schema")
        return log_state

    def generate_query(self, select_fields: tuple = None, custom_string_filter: str = str()) -> str:
        return generate_initial_query(
            self.init_table_name(self.log_state),
            select_fields=select_fields,
            custom_string_filter=custom_string_filter
        )

    def read_df(self, query: str) -> DataFrame:
        df = add_prefix(read_postgresql_with_schema(self.spark_postgres_sql,
                                                    self.uri,
                                                    self.config_dict["user"],
                                                    self.config_dict["password"],
                                                    query
                                                    ), "user_authority")
        return df

    def filter_df(self) -> DataFrame:
        df = self.read_df(self.generate_query())
        return df


class AuthorityActionSync:
    def __init__(self, app_name: str, log_state: SynchronizationState, config_dict: dict):
        self.spark_postgres_sql = create_spark_session(app_name)
        self.config_dict = config_dict
        self.uri = f"jdbc:postgresql://{config_dict['host']}:5432/auth2"
        self.log_state = log_state
        self.custom_string_filter = str()

    def init_table_name(self, log_state: SynchronizationState) -> SynchronizationState:
        log_state.table_name = Table()
        log_state.table_name.__tablename__ = 'authority_action'
        log_state.schema = self.config_dict.get("schema")
        return log_state

    def generate_query(self, select_fields: tuple = None, custom_string_filter: str = str()) -> str:
        return generate_initial_query(
            self.init_table_name(self.log_state),
            select_fields=select_fields,
            custom_string_filter=custom_string_filter
        )

    def read_df(self, query: str) -> DataFrame:
        df = add_prefix(read_postgresql_with_schema(self.spark_postgres_sql,
                                                    self.uri,
                                                    self.config_dict["user"],
                                                    self.config_dict["password"],
                                                    query
                                                    ), "authority_action")
        return df

    def filter_df(self, custom_string_filter: Optional[str] = None) -> DataFrame:
        df = self.read_df(self.generate_query(custom_string_filter=custom_string_filter))
        return df

    def get_access_phone_action(self):
        action_id_filter = 'action_id = 337'
        df = self.filter_df(action_id_filter)
        return df
