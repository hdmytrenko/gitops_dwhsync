--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: es_profile_trading_payment_luxistrade; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.es_profile_trading_payment_luxistrade AS
 SELECT ep.company,
    ep.brand_country,
    ep.profile_id,
    ep.login,
    ep.acquisition_status,
    ep.player_status,
    ep.sales_status,
    ep.sales_rep,
    ep.sales_name,
    ep.sales_user_type,
    ep.sales_desk_name,
    ep.sales_language,
    ep.retention_status,
    ep.retention_rep,
    ep.retention_name,
    ep.reten_user_type,
    ep.reten_desk_name,
    ep.reten_language,
    ep.creation_time_profile,
    ep.day_of_week_creation_time_profile,
    ep.day_name_creation_time_profile,
    ep.kyc_status,
    ep.mt_group,
    ep.payment_id,
    ep.ftd_status,
    ep.ftd,
    ep.ftd_deposit,
    ep.ftd_deposit_usd,
    ep.noftd,
    ep.redeposit,
    ep.redeposit_usd,
    ep.ftd_time,
    ep.day_to_ftd,
    ep.last_deposit_time,
    ep.currency,
    ep.deposit,
    ep.withdraw,
    ep.net_deposit,
    ep.transfer_in,
    ep.transfer_out,
    ep.credit_in,
    ep.credit_out,
    ep.deposit_usd,
    ep.withdraw_usd,
    ep.net_deposit_usd,
    ep.rate,
    ep.paymant_creation_time,
    ep.profile_country,
    ep.country,
    ep.profile_country_name,
    ep.language,
    ep.brand_id,
    ep.payment_method,
    ep.is_published,
    ep.profile_name,
    ep.email,
    ep.client_ip,
    ep.is_mobile,
    ep.payment_aggregator,
    ep.agent_id,
    ep.agent_name,
    ep.agent_desk_name,
    ep.agent_language,
    ep.user_type,
    ep.group_payment_method,
    ep.user_agent,
    ep.index,
    ep.affiliate_uuid,
    ep.affiliate_name,
    ep.affiliate_referral,
    ep.affiliate_source,
    ep.last_note,
    ep.last_note_date,
    ep.questionnaire_status,
    ep.last_login,
    ep.last_trade_date,
    ep.deposit_count,
    ep.amount,
    ep.amount_usd,
    ep.type,
    ep.profile_status,
    ep.fsa_migration_status,
    ep.mt4_balance,
    ep.mt4_balance_currency,
    ep.first_note,
    ep.first_note_date,
    ep.ftd_amount,
    ep.ftd_currency,
    ep.ltd_time,
    ep.migration_id,
    ep.kyc_changed_at
   FROM public.es_profile_trading_payment ep
  WHERE ((ep.brand_id)::text = 'luxistrade'::text);


ALTER TABLE public.es_profile_trading_payment_luxistrade OWNER TO postgres;

--
-- Name: TABLE es_profile_trading_payment_luxistrade; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public.es_profile_trading_payment_luxistrade TO prod01_metabase;
GRANT SELECT ON TABLE public.es_profile_trading_payment_luxistrade TO ybashkatov;


--
-- PostgreSQL database dump complete
--

