from sqlalchemy import Column, Integer, DATETIME, String, LargeBinary

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.ext.hybrid import hybrid_property

base = declarative_base()


class Department(base):
    __tablename__ = "department"
    id = Column(Integer, primary_key=True)
    name = Column(String(255))
    type = Column(String(255))
    sla = Column(Integer)
    manager_id = Column(Integer)
    ticket_assignment = Column(String(255))
    outgoing_email = Column(String(255))
    auto_ticket_response = Column(String(255))
    auto_message_response = Column(String(255))
    auto_response_email = Column(String(255))
    recipient = Column(String(255))
    group_access = Column(String(255))
    department_sign = Column(String(255))
    is_internal = Column(Integer)
    default_custom_field_id = Column(Integer)
    slack_channel = Column(String(255))
    send_slack_notifications = Column(Integer)
    default_help_topic_for_email_creation_id = Column(Integer)
    created_at = Column(DATETIME(timezone=False))
    updated_at = Column(DATETIME(timezone=False))

    @hybrid_property
    def filter_column(self):
        return self.updated_at


class Emails(base):
    __tablename__ = "emails"
    id = Column(Integer, primary_key=True)
    email_address = Column(String(255))
    email_name = Column(String(255))
    department = Column(Integer)
    priority = Column(Integer)
    help_topic = Column(Integer)
    user_name = Column(String(255))
    password = Column(String(512))
    fetching_host = Column(String(255))
    fetching_port = Column(String(255))
    fetching_protocol = Column(String(255))
    fetching_encryption = Column(String(255))
    mailbox_protocol = Column(String(255))
    imap_config = Column(String(255))
    folder = Column(String(255))
    sending_host = Column(String(255))
    sending_port = Column(String(255))
    sending_protocol = Column(String(255))
    sending_encryption = Column(String(255))
    smtp_validate = Column(String(255))
    smtp_authentication = Column(String(255))
    internal_notes = Column(String(255))
    fetching_status = Column(Integer)
    move_to_folder = Column(Integer)
    delete_email = Column(Integer)
    do_nothing = Column(Integer)
    sending_status = Column(Integer)
    authentication = Column(Integer)
    header_spoofing = Column(Integer)
    created_at = Column(DATETIME(timezone=False))
    updated_at = Column(DATETIME(timezone=False))

    @hybrid_property
    def filter_column(self):
        return self.updated_at


class Tickets(base):
    __tablename__ = "tickets"
    id = Column(Integer, primary_key=True)
    ticket_number = Column(Integer)
    user_id = Column(Integer)
    dept_id = Column(Integer)
    priority_id = Column(Integer)
    sla = Column(Integer)
    help_topic_id = Column(Integer)
    status = Column(String(255))
    rating = Column(Integer)
    ratingreply = Column(Integer)
    assigned_to = Column(Integer)
    source = Column(Integer)
    isanswered = Column(Integer)
    is_deleted = Column(Integer)
    reopened_at = Column(DATETIME(timezone=False))
    duedate = Column(DATETIME(timezone=False))
    closed_at = Column(DATETIME(timezone=False))
    last_thread_replied_at = Column(DATETIME(timezone=False))
    created_at = Column(DATETIME(timezone=False))
    updated_at = Column(DATETIME(timezone=False))
    is_internal = Column(Integer)
    title = Column(String(255))
    last_thread_id = Column(String(255))
    last_thread_user_id = Column(String(255))
    parent_id = Column(Integer)
    last_thread_replier_id = Column(String(255))
    body = Column(LargeBinary)
    brand_name = Column(String(255))
    sub_topic_id = Column(Integer)
    threads_count = Column(Integer)
    css = Column(String(255))
    resolved_by = Column(Integer)
    outgoing_email_id = Column(Integer)
    client_email = Column(String(255))
    original_fetched_from_email_id = Column(Integer)

    @hybrid_property
    def filter_column(self):
        return self.updated_at


class Users(base):
    __tablename__ = "users"
    id = Column(Integer, primary_key=True)
    user_name = Column(String(255))
    first_name = Column(String(255))
    last_name = Column(String(255))
    gender = Column(Integer)
    email = Column(String(255))
    ban = Column(Integer)
    password = Column(String(60))
    active = Column(Integer)
    is_delete = Column(Integer)
    ext = Column(String(255))
    country_code = Column(Integer)
    phone_number = Column(String(255))
    mobile = Column(String(255))
    agent_sign = Column(String(255))
    account_type = Column(String(255))
    account_status = Column(String(255))
    assign_group = Column(Integer)
    primary_dpt = Column(Integer)
    agent_tzone = Column(String(255))
    daylight_save = Column(String(255))
    limit_access = Column(String(255))
    directory_listing = Column(String(255))
    vacation_mode = Column(String(255))
    company = Column(String(255))
    role = Column(String(255))
    internal_note = Column(String(255))
    profile_pic = Column(String(255))
    remember_token = Column(String(100))
    created_at = Column(DATETIME(timezone=False))
    updated_at = Column(DATETIME(timezone=False))
    user_language = Column(String(10))
    ib_name = Column(String(255))
    can_receive_notifications_manager = Column(Integer)
    is_permitted_news = Column(Integer)
    organization_id = Column(Integer)
    tickets_count = Column(Integer)
    last_login = Column(DATETIME(timezone=False))
    is_permitted_reports = Column(Integer)
    slack_user_id = Column(String(255))
    can_track_time = Column(Integer)
    send_notifications_on_reply_for_tagged = Column(Integer)
    is_verified = Column(Integer)

    @staticmethod
    def filter_column():
        return None


class WorkLogs(base):
    __tablename__ = "work_logs"
    id = Column(Integer, primary_key=True)
    users_id = Column(Integer)
    tickets_id = Column(Integer)
    time_spent = Column(Integer)
    created_at = Column(DATETIME(timezone=False))
    updated_at = Column(DATETIME(timezone=False))
    comment = Column(String(255))
    deleted_at = Column(DATETIME(timezone=False))

    @hybrid_property
    def filter_column(self):
        return self.updated_at


class TicketThread(base):
    __tablename__ = "ticket_thread"
    id = Column(Integer, primary_key=True)
    ticket_id = Column(Integer)
    user_id = Column(Integer)
    poster = Column(String(255))
    is_internal = Column(Integer)
    title = Column(String(255))
    body = Column(String)
    #format = Column(String(255))
    created_at = Column(DATETIME(timezone=False))
    updated_at = Column(DATETIME(timezone=False))
    is_tagged_users_action = Column(Integer)
    is_edited = Column(Integer)
    is_created_from_user_panel = Column(Integer)
    was_mail_sent = Column(Integer)
    email_message_id = Column(String(255))
    edited_at = Column(DATETIME(timezone=False))
    email_receiving_date = Column(DATETIME(timezone=False))
    previous_status = Column(String(255))
    is_ticket_status_changed_manually_thread = Column(Integer)
    system_message_thread_id = Column(Integer)
    fetched_from_email_id = Column(Integer)
    sent_from_email_id = Column(Integer)

    @hybrid_property
    def filter_column(self):
        return self.updated_at


class HelpTopic(base):
    __tablename__ = "help_topic"
    id = Column(Integer, primary_key=True)
    topic = Column(String(255))
    parent_topic = Column(String(255))
    custom_form = Column(Integer)
    ticket_status = Column(String(255))
    priority = Column(Integer)
    sla_plan = Column(Integer)
    thank_page = Column(String(255))
    ticket_num_format = Column(String(255))
    internal_notes = Column(String(255))
    status = Column(Integer)
    type = Column(Integer)
    auto_assign = Column(Integer)
    auto_response = Column(Integer)
    created_at = Column(DATETIME(timezone=False))
    updated_at = Column(DATETIME(timezone=False))
    for_all_organizations = Column(Integer)
    is_default_for_email_creation = Column(Integer)

    @hybrid_property
    def filter_column(self):
        return self.updated_at


class SubTopic(base):
    __tablename__ = "sub_topic"
    id = Column(Integer, primary_key=True)
    name = Column(String(255))
    status = Column(Integer)
    created_at = Column(DATETIME(timezone=False))
    updated_at = Column(DATETIME(timezone=False))
    help_topic_id = Column(Integer)
    custom_forms_id = Column(Integer)

    @hybrid_property
    def filter_column(self):
        return self.updated_at


class TicketPriority(base):
    __tablename__ = "ticket_priority"
    priority_id = Column(Integer, primary_key=True)
    priority = Column(String(255))
    status = Column(String(255))
    priority_desc = Column(String(255))
    priority_color = Column(String(255))
    priority_urgency = Column(Integer)
    ispublic = Column(Integer)
    is_default = Column(String(255))
    created_at = Column(DATETIME(timezone=False))
    updated_at = Column(DATETIME(timezone=False))

    @hybrid_property
    def filter_column(self):
        return self.updated_at


class DepartmentsHelpTopics(base):
    __tablename__ = "departments_help_topics"
    department_id = Column(Integer, primary_key=True)
    help_topic_id = Column(Integer, primary_key=True)

    @staticmethod
    def filter_column():
        return None


class Teams(base):
    __tablename__ = "teams"
    id = Column(Integer, primary_key=True)
    name = Column(String(255))
    status = Column(Integer)
    team_lead = Column(Integer)
    assign_alert = Column(Integer)
    admin_notes = Column(String(255))
    created_at = Column(DATETIME(timezone=False))
    updated_at = Column(DATETIME(timezone=False))
    type = Column(Integer)
    department_id = Column(Integer)

    @hybrid_property
    def filter_column(self):
        return self.updated_at


class TeamsAgents(base):
    __tablename__ = "teams_agents"
    teams_id = Column(Integer, primary_key=True)
    agents_id = Column(Integer, primary_key=True)

    @staticmethod
    def filter_column():
        return None


class TicketAccessLog(base):
    __tablename__ = "ticket_access_log"
    id = Column(Integer, primary_key=True)
    route_name = Column(String(255))
    route = Column(String(255))
    client_ip = Column(String(255))
    http_client_ip = Column(String(255))
    x_forwarded_for = Column(String(255))
    remote_addr = Column(String(255))
    http_headers = Column(String)
    ticket_id = Column(Integer)
    user_id = Column(Integer)
    user_email = Column(String(255))
    user_role = Column(String(255))
    created_at = Column(DATETIME(timezone=False))
    updated_at = Column(DATETIME(timezone=False))
    original_user_id = Column(Integer)
    original_user_email = Column(String)
    original_user_role = Column(String)

    @hybrid_property
    def filter_column(self):
        return self.updated_at
