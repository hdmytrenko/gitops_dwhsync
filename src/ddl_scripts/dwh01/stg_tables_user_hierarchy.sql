--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

--
-- Name: user_hierarchy; Type: TABLE; Schema: stg_tables; Owner: postgres
--

CREATE TABLE stg_tables.user_hierarchy (
    id bigint NOT NULL,
    created_at timestamp without time zone NOT NULL,
    deleted_at timestamp without time zone,
    user_type character varying(255) NOT NULL,
    uuid character varying(60) NOT NULL,
    brand_ids text[] DEFAULT '{}'::text[] NOT NULL,
    version integer DEFAULT 0 NOT NULL,
    observer_for text[],
    observable_from text[],
    updated_at timestamp without time zone,
    prod_num integer
);


ALTER TABLE stg_tables.user_hierarchy OWNER TO postgres;

--
-- Name: user_hierarchy_id_seq; Type: SEQUENCE; Schema: stg_tables; Owner: postgres
--

CREATE SEQUENCE stg_tables.user_hierarchy_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE stg_tables.user_hierarchy_id_seq OWNER TO postgres;

--
-- Name: user_hierarchy_id_seq; Type: SEQUENCE OWNED BY; Schema: stg_tables; Owner: postgres
--

ALTER SEQUENCE stg_tables.user_hierarchy_id_seq OWNED BY stg_tables.user_hierarchy.id;


--
-- Name: user_hierarchy id; Type: DEFAULT; Schema: stg_tables; Owner: postgres
--

ALTER TABLE ONLY stg_tables.user_hierarchy ALTER COLUMN id SET DEFAULT nextval('stg_tables.user_hierarchy_id_seq'::regclass);


--
-- Name: user_hierarchy stg_user_hierarchy_pk; Type: CONSTRAINT; Schema: stg_tables; Owner: postgres
--

ALTER TABLE ONLY stg_tables.user_hierarchy
    ADD CONSTRAINT stg_user_hierarchy_pk PRIMARY KEY (id);


--
-- Name: user_hierarchy stg_user_hierarchy_uuid_key; Type: CONSTRAINT; Schema: stg_tables; Owner: postgres
--

ALTER TABLE ONLY stg_tables.user_hierarchy
    ADD CONSTRAINT stg_user_hierarchy_uuid_key UNIQUE (uuid);


--
-- Name: idx_stg_user_hierarchy_created_at; Type: INDEX; Schema: stg_tables; Owner: postgres
--

CREATE INDEX idx_stg_user_hierarchy_created_at ON stg_tables.user_hierarchy USING btree (created_at);


--
-- Name: idx_stg_user_hierarchy_updated_at; Type: INDEX; Schema: stg_tables; Owner: postgres
--

CREATE INDEX idx_stg_user_hierarchy_updated_at ON stg_tables.user_hierarchy USING btree (updated_at);


--
-- Name: idx_stg_user_hierarchy_user_type; Type: INDEX; Schema: stg_tables; Owner: postgres
--

CREATE INDEX idx_stg_user_hierarchy_user_type ON stg_tables.user_hierarchy USING btree (user_type);


--
-- Name: TABLE user_hierarchy; Type: ACL; Schema: stg_tables; Owner: postgres
--

GRANT ALL ON TABLE stg_tables.user_hierarchy TO data_sync;
GRANT SELECT ON TABLE stg_tables.user_hierarchy TO prod01_metabase;
GRANT SELECT ON TABLE stg_tables.user_hierarchy TO ybashkatov;


--
-- PostgreSQL database dump complete
--

