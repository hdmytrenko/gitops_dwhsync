--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

--
-- Name: trade_record; Type: TABLE; Schema: stg_tables; Owner: postgres
--

CREATE TABLE stg_tables.trade_record (
    id bigint NOT NULL,
    trade_id integer NOT NULL,
    login integer NOT NULL,
    symbol character varying(12),
    digits integer,
    cmd character varying(255),
    volume numeric(10,2),
    open_time bigint,
    close_time bigint,
    open_price double precision,
    close_price double precision,
    open_rate double precision,
    close_rate double precision,
    stop_loss double precision,
    take_profit double precision,
    expiration bigint,
    reason character varying(1),
    commission double precision,
    commission_agent double precision,
    storage double precision,
    profit double precision,
    taxes double precision,
    magic integer,
    comment character varying(64),
    "timestamp" bigint,
    closed boolean,
    trade_type character varying(255) NOT NULL,
    agent_id character varying(64),
    account_uuid character varying(128),
    profile_uuid character varying(128),
    brand_id character varying(128),
    platform_type character varying(64),
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    prod_num integer,
    migrated boolean,
    operation_type character varying(255),
    swap numeric(12,2)
);


ALTER TABLE stg_tables.trade_record OWNER TO postgres;

--
-- Name: trade_record stg_trade_record_pk; Type: CONSTRAINT; Schema: stg_tables; Owner: postgres
--

ALTER TABLE ONLY stg_tables.trade_record
    ADD CONSTRAINT stg_trade_record_pk PRIMARY KEY (id);


--
-- Name: idx_stg_trade_record_brand2prod; Type: INDEX; Schema: stg_tables; Owner: postgres
--

CREATE INDEX idx_stg_trade_record_brand2prod ON stg_tables.trade_record USING btree (public.brand2prod(brand_id));


--
-- Name: idx_stg_trade_record_cmd; Type: INDEX; Schema: stg_tables; Owner: postgres
--

CREATE INDEX idx_stg_trade_record_cmd ON stg_tables.trade_record USING btree (cmd);


--
-- Name: idx_stg_trade_record_created_at; Type: INDEX; Schema: stg_tables; Owner: postgres
--

CREATE INDEX idx_stg_trade_record_created_at ON stg_tables.trade_record USING btree (created_at);


--
-- Name: idx_stg_trade_record_login; Type: INDEX; Schema: stg_tables; Owner: postgres
--

CREATE INDEX idx_stg_trade_record_login ON stg_tables.trade_record USING btree (login);


--
-- Name: idx_stg_trade_record_trade_id; Type: INDEX; Schema: stg_tables; Owner: postgres
--

CREATE INDEX idx_stg_trade_record_trade_id ON stg_tables.trade_record USING btree (trade_id);


--
-- Name: idx_stg_trade_record_updated_at; Type: INDEX; Schema: stg_tables; Owner: postgres
--

CREATE INDEX idx_stg_trade_record_updated_at ON stg_tables.trade_record USING btree (updated_at);


--
-- Name: TABLE trade_record; Type: ACL; Schema: stg_tables; Owner: postgres
--

GRANT ALL ON TABLE stg_tables.trade_record TO data_sync;
GRANT SELECT ON TABLE stg_tables.trade_record TO prod01_metabase;
GRANT SELECT ON TABLE stg_tables.trade_record TO ybashkatov;


--
-- PostgreSQL database dump complete
--

