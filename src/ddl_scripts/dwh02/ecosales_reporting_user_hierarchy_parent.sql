--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: user_hierarchy_parent; Type: VIEW; Schema: ecosales_reporting; Owner: postgres
--

CREATE VIEW ecosales_reporting.user_hierarchy_parent AS
 SELECT user_hierarchy_parent.user_hierarchy_id,
    user_hierarchy_parent.parent_id
   FROM stg_tables.user_hierarchy_parent;


ALTER TABLE ecosales_reporting.user_hierarchy_parent OWNER TO postgres;

--
-- Name: TABLE user_hierarchy_parent; Type: ACL; Schema: ecosales_reporting; Owner: postgres
--

GRANT ALL ON TABLE ecosales_reporting.user_hierarchy_parent TO data_sync;
GRANT SELECT ON TABLE ecosales_reporting.user_hierarchy_parent TO ybashkatov;


--
-- PostgreSQL database dump complete
--

