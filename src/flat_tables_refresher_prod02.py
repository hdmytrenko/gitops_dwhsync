import sys
import uuid

from sqlalchemy import Table

from utils.common import create_session_new, create_config_dictionary, write_exception, write_log, \
    run_function, obtain_lock
from utils.constants import DbType, SynchronizationState, EngineType, ProdNumber, LOCK_QUERY

config_path = "settings_prod02.ini"
log_session = create_session_new(create_config_dictionary(config_path,
                                                          DbType.DWH.value,
                                                          engine_type=EngineType.postgres.value))
dwh_session = create_session_new(create_config_dictionary(config_path,
                                                          DbType.DWH.value,
                                                          schema_translate_map=True,
                                                          engine_type=EngineType.postgres.value))
lock_session = create_session_new(create_config_dictionary(config_path,
                                                           DbType.DWH.value,
                                                           engine_type=EngineType.postgres.value))

log_state = SynchronizationState(log_session,
                                 uuid.uuid1(),
                                 dwh_session=dwh_session,
                                 lock_session=lock_session,
                                 source_engine_type=EngineType.postgres.value,
                                 prod_number=ProdNumber.prod02.value,
                                 sync_name=__file__)

refresh_functions = {"stg_tables_flat.refresh_trading_payment", "stg_tables_flat.refresh_es_profile"}


def main():
    for function_name in refresh_functions:
        try:
            log_state.table_name = Table()
            log_state.table_name.__tablename__ = function_name
            if not obtain_lock(lock_session, LOCK_QUERY, log_state):
                continue
            response = run_function(dwh_session, log_state, function_name)
            if response:
                log_state.message = response
            else:
                log_state.message = 'No tables found to upsert'
            write_log(log_state)
        except Exception as ex:
            print(f'\n{"-" * 40}\nException: {str(ex)}\n{"-" * 40}\n')
            log_state.message = f"DWH PROD02 Exception: {str(ex)}"
            write_exception(log_state)


if __name__ == "__main__":
    sys.exit(main())
