--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: es_profile_trading_payment; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.es_profile_trading_payment AS
 SELECT (ep.prod_num)::text AS company,
    bc.brand_country,
    ep.player_uuid AS profile_id,
    tp.login,
    ep.acquisition_status,
        CASE ep.acquisition_status
            WHEN 'SALES'::text THEN ep.sales_status
            ELSE ep.retention_status
        END AS player_status,
    ep.sales_status,
    ep.sales_rep,
    sales.name AS sales_name,
    sales.user_type AS sales_user_type,
    COALESCE(sales.agent_desk_name, 'Null'::text) AS sales_desk_name,
    COALESCE(sales.agent_language, 'Null'::text) AS sales_language,
    ep.retention_status,
    ep.retention_rep,
    reten.name AS retention_name,
    reten.user_type AS reten_user_type,
    COALESCE(reten.agent_desk_name, 'Null'::text) AS reten_desk_name,
    COALESCE(reten.agent_language, 'Null'::text) AS reten_language,
    ep.registration_date AS creation_time_profile,
    date_part('isodow'::text, ep.registration_date) AS day_of_week_creation_time_profile,
    to_char(ep.registration_date, 'Day'::text) AS day_name_creation_time_profile,
    ep.kyc_status,
    ep.mt_group,
    tp.payment_id,
    tp.ftd_status,
    tp.ftd,
    tp.ftd_deposit,
    tp.ftd_deposit_usd,
    tp.noftd,
    tp.redeposit,
    tp.redeposit_usd,
    tp.ftd_time,
    ((tp.ftd_time)::date - (ep.registration_date)::date) AS day_to_ftd,
    tp.last_deposit_time,
    tp.currency,
    tp.deposit,
    tp.withdraw,
    tp.net_deposit,
    tp.transfer_in,
    tp.transfer_out,
    tp.credit_in,
    tp.credit_out,
    tp.deposit_usd,
    tp.withdraw_usd,
    tp.net_deposit_usd,
    tp.rate,
    tp.creation_time AS paymant_creation_time,
    upper((ep.country)::text) AS profile_country,
    upper((tp.country)::text) AS country,
    c.name AS profile_country_name,
    upper((ep.language_code)::text) AS language,
    ep.brand_id,
    tp.payment_method,
    tp.is_published,
    public.normalize_space((((ep.first_name)::text || ' '::text) || (ep.last_name)::text)) AS profile_name,
    ep.email,
    tp.client_ip,
    tp.is_mobile,
    tp.payment_aggregator,
    tp.agent_id,
    tp.agent_name,
    tp.agent_desk_name,
    tp.agent_language,
    tp.user_type,
    tp.group_payment_method,
    tp.user_agent,
    tp.index,
    (COALESCE(ep.affiliate_uuid, ('Null'::text)::character varying))::character varying(255) AS affiliate_uuid,
    COALESCE(concat(aff.first_name, ' ', aff.last_name), 'Null'::text) AS affiliate_name,
    (COALESCE(ep.affiliate_referral, ('Null'::text)::character varying))::character varying(255) AS affiliate_referral,
    (COALESCE(ep.affiliate_source, ('Null'::text)::character varying))::character varying(1024) AS affiliate_source,
    ep.last_note,
    ep.last_note_date,
    ep.questionnaire_status,
    ep.last_login,
    ep.last_trade_date,
    ep.deposit_count,
    tp.amount,
    tp.amount_usd,
    tp.type,
    ep.profile_status,
    ep.fsa_migration_status,
    ep.mt4_balance,
    ep.mt4_balance_currency,
    ep.first_note,
    ep.first_note_date,
    ep.ftd_amount,
    ep.ftd_currency,
    ep.ltd_time,
    ep.migration_id,
    ep.kyc_changed_at,
    ep.prod_num
   FROM ((((((frx_reporting.elastic_profile ep
     LEFT JOIN public.view_payment tp ON ((((ep.player_uuid)::text = (tp.profile_id)::text) AND ((tp.status)::text = 'PAYMENT_COMPLETED'::text))))
     LEFT JOIN stg_tables.country c ON (((c.alpha_2)::text = (ep.country)::text)))
     LEFT JOIN public.brand_country bc ON (((bc.brand_id)::text = (ep.brand_id)::text)))
     LEFT JOIN public.agent_v sales ON ((((ep.sales_rep)::text = (sales.agent_user_hierarchy_uuid)::text) AND ((sales.user_type)::text = ANY (ARRAY[('SALES_AGENT'::character varying)::text, ('SALES_HOD'::character varying)::text, ('SALES_LEAD'::character varying)::text, ('SALES_MANAGER'::character varying)::text])))))
     LEFT JOIN public.agent_v reten ON ((((ep.retention_rep)::text = (reten.agent_user_hierarchy_uuid)::text) AND ((reten.user_type)::text = ANY (ARRAY[('RETENTION_AGENT'::character varying)::text, ('RETENTION_HOD'::character varying)::text, ('RETENTION_LEAD'::character varying)::text, ('RETENTION_MANAGER'::character varying)::text])))))
     LEFT JOIN frx_reporting.affiliate aff ON ((((ep.affiliate_uuid)::text = (aff.uuid)::text) AND (aff.deleted = false))));


ALTER TABLE public.es_profile_trading_payment OWNER TO postgres;

--
-- Name: TABLE es_profile_trading_payment; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public.es_profile_trading_payment TO prod01_metabase;
GRANT SELECT ON TABLE public.es_profile_trading_payment TO ybashkatov;


--
-- PostgreSQL database dump complete
--

