--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

--
-- Name: group_type; Type: TABLE; Schema: stg_tables; Owner: data_sync
--

CREATE TABLE stg_tables.group_type (
    group_name character varying(64) NOT NULL,
    group_country character varying(2) NOT NULL,
    group_type character varying(16) NOT NULL,
    prod_num smallint
);


ALTER TABLE stg_tables.group_type OWNER TO data_sync;

--
-- Name: group_type group_type_pkey; Type: CONSTRAINT; Schema: stg_tables; Owner: data_sync
--

ALTER TABLE ONLY stg_tables.group_type
    ADD CONSTRAINT group_type_pkey PRIMARY KEY (group_name);


--
-- Name: TABLE group_type; Type: ACL; Schema: stg_tables; Owner: data_sync
--

GRANT SELECT ON TABLE stg_tables.group_type TO prod01_metabase;
GRANT SELECT ON TABLE stg_tables.group_type TO ybashkatov;


--
-- PostgreSQL database dump complete
--

