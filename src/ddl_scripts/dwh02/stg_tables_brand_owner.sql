--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

--
-- Name: brand_owner; Type: TABLE; Schema: stg_tables; Owner: postgres
--

CREATE TABLE stg_tables.brand_owner (
    brand_id character varying(32) NOT NULL,
    owner_name character varying(256) NOT NULL
);


ALTER TABLE stg_tables.brand_owner OWNER TO postgres;

--
-- Name: brand_owner brand_owner_pkey; Type: CONSTRAINT; Schema: stg_tables; Owner: postgres
--

ALTER TABLE ONLY stg_tables.brand_owner
    ADD CONSTRAINT brand_owner_pkey PRIMARY KEY (brand_id);


--
-- PostgreSQL database dump complete
--

