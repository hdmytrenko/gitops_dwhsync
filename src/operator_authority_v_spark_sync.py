import uuid

from pyspark.sql import DataFrame
from pyspark.sql.functions import lit, col, when
from sqlalchemy import Table
from sqlalchemy.orm import close_all_sessions

from sync_entities.spark_auth_sync_entities import UsersSync, AuthoritiesSync, UserAuthoritySync, AuthorityActionSync
from utils.common import create_config_dictionary, create_session_new, obtain_lock, write_exception, run_function, \
    write_log, create_spark_session
from utils.constants import SynchronizationState, DbType, EngineType, LOCK_QUERY
from utils.readers import generate_initial_query, read_postgresql_with_schema
from utils.transformers import add_compound_columns
from utils.writers import save_dataframe

config_path = "settings_spark_auth.ini"
app_name = 'operator_authority_v_spark_sync'
config_dict_source = create_config_dictionary(config_path,
                                              DbType.SOURCE.value,
                                              engine_type=EngineType.postgres.value)
config_dict_dwh = create_config_dictionary(config_path,
                                           DbType.DWH.value,
                                           engine_type=EngineType.postgres.value)

log_session = create_session_new(config_dict_dwh)
lock_session = create_session_new(config_dict_dwh)
dwh_session = create_session_new(create_config_dictionary(config_path,
                                                          DbType.DWH.value,
                                                          schema_translate_map=True,
                                                          engine_type=EngineType.postgres.value))
log_state = SynchronizationState(
    log_session,
    uuid.uuid1(),
    source_engine_type=EngineType.postgres.value,
    prod_number=config_dict_dwh['prod_num'],
    sync_name=__file__
)


class OperatorAuthorityV:
    def __init__(self):
        self.config_dict = config_dict_source
        self.log_state = log_state
        self.users = UsersSync(app_name, log_state, self.config_dict)
        self.authorities = AuthoritiesSync(app_name, log_state, self.config_dict)
        self.user_authority = UserAuthoritySync(app_name, log_state, self.config_dict)
        self.authority_action = AuthorityActionSync(app_name, log_state, self.config_dict)

    def user_authority_join(self):
        users_df = self.users.get_operators_df()
        user_authority_df = self.user_authority.filter_df()
        authorities_df = self.authorities.filter_df()
        df = users_df.join(
            user_authority_df,
            users_df['users_id'] == user_authority_df['user_authority_user_id'],
            'inner'
        ).join(
            authorities_df,
            user_authority_df['user_authority_authority_id'] == authorities_df['authorities_id'],
            'inner'
        )
        return df

    def user_authority_action_join(self, user_authority_df: DataFrame):
        authority_action_df = self.authority_action.get_access_phone_action()
        df = user_authority_df.join(
            authority_action_df,
            user_authority_df['authorities_id'] == authority_action_df['authority_action_authority_id'],
            'left'
        )
        return df

    def filter_df(self) -> DataFrame:
        user_authority_df = self.user_authority_join()
        df_joined = self.user_authority_action_join(user_authority_df)
        log_state.table_name.uuids = ['users_uuid']
        df_compounded = add_compound_columns(df_joined, log_state).withColumn('prod_num',
                                                                              lit(self.log_state.prod_number))
        df_filtered = df_compounded.select(
            col('users_uuid').alias('uuid'),
            col('users_uuid_compound').alias('uuid_compound'),
            col('authorities_brand').alias('brand'),
            col('authorities_department').alias('department'),
            col('authorities_role').alias('role'),
            col('prod_num'),
            when(col('authority_action_action_id').isNotNull(), lit(1)).otherwise(lit(0)).alias(
                'access_client_phone_number')
        )
        return df_filtered


def main():
    log_state.table_name = Table()
    log_state.table_name.__tablename__ = app_name + '_prod0' + config_dict_dwh['prod_num']
    if not obtain_lock(lock_session, LOCK_QUERY, log_state):
        close_all_sessions()
        exit(1)
    write_log(log_state, 'Start assembling dataframe')
    operator_authority_df = OperatorAuthorityV().filter_df()
    save_dataframe(config_dict_dwh, log_state, operator_authority_df,
                   'operator_authority_v_prod0' + log_state.prod_number)
    response = run_function(dwh_session, log_state, 'stg_auth2_tables.upsert_operator_authority_v',
                            log_state.prod_number)
    write_log(log_state, response)

    oa_dwh_sample = read_dwh_sample_table()
    rejected_df = oa_dwh_sample.join(
        operator_authority_df,
        [oa_dwh_sample['uuid_compound'] == operator_authority_df['uuid_compound'],
         oa_dwh_sample['brand'] == operator_authority_df['brand'],
         oa_dwh_sample['department'] == operator_authority_df['department'],
         oa_dwh_sample['role'] == operator_authority_df['role']],
        'left'
    ).filter(
        operator_authority_df['uuid_compound'].isNull()
    ).select(
        oa_dwh_sample['uuid_compound'],
        oa_dwh_sample['brand'],
        oa_dwh_sample['department'],
        oa_dwh_sample['role']
    )
    save_dataframe(config_dict_dwh, log_state, rejected_df,
                   'operator_authority_v_prod0' + log_state.prod_number + '_delete')
    response = run_function(dwh_session, log_state, 'stg_auth2_tables.delete_from_operator_authority_v',
                            log_state.prod_number)
    write_log(log_state, response)

    lock_session.rollback()
    write_log(log_state, 'Dataframe assembling complete')


def read_dwh_sample_table():
    log_state.table_name.__tablename__ = 'operator_authority_v'
    log_state.schema = 'public'
    query = generate_initial_query(
        log_state,
        select_fields=['uuid_compound', 'brand', 'department', 'role'],
        custom_string_filter=f"prod_num = '{log_state.prod_number}'"
    )
    return read_postgresql_with_schema(
        create_spark_session(app_name),
        f"jdbc:postgresql://{config_dict_dwh['host']}:5432/{config_dict_dwh['dbname']}",
        config_dict_dwh['user'],
        config_dict_dwh['password'],
        query
    )


if __name__ == '__main__':
    try:
        main()
    except Exception as ex:
        print(f'\n{"-" * 40}\nException: {str(ex)}\n{"-" * 40}\n')
        log_state.message = f"DWH PROD0{log_state.prod_number} Exception: {str(ex)}"
        write_exception(log_state)
