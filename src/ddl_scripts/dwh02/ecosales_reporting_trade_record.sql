--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: trade_record; Type: VIEW; Schema: ecosales_reporting; Owner: postgres
--

CREATE VIEW ecosales_reporting.trade_record AS
 SELECT trade_record.id,
    trade_record.trade_id,
    trade_record.login,
    trade_record.symbol,
    trade_record.digits,
    trade_record.cmd,
    trade_record.volume,
    trade_record.open_time,
    trade_record.close_time,
    trade_record.open_price,
    trade_record.close_price,
    trade_record.open_rate,
    trade_record.close_rate,
    trade_record.stop_loss,
    trade_record.take_profit,
    trade_record.expiration,
    trade_record.reason,
    trade_record.commission,
    trade_record.commission_agent,
    trade_record.storage,
    trade_record.profit,
    trade_record.taxes,
    trade_record.magic,
    trade_record.comment,
    trade_record."timestamp",
    trade_record.closed,
    trade_record.trade_type,
    trade_record.agent_id,
    trade_record.account_uuid,
    trade_record.profile_uuid,
    trade_record.brand_id,
    trade_record.platform_type,
    trade_record.created_at,
    trade_record.updated_at,
    trade_record.prod_num
   FROM stg_tables.trade_record
  WHERE ((trade_record.trade_type)::text <> 'DEMO'::text);


ALTER TABLE ecosales_reporting.trade_record OWNER TO postgres;

--
-- Name: TABLE trade_record; Type: ACL; Schema: ecosales_reporting; Owner: postgres
--

GRANT ALL ON TABLE ecosales_reporting.trade_record TO data_sync;
GRANT SELECT ON TABLE ecosales_reporting.trade_record TO ybashkatov;


--
-- PostgreSQL database dump complete
--

