--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

--
-- Name: sync_errors; Type: TABLE; Schema: full_sync_copies; Owner: data_sync
--

CREATE TABLE full_sync_copies.sync_errors (
    err_id integer NOT NULL,
    session_uuid uuid NOT NULL,
    execution_time timestamp without time zone,
    table_name character varying(256),
    pk_val character varying(512),
    error_msg text
);


ALTER TABLE full_sync_copies.sync_errors OWNER TO data_sync;

--
-- Name: sync_errors_err_id_seq; Type: SEQUENCE; Schema: full_sync_copies; Owner: data_sync
--

CREATE SEQUENCE full_sync_copies.sync_errors_err_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE full_sync_copies.sync_errors_err_id_seq OWNER TO data_sync;

--
-- Name: sync_errors_err_id_seq; Type: SEQUENCE OWNED BY; Schema: full_sync_copies; Owner: data_sync
--

ALTER SEQUENCE full_sync_copies.sync_errors_err_id_seq OWNED BY full_sync_copies.sync_errors.err_id;


--
-- Name: sync_errors err_id; Type: DEFAULT; Schema: full_sync_copies; Owner: data_sync
--

ALTER TABLE ONLY full_sync_copies.sync_errors ALTER COLUMN err_id SET DEFAULT nextval('full_sync_copies.sync_errors_err_id_seq'::regclass);


--
-- Name: sync_errors pk_sync_errors; Type: CONSTRAINT; Schema: full_sync_copies; Owner: data_sync
--

ALTER TABLE ONLY full_sync_copies.sync_errors
    ADD CONSTRAINT pk_sync_errors PRIMARY KEY (err_id);


--
-- Name: TABLE sync_errors; Type: ACL; Schema: full_sync_copies; Owner: data_sync
--

GRANT SELECT ON TABLE full_sync_copies.sync_errors TO prod01_metabase;


--
-- PostgreSQL database dump complete
--

