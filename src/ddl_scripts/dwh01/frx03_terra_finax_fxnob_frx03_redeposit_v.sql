--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: frx03_redeposit_v; Type: VIEW; Schema: frx03_terra_finax_fxnob; Owner: data_sync
--

CREATE VIEW frx03_terra_finax_fxnob.frx03_redeposit_v AS
 SELECT trading_payment_v.brand_id,
    trading_payment_v.profile_id,
    trading_payment_v.creation_time,
    trading_payment_v.profile_country_name,
    trading_payment_v.country,
    trading_payment_v.agent_name,
    trading_payment_v.agent_desk_name,
    trading_payment_v.user_type,
    sum(trading_payment_v.deposit_usd) AS deposit_usd,
    sum(trading_payment_v.withdraw_usd) AS withdraw_usd
   FROM public.trading_payment_v
  WHERE (((trading_payment_v.brand_id)::text = ANY (ARRAY[('finaxis'::character varying)::text, ('fxnobels'::character varying)::text, ('trustfx'::character varying)::text, ('terrafinance'::character varying)::text])) AND ((trading_payment_v.type)::text = ANY (ARRAY[('DEPOSIT'::character varying)::text, ('WITHDRAW'::character varying)::text])))
  GROUP BY trading_payment_v.brand_id, trading_payment_v.profile_id, trading_payment_v.creation_time, trading_payment_v.profile_country_name, trading_payment_v.country, trading_payment_v.agent_name, trading_payment_v.agent_desk_name, trading_payment_v.user_type
  ORDER BY trading_payment_v.brand_id, trading_payment_v.profile_id, trading_payment_v.creation_time, trading_payment_v.profile_country_name, trading_payment_v.country, trading_payment_v.agent_name, trading_payment_v.user_type;


ALTER TABLE frx03_terra_finax_fxnob.frx03_redeposit_v OWNER TO data_sync;

--
-- Name: TABLE frx03_redeposit_v; Type: ACL; Schema: frx03_terra_finax_fxnob; Owner: data_sync
--

REVOKE ALL ON TABLE frx03_terra_finax_fxnob.frx03_redeposit_v FROM data_sync;
GRANT ALL ON TABLE frx03_terra_finax_fxnob.frx03_redeposit_v TO postgres;
GRANT SELECT ON TABLE frx03_terra_finax_fxnob.frx03_redeposit_v TO prod01_metabase;
GRANT SELECT ON TABLE frx03_terra_finax_fxnob.frx03_redeposit_v TO ybashkatov;


--
-- PostgreSQL database dump complete
--

