--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

--
-- Name: currency; Type: TABLE; Schema: stg_tables; Owner: aleleko
--

CREATE TABLE stg_tables.currency (
    code integer NOT NULL,
    alpha_3 character(3),
    name character varying(255)
);


ALTER TABLE stg_tables.currency OWNER TO aleleko;

--
-- Name: currency currency_pkey; Type: CONSTRAINT; Schema: stg_tables; Owner: aleleko
--

ALTER TABLE ONLY stg_tables.currency
    ADD CONSTRAINT currency_pkey PRIMARY KEY (code);


--
-- Name: TABLE currency; Type: ACL; Schema: stg_tables; Owner: aleleko
--

GRANT SELECT ON TABLE stg_tables.currency TO ybashkatov;


--
-- PostgreSQL database dump complete
--

