--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: es_profile_trading_payment_macedonia; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.es_profile_trading_payment_macedonia AS
 SELECT eptp.company,
    eptp.brand_country,
    eptp.profile_id,
    eptp.login,
    eptp.acquisition_status,
    eptp.player_status,
    eptp.sales_status,
    eptp.sales_rep,
    eptp.sales_name,
    eptp.sales_user_type,
    eptp.sales_desk_name,
    eptp.sales_language,
    eptp.retention_status,
    eptp.retention_rep,
    eptp.retention_name,
    eptp.reten_user_type,
    eptp.reten_desk_name,
    eptp.reten_language,
    eptp.creation_time_profile,
    eptp.day_of_week_creation_time_profile,
    eptp.day_name_creation_time_profile,
    eptp.kyc_status,
    eptp.mt_group,
    eptp.payment_id,
    eptp.ftd_status,
    eptp.ftd,
    eptp.ftd_deposit,
    eptp.ftd_deposit_usd,
    eptp.noftd,
    eptp.redeposit,
    eptp.redeposit_usd,
    eptp.ftd_time,
    eptp.day_to_ftd,
    eptp.last_deposit_time,
    eptp.currency,
    eptp.deposit,
    eptp.withdraw,
    eptp.net_deposit,
    eptp.transfer_in,
    eptp.transfer_out,
    eptp.credit_in,
    eptp.credit_out,
    eptp.deposit_usd,
    eptp.withdraw_usd,
    eptp.net_deposit_usd,
    eptp.rate,
    eptp.paymant_creation_time,
    eptp.profile_country,
    eptp.country,
    eptp.profile_country_name,
    eptp.language,
    eptp.brand_id,
    eptp.payment_method,
    eptp.is_published,
    eptp.profile_name,
    eptp.email,
    eptp.client_ip,
    eptp.is_mobile,
    eptp.payment_aggregator,
    eptp.agent_id,
    eptp.agent_name,
    eptp.agent_desk_name,
    eptp.agent_language,
    eptp.user_type,
    eptp.group_payment_method,
    eptp.user_agent,
    eptp.index,
    eptp.affiliate_uuid,
    eptp.affiliate_name,
    eptp.affiliate_referral,
    eptp.affiliate_source,
    eptp.last_note,
    eptp.last_note_date,
    eptp.questionnaire_status,
    eptp.last_login,
    eptp.last_trade_date,
    eptp.deposit_count,
    eptp.amount,
    eptp.amount_usd,
    eptp.type,
    eptp.profile_status
   FROM public.es_profile_trading_payment eptp
  WHERE ((eptp.brand_country)::text = 'Macedonia'::text);


ALTER TABLE public.es_profile_trading_payment_macedonia OWNER TO postgres;

--
-- Name: TABLE es_profile_trading_payment_macedonia; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public.es_profile_trading_payment_macedonia TO prod01_metabase;
GRANT SELECT ON TABLE public.es_profile_trading_payment_macedonia TO ybashkatov;


--
-- PostgreSQL database dump complete
--

