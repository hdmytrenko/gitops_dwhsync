--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

--
-- Name: operator_action; Type: TABLE; Schema: stg_tables; Owner: postgres
--

CREATE TABLE stg_tables.operator_action (
    id integer NOT NULL,
    operator_uuid character varying(255) NOT NULL,
    operator_email character varying(64) NOT NULL,
    profile_uuid character varying(255) NOT NULL,
    profile_email character varying(100) NOT NULL,
    type character varying(64) NOT NULL,
    brand_id character varying(128) NOT NULL,
    action_date timestamp without time zone NOT NULL
);


ALTER TABLE stg_tables.operator_action OWNER TO postgres;

--
-- Name: operator_action_id_seq; Type: SEQUENCE; Schema: stg_tables; Owner: postgres
--

CREATE SEQUENCE stg_tables.operator_action_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE stg_tables.operator_action_id_seq OWNER TO postgres;

--
-- Name: operator_action_id_seq; Type: SEQUENCE OWNED BY; Schema: stg_tables; Owner: postgres
--

ALTER SEQUENCE stg_tables.operator_action_id_seq OWNED BY stg_tables.operator_action.id;


--
-- Name: operator_action id; Type: DEFAULT; Schema: stg_tables; Owner: postgres
--

ALTER TABLE ONLY stg_tables.operator_action ALTER COLUMN id SET DEFAULT nextval('stg_tables.operator_action_id_seq'::regclass);


--
-- Name: operator_action operator_action_pkey; Type: CONSTRAINT; Schema: stg_tables; Owner: postgres
--

ALTER TABLE ONLY stg_tables.operator_action
    ADD CONSTRAINT operator_action_pkey PRIMARY KEY (id);


--
-- Name: operator_action_profile_uuid_index; Type: INDEX; Schema: stg_tables; Owner: postgres
--

CREATE INDEX operator_action_profile_uuid_index ON stg_tables.operator_action USING btree (profile_uuid);


--
-- Name: TABLE operator_action; Type: ACL; Schema: stg_tables; Owner: postgres
--

GRANT ALL ON TABLE stg_tables.operator_action TO data_sync;
GRANT SELECT ON TABLE stg_tables.operator_action TO prod01_metabase;
GRANT SELECT ON TABLE stg_tables.operator_action TO ybashkatov;


--
-- PostgreSQL database dump complete
--

