--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: mt4_login1; Type: VIEW; Schema: public; Owner: data_sync
--

CREATE VIEW public.mt4_login1 AS
 WITH trade AS (
         SELECT trade_record.login,
            min(to_timestamp((trade_record.open_time)::double precision)) AS first_open_trade,
            min(to_timestamp((trade_record.close_time)::double precision)) AS first_close_trade,
            max(to_timestamp((trade_record.open_time)::double precision)) AS last_open_trade,
            max(to_timestamp((trade_record.close_time)::double precision)) AS last_close_trade,
            b.profit,
            b.balance
           FROM (ecosales_reporting.trade_record
             LEFT JOIN ( SELECT trade_record_1.login,
                    sum(
                        CASE
                            WHEN ((trade_record_1.cmd)::text <> 'OP_BALANCE'::text) THEN trade_record_1.profit
                            ELSE NULL::double precision
                        END) AS profit,
                    sum(
                        CASE
                            WHEN ((trade_record_1.cmd)::text = 'OP_BALANCE'::text) THEN trade_record_1.profit
                            ELSE NULL::double precision
                        END) AS balance
                   FROM ecosales_reporting.trade_record trade_record_1
                  WHERE (((trade_record_1.cmd)::text = ANY (ARRAY[('OP_BUY'::character varying)::text, ('OP_SELL'::character varying)::text, ('OP_BALANCE'::character varying)::text])) AND (trade_record_1.close_time >= trade_record_1.open_time))
                  GROUP BY trade_record_1.login) b ON ((trade_record.login = b.login)))
          WHERE ((trade_record.cmd)::text = ANY (ARRAY[('OP_BUY'::character varying)::text, ('OP_SELL'::character varying)::text]))
          GROUP BY trade_record.login, b.profit, b.balance
        )
 SELECT ta.profile_uuid AS profile_id,
    ta.login,
    ta.brand_id,
    trade.first_open_trade,
    trade.first_close_trade,
    trade.last_open_trade,
    trade.last_close_trade,
    trade.profit,
    trade.balance
   FROM (ecosales_reporting.trading_account ta
     LEFT JOIN trade ON ((ta.login = trade.login)))
  WHERE (ta.brand_id = '24newstrade'::text);


ALTER TABLE public.mt4_login1 OWNER TO data_sync;

--
-- Name: TABLE mt4_login1; Type: ACL; Schema: public; Owner: data_sync
--

GRANT SELECT ON TABLE public.mt4_login1 TO ybashkatov;


--
-- PostgreSQL database dump complete
--

