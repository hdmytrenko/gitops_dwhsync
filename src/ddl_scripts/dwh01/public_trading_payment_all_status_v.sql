--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: trading_payment_all_status_v; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.trading_payment_all_status_v AS
 SELECT (tp.prod_num)::text AS company,
    1 AS index,
    tp.id,
    bc.brand_country,
    tp.login,
    tp.profile_id,
    tp.payment_id,
    tp.currency,
    tp.external_reference,
    tp.deposit,
    tp.withdraw,
    tp.net_deposit,
    tp.transfer_in,
    tp.transfer_out,
    tp.credit_in,
    tp.credit_out,
    tp.deposit_usd,
    tp.withdraw_usd,
    tp.net_deposit_usd,
    tp.type,
    tp.rate,
    tp.status,
    ep.kyc_status,
    ep.acquisition_status,
    ep.registration_date AS creation_time_profile,
    tp.creation_time,
    (date_part('day'::text, tp.creation_time))::integer AS day_payment,
    (date_part('month'::text, tp.creation_time))::integer AS month_payment,
    (date_part('year'::text, tp.creation_time))::text AS year_payment,
    (date_part('week'::text, tp.creation_time))::integer AS week_payment,
    (date_part('isodow'::text, tp.creation_time))::integer AS day_of_week_creation_time,
    to_char(ep.registration_date, 'Day'::text) AS day_name_creation_time,
    (tp.country)::text AS country,
    upper((ep.language_code)::text) AS language,
    tp.brand_id,
    tp.payment_method,
    tp.group_payment_method,
    tp.is_published,
    tp.payment_transaction_id,
    tp.expiration_date,
    tp.version,
    ep.first_name AS profile_first_name,
    ep.last_name AS profile_last_name,
    ep.email,
    tp.client_ip,
    tp.created_by,
    tp.is_mobile,
    tp.user_agent,
    (ep.country)::text AS profile_country,
    tp.payment_aggregator,
    tp.agent_id,
    COALESCE(tp.name, 'Null'::text) AS agent_name,
    ep.sales_status,
    ep.sales_rep,
    COALESCE(sales.name, 'Null'::text) AS sales_name,
    (COALESCE(sales.user_type, 'Null'::character varying))::character varying(1020) AS sales_user_type,
    COALESCE(sales.agent_desk_name, 'Null'::text) AS sales_desk_name,
    COALESCE(sales.agent_language, 'Null'::text) AS sales_language,
    ep.retention_status,
    ep.retention_rep,
    COALESCE(reten.name, 'Null'::text) AS retention_name,
    (COALESCE(reten.user_type, 'Null'::character varying))::character varying(1020) AS reten_user_type,
    COALESCE(reten.agent_desk_name, 'Null'::text) AS reten_desk_name,
    COALESCE(reten.agent_language, 'Null'::text) AS reten_language,
    ep.affiliate_uuid,
    COALESCE(concat(aff.first_name, ' ', aff.last_name), 'Null'::text) AS affiliate_name,
    ep.affiliate_referral,
    ep.affiliate_source,
    c.name AS profile_country_name,
    (COALESCE(tp.user_type, 'Null'::character varying))::character varying(1020) AS user_type,
    COALESCE(tp.agent_desk_name, 'Null'::text) AS agent_desk_name,
    tp.agent_language,
    tp.ftd_time,
    tp.ftd_status,
    ((tp.ftd_time)::date - (ep.registration_date)::date) AS day_to_ftd,
    tp.ftd,
    tp.ftd_deposit,
    tp.ftd_deposit_usd,
    tp.noftd,
    tp.redeposit,
    tp.redeposit_usd,
    tp.last_deposit_time,
    ep.last_note,
    ep.last_note_date,
    tp.status_changed_at AS modification_time,
        CASE tp.type
            WHEN 'WITHDRAW'::text THEN tp.status_changed_at
            ELSE tp.creation_time
        END AS execution_time,
    tp.amount,
    tp.normalized_amount AS amount_usd,
    ep.questionnaire_status,
    ep.last_login,
    ep.last_trade_date,
    ep.deposit_count,
    ep.mt_balance,
    ep.profile_status,
    tp.prod_num,
    tp.decline_reason
   FROM ((((((public.view_payment tp
     LEFT JOIN frx_reporting.elastic_profile ep ON (((tp.profile_id)::text = (ep.player_uuid)::text)))
     LEFT JOIN stg_tables.country c ON (((c.alpha_2)::text = upper((ep.country)::text))))
     LEFT JOIN public.brand_country bc ON (((tp.brand_id)::text = (bc.brand_id)::text)))
     LEFT JOIN frx_reporting.affiliate aff ON ((((ep.affiliate_uuid)::text = (aff.uuid)::text) AND (aff.deleted = false))))
     LEFT JOIN public.agent_v sales ON ((((ep.sales_rep)::text = (sales.agent_user_hierarchy_uuid)::text) AND ((sales.user_type)::text = ANY (ARRAY[('SALES_AGENT'::character varying)::text, ('SALES_HOD'::character varying)::text, ('SALES_LEAD'::character varying)::text, ('SALES_MANAGER'::character varying)::text])))))
     LEFT JOIN public.agent_v reten ON ((((ep.retention_rep)::text = (reten.agent_user_hierarchy_uuid)::text) AND ((reten.user_type)::text = ANY (ARRAY[('RETENTION_AGENT'::character varying)::text, ('RETENTION_HOD'::character varying)::text, ('RETENTION_LEAD'::character varying)::text, ('RETENTION_MANAGER'::character varying)::text])))))
  WHERE (tp.deleted = false);


ALTER TABLE public.trading_payment_all_status_v OWNER TO postgres;

--
-- Name: TABLE trading_payment_all_status_v; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public.trading_payment_all_status_v TO prod01_metabase;
GRANT SELECT ON TABLE public.trading_payment_all_status_v TO ybashkatov;


--
-- PostgreSQL database dump complete
--

