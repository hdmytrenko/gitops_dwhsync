import collections
import sys
import uuid

from utils.common import create_config_dictionary, create_session_new, write_exception

from dwh_sync.dwh.mysql_entities import WorkLogs, Tickets, Users, TicketThread, Department, HelpTopic, SubTopic, \
    TicketPriority, SlaPlan, Teams, TeamsAgents, TicketAccessLog
from utils.common import get_sync_length, write_log, obtain_lock
from utils.constants import SynchronizationState, DbType, LOCK_TICKET_QUERY, EngineType
from utils.transformers import get_filter_or_backfill
from utils.writers import refresh_mat_views, write_rows, remove_prefetched_rows

config_path = "settings_mysql.ini"
dwh_session = create_session_new(create_config_dictionary(config_path,
                                                          DbType.DWH.value,
                                                          schema_translate_map=True,
                                                          engine_type=EngineType.postgres.value))
log_session = create_session_new(create_config_dictionary(config_path,
                                                          DbType.DWH.value,
                                                          engine_type=EngineType.postgres.value))
source_session = create_session_new(create_config_dictionary(config_path,
                                                             DbType.SOURCE.value,
                                                             engine_type=EngineType.mysql.value))

lock_session = create_session_new(create_config_dictionary(config_path,
                                                           DbType.DWH.value,
                                                           engine_type=EngineType.postgres.value))
log_state = SynchronizationState(log_session,
                                 uuid.uuid1(),
                                 source_session=source_session,
                                 dwh_session=dwh_session,
                                 lock_session=lock_session,
                                 source_engine_type=EngineType.mysql.value,
                                 sync_name=__file__)
tables = [
    TicketThread,
    Users,
    WorkLogs,
    Tickets,
    Department,
    HelpTopic,
    SubTopic,
    TicketPriority,
    SlaPlan,
    Teams,
    TeamsAgents,
    TicketAccessLog
]

views = ["ticket_system.ticket_answer_time"]


def main():
    try:
        for table in tables:
            log_state.table_name = table
            if not obtain_lock(log_state.lock_session, LOCK_TICKET_QUERY, log_state):
                continue
            f = get_filter_or_backfill(log_state)
            if isinstance(log_state.table_name.filter_column, collections.Callable):
                remove_prefetched_rows(log_state, table, table)
            log_state.message = f"size of sync is no more than " \
                                f"{len(get_sync_length(log_state, f)) * log_state.batch_size} rows for " \
                                f"{log_state.table_name.__tablename__}"
            write_log(log_state)
            write_rows(log_state, get_sync_length(log_state, f), f)
            log_state.message = f"table {log_state.table_name.__tablename__} synchronized"
            write_log(log_state)
            log_state.lock_session.rollback()
    except Exception as ex:
        log_state.lock_session.rollback()
        print(f'\n{"-" * 40}\nException: {str(ex)}\n{"-" * 40}\n')
        log_state.message = f"DWH TICKET_SYSTEM Exception: {str(ex)}"
        write_exception(log_state)
    refresh_mat_views(LOCK_TICKET_QUERY, views, log_state)


if __name__ == "__main__":
    sys.exit(main())
