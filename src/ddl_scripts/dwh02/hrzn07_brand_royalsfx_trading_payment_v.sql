--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: trading_payment_v; Type: VIEW; Schema: hrzn07_brand_royalsfx; Owner: postgres
--

CREATE VIEW hrzn07_brand_royalsfx.trading_payment_v AS
 SELECT f_trading_payment_v.login,
    f_trading_payment_v.profile_id,
    f_trading_payment_v.email,
    f_trading_payment_v.payment_id,
    f_trading_payment_v.currency,
    f_trading_payment_v.external_reference,
    f_trading_payment_v.type,
    f_trading_payment_v.status,
    f_trading_payment_v.deposit,
    f_trading_payment_v.withdraw,
    f_trading_payment_v.net_deposit,
    f_trading_payment_v.transfer_in,
    f_trading_payment_v.transfer_out,
    f_trading_payment_v.credit_in,
    f_trading_payment_v.credit_out,
    f_trading_payment_v.deposit_usd,
    f_trading_payment_v.withdraw_usd,
    f_trading_payment_v.net_deposit_usd,
    f_trading_payment_v.rate,
    f_trading_payment_v.kyc_status,
    f_trading_payment_v.acquisition_status,
    f_trading_payment_v.creation_time_profile,
    f_trading_payment_v.creation_time,
    f_trading_payment_v.day_payment,
    f_trading_payment_v.month_payment,
    f_trading_payment_v.year_payment,
    f_trading_payment_v.week_payment,
    f_trading_payment_v.day_of_week_creation_time,
    f_trading_payment_v.day_name_creation_time,
    f_trading_payment_v.country,
    f_trading_payment_v.language,
    f_trading_payment_v.brand_id,
    f_trading_payment_v.payment_method,
    f_trading_payment_v.group_payment_method,
    f_trading_payment_v.is_published,
    f_trading_payment_v.payment_transaction_id,
    f_trading_payment_v.expiration_date,
    f_trading_payment_v.version,
    f_trading_payment_v.profile_first_name,
    f_trading_payment_v.profile_last_name,
    f_trading_payment_v.client_ip,
    f_trading_payment_v.created_by,
    f_trading_payment_v.is_mobile,
    f_trading_payment_v.user_agent,
    f_trading_payment_v.profile_country,
    f_trading_payment_v.payment_aggregator,
    f_trading_payment_v.agent_id,
    f_trading_payment_v.agent_name,
    f_trading_payment_v.sales_status,
    f_trading_payment_v.sales_rep,
    f_trading_payment_v.sales_name,
    f_trading_payment_v.sales_user_type,
    f_trading_payment_v.sales_desk_name,
    f_trading_payment_v.sales_language,
    f_trading_payment_v.retention_status,
    f_trading_payment_v.retention_rep,
    f_trading_payment_v.retention_name,
    f_trading_payment_v.reten_user_type,
    f_trading_payment_v.reten_desk_name,
    f_trading_payment_v.reten_language,
    f_trading_payment_v.affiliate_uuid,
    f_trading_payment_v.affiliate_name,
    f_trading_payment_v.affiliate_referral,
    f_trading_payment_v.affiliate_source,
    f_trading_payment_v.profile_country_name,
    f_trading_payment_v.user_type,
    f_trading_payment_v.agent_desk_name,
    f_trading_payment_v.agent_language,
    f_trading_payment_v.ftd_status,
    f_trading_payment_v.ftd_time,
    f_trading_payment_v.day_to_ftd,
    f_trading_payment_v.ftd,
    f_trading_payment_v.ftd_deposit,
    f_trading_payment_v.ftd_deposit_usd,
    f_trading_payment_v.noftd,
    f_trading_payment_v.redeposit,
    f_trading_payment_v.redeposit_usd,
    f_trading_payment_v.last_deposit_time,
    f_trading_payment_v.modification_time,
    f_trading_payment_v.execution_time,
    f_trading_payment_v.amount,
    f_trading_payment_v.amount_usd
   FROM public.f_trading_payment_v('royalsfx'::text) f_trading_payment_v(login, profile_id, email, payment_id, currency, external_reference, type, status, deposit, withdraw, net_deposit, transfer_in, transfer_out, credit_in, credit_out, deposit_usd, withdraw_usd, net_deposit_usd, rate, kyc_status, acquisition_status, creation_time_profile, creation_time, day_payment, month_payment, year_payment, week_payment, day_of_week_creation_time, day_name_creation_time, country, language, brand_id, payment_method, group_payment_method, is_published, payment_transaction_id, expiration_date, version, profile_first_name, profile_last_name, client_ip, created_by, is_mobile, user_agent, profile_country, payment_aggregator, agent_id, agent_name, sales_status, sales_rep, sales_name, sales_user_type, sales_desk_name, sales_language, retention_status, retention_rep, retention_name, reten_user_type, reten_desk_name, reten_language, affiliate_uuid, affiliate_name, affiliate_referral, affiliate_source, profile_country_name, user_type, agent_desk_name, agent_language, ftd_status, ftd_time, day_to_ftd, ftd, ftd_deposit, ftd_deposit_usd, noftd, redeposit, redeposit_usd, last_deposit_time, modification_time, execution_time, amount, amount_usd);


ALTER TABLE hrzn07_brand_royalsfx.trading_payment_v OWNER TO postgres;

--
-- Name: TABLE trading_payment_v; Type: ACL; Schema: hrzn07_brand_royalsfx; Owner: postgres
--

GRANT SELECT ON TABLE hrzn07_brand_royalsfx.trading_payment_v TO ybashkatov;


--
-- PostgreSQL database dump complete
--

