--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: symbols; Type: VIEW; Schema: frx_reporting; Owner: postgres
--

CREATE VIEW frx_reporting.symbols AS
 SELECT symbols.server_id,
    symbols.swap_enable,
    symbols.manager_login,
    symbols.security_id,
    symbols.digits,
    symbols.trade,
    symbols.background_color,
    symbols.count,
    symbols.count_original,
    symbols.realtime,
    symbols.starting,
    symbols.profit_mode,
    symbols.filter,
    symbols.filter_counter,
    symbols.filter_smoothing,
    symbols.logging,
    symbols.spread,
    symbols.spread_balance,
    symbols.execution_mode,
    symbols.swap_type,
    symbols.swap_rollover_3_days,
    symbols.stops_level,
    symbols.gtc_pendings,
    symbols.margin_mode,
    symbols.long_only,
    symbols.instant_max_volume,
    symbols.freeze_level,
    symbols.margin_hedged_strong,
    symbols.value_date,
    symbols.quotes_delay,
    symbols.swap_open_price,
    symbols.swap_variation_margin,
    symbols.expiration_time,
    symbols.symbol,
    symbols.description,
    symbols.source,
    symbols.currency,
    symbols.margin_currency,
    symbols.filter_limit,
    symbols.swap_long,
    symbols.swap_short,
    symbols.contract_size,
    symbols.tick_value,
    symbols.tick_size,
    symbols.margin_initial,
    symbols.margin_maintenance,
    symbols.margin_hedged,
    symbols.margin_divider,
    symbols.point,
    symbols.multiply,
    symbols.bid_tick_value,
    symbols.ask_tick_value,
    symbols.prod_num
   FROM stg_tables.symbols;


ALTER TABLE frx_reporting.symbols OWNER TO postgres;

--
-- Name: TABLE symbols; Type: ACL; Schema: frx_reporting; Owner: postgres
--

GRANT SELECT ON TABLE frx_reporting.symbols TO prod01_metabase;
GRANT SELECT ON TABLE frx_reporting.symbols TO ybashkatov;


--
-- PostgreSQL database dump complete
--

