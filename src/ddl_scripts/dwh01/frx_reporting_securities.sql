--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: securities; Type: VIEW; Schema: frx_reporting; Owner: postgres
--

CREATE VIEW frx_reporting.securities AS
 SELECT securities.server_id,
    securities.id,
    securities.name,
    securities.description,
    securities.prod_num
   FROM stg_tables.securities;


ALTER TABLE frx_reporting.securities OWNER TO postgres;

--
-- Name: TABLE securities; Type: ACL; Schema: frx_reporting; Owner: postgres
--

GRANT SELECT ON TABLE frx_reporting.securities TO prod01_metabase;
GRANT SELECT ON TABLE frx_reporting.securities TO ybashkatov;


--
-- PostgreSQL database dump complete
--

