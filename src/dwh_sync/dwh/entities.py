from dataclasses import dataclass
from datetime import datetime
from multiprocessing import Process
from queue import Queue

from sqlalchemy import and_
from sqlalchemy.orm import Session

from dwh_sync.dwh.mapping_writer_classes import *
from dwh_sync.dwh.mapping_writer_hierarchy import *
from utils.common import create_session_autocommit, create_session_new, write_log, write_exception, commit_session, \
    run_function
from utils.constants import SynchronizationState, IndexManipulationFunc

entities_for_matviews_creation = {'profile': ProfileDWH,
                                  'profile_ext': ProfileExtDWH,
                                  'payment': PaymentDWH,
                                  'operator': OperatorDWH,
                                  'hierarchy_customer': HierarchyCustomerDWH,
                                  'affiliate': AffiliateDWH
                                  }


class MatviewRefreshingProcess(Process):
    def __init__(self, return_result: Queue, view_name: str, max_id: int, connection_config: dict,
                 log_state: SynchronizationState):
        super(MatviewRefreshingProcess, self).__init__()
        self.return_result = return_result
        self.view_name = view_name
        self.max_id = max_id
        self.connection_config = connection_config
        self.log_state = log_state

    def run(self):
        refresh_mat_views_session = create_session_autocommit(
            self.connection_config,
            statement_timeout=self.log_state.postgres_statement_timeout
        )
        self.log_state.log_session = create_session_new(self.connection_config)
        try:
            with IndexManager(refresh_mat_views_session, self.log_state, self.view_name):
                self.log_state.message = f"DANGER! start vacuum {self.view_name}"
                write_log(self.log_state)
                refresh_mat_views_session.execute(f'VACUUM FULL {self.view_name}')
                self.log_state.message = f"End vacuum: {self.view_name} unlocked"
                write_log(self.log_state)
                self.log_state.message = f"start refreshing {self.view_name}"
                write_log(self.log_state)
                refresh_mat_views_session.execute(f'REFRESH MATERIALIZED VIEW CONCURRENTLY {self.view_name}')
                self.log_state.message = f"mat view {self.view_name} refreshed"
                write_log(self.log_state)
            # refresh_mat_views_session.query(RefreshingPlan) \
            #     .filter(and_(RefreshingPlan.id <= self.max_id,
            #                  RefreshingPlan.matview_name == self.view_name.split('.')[1],
            #                  RefreshingPlan.refresh_time.is_(None))) \
            #     .update({'refresh_time': datetime.now()})
            commit_session(refresh_mat_views_session, self.log_state)
            self.return_result.put(self.view_name)
        except Exception as ex:
            if ex.__dict__.get('code') == 'f405':
                self.log_state.message = f"Exception while refreshing: {str(ex)} - can't find view"
                write_log(self.log_state)
            else:
                self.log_state.message = f"Exception while refreshing: {str(ex)}"
                write_exception(self.log_state)
            refresh_mat_views_session.close()
            self.return_result.put(self.view_name)


@dataclass
class IndexManager:
    session: Session
    log_state: SynchronizationState
    view_name: str

    def __enter__(self):
        self.log_state.message = run_function(
            self.session,
            self.log_state,
            IndexManipulationFunc.drop_indexes.value,
            self.view_name
        )
        write_log(self.log_state)

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.log_state.message = run_function(
            self.session,
            self.log_state,
            IndexManipulationFunc.create_indexes.value,
            self.view_name
        )
        self.session.execute(f'ANALYZE {self.view_name}')
        write_log(self.log_state)
