--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: user_hierarchy_v; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.user_hierarchy_v AS
 SELECT (uh.prod_num)::text AS company,
    uh.id AS user_hierarchy_id,
    uh.uuid AS agent_user_hierarchy_uuid,
    hb.branch_id,
    uh.user_type AS agent_type,
    hb.dtype AS agent_dtype,
    hb.name AS agent_desk_name,
    hb.uuid AS agent_id_branch_hierarchy_uuid,
    hb.desk_type AS agent_desk_type,
    hb.language AS agent_language
   FROM (frx_reporting.user_hierarchy uh
     LEFT JOIN ( SELECT uhb.branch_id,
            uhb.user_hierarchy_id,
            bh.dtype,
            bh.name,
            bh.language,
            bh.uuid,
            bh.desk_type
           FROM (frx_reporting.user_hierarchy_branch uhb
             LEFT JOIN frx_reporting.branch_hierarchy bh ON (((uhb.branch_id = bh.id) AND (bh.deleted_at IS NULL))))
          WHERE (uhb.branch_id IS NOT NULL)) hb ON ((uh.id = hb.user_hierarchy_id)))
  WHERE ((uh.deleted_at IS NULL) AND ((uh.user_type)::text = ANY (ARRAY['AFFILIATE_MANAGER'::text, 'AFFILIATE_PARTNER'::text, 'BRAND_ADMIN'::text, 'RETENTION_AGENT'::text, 'RETENTION_HOD'::text, 'RETENTION_LEAD'::text, 'RETENTION_MANAGER'::text, 'SALES_AGENT'::text, 'SALES_HOD'::text, 'SALES_LEAD'::text, 'SALES_MANAGER'::text])));


ALTER TABLE public.user_hierarchy_v OWNER TO postgres;

--
-- Name: TABLE user_hierarchy_v; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public.user_hierarchy_v TO prod01_metabase;
GRANT SELECT ON TABLE public.user_hierarchy_v TO ybashkatov;


--
-- PostgreSQL database dump complete
--

