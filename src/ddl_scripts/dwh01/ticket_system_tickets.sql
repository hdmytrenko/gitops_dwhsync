--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

--
-- Name: tickets; Type: TABLE; Schema: ticket_system; Owner: data_sync
--

CREATE TABLE ticket_system.tickets (
    id integer,
    ticket_number text,
    user_id integer,
    dept_id integer,
    team_id integer,
    priority_id integer,
    sla integer,
    help_topic_id integer,
    status integer,
    rating integer,
    ratingreply integer,
    flags integer,
    ip_address integer,
    assigned_to integer,
    lock_by integer,
    lock_at timestamp without time zone,
    source integer,
    isoverdue integer,
    reopened integer,
    isanswered integer,
    html integer,
    is_deleted integer,
    closed integer,
    is_transferred integer,
    transferred_at timestamp without time zone,
    reopened_at timestamp without time zone,
    duedate timestamp without time zone,
    closed_at timestamp without time zone,
    last_message_at timestamp without time zone,
    last_response_at timestamp without time zone,
    approval integer,
    follow_up integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    is_internal integer,
    title text,
    poster text,
    last_thread_id text,
    last_thread_user_id text,
    parent_id integer,
    last_thread_replier_id text,
    pending_reason text,
    pending_reason_comment text,
    body bytea,
    brand_name text,
    sub_topic_id integer,
    threads_count integer,
    css text
);


ALTER TABLE ticket_system.tickets OWNER TO data_sync;

--
-- PostgreSQL database dump complete
--

