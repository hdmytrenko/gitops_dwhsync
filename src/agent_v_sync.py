import uuid
from dataclasses import dataclass

from pyspark.sql import Window
from pyspark.sql.functions import *
from sqlalchemy import Table
from sqlalchemy.orm import close_all_sessions

from sync_entities.spark_sync_entities import HierarchyBranchSync, \
    HierarchyOperatorSync, HierarchyOperatorBranchSync, OperatorSync
from utils.common import create_config_dictionary, create_session_new, obtain_lock, write_log, run_function, \
    write_exception
from utils.constants import DbType, EngineType, SynchronizationState, LOCK_QUERY
from utils.writers import save_dataframe

if __name__ == '__main__':
    config_path = "settings_agent_v_sync.ini"
    app_name = 'agent_v_sync'
    config_dict_source = create_config_dictionary(config_path,
                                                  DbType.SOURCE.value,
                                                  engine_type=EngineType.postgres.value)
    config_dict_dwh = create_config_dictionary(config_path,
                                               DbType.DWH.value,
                                               engine_type=EngineType.postgres.value)

    log_session = create_session_new(config_dict_dwh)
    lock_session = create_session_new(config_dict_dwh)
    dwh_session = create_session_new(create_config_dictionary(config_path,
                                                              DbType.DWH.value,
                                                              schema_translate_map=True,
                                                              engine_type=EngineType.postgres.value))
    log_state = SynchronizationState(
        log_session,
        uuid.uuid1(),
        source_engine_type=EngineType.postgres.value,
        prod_number=config_dict_dwh['prod_num'],
        sync_name=__file__
    )


class HDeskVSync:
    def __init__(self, app_name, log_state, config_dict):
        self.hierarchy_branch_df = HierarchyBranchSync(app_name, log_state, config_dict)

    def filter_df(self):
        hierarchy_branch_df = self.hierarchy_branch_df.filter_df()
        return hierarchy_branch_df.select(
            col('hierarchy_branch_id').alias('desk_id'),
            upper(col('hierarchy_branch_language')).alias('desk_language')
        ).filter(col('hierarchy_branch_type') == 'DESK')


class HTeamVSync:
    def __init__(self, app_name, log_state, config_dict):
        self.hierarchy_branch_df = HierarchyBranchSync(app_name, log_state, config_dict)
        self.h_desc_v_df = HDeskVSync(app_name, log_state, config_dict)

    def filter_df(self):
        hierarchy_branch_df = self.hierarchy_branch_df.filter_df()
        h_desc_v_df = self.h_desc_v_df.filter_df()
        return hierarchy_branch_df.filter(
            (col('hierarchy_branch_type') == 'TEAM') &
            col('hierarchy_branch_parent_id').isNotNull()
        ).alias('df_1').join(
            h_desc_v_df.alias('df_2'),
            hierarchy_branch_df["hierarchy_branch_parent_id"] == h_desc_v_df["desk_id"],
            'left'
        ).select("df_1.*", "df_2.*").select(
            col('hierarchy_branch_parent_id').alias('desk_id'),
            col('hierarchy_branch_id').alias('team_id'),
            col('hierarchy_branch_type').alias('team_dtype'),
            col('hierarchy_branch_name').alias('team_name'),
            col('desk_language'),
            col('hierarchy_branch_assign_index').alias('assign_agents')
        )


@dataclass
class HUserVSync:
    def __init__(self, app_name, log_state, config_dict):
        self.hierarchy_operator_df = HierarchyOperatorSync(app_name, log_state, config_dict)
        self.hierarchy_operator_branch_df = HierarchyOperatorBranchSync(app_name, log_state, config_dict)
        self.h_team_v_df = HTeamVSync(app_name, log_state, config_dict)

    def join_df(self) -> DataFrame:
        hierarchy_operator_df = self.hierarchy_operator_df.filter_df()
        hierarchy_operator_branch_df = self.hierarchy_operator_branch_df.filter_df()
        return hierarchy_operator_df.alias('df_1').join(
            hierarchy_operator_branch_df.alias('df_2'),
            hierarchy_operator_df["hierarchy_operator_id"]
            == hierarchy_operator_branch_df['hierarchy_operator_branch_operator_id'],
            'left'
        ).select("df_1.*", "df_2.*")

    def filter_df(self):
        join_df = self.join_df()
        h_team_v_df = self.h_team_v_df.filter_df()
        return join_df.alias("df_1").join(
            h_team_v_df.alias("df_2"),
            join_df["hierarchy_operator_branch_branch_id"] == h_team_v_df["team_id"],
            'left'
        ).select("df_1.*", "df_2.*").filter(
            col('hierarchy_operator_branch_branch_id').isNotNull()
        ).select(
            col('hierarchy_operator_branch_branch_id').alias('parent_id'),
            col('hierarchy_operator_id').alias('user_id'),
            col('hierarchy_operator_type').alias('user_type'),
            col('hierarchy_operator_uuid').alias('user_uuid'),
            col('hierarchy_operator_brand_ids').alias('user_brand_id'),
            col('desk_language'),
            col('hierarchy_operator_prod_num'),
            col('hierarchy_operator_uuid_compound').alias('user_uuid_compound')
        )


class UserHierarchyVSync:
    def __init__(self, app_name, log_state, config_dict):
        self.hierarchy_operator_df = HierarchyOperatorSync(app_name, log_state, config_dict)
        self.hierarchy_operator_branch_df = HierarchyOperatorBranchSync(app_name, log_state, config_dict)
        self.hierarchy_branch_df = HierarchyBranchSync(app_name, log_state, config_dict)

    def join_df(self):
        hierarchy_operator_branch_df = self.hierarchy_operator_branch_df.filter_df()
        hierarchy_branch_df = self.hierarchy_branch_df.filter_df()
        return hierarchy_operator_branch_df.alias('df_1').join(
            hierarchy_branch_df.alias('df_2'),
            hierarchy_operator_branch_df['hierarchy_operator_branch_branch_id']
            == hierarchy_branch_df['hierarchy_branch_id'],
            'left'
        ).select('df_1.*', 'df_2.*').filter(col('hierarchy_operator_branch_operator_id').isNotNull()) \
            .select(
            col('hierarchy_operator_branch_branch_id'),
            col('hierarchy_operator_branch_operator_id'),
            col('hierarchy_branch_type'),
            col('hierarchy_branch_name'),
            col('hierarchy_branch_language'),
            col('hierarchy_branch_uuid'),
            col('hierarchy_branch_desk_type'),
            col('hierarchy_branch_prod_num'),
            col('hierarchy_branch_uuid_compound')
        )

    def filter_df(self):
        join_df = self.join_df()
        hierarchy_operator_df = self.hierarchy_operator_df.filter_df()
        return hierarchy_operator_df.alias('df_1') \
            .join(join_df.alias('df_2'), join_df['hierarchy_operator_branch_operator_id'] ==
                  hierarchy_operator_df['hierarchy_operator_id'], 'left') \
            .select(col('hierarchy_operator_id').alias('user_hierarchy_id'),
                    col('hierarchy_operator_uuid').alias('agent_user_hierarchy_uuid'),
                    col('hierarchy_operator_branch_branch_id').alias('branch_id'),
                    col('hierarchy_operator_type').alias('agent_type'),
                    col('hierarchy_branch_type').alias('agent_dtype'),
                    col('hierarchy_branch_name').alias('agent_desk_name'),
                    col('hierarchy_branch_uuid').alias('agent_id_branch_hierarchy_uuid'),
                    col('hierarchy_branch_desk_type').alias('agent_desk_type'),
                    col('hierarchy_branch_language').alias('agent_language'),
                    col('hierarchy_operator_prod_num').alias('prod_num'),
                    col('hierarchy_branch_uuid_compound').alias('agent_id_branch_hierarchy_uuid_compound'),
                    col('hierarchy_operator_uuid_compound').alias('agent_user_hierarchy_uuid_compound'),
                    )


class AgentVSync:
    def __init__(self, app_name, log_state, config_dict, date_filter=None):
        self.app_name = app_name
        self.log_state = log_state
        self.config_dict = config_dict
        self.date_filter = date_filter
        self.operator_df = OperatorSync(app_name, log_state, config_dict, date_filter=self.date_filter)
        self.h_user_v_df = HUserVSync(app_name, log_state, config_dict)
        self.user_hierarchy_v_df = UserHierarchyVSync(app_name, log_state, config_dict)

    def op_df(self):
        operator_df = self.operator_df.filter_df()
        return operator_df.select(
            col('operator_uuid'),
            concat(trim(col('operator_first_name')), lit(' '), trim(col('operator_last_name'))).alias('name'),
            col('operator_email'),
            col('operator_registration_date'),
            col('operator_status'))

    @staticmethod
    def lan_df(h_user_v_df):
        pre_lan_df = (h_user_v_df.filter(col('desk_language').isNotNull())
                      .select(col('user_uuid'), col('desk_language')).distinct())
        w = Window.partitionBy('user_uuid').orderBy('desk_language')
        lan_df = pre_lan_df.withColumn(
            'lan', collect_list(col('desk_language')).over(w)
        ).drop('desk_language').groupBy('user_uuid') \
            .agg(max('lan').alias('lan_pre')).withColumn('lan', concat_ws(", ", "lan_pre"))
        return lan_df

    def desk_df(self):
        user_hierarchy_v_df = self.user_hierarchy_v_df.filter_df()
        return user_hierarchy_v_df.filter(col('agent_desk_name').isNotNull()
                                          & col('agent_dtype').isin(['DESK', 'TEAM'])).select(
            col('agent_user_hierarchy_uuid'), upper(col('agent_desk_name')).alias('agent_desk_name')).distinct() \
            .groupby(col('agent_user_hierarchy_uuid')).agg(
            concat_ws(", ", sort_array(collect_list(col('agent_desk_name'))))
                .alias('desk'))

    def pre_join_df(self, h_user_v_df):
        op_df = self.op_df()
        return h_user_v_df.alias('df_1').join(
            op_df.alias('df_2'), h_user_v_df['user_uuid'] == op_df['operator_uuid'], 'left') \
            .select('df_1.*', 'df_2.*')

    def join_df(self, h_user_v_df):
        lan_df = self.lan_df(h_user_v_df)
        return self.pre_join_df(h_user_v_df).alias('df_1').join(
            lan_df.alias('df_2'), h_user_v_df['user_uuid'] == lan_df['user_uuid'], 'left') \
            .select('df_1.*', 'df_2.lan')

    def filter_df(self):
        h_user_v_df = self.h_user_v_df.filter_df()
        desk_df = self.desk_df()
        join_df = self.join_df(h_user_v_df)
        return join_df.alias('df_1').join(
            desk_df.alias('df_2'),
            join_df['user_uuid'] == desk_df['agent_user_hierarchy_uuid'], 'left') \
            .select('df_1.*', 'df_2.*').select(
            col('user_uuid').alias('agent_user_hierarchy_uuid'),
            col('name'),
            col('user_type'),
            coalesce(col('desk'), lit("Null")).alias('agent_desk_name'),
            col('lan').alias('agent_language'),
            col('operator_email').alias('email'),
            col('operator_registration_date').alias('registration_date'),
            col('operator_status').alias('status'),
            concat(lit('{'), concat_ws(',', col('user_brand_id')), lit('}')).alias('user_brand_id'),
            col('hierarchy_operator_prod_num').cast('int').alias('prod_num'),
            col('user_uuid_compound').alias('agent_user_hierarchy_uuid_compound')).distinct()

    def get_incomplete_df(self):
        operator_df = self.operator_df.get_incomplete_df()
        branch_df = HierarchyBranchSync(self.app_name, self.log_state, self.config_dict,
                                        self.date_filter).get_incomplete_df()
        operator_h_df = HierarchyOperatorSync(self.app_name, self.log_state, self.config_dict,
                                              self.date_filter).get_incomplete_df()
        operator_h_complete_df = HierarchyOperatorSync(self.app_name, self.log_state, self.config_dict).filter_df()
        operator_branch_df = HierarchyOperatorBranchSync(self.app_name, self.log_state, self.config_dict).filter_df()
        pre_joined_branch = branch_df.join(
            operator_branch_df,
            operator_branch_df['hierarchy_operator_branch_branch_id'] == branch_df['branchID_branch'],
            'left'
        ).withColumnRenamed("hierarchy_operator_branch_operator_id", "operatorID_branch").drop(
            'hierarchy_operator_branch_branch_id')

        operator_h_branch = pre_joined_branch.join(
            operator_h_complete_df,
            operator_h_complete_df['hierarchy_operator_id'] == pre_joined_branch['operatorID_branch'],
            'left'
        ).select('hierarchy_operator_uuid').withColumnRenamed('hierarchy_operator_uuid', 'operatorUUID_branch')

        df = operator_h_df.join(
            operator_h_branch,
            operator_h_branch['operatorUUID_branch'] == operator_h_df['operatorUUID_operatorH'],
            'full_outer'
        ).join(
            operator_df,
            operator_df['operatorUUID_operator'] == operator_h_df['operatorUUID_operatorH'],
            'full_outer'
        ).select(coalesce(col('operatorUUID_operatorH'), col('operatorUUID_branch'),
                          col('operatorUUID_operator'))).withColumnRenamed(
            'coalesce(operatorUUID_operatorH, operatorUUID_branch, operatorUUID_operator)',
            'operatorUUID_agentV').na.drop(
            'all')
        return df


def main():
    log_state.table_name = Table()
    log_state.table_name.__tablename__ = app_name + '_prod0' + config_dict_dwh['prod_num']
    if not obtain_lock(lock_session, LOCK_QUERY, log_state):
        close_all_sessions()
        exit(1)
    write_log(log_state, 'Start assembling dataframe')
    agent_v_df = AgentVSync(app_name, log_state, config_dict_source).filter_df()
    save_dataframe(config_dict_dwh, log_state, agent_v_df, 'agent_v_sync_prod0' + log_state.prod_number)
    response = run_function(dwh_session, log_state, 'upsert_agent_v', log_state.prod_number)
    write_log(log_state, response)
    lock_session.rollback()
    write_log(log_state, 'Dataframe assembling complete')


if __name__ == '__main__':
    try:
        main()
    except Exception as ex:
        print(f'\n{"-" * 40}\nException: {str(ex)}\n{"-" * 40}\n')
        log_state.message = f"DWH PROD0{log_state.prod_number} Exception: {str(ex)}"
        write_exception(log_state)