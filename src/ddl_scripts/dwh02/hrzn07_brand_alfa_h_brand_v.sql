--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: h_brand_v; Type: VIEW; Schema: hrzn07_brand_alfa; Owner: ybashkatov
--

CREATE VIEW hrzn07_brand_alfa.h_brand_v AS
 SELECT f_brand_v.company_id,
    f_brand_v.id,
    f_brand_v.brand_dtype,
    f_brand_v.brand_name,
    f_brand_v.brand_id
   FROM public.f_brand_v('consulting-alfa'::text) f_brand_v(company_id, id, brand_dtype, brand_name, brand_id);


ALTER TABLE hrzn07_brand_alfa.h_brand_v OWNER TO ybashkatov;

--
-- PostgreSQL database dump complete
--

