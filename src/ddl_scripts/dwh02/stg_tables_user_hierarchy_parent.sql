--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

--
-- Name: user_hierarchy_parent; Type: TABLE; Schema: stg_tables; Owner: postgres
--

CREATE TABLE stg_tables.user_hierarchy_parent (
    user_hierarchy_id bigint NOT NULL,
    parent_id bigint NOT NULL
);


ALTER TABLE stg_tables.user_hierarchy_parent OWNER TO postgres;

--
-- Name: user_hierarchy_parent stg_user_hierarchy_parent_pkey; Type: CONSTRAINT; Schema: stg_tables; Owner: postgres
--

ALTER TABLE ONLY stg_tables.user_hierarchy_parent
    ADD CONSTRAINT stg_user_hierarchy_parent_pkey PRIMARY KEY (user_hierarchy_id, parent_id);


--
-- Name: idx_stg_user_hierarchy_parent_parent_id; Type: INDEX; Schema: stg_tables; Owner: postgres
--

CREATE INDEX idx_stg_user_hierarchy_parent_parent_id ON stg_tables.user_hierarchy_parent USING btree (parent_id);


--
-- Name: idx_stg_user_hierarchy_parent_user_hierarchy_id; Type: INDEX; Schema: stg_tables; Owner: postgres
--

CREATE INDEX idx_stg_user_hierarchy_parent_user_hierarchy_id ON stg_tables.user_hierarchy_parent USING btree (user_hierarchy_id);


--
-- Name: TABLE user_hierarchy_parent; Type: ACL; Schema: stg_tables; Owner: postgres
--

GRANT ALL ON TABLE stg_tables.user_hierarchy_parent TO data_sync;
GRANT SELECT ON TABLE stg_tables.user_hierarchy_parent TO ybashkatov;


--
-- PostgreSQL database dump complete
--

