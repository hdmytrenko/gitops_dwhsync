--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

--
-- Name: branch_hierarchy; Type: TABLE; Schema: stg_tables; Owner: postgres
--

CREATE TABLE stg_tables.branch_hierarchy (
    dtype character varying(31) NOT NULL,
    id bigint NOT NULL,
    created_at timestamp without time zone,
    deleted_at timestamp without time zone,
    name character varying(255),
    uuid character varying(60) NOT NULL,
    desk_type character varying(255),
    language character varying(255),
    country character varying(255),
    default_branch_id bigint,
    default_branch_user_id bigint,
    parent_id bigint,
    brand_id character varying(32) DEFAULT 'nasfx'::character varying NOT NULL,
    assign_index integer DEFAULT 0 NOT NULL,
    version integer DEFAULT 0 NOT NULL,
    updated_at timestamp without time zone,
    manager_id bigint,
    prod_num integer
);


ALTER TABLE stg_tables.branch_hierarchy OWNER TO postgres;

--
-- Name: branch_hierarchy_id_seq; Type: SEQUENCE; Schema: stg_tables; Owner: postgres
--

CREATE SEQUENCE stg_tables.branch_hierarchy_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE stg_tables.branch_hierarchy_id_seq OWNER TO postgres;

--
-- Name: branch_hierarchy_id_seq; Type: SEQUENCE OWNED BY; Schema: stg_tables; Owner: postgres
--

ALTER SEQUENCE stg_tables.branch_hierarchy_id_seq OWNED BY stg_tables.branch_hierarchy.id;


--
-- Name: branch_hierarchy id; Type: DEFAULT; Schema: stg_tables; Owner: postgres
--

ALTER TABLE ONLY stg_tables.branch_hierarchy ALTER COLUMN id SET DEFAULT nextval('stg_tables.branch_hierarchy_id_seq'::regclass);


--
-- Name: branch_hierarchy stg_branch_hierarchy_pk; Type: CONSTRAINT; Schema: stg_tables; Owner: postgres
--

ALTER TABLE ONLY stg_tables.branch_hierarchy
    ADD CONSTRAINT stg_branch_hierarchy_pk PRIMARY KEY (id);


--
-- Name: branch_hierarchy uk_stg_branch_hierarchy_uuid; Type: CONSTRAINT; Schema: stg_tables; Owner: postgres
--

ALTER TABLE ONLY stg_tables.branch_hierarchy
    ADD CONSTRAINT uk_stg_branch_hierarchy_uuid UNIQUE (uuid);


--
-- Name: idx_stg_branch_hierarchy_created_at; Type: INDEX; Schema: stg_tables; Owner: postgres
--

CREATE INDEX idx_stg_branch_hierarchy_created_at ON stg_tables.branch_hierarchy USING btree (created_at);


--
-- Name: idx_stg_branch_hierarchy_default_branch_id; Type: INDEX; Schema: stg_tables; Owner: postgres
--

CREATE INDEX idx_stg_branch_hierarchy_default_branch_id ON stg_tables.branch_hierarchy USING btree (default_branch_id, deleted_at);


--
-- Name: idx_stg_branch_hierarchy_default_branch_user_id; Type: INDEX; Schema: stg_tables; Owner: postgres
--

CREATE INDEX idx_stg_branch_hierarchy_default_branch_user_id ON stg_tables.branch_hierarchy USING btree (default_branch_user_id, deleted_at);


--
-- Name: idx_stg_branch_hierarchy_deleted_at; Type: INDEX; Schema: stg_tables; Owner: postgres
--

CREATE INDEX idx_stg_branch_hierarchy_deleted_at ON stg_tables.branch_hierarchy USING btree (deleted_at);


--
-- Name: idx_stg_branch_hierarchy_parent_id; Type: INDEX; Schema: stg_tables; Owner: postgres
--

CREATE INDEX idx_stg_branch_hierarchy_parent_id ON stg_tables.branch_hierarchy USING btree (parent_id, deleted_at);


--
-- Name: idx_stg_branch_hierarchy_updated_at; Type: INDEX; Schema: stg_tables; Owner: postgres
--

CREATE INDEX idx_stg_branch_hierarchy_updated_at ON stg_tables.branch_hierarchy USING btree (updated_at);


--
-- Name: TABLE branch_hierarchy; Type: ACL; Schema: stg_tables; Owner: postgres
--

GRANT ALL ON TABLE stg_tables.branch_hierarchy TO data_sync;
GRANT SELECT ON TABLE stg_tables.branch_hierarchy TO ybashkatov;


--
-- PostgreSQL database dump complete
--

