--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: lead; Type: VIEW; Schema: ecosales_reporting; Owner: postgres
--

CREATE VIEW ecosales_reporting.lead AS
 SELECT lead.id,
    lead.status,
    lead.name,
    lead.surname,
    lead.email,
    lead.phone,
    lead.mobile,
    lead.country,
    lead.source,
    lead.sales_status,
    lead.sales_agent,
    lead.birth_date,
    lead.affiliate,
    lead.gender,
    lead.city,
    lead.language,
    lead.brand_id,
    lead.status_changed_date,
    lead.converted_to_client_uuid,
    lead.converted_by_operator_uuid,
    lead.version,
    lead.migration_id,
    lead.updated_at,
    lead.deleted,
    lead.created_at,
    lead.prod_num
   FROM stg_tables.lead
  WHERE ((NOT lead.deleted) AND ((lead.email IS NULL) OR (upper(lead.email) !~~ '%NEWAGESOL.COM'::text)) AND ((lead.name IS NULL) OR (upper((lead.name)::text) !~ similar_escape('%(TEST|TRASH)%'::text, NULL::text)) OR (lead.surname IS NULL) OR ((lead.surname)::text !~ similar_escape('%(TEST|TRASH)%'::text, NULL::text))));


ALTER TABLE ecosales_reporting.lead OWNER TO postgres;

--
-- Name: TABLE lead; Type: ACL; Schema: ecosales_reporting; Owner: postgres
--

GRANT ALL ON TABLE ecosales_reporting.lead TO data_sync;
GRANT SELECT ON TABLE ecosales_reporting.lead TO ybashkatov;


--
-- PostgreSQL database dump complete
--

