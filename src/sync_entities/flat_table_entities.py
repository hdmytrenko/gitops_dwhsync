from dataclasses import dataclass

from pyspark.sql.functions import *

from sync_entities.spark_sync_entities import HierarchyBranchSync, \
    HierarchyOperatorSync, HierarchyOperatorBranchSync, OperatorSync


class HDeskVSync:
    def __init__(self, app_name, log_state, config_dict):
        self.hierarchy_branch_df = HierarchyBranchSync(app_name, log_state, config_dict).filter_df()

    def filter_df(self):
        return self.hierarchy_branch_df.select(
            col('hierarchy_branch_id').alias('desk_id'),
            upper(col('hierarchy_branch_language')).alias('desk_language')
        ).filter(col('hierarchy_branch_type') == 'DESK')


class HTeamVSync:
    def __init__(self, app_name, log_state, config_dict):
        self.hierarchy_branch_df = HierarchyBranchSync(app_name, log_state, config_dict).filter_df()
        self.h_desc_v_df = HDeskVSync(app_name, log_state, config_dict).filter_df()

    def filter_df(self):
        return self.hierarchy_branch_df.filter(
            (col('hierarchy_branch_type') == 'TEAM') &
            col('hierarchy_branch_parent_id').isNotNull()
        ).alias('df_1').join(
            self.h_desc_v_df.alias('df_2'),
            self.hierarchy_branch_df["hierarchy_branch_parent_id"] == self.h_desc_v_df["desk_id"],
            'left'
        ).select("df_1.*", "df_2.*").select(
            col('hierarchy_branch_parent_id').alias('desk_id'),
            col('hierarchy_branch_id').alias('team_id'),
            col('hierarchy_branch_type').alias('team_dtype'),
            col('hierarchy_branch_name').alias('team_name'),
            col('desk_language'),
            col('hierarchy_branch_assign_index').alias('assign_agents')
        )


@dataclass
class HUserVSync:
    def __init__(self, app_name, log_state, config_dict):
        self.hierarchy_operator_df = HierarchyOperatorSync(app_name, log_state, config_dict).filter_df()
        self.hierarchy_operator_branch_df = HierarchyOperatorBranchSync(app_name, log_state, config_dict).filter_df()
        self.h_team_v_df = HTeamVSync(app_name, log_state, config_dict).filter_df()

    def join_df(self) -> DataFrame:
        return self.hierarchy_operator_df.alias('df_1').join(
            self.hierarchy_operator_branch_df.alias('df_2'),
            self.hierarchy_operator_df["hierarchy_operator_id"]
            == self.hierarchy_operator_branch_df['hierarchy_operator_branch_operator_id'],
            'left'
        ).select("df_1.*", "df_2.*")

    def filter_df(self):
        return self.join_df().alias("df_1").join(
            self.h_team_v_df.alias("df_2"),
            self.join_df()["hierarchy_operator_branch_branch_id"] == self.h_team_v_df["team_id"],
            'left'
        ).select("df_1.*", "df_2.*").filter(
            col('hierarchy_operator_branch_branch_id').isNotNull()
        ).select(
            col('hierarchy_operator_branch_branch_id').alias('parent_id'),
            col('hierarchy_operator_id').alias('user_id'),
            col('hierarchy_operator_type').alias('user_type'),
            col('hierarchy_operator_uuid').alias('user_uuid'),
            col('hierarchy_operator_brand_ids').alias('user_brand_id'),
            col('desk_language'),
            col('hierarchy_operator_prod_num'),
            col('hierarchy_operator_uuid_compound').alias('user_uuid_compound')
        )


class UserHierarchyVSync:
    def __init__(self, app_name, log_state, config_dict):
        self.hierarchy_operator_df = HierarchyOperatorSync(app_name, log_state, config_dict).filter_df()
        self.hierarchy_operator_branch_df = HierarchyOperatorBranchSync(app_name, log_state, config_dict).filter_df()
        self.hierarchy_branch_df = HierarchyBranchSync(app_name, log_state, config_dict).filter_df()

    def join_df(self):
        return self.hierarchy_operator_branch_df.alias('df_1').join(
            self.hierarchy_branch_df.alias('df_2'),
            self.hierarchy_operator_branch_df['hierarchy_operator_branch_operator_id']
            == self.hierarchy_branch_df['hierarchy_branch_id'],
            'left'
        ).select('df_1.*', 'df_2.*').filter(col('hierarchy_operator_branch_operator_id').isNotNull()) \
            .select(
            col('hierarchy_operator_branch_branch_id'),
            col('hierarchy_operator_branch_operator_id'),
            col('hierarchy_branch_type'),
            col('hierarchy_branch_name'),
            col('hierarchy_branch_language'),
            col('hierarchy_branch_uuid'),
            col('hierarchy_branch_desk_type'),
            col('hierarchy_branch_prod_num'),
            col('hierarchy_branch_uuid_compound')
        )

    def filter_df(self):
        return self.hierarchy_operator_df.alias('df_1') \
            .join(self.join_df().alias('df_2'), self.join_df()['hierarchy_operator_branch_operator_id'] ==
                  self.hierarchy_operator_df['hierarchy_operator_id']) \
            .select(col('hierarchy_operator_id').alias('user_hierarchy_id'),
                    col('hierarchy_operator_uuid').alias('agent_user_hierarchy_uuid'),
                    col('hierarchy_operator_branch_branch_id').alias('branch_id'),
                    col('hierarchy_operator_type').alias('agent_type'),
                    col('hierarchy_branch_type').alias('agent_dtype'),
                    col('hierarchy_branch_name').alias('agent_desk_name'),
                    col('hierarchy_branch_uuid').alias('agent_id_branch_hierarchy_uuid'),
                    col('hierarchy_branch_desk_type').alias('agent_desk_type'),
                    col('hierarchy_branch_language').alias('agent_language'),
                    col('hierarchy_operator_prod_num').alias('prod_num'),
                    col('hierarchy_branch_uuid_compound').alias('agent_id_branch_hierarchy_uuid_compound'),
                    col('hierarchy_operator_uuid_compound').alias('agent_user_hierarchy_uuid_compound'),
                    )


class AgentVSync:
    def __init__(self, app_name, log_state, config_dict):
        self.operator_df = OperatorSync(app_name, log_state, config_dict).filter_df()
        self.h_user_v_df = HUserVSync(app_name, log_state, config_dict).filter_df()
        self.user_hierarchy_v_df = UserHierarchyVSync(app_name, log_state, config_dict).filter_df()

    def op_df(self):
        return self.operator_df.select(
            col('operator_uuid'),
            concat(trim(col('operator_first_name')), lit(' '), trim(col('operator_last_name'))).alias('name'),
            col('operator_email'),
            col('operator_registration_date'),
            col('operator_status'))

    def lan_df(self):
        pre_lan_df = (self.h_user_v_df.filter(col('desk_language').isNotNull())
                      .select(col('user_uuid'), col('desk_language')).distinct())
        lan_df = pre_lan_df.groupby(col('user_uuid')) \
            .agg(concat_ws(", ", collect_list(col('desk_language'))).alias('lan'))
        return lan_df

    def desk_df(self):
        return self.user_hierarchy_v_df.filter(col('agent_desk_name').isNotNull()
                                               & col('agent_dtype').isin(['DESK', 'TEAM'])).select(
            col('agent_user_hierarchy_uuid'), upper(col('agent_desk_name')).alias('agent_desk_name')).distinct() \
            .groupby(col('agent_user_hierarchy_uuid')).agg(concat_ws(", ", collect_list(col('agent_desk_name')))
                                                           .alias('desk'))

    def pre_join_df(self):
        return self.h_user_v_df.alias('df_1').join(
            self.op_df().alias('df_2'), self.h_user_v_df['user_uuid'] == self.op_df()['operator_uuid'], 'left') \
            .select('df_1.*', 'df_2.*')

    def join_df(self):
        return self.pre_join_df().alias('df_1').join(
            self.lan_df().alias('df_2'), self.h_user_v_df['user_uuid'] == self.lan_df()['user_uuid'], 'left') \
            .select('df_1.*', 'df_2.lan')

    def filter_df(self):
        return self.join_df().alias('df_1').join(
            self.desk_df().alias('df_2'),
            self.join_df()['user_uuid'] == self.desk_df()['agent_user_hierarchy_uuid'], 'left') \
            .select('df_1.*', 'df_2.*').select(
            col('user_uuid').alias('agent_user_hierarchy_uuid'),
            col('name'),
            col('user_type'),
            coalesce(col('desk'), lit("Null")).alias('agent_desk_name'),
            col('lan').alias('agent_language'),
            col('operator_email').alias('email'),
            col('operator_registration_date').alias('registration_date'),
            col('operator_status').alias('status'),
            col('user_brand_id'),
            col('hierarchy_operator_prod_num').alias('prod_num'),
            col('user_uuid_compound').alias('agent_user_hierarchy_uuid_compound')).distinct()


