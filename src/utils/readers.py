import collections
import configparser
import subprocess
from datetime import datetime, timedelta
from typing import Union, Any

from sqlalchemy import func, nullsfirst, Table, inspect
from sqlalchemy.orm import Session, load_only

from utils.common import write_log
from utils.constants import SynchronizationState, EngineType


def get_data(log_state: SynchronizationState,
             filter_expression: Union[str, bool],
             offset: int = 0) -> list:
    """
    Gets data from source database
    :param log_state: instance of SynchronizationState class which consists of log arguments
    :param filter_expression: string for synchronization
    :param offset: step for batch moving
    :return: list of instances
    """
    if 'get_prefiltered_dataset' in log_state.table_name.__dict__.keys():
        query = log_state.table_name.get_prefiltered_dataset(log_state.source_session)
    else:
        query = log_state.source_session.query(log_state.table_name)
    df = query.filter(filter_expression) \
        .order_by(*create_order_rule(log_state)) \
        .offset(offset * log_state.batch_size) \
        .limit(log_state.batch_size) \
        .all()
    log_state.message = f"uploaded {len(df)} rows, offset {offset}"
    write_log(log_state)
    return df


def get_max_date(log_state: SynchronizationState) -> Union[datetime, Any]:
    """
    Get maximal date from column
    :param log_state: instance of SynchronizationState class which consists of log arguments
    :return:  maximal datetime value for column
    """
    return log_state.dwh_session \
        .query(func.max(log_state.table_name.filter_column)) \
        .filter(log_state.table_name.filter_column < func.now()) \
        .first()[0]


def create_filter(log_state: SynchronizationState,
                  max_value: Union[datetime, bool]) -> Union[bool, str]:
    """
    Creates filter for batch getting
    :param log_state: instance of SynchronizationState class which consists of log arguments
    :param max_value: value for filtering
    :return: text of filter expression
    """
    if max_value is None:
        return True
    filter_expression = log_state.table_name.filter_column >= max_value
    return filter_expression


def get_ddl(configuration: configparser.SectionProxy, schema: str, table: str) -> str:
    """
    Get SQL dump for schema of database object
    :param configuration: part of configuration regarding schema
    :param schema: schema name
    :param table: table name
    :return: pure  DDL script
    """
    process = subprocess.Popen(["pg_dump",
                                f"--host={configuration['host']}",
                                f"--dbname={configuration['dbname']}",
                                f"--table={schema}.{table}",
                                f"--username={configuration['user']}",
                                '--schema-only'],
                               stdin=subprocess.PIPE,
                               stdout=subprocess.PIPE)
    return process.communicate(input=f"{configuration['password']}".encode())[0].decode("utf-8")


def create_order_rule(log_state) -> Union[tuple, Any]:
    """
    Creates order tuple
    :param log_state: instance of SynchronizationState class which consists of log arguments
    :return: tuple of columns with rules
    """
    pk_columns = tuple(log_state.table_name.__table__.primary_key)
    if isinstance(log_state.table_name.filter_column, collections.Callable):
        return pk_columns
    elif log_state.source_engine_type == EngineType.mysql.value:
        return log_state.table_name.filter_column.is_(None), log_state.table_name.filter_column, *pk_columns
    elif log_state.source_engine_type == EngineType.postgres.value:
        return nullsfirst(log_state.table_name.filter_column), *pk_columns
    else:
        log_state.message = "Unknown engine type"
        write_log(log_state)
        return None


def create_order_rule_string(log_state) -> Union[tuple, Any]:
    """
    Creates order string
    :param log_state: instance of SynchronizationState class which consists of log arguments
    :return: tuple of columns with rules
    """
    return f" ORDER BY {log_state.table_name.filter_column_string} NULLS FIRST, " \
           f"{', '.join(log_state.table_name.primary_keys)}"


def create_mongo_filter(log_state: SynchronizationState, max_date: datetime) -> dict:
    """
    Creates filter expression by two fields or return empty dictionary for backfill
    :param log_state: instance of SynchronizationState class which consists of log arguments
    :param max_date: max date from dwh
    :return: filter expression for mongo query
    """
    if max_date is None:
        return {}
    filter_expression = {log_state.table_name.mongo_filter_field: {'$gte': max_date}}
    return filter_expression


def create_elasticsearch_query(log_state: SynchronizationState, max_date: datetime) -> dict:
    """
    Creates filter expression by two fields or return empty dictionary for backfill
    :param log_state: instance of SynchronizationState class which consists of log arguments
    :param max_date: tuple of two datetime objects
    :return: filter expression for mongo query
    """
    if max_date is None:
        return {}
    # TODO research how to change query
    filter_expression = {"query": {"bool": {"must": {"exists": {"field": "uuid"}},
                                            "filter":
                                                {'range': {log_state.table_name.elastic_filter_field:
                                                               {'gte': max_date}}}}},
                         "sort": [{log_state.table_name.elastic_filter_field: {"order": "asc"}}]}
    return filter_expression


def get_elastic_sync_size(log_state: SynchronizationState) -> int:
    """
    Get size of elasticsearch sync
    :param log_state: instance of SynchronizationState class which consists of log arguments
    :return: number of rows for read from elastic search
    """
    return log_state.source_session.count(body={"query":
                                                    {"bool": {"must": {"exists": {"field": "uuid"}},
                                                              "filter": {'range':
                                                                  {log_state.table_name.elastic_filter_field:
                                                                      {'gte': get_max_date(
                                                                          log_state)}}}}}})['count']


def create_order_rule_reconciliation(log_state) -> Union[tuple, Any]:
    """
    Creates order tuple
    :param log_state: instance of SynchronizationState class which consists of log arguments
    :return: tuple of columns with rules
    """
    pk_columns = tuple(log_state.table_name.__table__.primary_key)
    if isinstance(log_state.table_name.filter_column_reconciliation, collections.Callable):
        return pk_columns
    elif log_state.source_engine_type == EngineType.mysql.value:
        return (log_state.table_name.filter_column_reconciliation.is_(None),
                log_state.table_name.filter_column_reconciliation, *pk_columns)
    elif log_state.source_engine_type == EngineType.postgres.value:
        return nullsfirst(log_state.table_name.filter_column_reconciliation), *pk_columns
    else:
        log_state.message = "Unknown engine type"
        write_log(log_state)
        return None


def get_data_for_fix(log_state: SynchronizationState, bad_hour: datetime, offset: int) -> list:
    """
    Gets data from source database for reconciliation
    :param log_state: instance of SynchronizationState class which consists of log arguments
    :param bad_hour: datetime object
    :param offset: step for batch moving
    :return: list of source objects
    """
    df = log_state.source_session.query(log_state.table_name) \
        .filter(fix_filter(log_state, bad_hour)) \
        .order_by(*create_order_rule_reconciliation(log_state)) \
        .offset(offset * log_state.batch_size) \
        .limit(log_state.batch_size) \
        .all()
    log_state.message = f"uploaded {len(df)} rows, offset {offset}"
    write_log(log_state)
    return df


def get_reconciliation_length(log_state: SynchronizationState, bad_hour: datetime) -> range:
    """
    Gets length list of rows to fetch
    :param log_state: instance of SynchronizationState class which consists of log arguments
    :param bad_hour: datetime object
    :return:  range for offset creation
    """
    return range(log_state
                 .source_session
                 .query(log_state.table_name)
                 .filter(fix_filter(log_state, bad_hour)).count() // log_state.batch_size + 1)


def fix_filter(log_state: SynchronizationState, bad_hour: datetime) -> str:
    """
    Create filter expression
    :param log_state: instance of SynchronizationState class which consists of log arguments
    :param bad_hour: datetime object
    :return: filter expression
    """
    return func.date_trunc('hour', log_state.table_name.filter_column_reconciliation) == bad_hour


def create_mongo_filter_reconciliation(log_state: SynchronizationState,
                                       bad_hour: datetime) -> dict:
    """
    Creates filter expression by two fields or return empty dictionary for backfill
    :param log_state: instance of SynchronizationState class which consists of log arguments
    :param bad_hour: datetime object
    :return: filter expression for mongo query
    """
    return {log_state.table_name.mongo_filter_field: {'$gte': bad_hour,
                                                      '$lt': bad_hour + timedelta(hours=1)}}


def get_max_date_by_prod_num(log_state: SynchronizationState, dwh_table: Union[Table, str]) -> Union[datetime, Any]:
    """
    Get maximal date from column
    :param log_state: instance of SynchronizationState class which consists of log arguments
    :param dwh_table: instance of dwh table with prod_num column
    :return:  maximal datetime value for column
    """
    return log_state.dwh_session \
        .query(func.max(dwh_table.filter_column)) \
        .filter(dwh_table.prod_num == log_state.prod_number) \
        .filter(dwh_table.filter_column < func.now()) \
        .first()[0]


def get_max_id_by_prod_num(log_state: SynchronizationState, dwh_table: Union[Table, str]) -> Union[int, Any]:
    """
    Get maximal date from column
    :param log_state: instance of SynchronizationState class which consists of log arguments
    :param dwh_table: instance of dwh table with prod_num column
    :return:  maximal datetime value for column
    """
    return log_state.dwh_session \
        .query(func.max(dwh_table.filter_column)) \
        .filter(dwh_table.prod_num == log_state.prod_number) \
        .first()[0]


def pre_fetch(log_state: SynchronizationState, session: Session, table: Table) -> dict:
    """
    Get all values in primary keys columns and add them into dictionary
    :param log_state: instance of SynchronizationState class which consists of log arguments
    :param session: session for query
    :param table: table for query
    :return: dictionary of column identity as keys and row as values
    """
    deleted_from_dwh = {}
    if 'get_prefiltered_dataset' in table.__dict__.keys():
        query = table.get_prefiltered_dataset(session)
    else:
        query = session.query(table)
    result = query \
        .options(load_only(*[pk.name for pk in inspect(table).primary_key])) \
        .filter(generate_filter_for_prefetch(log_state, table)) \
        .all()
    for r in result:
        pk_val = inspect(r).identity
        if 'prod_num' not in inspect(r).dict.keys():
            pk_val += (log_state.prod_number,)
        deleted_from_dwh[pk_val] = r
    return deleted_from_dwh


def generate_filter_for_prefetch(log_state: SynchronizationState, table: Table) -> Union[bool, str]:
    """
    Generates filter based on prod number
    :param log_state: instance of SynchronizationState class which consists of log arguments
    :param table: table for query
    :return:  text of filter expression
    """
    if 'prod_num' in table.__table__.c:
        return table.prod_num == log_state.prod_number
    else:
        return True


def read_mongo_with_schema(spark, log_state, mongo_uri, mongo_database, mongo_collection, schema,
                           date_range_filter: tuple = tuple(),
                           fields_filter_dict: dict = None,
                           select_fields: tuple = tuple('*'),
                           max_connection_attempts: int = 3):
    match_filter = {'$match': {'$or': []}}
    if date_range_filter:
        date_field_name = list(date_range_filter)[0]
        date_filter = {
            date_field_name: {
                '$gte': {'$date': date_range_filter[date_field_name][0].isoformat() + 'Z'},
                '$lt': {'$date': date_range_filter[date_field_name][1].isoformat() + 'Z'}
            }
        }
        match_filter['$match']['$or'].append(date_filter)
    if fields_filter_dict:
        fields = list()
        for field in fields_filter_dict:
            fields.append({field: {'$in': list(fields_filter_dict[field])}})
        match_filter['$match']['$or'].extend(fields)
    if not match_filter['$match']['$or']:
        match_filter = {'$match': {'_id': {'$exists': 'true'}}}
    pipeline = [
        match_filter,
        {'$sort': {'lastUpdatedDate': 1, '_id': 1}}
    ]
    attempt = 0
    while attempt < max_connection_attempts:
        try:
            result_df = spark.read \
                .option("spark.mongodb.input.uri", mongo_uri) \
                .option("spark.mongodb.input.database", mongo_database) \
                .option("spark.mongodb.input.collection", mongo_collection) \
                .option("spark.mongodb.input.partitioner", "MongoShardedPartitioner") \
                .option("pipeline", pipeline) \
                .format('com.mongodb.spark.sql.DefaultSource').schema(schema).load().select(*select_fields)
        except Exception as exc:
            log_state.message = f"DWH PROD0{log_state.prod_number} Exception: {str(exc)}"
            write_log(log_state)
            attempt += 1
            continue
        else:
            break
    else:
        raise Exception("Unsuccessful attempt to read data from mongodb. Read log for more information")
    return result_df


def read_elastic_search_with_schema(spark, nodes, as_array, profile_elastic_search_schema, index,
                                    date_range_filter: tuple = tuple(),
                                    fields_filter_dict: dict = None,
                                    select_fields=None):
    stringency = 'must'
    if date_range_filter:
        date_field = list(date_range_filter)[0]
        date_filter_string = """{
                                "range": {
                                    "%s": {
                                        "gte": "%s",
                                        "lt": "%s",
                                        "format": "yyyy-MM-dd HH:mm:ss"
                                    }
                                }
                            }""" % (date_field, date_range_filter[date_field][0], date_range_filter[date_field][1])
    else:
        date_filter_string = str()
    if fields_filter_dict:
        fields = list()
        for field in fields_filter_dict:
            string = '{"terms": {"%s": [%s]}}' % (
                field,
                ','.join(list(map(lambda e: '"' + e + '"', fields_filter_dict[field])))
            )
            fields.append(string)
        fields_filter_string = ','.join(fields)
        stringency = 'should'
    else:
        fields_filter_string = '{"exists": {"field": "uuid"}}'
    query = """
    {
        "query": {
            "bool": {
                "%s": [
                %s
                %s
                %s
                ]
            }
        }
    }
    """ % (
        stringency,
        date_filter_string,
        ',' if all([date_filter_string, fields_filter_string]) else '',
        fields_filter_string
    )
    return spark.read.format("org.elasticsearch.spark.sql") \
        .option("index.max_terms_count", 100000) \
        .option("es.nodes", nodes) \
        .option("es.nodes.wan.only", "true") \
        .option("es.read.field.as.array.include", as_array) \
        .option("mode", "DROPMALFORMED") \
        .option("es.mapping.date.rich", "false") \
        .option("es.resource", index) \
        .option("es.query", query) \
        .option("pushdown", "true") \
        .schema(profile_elastic_search_schema) \
        .load().select(*select_fields if select_fields else '*')


def read_postgresql_with_schema(spark, postgresql_uri, user, password, query):
    return spark.read \
        .format("jdbc") \
        .option("url", postgresql_uri) \
        .option("query", query) \
        .option("user", user) \
        .option("password", password) \
        .option("driver", "org.postgresql.Driver").load()


def get_data_from_string_query(log_state: SynchronizationState, query: str) -> list:
    """
    Gets data from source database
    :param log_state: instance of SynchronizationState class which consists of log arguments
    :param query: string for raw query
    :return: list of instances
    """
    df = log_state \
        .source_session.execute(query) \
        .fetchall()
    return df


def generate_initial_query(log_state,
                           date_range_filter: dict = None,
                           fields_filter_dict: dict = None,
                           custom_string_filter: str = None,
                           select_fields: tuple = None) -> str:
    if date_range_filter:
        date_field = list(date_range_filter)[0]
        date_range_string = "and %s between '%s' and '%s'" % (
            date_field,
            date_range_filter[date_field][0],
            date_range_filter[date_field][1]
        )
    else:
        date_range_string = str()
    if fields_filter_dict and any(fields_filter_dict.values()):
        conditions = list()
        for field in fields_filter_dict:
            if not fields_filter_dict[field]:
                continue
            conditions.append(f'{field} in (%s)'
                              % ', '.join(list(map(lambda e: "'" + str(e) + "'", fields_filter_dict[field]))))
        conditions_string = ' or '.join(conditions)
        if date_range_string:
            fields_filter_string = f'or ({conditions_string})'
        else:
            fields_filter_string = f'and ({conditions_string})'
    else:
        fields_filter_string = ''
    if custom_string_filter:
        custom_string_filter = 'and ' + custom_string_filter
    else:
        custom_string_filter = str()
    additional_where_string = date_range_string + '\n' + fields_filter_string + '\n' + custom_string_filter
    return f"""
    select {','.join(select_fields) if select_fields else '*'}
    from {log_state.schema if log_state.schema else 'public'}.{log_state.table_name.__tablename__} 
    where 1=1 
    {additional_where_string}
"""


def create_filter_string(filter_column_string: str,
                         max_value: Union[datetime, bool]) -> Union[bool, str]:
    """
    Creates string filter for batch getting
    :param filter_column_string: column
    :param max_value: value for filtering
    :return: text of filter expression
    """
    if max_value is None:
        return ' where 1=1'
    filter_expression = f" where {filter_column_string} " \
                        f">= TO_DATE('{max_value}')"
    return filter_expression


def get_max_value_from_string_query(log_state):
    if log_state.table_name.filter_column is None:
        return None
    max_value = log_state.dwh_session. \
        execute(f"select max({log_state.table_name.filter_column}) "
                f"from {log_state.schema}.{log_state.table_name.__tablename__};").first()[0]
    return max_value
