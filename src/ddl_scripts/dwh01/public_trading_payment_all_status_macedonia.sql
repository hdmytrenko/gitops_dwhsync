--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: trading_payment_all_status_macedonia; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.trading_payment_all_status_macedonia AS
 SELECT tpasv.company,
    tpasv.index,
    tpasv.id,
    tpasv.brand_country,
    tpasv.login,
    tpasv.profile_id,
    tpasv.payment_id,
    tpasv.currency,
    tpasv.external_reference,
    tpasv.deposit,
    tpasv.withdraw,
    tpasv.net_deposit,
    tpasv.transfer_in,
    tpasv.transfer_out,
    tpasv.credit_in,
    tpasv.credit_out,
    tpasv.deposit_usd,
    tpasv.withdraw_usd,
    tpasv.net_deposit_usd,
    tpasv.type,
    tpasv.rate,
    tpasv.status,
    tpasv.kyc_status,
    tpasv.acquisition_status,
    tpasv.creation_time_profile,
    tpasv.creation_time,
    tpasv.day_payment,
    tpasv.month_payment,
    tpasv.year_payment,
    tpasv.week_payment,
    tpasv.day_of_week_creation_time,
    tpasv.day_name_creation_time,
    tpasv.country,
    tpasv.language,
    tpasv.brand_id,
    tpasv.payment_method,
    tpasv.group_payment_method,
    tpasv.is_published,
    tpasv.payment_transaction_id,
    tpasv.expiration_date,
    tpasv.version,
    tpasv.profile_first_name,
    tpasv.profile_last_name,
    tpasv.email,
    tpasv.client_ip,
    tpasv.created_by,
    tpasv.is_mobile,
    tpasv.user_agent,
    tpasv.profile_country,
    tpasv.payment_aggregator,
    tpasv.agent_id,
    tpasv.agent_name,
    tpasv.sales_status,
    tpasv.sales_rep,
    tpasv.sales_name,
    tpasv.sales_user_type,
    tpasv.sales_desk_name,
    tpasv.sales_language,
    tpasv.retention_status,
    tpasv.retention_rep,
    tpasv.retention_name,
    tpasv.reten_user_type,
    tpasv.reten_desk_name,
    tpasv.reten_language,
    tpasv.affiliate_uuid,
    tpasv.affiliate_name,
    tpasv.affiliate_referral,
    tpasv.affiliate_source,
    tpasv.profile_country_name,
    tpasv.user_type,
    tpasv.agent_desk_name,
    tpasv.agent_language,
    tpasv.ftd_time,
    tpasv.ftd_status,
    tpasv.day_to_ftd,
    tpasv.ftd,
    tpasv.ftd_deposit,
    tpasv.ftd_deposit_usd,
    tpasv.noftd,
    tpasv.redeposit,
    tpasv.redeposit_usd,
    tpasv.last_deposit_time,
    tpasv.last_note,
    tpasv.last_note_date,
    tpasv.modification_time,
    tpasv.execution_time,
    tpasv.amount,
    tpasv.amount_usd,
    tpasv.questionnaire_status,
    tpasv.last_login,
    tpasv.last_trade_date,
    tpasv.deposit_count,
    tpasv.mt_balance,
    tpasv.profile_status
   FROM public.trading_payment_all_status_v tpasv
  WHERE ((tpasv.brand_country)::text = 'Macedonia'::text);


ALTER TABLE public.trading_payment_all_status_macedonia OWNER TO postgres;

--
-- Name: TABLE trading_payment_all_status_macedonia; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public.trading_payment_all_status_macedonia TO prod01_metabase;
GRANT SELECT ON TABLE public.trading_payment_all_status_macedonia TO ybashkatov;


--
-- PostgreSQL database dump complete
--

