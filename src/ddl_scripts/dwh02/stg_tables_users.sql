--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

--
-- Name: users; Type: TABLE; Schema: stg_tables; Owner: data_sync
--

CREATE TABLE stg_tables.users (
    server_id smallint NOT NULL,
    enable boolean NOT NULL,
    enable_change_password boolean NOT NULL,
    enable_read_only boolean NOT NULL,
    enable_otp boolean NOT NULL,
    send_reports boolean NOT NULL,
    manager_login integer NOT NULL,
    login integer NOT NULL,
    mq_id integer NOT NULL,
    registration_date integer NOT NULL,
    last_date integer NOT NULL,
    leverage integer NOT NULL,
    agent_account integer NOT NULL,
    last_ip integer NOT NULL,
    "timestamp" integer NOT NULL,
    "group" character varying(16) NOT NULL,
    password character varying(16) NOT NULL,
    password_investor character varying(16) NOT NULL,
    password_phone character varying(32) NOT NULL,
    name character varying(128) NOT NULL,
    country character varying(32) NOT NULL,
    city character varying(32) NOT NULL,
    state character varying(32) NOT NULL,
    zip_code character varying(32) NOT NULL,
    address character varying(96) NOT NULL,
    lead_source character varying(32) NOT NULL,
    phone character varying(32) NOT NULL,
    email character varying(48) NOT NULL,
    comment character varying(64) NOT NULL,
    id character varying(32) NOT NULL,
    status character varying(16) NOT NULL,
    otp_secret character varying(32) NOT NULL,
    balance double precision,
    previous_month_balance double precision NOT NULL,
    previous_balance double precision NOT NULL,
    credit double precision,
    interest_rate double precision NOT NULL,
    taxes double precision NOT NULL,
    previous_month_equity double precision NOT NULL,
    previous_equity double precision NOT NULL,
    margin double precision,
    proxy_timestamp bigint NOT NULL,
    deleted_at bigint,
    prod_num smallint
);


ALTER TABLE stg_tables.users OWNER TO data_sync;

--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: stg_tables; Owner: data_sync
--

ALTER TABLE ONLY stg_tables.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (server_id, login);


--
-- Name: TABLE users; Type: ACL; Schema: stg_tables; Owner: data_sync
--

GRANT SELECT ON TABLE stg_tables.users TO ybashkatov;


--
-- PostgreSQL database dump complete
--

