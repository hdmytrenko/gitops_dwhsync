from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, String, Integer, DATETIME, ARRAY, BOOLEAN, Numeric, Float, Text, Date, func
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.ext.hybrid import hybrid_property

base = declarative_base()


class Affiliate(base):
    __tablename__ = "affiliate"
    __table_args__ = {'schema': 'public'}
    id = Column(Integer, primary_key=True)
    version = Column(Integer)
    uuid = Column(String(60))
    brand = Column(String(30))
    first_name = Column(String(255))
    last_name = Column(String(255))
    phone = Column(String(255))
    email = Column(String(255))
    country = Column(String(255))
    # affiliate_type = Column(String(30))
    status = Column(String(255))
    created_by = Column(String(255))
    created_at = Column(DATETIME(timezone=False))
    updated_at = Column(DATETIME(timezone=False))
    last_updated_by = Column(String(255))
    external_affiliate_id = Column(String(255))
    status_change_date = Column(DATETIME(timezone=False))
    status_change_author = Column(String(255))
    status_reason = Column(String(255))
    show_notes = Column(BOOLEAN)
    show_sales_status = Column(BOOLEAN)
    show_ftd_amount = Column(BOOLEAN)
    allowed_ip_addresses = Column(ARRAY(String))
    forbidden_countries = Column(ARRAY(String))
    deleted = Column(BOOLEAN)
    public = Column(BOOLEAN)
    cellexpert = Column(BOOLEAN)
    is_cde_affiliate = Column(BOOLEAN)

    @hybrid_property
    def filter_column(self):
        return self.updated_at

    @hybrid_property
    def filter_column_reconciliation(self):
        return self.created_at

    matview_refresh_date_filter = None

    def __repr__(self):
        return '<UserHierarchy(id="{}", uuid={}, created_at="{}", updated_at="{}")'.format(self.id, self.uuid,
                                                                                           self.created_at,
                                                                                           self.updated_at)


class Payment(base):
    __tablename__ = "payment"
    __table_args__ = {'schema': 'public'}
    id = Column(Integer, primary_key=True)
    login = Column(String(50))
    profile_id = Column(String(50))
    payment_id = Column(String(50))
    amount = Column(Numeric)
    currency = Column(String(20))
    external_reference = Column(String(250))
    type = Column(String(30))
    status = Column(String(30))
    creation_time = Column(DATETIME(timezone=False))
    country = Column(String(20))
    language = Column(String(20))
    brand_id = Column(Text)
    payment_method = Column(String(55))
    is_published = Column(BOOLEAN)
    payment_transaction_id = Column(String(50))
    expiration_date = Column(DATETIME(timezone=False))
    version = Column(Integer)
    profile_first_name = Column(String(100))
    profile_last_name = Column(String(100))
    client_ip = Column(String(50))
    created_by = Column(String(50))
    is_mobile = Column(BOOLEAN)
    user_agent = Column(String(100))
    profile_country = Column(String(20))
    payment_aggregator = Column(String(20))
    agent_name = Column(String(100))
    agent_id = Column(String(50))
    payment_migration_id = Column(String(30))
    user_migration_id = Column(String(30))
    normalized_amount = Column(Numeric)
    decline_reason = Column(String(100))
    modified_by = Column(String(50))
    ex_rate = Column(Numeric)
    linked_transaction_id = Column(String(50))
    moto = Column(BOOLEAN)
    last_operation_type = Column(String(55))
    updated_at = Column(DATETIME(timezone=False))
    status_changed_at = Column(DATETIME(timezone=False))
    account_uuid = Column(String(128))
    first_time_deposit = Column(BOOLEAN)
    deleted = Column(BOOLEAN)
    affiliate_uuid = Column(String(50))
    agent_branches = Column(ARRAY(String))
    platform_type = Column(String(128))
    account_type = Column(String(128))
    comment = Column(String(256))
    sequence_position = Column(Integer)
    bank_name = Column(String(100))
    masked_pan = Column(String(100))

    @hybrid_property
    def filter_column(self):
        return self.updated_at

    @hybrid_property
    def filter_column_reconciliation(self):
        return self.creation_time

    matview_refresh_date_filter = "creation_time"

    def __repr__(self):
        return '<Payment((id="{}", payment_id={}, creation_time="{}", updated_at="{}")'.format(self.id,
                                                                                               self.payment_id,
                                                                                               self.creation_time,
                                                                                               self.updated_at)


class Operator(base):
    __tablename__ = "operator"
    __table_args__ = {'schema': 'public'}
    id = Column(Integer, primary_key=True)
    uuid = Column(String(255))
    first_name = Column(String(255))
    last_name = Column(String(255))
    phone_number = Column(String(255))
    email = Column(String(255))
    country = Column(String(255))
    registration_date = Column(DATETIME(timezone=False))
    status = Column(String(255))
    status_change_date = Column(DATETIME(timezone=False))
    status_change_author = Column(String(255))
    registered_by = Column(String(255))
    status_reason = Column(String(255))
    operator_role = Column(String(15))
    # didlogic_password = Column(String(100))
    # external_affiliate_id = Column(Integer)
    updated_at = Column(DATETIME(timezone=False))
    created_at = Column(DATETIME(timezone=False))

    @hybrid_property
    def filter_column(self):
        return self.updated_at

    @hybrid_property
    def filter_column_reconciliation(self):
        return self.created_at

    matview_refresh_date_filter = None

    def __repr__(self):
        return '<Operator((id="{}", uuid={}, created_at="{}", updated_at="{}")'.format(self.id,
                                                                                       self.uuid,
                                                                                       self.created_at,
                                                                                       self.updated_at)


class Callback(base):
    __tablename__ = "callback"
    __table_args__ = {'schema': 'public'}
    id = Column(Integer, primary_key=True)
    user_id = Column(String(255))
    operator_id = Column(String(255))
    callback_time = Column(DATETIME(timezone=False))
    status = Column(String(32))
    creation_time = Column(DATETIME(timezone=False))
    update_time = Column(DATETIME(timezone=False))
    is_deleted = Column(BOOLEAN)
    callback_id = Column(String(50))
    brand_id = Column(String(50))
    created_by = Column(String(50))

    @hybrid_property
    def filter_column(self):
        return self.update_time

    @hybrid_property
    def filter_column_reconciliation(self):
        return self.creation_time

    matview_refresh_date_filter = None

    def __repr__(self):
        return '<BranchHierarchy((id="{}", user_id={}, creation_time="{}", update_time="{}")'. \
            format(self.id, self.user_id, self.creation_time, self.update_time)


class TradeRecord(base):
    __tablename__ = "trade_record"
    __table_args__ = {'schema': 'public'}
    id = Column(Integer, primary_key=True)
    trade_id = Column(Integer)
    login = Column(Integer)
    symbol = Column(String(12))
    digits = Column(Integer)
    operation_type = Column(String(255))
    volume = Column(Numeric(10, 2))
    open_time = Column(Integer)
    close_time = Column(Integer)
    open_price = Column(Float)
    close_price = Column(Float)
    open_rate = Column(Float)
    close_rate = Column(Float)
    stop_loss = Column(Float)
    take_profit = Column(Float)
    expiration = Column(Integer)
    reason = Column(String(1))
    commission = Column(Numeric(12, 2))
    commission_agent = Column(Float)
    swap = Column(Numeric(12, 2))
    profit = Column(Numeric(12, 2))
    taxes = Column(Numeric(12, 2))
    magic = Column(Integer)
    comment = Column(String(64))
    timestamp = Column(Integer)
    closed = Column(BOOLEAN)
    trade_type = Column(String(255))
    agent_id = Column(String(64))
    account_uuid = Column(String(128))
    profile_uuid = Column(String(128))
    brand_id = Column(String(128))
    platform_type = Column(String(64))
    created_at = Column(DATETIME(timezone=False))
    updated_at = Column(DATETIME(timezone=False))
    migrated = Column(BOOLEAN)

    @hybrid_property
    def filter_column(self):
        return self.updated_at

    @hybrid_property
    def filter_column_reconciliation(self):
        return self.created_at

    matview_refresh_date_filter = None

    def __repr__(self):
        return '<TradeRecord(id="{}", trade_id={}, login="{}", created_at="{}", updated_at="{}")'. \
            format(self.id, self.trade_id, self.login, self.created_at, self.updated_at)


class Trades(base):
    __tablename__ = "trades"
    __table_args__ = {'schema': 'public'}
    server_id = Column(Integer, primary_key=True)
    cmd = Column(Integer)
    manager_login = Column(Integer)
    order = Column(Integer, primary_key=True)
    login = Column(Integer)
    digits = Column(Integer)
    volume = Column(Integer)
    opening_time = Column(Integer)
    magic = Column(Integer)
    activation = Column(Integer)
    state = Column(Integer)
    gw_order = Column(Integer)
    gw_volume = Column(Integer)
    gw_open_price = Column(Integer)
    gw_close_price = Column(Integer)
    closing_time = Column(Integer)
    expiration_time = Column(Integer)
    timestamp = Column(Integer)
    symbol = Column(String(12))
    comment = Column(String(32))
    reason = Column(String(1))
    opening_price = Column(Float)
    stop_loss = Column(Float)
    take_profit = Column(Float)
    commission = Column(Float)
    commission_agent = Column(Float)
    storage = Column(Float)
    closing_price = Column(Float)
    profit = Column(Float)
    taxes = Column(Float)
    margin_rate = Column(Float)
    opening_rate = Column(Float)
    closing_rate = Column(Float)
    proxy_timestamp = Column(Integer)
    deleted_at = Column(Integer)

    @hybrid_property
    def filter_column(self):
        return func.to_timestamp(self.timestamp)

    @hybrid_property
    def filter_column_reconciliation(self):
        return func.to_timestamp(self.timestamp)

    matview_refresh_date_filter = None

    def __repr__(self):
        return '<Trades(order="{}", server_id={}, timestamp="{}", closing_time="{}")'. \
            format(self.order, self.server_id, self.timestamp, self.closing_time)


class Lead(base):
    __tablename__ = "lead"
    __table_args__ = {'schema': 'public'}
    id = Column(UUID(), primary_key=True)
    status = Column(String(50))
    name = Column(String(50))
    surname = Column(String(50))
    email = Column(Text)
    phone = Column(String(30))
    mobile = Column(String(30))
    country = Column(String(50))
    source = Column(String(50))
    # sales_status = Column(String(50))
    # sales_agent = Column(String(50))
    birth_date = Column(Date)
    affiliate = Column(String(50))
    gender = Column(String(10))
    city = Column(String(50))
    language = Column(String(50))
    brand_id = Column(String(100))
    status_changed_date = Column(DATETIME(timezone=False))
    converted_to_client_uuid = Column(String(50))
    converted_by_operator_uuid = Column(String(50))
    version = Column(Integer)
    migration_id = Column(Integer)
    updated_at = Column(DATETIME(timezone=False))
    # deleted = Column(BOOLEAN)
    created_at = Column(DATETIME(timezone=False))

    @hybrid_property
    def filter_column(self):
        return self.updated_at

    @hybrid_property
    def filter_column_reconciliation(self):
        return self.created_at

    matview_refresh_date_filter = None

    def __repr__(self):
        return '<Lead(id="{}", email={}, created_at="{}", updated_at="{}")'. \
            format(self.id, self.email, self.created_at, self.updated_at)


class Users(base):
    __tablename__ = "users"
    __table_args__ = {'schema': 'public'}
    server_id = Column(Integer, primary_key=True)
    enable = Column(BOOLEAN)
    enable_change_password = Column(BOOLEAN)
    enable_read_only = Column(BOOLEAN)
    enable_otp = Column(BOOLEAN)
    send_reports = Column(BOOLEAN)
    manager_login = Column(Integer)
    login = Column(Integer, primary_key=True)
    mq_id = Column(Integer)
    registration_date = Column(Integer)
    last_date = Column(Integer)
    leverage = Column(Integer)
    agent_account = Column(Integer)
    last_ip = Column(Integer)
    timestamp = Column(Integer)
    group = Column(String(16))
    password = Column(String(16))
    password_investor = Column(String(16))
    password_phone = Column(String(32))
    name = Column(String(128))
    country = Column(String(32))
    city = Column(String(32))
    state = Column(String(32))
    zip_code = Column(String(32))
    address = Column(String(96))
    lead_source = Column(String(32))
    # phone = Column(String(32))
    email = Column(String(48))
    comment = Column(String(64))
    id = Column(String(32))
    status = Column(String(16))
    otp_secret = Column(String(32))
    previous_month_balance = Column(Float)
    previous_balance = Column(Float)
    interest_rate = Column(Float)
    taxes = Column(Float)
    previous_month_equity = Column(Float)
    previous_equity = Column(Float)
    proxy_timestamp = Column(Integer)
    deleted_at = Column(Integer)

    @hybrid_property
    def filter_column(self):
        return func.to_timestamp(self.timestamp)

    @hybrid_property
    def filter_column_reconciliation(self):
        return func.to_timestamp(self.timestamp)

    matview_refresh_date_filter = None

    def __repr__(self):
        return '<Users(server_id="{}", login={}, timestamp="{}", email="{}")'. \
            format(self.server_id, self.login, self.timestamp, self.email)


class LoggingClass(base):
    __tablename__ = "sync_logs"
    __table_args__ = {'schema': 'full_sync_copies'}
    id = Column(Integer, primary_key=True)
    session_uuid = Column(UUID())
    execution_time = Column(DATETIME(timezone=False))
    table_name = Column(String(256))
    error_message = Column(String)
    message_type = Column(String(32))
    prod_number = Column(Integer)
    sync_name = Column(String(64))

    def __repr__(self):
        return '<LoggingClass(execution_time="{}", table_name={}, prod_number="{}", error_message="{}")'. \
            format(self.execution_time, self.table_name, self.prod_number, self.error_message)


class Securities(base):
    __tablename__ = 'securities'
    __table_args__ = {'schema': 'public'}
    server_id = Column(Integer, primary_key=True)
    id = Column(Integer, primary_key=True)
    name = Column(String(16))
    description = Column(String(64))

    @staticmethod
    def filter_column():
        return None

    matview_refresh_date_filter = None

    def __repr__(self):
        return '<Securities(server_id={}, id={}, name="{}")'. \
            format(self.server_id, self.id, self.name)


class Groups(base):
    __tablename__ = 'groups'
    __table_args__ = {'schema': 'public'}
    server_id = Column(Integer, primary_key=True)
    enable = Column(BOOLEAN)
    use_swap = Column(BOOLEAN)
    hedge_prohibited = Column(BOOLEAN)
    stopout_skip_hedged = Column(BOOLEAN)
    manager_login = Column(Integer)
    timeout = Column(Integer)
    otp_mode = Column(Integer)
    copies = Column(Integer)
    reports = Column(Integer)
    default_leverage = Column(Integer)
    margin_call = Column(Integer)
    margin_mode = Column(Integer)
    margin_stopout = Column(Integer)
    news = Column(Integer)
    check_ie_prices = Column(Integer)
    max_positions = Column(Integer)
    close_reopen = Column(Integer)
    close_fifo = Column(Integer)
    hedge_large_leg = Column(Integer)
    margin_type = Column(Integer)
    archive_period = Column(Integer)
    archive_max_balance = Column(Integer)
    archive_pending_period = Column(Integer)
    group = Column(String(16), primary_key=True)
    company = Column(String(128))
    signature = Column(String(128))
    support_page = Column(String(128))
    smtp_server = Column(String(64))
    smtp_login = Column(String(32))
    smtp_password = Column(String(32))
    support_email = Column(String(64))
    templates = Column(String(32))
    currency = Column(String(12))
    default_deposit = Column(Float)
    credit = Column(Float)
    interest_rate = Column(Float)

    @staticmethod
    def filter_column():
        return None

    matview_refresh_date_filter = None

    def __repr__(self):
        return '<Groups(server_id={}, group="{}")'. \
            format(self.server_id, self.group)


class Symbols(base):
    __tablename__ = 'symbols'
    __table_args__ = {'schema': 'public'}
    server_id = Column(Integer, primary_key=True)
    swap_enable = Column(BOOLEAN)
    manager_login = Column(Integer)
    security_id = Column(Integer)
    digits = Column(Integer)
    trade = Column(Integer)
    background_color = Column(Integer)
    count = Column(Integer)
    count_original = Column(Integer)
    realtime = Column(Integer)
    starting = Column(Integer)
    profit_mode = Column(Integer)
    filter = Column(Integer)
    filter_counter = Column(Integer)
    filter_smoothing = Column(Integer)
    logging = Column(Integer)
    spread = Column(Integer)
    spread_balance = Column(Integer)
    execution_mode = Column(Integer)
    swap_type = Column(Integer)
    swap_rollover_3_days = Column(Integer)
    stops_level = Column(Integer)
    gtc_pendings = Column(Integer)
    margin_mode = Column(Integer)
    long_only = Column(Integer)
    instant_max_volume = Column(Integer)
    freeze_level = Column(Integer)
    margin_hedged_strong = Column(Integer)
    value_date = Column(Integer)
    quotes_delay = Column(Integer)
    swap_open_price = Column(Integer)
    swap_variation_margin = Column(Integer)
    expiration_time = Column(Integer)
    symbol = Column(String(12), primary_key=True)
    description = Column(String(64))
    source = Column(String(12))
    currency = Column(String(12))
    margin_currency = Column(String(12))
    filter_limit = Column(Float)
    swap_long = Column(Float)
    swap_short = Column(Float)
    contract_size = Column(Float)
    tick_value = Column(Float)
    tick_size = Column(Float)
    margin_initial = Column(Float)
    margin_maintenance = Column(Float)
    margin_hedged = Column(Float)
    margin_divider = Column(Float)
    point = Column(Float)
    multiply = Column(Float)
    bid_tick_value = Column(Float)
    ask_tick_value = Column(Float)

    @staticmethod
    def filter_column():
        return None

    matview_refresh_date_filter = None

    def __repr__(self):
        return '<Symbols(server_id={}, symbol="{}")'. \
            format(self.server_id, self.symbol)


class OperatorAction(base):
    __tablename__ = 'operator_action'
    __table_args__ = {'schema': 'public'}
    id = Column(Integer, primary_key=True)
    operator_uuid = Column(String(255))
    operator_email = Column(String(64))
    profile_uuid = Column(String(255))
    profile_email = Column(String(64))
    type = Column(String(64))
    brand_id = Column(String(128))
    action_date = Column(DATETIME(timezone=False))

    @hybrid_property
    def filter_column(self):
        return self.action_date

    matview_refresh_date_filter = None

    def __repr__(self):
        return '<OperatorAction(id={}, operator_uuid="{}", profile_uuid="{}", brand_id="{}", action_date="{}")'. \
            format(self.id, self.operator_uuid, self.profile_uuid, self.brand_id, self.action_date)


class Profile(base):
    __tablename__ = 'elastic_profile'
    __table_args__ = {'schema': 'public'}
    player_uuid = Column(String(255), primary_key=True)
    username = Column(String(255))
    last_name = Column(String(255))
    first_name = Column(String(255))
    profile_status = Column(String(127))
    profile_status_reason = Column(String(255))
    login = Column(ARRAY(Integer))
    country = Column(String(255))
    currency = Column(String(3))
    address = Column(String(1024))
    city = Column(String(255))
    # phone = Column(String(255))
    email = Column(String(255))
    email_verified = Column(BOOLEAN)
    marketing_mail = Column(BOOLEAN)
    marketing_sms = Column(BOOLEAN)
    language_code = Column(String(255))
    completed = Column(BOOLEAN)
    migrated = Column(BOOLEAN)
    creation_date = Column(DATETIME(timezone=False))
    created_at = Column(DATETIME(timezone=False))
    send_mail = Column(BOOLEAN)
    registration_date = Column(DATETIME(timezone=False))
    registration_ip = Column(String(255))
    ip = Column(String(255))
    gender = Column(String(255))
    updated_date = Column(DATETIME(timezone=False))
    post_code = Column(String(255))
    last_note = Column(String(2048))
    last_note_date = Column(DATETIME(timezone=False))
    birth_date = Column(Date)
    withdrawable_amount_amount = Column(Numeric(20, 10))
    withdrawable_amount_currency = Column(String(3))
    real_money_balance_amount = Column(Numeric(20, 10))
    real_money_balance_currency = Column(String(3))
    bonus_balance_amount = Column(Numeric(20, 10))
    bonus_balance_currency = Column(String(3))
    total_balance_amount = Column(Numeric(20, 10))
    total_balance_currency = Column(String(3))
    first_deposit = Column(BOOLEAN)
    tailor_made_email = Column(BOOLEAN)
    tailor_made_sms = Column(BOOLEAN)
    author_uuid = Column(String(255))
    kyc_rep_name = Column(String(255))
    fns_status = Column(String(255))
    last_trade_date = Column(DATETIME(timezone=False))
    affiliate_uuid = Column(String(255))
    affiliate_source = Column(String(1024))
    is_test_user = Column(BOOLEAN)
    acquisition_status = Column(String(255))
    sales_status = Column(String(255))
    retention_status = Column(String(255))
    equity = Column(Numeric(20, 10))
    balance = Column(Numeric(20, 10))
    kyc_status = Column(String(255))
    base_currency_equity = Column(Numeric(20, 10))
    base_currency_credit = Column(Numeric(20, 10))
    mt_balance = Column(Numeric(20, 10))
    base_currency_margin = Column(Numeric(20, 10))
    credit = Column(Numeric(20, 10))
    margin = Column(Numeric(20, 10))
    brand_id = Column(String(255))
    country_specific_identifier = Column(String(255))
    country_specific_identifier_type = Column(String(255))
    first_deposit_date = Column(DATETIME(timezone=False))
    last_deposit_date = Column(DATETIME(timezone=False))
    retention_rep = Column(String(255))
    sales_rep = Column(String(255))
    deposit_count = Column(Integer)
    passport_number = Column(String(255))
    mt_group = Column(String(255))
    affiliate_referral = Column(String(1024))
    passport_expiration_date = Column(Date)
    questionnaire_status = Column(String(20))
    crs = Column(BOOLEAN)
    last_login = Column(DATETIME(timezone=False))
    withdrawal_count = Column(Integer)
    prod_num = Column(Integer)
    fsa_migration_status = Column(String)
    kyc_changed_at = Column(DATETIME(timezone=False))
    internal_transfer = Column(BOOLEAN)
    fatca = Column(BOOLEAN)
    gdpr_sms = Column(BOOLEAN)
    gdpr_email = Column(BOOLEAN)
    gdpr_phone = Column(BOOLEAN)
    gdpr_social_media = Column(BOOLEAN)
    referrer_profile_uuid = Column(String(255))
    referral_hash = Column(String(255))
    referrer_full_name = Column(String)
    spam_market_news = Column(BOOLEAN)
    spam_promos_and_offers = Column(BOOLEAN)
    spam_information = Column(BOOLEAN)
    spam_statistics_and_summary = Column(BOOLEAN)
    spam_educational = Column(BOOLEAN)
    spam_web_cookies = Column(BOOLEAN)
    verifications = Column(ARRAY(String))

    @hybrid_property
    def filter_column(self):
        return self.updated_date

    @hybrid_property
    def filter_column_reconciliation(self):
        return self.updated_date

    mongo_collection = "profile"

    mongo_map = {"_id": "player_uuid",
                 "lastUpdatedDate": "updated_date",
                 "brandId": "brand_id",
                 "firstName": "first_name",
                 "lastName": "last_name",
                 "languageCode": "language_code",
                 "birthDate": "birth_date",
                 "gender": "gender",
                 "verifications": "verifications",
                 "contacts.email": "email",
                 "passport.countrySpecificIdentifier": "country_specific_identifier",
                 "passport.countrySpecificIdentifierType": "country_specific_identifier_type",
                 "affiliate.uuid": "affiliate_uuid",
                 "affiliate.source": "affiliate_source",
                 "affiliate.referral": "affiliate_referral",
                 "address.address": "address",
                 "address.countryCode": "country",
                 "address.city": "city",
                 "address.postCode": "post_code",
                 "registrationDetails.registrationDate": "registration_date",
                 "kyc.status": "kyc_status",
                 "kyc.changedAt": "kyc_changed_at",
                 "passport.number": "passport_number",
                 "passport.expirationDate": "passport_expiration_date",
                 "status.type": "profile_status",
                 "status.reason": "profile_status_reason",
                 "status.comment": "last_note",
                 "configuration.internalTransfer": "internal_transfer",
                 "configuration.crs": "crs",
                 "configuration.fatca": "fatca",
                 "configuration.webCookiesEnabled": "spam_web_cookies",
                 "configuration.gdpr.sms": "gdpr_sms",
                 "configuration.gdpr.email": "gdpr_email",
                 "configuration.gdpr.phone": "gdpr_phone",
                 "configuration.gdpr.socialMedia": "gdpr_social_media",
                 "configuration.subscription.educational": "spam_educational",
                 "configuration.subscription.information": "spam_information",
                 "configuration.subscription.marketNews": "spam_market_news",
                 "configuration.subscription.promosAndOffers": "spam_promos_and_offers",
                 "configuration.subscription.statisticsAndSummary": "spam_statistics_and_summary",
                 "referrer.uuid": "referrer_profile_uuid",
                 "referrer.referralHash": "referral_hash",
                 "referrer.fullName": "referrer_full_name"
                 }

    mongo_filter_field = "lastUpdatedDate"

    mongo_filter_field_reconciliation = "lastUpdatedDate"

    matview_refresh_date_filter = "registration_date"

    def __repr__(self):
        return '<Profile((player_uuid={}, registration_date="{}", first_name="{}", last_name="{}")'. \
            format(self.player_uuid, self.registration_date, self.first_name, self.last_name)


class ProfileExt(base):
    __tablename__ = 'elastic_profile_ext'
    __table_args__ = {'schema': 'public'}
    player_uuid = Column(String(255), primary_key=True)
    acquisition_acquisition_status = Column(String)
    acquisition_retention_status = Column(String)
    acquisition_sales_status = Column(String)
    address_country_code = Column(String)
    affiliate = Column(Integer)
    affiliate_affiliate_referral = Column(String)
    affiliate_affiliate_source = Column(String)
    affiliate_affiliate_type = Column(String)
    affiliate_campaign_id = Column(String)
    affiliate_external_id = Column(String)
    affiliate_sms = Column(String)
    affiliate_uuid = Column(String)
    balance_amount = Column(Numeric(20, 10))
    balance_credit = Column(Numeric(20, 10))
    balance_currency = Column(String)
    brand_id = Column(String)
    contacts_additional_email = Column(String)
    email = Column(String)
    first_name = Column(String)
    first_note = Column(String)
    first_note_changed_at = Column(DATETIME(timezone=False))
    first_note_uuid = Column(String)
    fsa_migration_info = Column(Integer)
    ftd_amount = Column(Numeric(20, 10))
    ftd_currency = Column(String)
    ftd_time = Column(DATETIME(timezone=False))
    kyc_changed_at = Column(DATETIME(timezone=False))
    kyc_status = Column(String)
    language_code = Column(String)
    last_name = Column(String)
    last_note = Column(String)
    last_note_changed_at = Column(DATETIME(timezone=False))
    last_note_uuid = Column(String)
    last_trade = Column(Integer)
    last_trade_created_at = Column(DATETIME(timezone=False))
    last_updated_date = Column(DATETIME(timezone=False))
    ltd_time = Column(DATETIME(timezone=False))
    migration_id = Column(String)
    payment_details = Column(Integer)
    payment_details_deposits_count = Column(Integer)
    # phone = Column(String)
    questionnaire_passed = Column(String)
    registration_date = Column(DATETIME(timezone=False))
    registration_details_registered_by = Column(String)
    retention_rep = Column(String)
    sales_rep = Column(String)
    status_changed_at = Column(DATETIME(timezone=False))
    status_type = Column(String)

    @hybrid_property
    def filter_column(self):
        return self.last_updated_date

    @hybrid_property
    def filter_column_reconciliation(self):
        return self.last_updated_date

    elastic_filter_field = "lastUpdatedDate"

    elastic_map = {"uuid": "player_uuid",
                   "lastUpdatedDate": "last_updated_date",
                   "brandId": "brand_id",
                   "firstName": "first_name",
                   "lastName": "last_name",
                   "languageCode": "language_code",
                   "fsaMigrationInfo": "fsa_migration_info",
                   "migrationId": "migration_id",
                   "questionnairePassed": "questionnaire_passed",
                   "acquisition.acquisitionStatus": "acquisition_acquisition_status",
                   "acquisition.retentionStatus": "acquisition_retention_status",
                   "acquisition.salesStatus": "acquisition_sales_status",
                   "acquisition.retentionRepresentative": "retention_rep",
                   "acquisition.salesRepresentative": "sales_rep",
                   "address.countryCode": "address_country_code",
                   "affiliate.uuid": "affiliate_uuid",
                   "affiliate.sms": "affiliate_sms",
                   "affiliate.externalId": "affiliate_external_id",
                   "affiliate.campaignId": "affiliate_campaign_id",
                   "affiliate.affiliateType": "affiliate_affiliate_type",
                   "affiliate.source": "affiliate_affiliate_source",
                   "affiliate.referral": "affiliate_affiliate_referral",
                   "balance.amount": "balance_amount",
                   "balance.credit": "balance_credit",
                   "balance.currency": "balance_currency",
                   "contacts.additionalEmail": "contacts_additional_email",
                   "contacts.email": "email",
                   "firstNote.content": "first_note",
                   "firstNote.changedAt": "first_note_changed_at",
                   "firstNote.uuid": "first_note_uuid",
                   "paymentDetails.firstDepositAmount": "ftd_amount",
                   "paymentDetails.firstDepositCurrency": "ftd_currency",
                   "paymentDetails.firstDepositTime": "ftd_time",
                   "paymentDetails.lastDepositTime": "ltd_time",
                   "paymentDetails.depositsCount": "payment_details_deposits_count",
                   "kyc.status": "kyc_status",
                   "kyc.changedAt": "kyc_changed_at",
                   "lastNote.content": "last_note",
                   "lastNote.changedAt": "last_note_changed_at",
                   "lastNote.uuid": "last_note_uuid",
                   "lastTrade.createdAt": "last_trade_created_at",
                   "registrationDetails.registrationDate": "registration_date",
                   "registrationDetails.registeredBy": "registration_details_registered_by",
                   "status.changedAt": "status_changed_at",
                   "status.type": "status_type"
                   }

    matview_refresh_date_filter = "registration_date"

    def __repr__(self):
        return '<ProfileExt((player_uuid={}, updated_date="{}", first_name="{}", last_name="{}")'. \
            format(self.player_uuid, self.last_updated_date, self.first_name, self.last_name)


class DailyMonthlyPerformance(base):
    __tablename__ = 'daily_monthly_performance'
    __table_args__ = {'schema': 'public'}
    payment_id = Column(String(50), primary_key=True)
    brand_id = Column(String(20))
    creation_time = Column(DATETIME(timezone=False))
    execution_time = Column(DATETIME(timezone=False))
    deposit_usd = Column(Numeric)
    ftd = Column(Integer)
    withdraw_usd = Column(Numeric)

    @hybrid_property
    def filter_column(self):
        return self.execution_time

    def __repr__(self):
        return '<DailyMonthlyPerformance((payment_id={}, creation_time="{}", execution_time="{}")'. \
            format(self.payment_id, self.creation_time, self.execution_time)


class View165MonthlyGrossDeposit(base):
    __tablename__ = 'monthly_gross_deposit_crossenv_transferring'
    __table_args__ = {'schema': 'custom_reports'}
    payment_id = Column(String, primary_key=True)
    creation_time = Column(DATETIME(timezone=False))
    deposit_usd = Column(Float)
    withdraw_usd = Column(Float)
    net_deposit_usd = Column(Float)
    withdraw = Column(Numeric)
    noftd = Column(String)
    ftd = Column(Integer)
    brand_id = Column(String)
    language = Column(String)
    profile_country_name = Column(String)
    brand_country = Column(String)
    group_payment_method = Column(String)
    payment_method = Column(String)
    user_type = Column(String)
    agent_desk_name = Column(String)
    updated_at = Column(DATETIME(timezone=False))
    prod_num = Column(Integer)

    @hybrid_property
    def filter_column(self):
        return self.updated_at

    def __repr__(self):
        return '<View165MonthlyGrossDeposit((payment_id={}, creation_time="{}", updated_at="{}")'. \
            format(self.payment_id, self.creation_time, self.updated_at)


class TradingPaymentWhiteLabel(base):
    __tablename__ = 'trading_payment_white_label_crossenv_transfering'
    __table_args__ = {'schema': 'custom_reports'}
    payment_id = Column(String, primary_key=True)
    net_deposit_usd = Column(Float)
    deposit_usd = Column(Float)
    withdraw_usd = Column(Float)
    creation_time = Column(DATETIME(timezone=False))
    brand_id = Column(String)
    updated_at = Column(DATETIME(timezone=False))

    @hybrid_property
    def filter_column(self):
        return self.updated_at

    def __repr__(self):
        return '<TradingPaymentWhiteLabel((payment_id={}, creation_time="{}", updated_at="{}")'. \
            format(self.payment_id, self.creation_time, self.updated_at)


class TradingAccount(base):
    __tablename__ = 'trading_account'
    __table_args__ = {'schema': 'public'}
    _id = Column(Text, primary_key=True)
    balance = Column(Text)
    margin = Column(Text)
    credit = Column(Text)
    brand_id = Column(Text)
    login = Column(Integer)
    account_type = Column(Text)
    server_id = Column(Integer)
    currency = Column(Text)
    leverage = Column(Integer)
    name = Column(Text)
    mt4_group = Column(Text)
    created_by = Column(Text)
    created_at = Column(DATETIME(timezone=False))
    last_modified_date = Column(DATETIME(timezone=False))
    is_read_only = Column(BOOLEAN)
    archived = Column(BOOLEAN)
    profile_uuid = Column(Text)
    platform_type = Column(Text)
    _class = Column(Text)
    read_only_update_time = Column(DATETIME(timezone=False))
    prod_num = Column(Integer)
    normalized_balance = Column(Numeric(12, 2))
    normalized_credit = Column(Numeric(12, 2))

    @hybrid_property
    def filter_column(self):
        return self.last_modified_date

    @hybrid_property
    def filter_column_reconciliation(self):
        return self.created_at

    mongo_collection = "trading_accounts"

    mongo_map = {"_id": "_id",
                 "balance": "balance",
                 "margin": "margin",
                 "credit": "credit",
                 "profileUUID": "profile_uuid",
                 "platformType": "platform_type",
                 "brandId": "brand_id",
                 "login": "login",
                 "accountType": "account_type",
                 "serverId": "server_id",
                 "currency": "currency",
                 "leverage": "leverage",
                 "name": "name",
                 "group": "mt4_group",
                 "createdBy": "created_by",
                 "createdAt": "created_at",
                 "lastModifiedDate": "last_modified_date",
                 "isReadOnly": "is_read_only",
                 "archived": "archived",
                 "_class": "_class",
                 "readOnlyUpdateTime": "read_only_update_time",
                 "normalizedBalance": "normalized_balance",
                 "normalizedCredit": "normalized_credit"}

    mongo_filter_field = "lastModifiedDate"

    mongo_filter_field_reconciliation = "createdAt"

    matview_refresh_date_filter = None

    def __repr__(self):
        return '<TradingAccount((_id={}, last_modified_date="{}", brand_id="{}", login="{}")'. \
            format(self._id, self.last_modified_date, self.brand_id, self.login)
