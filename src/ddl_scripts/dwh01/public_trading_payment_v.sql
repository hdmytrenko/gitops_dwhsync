--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: trading_payment_v; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.trading_payment_v AS
 SELECT tp.company,
    tp.index,
    tp.id,
    tp.brand_country,
    tp.login,
    tp.profile_id,
    tp.payment_id,
    tp.currency,
    tp.external_reference,
    tp.deposit,
    tp.withdraw,
    tp.net_deposit,
    tp.transfer_in,
    tp.transfer_out,
    tp.credit_in,
    tp.credit_out,
    tp.deposit_usd,
    tp.withdraw_usd,
    tp.net_deposit_usd,
    tp.type,
    tp.rate,
    tp.status,
    tp.kyc_status,
    tp.acquisition_status,
    tp.creation_time_profile,
    tp.creation_time,
    tp.day_payment,
    tp.month_payment,
    tp.year_payment,
    tp.week_payment,
    tp.day_of_week_creation_time,
    tp.day_name_creation_time,
    tp.country,
    tp.language,
    tp.brand_id,
    tp.payment_method,
    tp.group_payment_method,
    tp.is_published,
    tp.payment_transaction_id,
    tp.expiration_date,
    tp.version,
    tp.profile_first_name,
    tp.profile_last_name,
    tp.email,
    tp.client_ip,
    tp.created_by,
    tp.is_mobile,
    tp.user_agent,
    tp.profile_country,
    tp.payment_aggregator,
    tp.agent_id,
    tp.agent_name,
    tp.sales_status,
    tp.sales_rep,
    tp.sales_name,
    tp.sales_user_type,
    tp.sales_desk_name,
    tp.sales_language,
    tp.retention_status,
    tp.retention_rep,
    tp.retention_name,
    tp.reten_user_type,
    tp.reten_desk_name,
    tp.reten_language,
    tp.affiliate_uuid,
    tp.affiliate_name,
    tp.affiliate_referral,
    tp.affiliate_source,
    tp.profile_country_name,
    tp.user_type,
    tp.agent_desk_name,
    tp.agent_language,
    tp.ftd_status,
    tp.ftd_time,
    tp.day_to_ftd,
    tp.ftd,
    tp.ftd_deposit,
    tp.ftd_deposit_usd,
    tp.noftd,
    tp.redeposit,
    tp.redeposit_usd,
    tp.last_deposit_time,
    tp.last_note,
    tp.last_note_date,
    tp.modification_time,
    tp.execution_time,
    tp.amount,
    tp.amount_usd,
    tp.questionnaire_status,
    tp.last_login,
    tp.last_trade_date,
    tp.deposit_count,
    tp.mt_balance
   FROM public.trading_payment_all_status_v tp
  WHERE ((tp.status)::text = 'PAYMENT_COMPLETED'::text);


ALTER TABLE public.trading_payment_v OWNER TO postgres;

--
-- Name: TABLE trading_payment_v; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public.trading_payment_v TO prod01_metabase;
GRANT SELECT ON TABLE public.trading_payment_v TO ybashkatov;


--
-- PostgreSQL database dump complete
--

