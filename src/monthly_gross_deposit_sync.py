import sys
import uuid

from dwh_sync.dwh.mapping_classes import View165MonthlyGrossDeposit, TradingPaymentWhiteLabel
from utils.common import write_exception, obtain_lock, create_config_dictionary, create_session_new, get_sync_length, \
    write_log
from utils.constants import DbType, SynchronizationState, LOCK_QUERY, EngineType
from utils.transformers import get_filter_or_backfill
from utils.writers import write_rows

tables = [
    {"table": View165MonthlyGrossDeposit, "dbname": 'prod02_dwh'},
    {"table": TradingPaymentWhiteLabel, "dbname": 'prod02_dwh'}
]
config_path = "settings_monthly_gross.ini"
log_session = create_session_new(create_config_dictionary(config_path,
                                                          DbType.DWH.value,
                                                          engine_type=EngineType.postgres.value,
                                                          ))
dwh_session = create_session_new(create_config_dictionary(config_path,
                                                          DbType.DWH.value,
                                                          schema_translate_map=True,
                                                          engine_type=EngineType.postgres.value))
lock_session = create_session_new(create_config_dictionary(config_path,
                                                           DbType.DWH.value,
                                                           engine_type=EngineType.postgres.value))
log_state = SynchronizationState(log_session,
                                 uuid.uuid1(),
                                 dwh_session=dwh_session,
                                 lock_session=lock_session,
                                 source_engine_type=EngineType.postgres.value,
                                 sync_name=__file__)


def main():
    for table in tables:
        try:
            log_state.table_name = table['table']
            log_state.source_session = create_session_new(
                create_config_dictionary(config_path,
                                         DbType.SOURCE.value,
                                         dbname=table["dbname"],
                                         engine_type=EngineType.postgres.value))
            if not obtain_lock(log_state.lock_session, LOCK_QUERY, log_state):
                continue
            f = get_filter_or_backfill(log_state)
            log_state.message = f"size of sync is no more than " \
                                f"{len(get_sync_length(log_state, f)) * log_state.batch_size} rows for " \
                                f"{log_state.table_name.__tablename__}"
            write_log(log_state)
            write_rows(log_state, get_sync_length(log_state, f), f)
            log_state.message = f"table {table['table'].__tablename__} synchronized"
            write_log(log_state)
            log_state.lock_session.rollback()
        except Exception as ex:
            print(f'\n{"-" * 40}\nException: {str(ex)}\n{"-" * 40}\n')
            log_state.message = f"DWH PROD01 OR PROD02 Exception: {str(ex)}"
            write_exception(log_state)
            continue


if __name__ == "__main__":
    sys.exit(main())
