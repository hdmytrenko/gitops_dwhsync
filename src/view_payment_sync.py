from pyspark.sql.functions import *

from agent_v_sync import AgentVSync
from sync_entities.spark_sync_entities import PaymentSync


class ViewPaymentSync:
    def __init__(self, app_name, log_state, config_dict, filters_dict: dict = None, date_filter=None):
        self.payment_df = PaymentSync(app_name, log_state, config_dict, filters_dict, date_filter)
        self.agent_v_df = AgentVSync(app_name, log_state, config_dict, date_filter=date_filter)

    @staticmethod
    def ftd_df(payment_df):
        pre_df = (
            payment_df.filter((~col("payments_deleted")) & (col("payments_status") == 'PAYMENT_COMPLETED') & (
                    col("payments_type") == 'DEPOSIT') & ((col('payments_payment_method').isNull()) |
                                                          ~(upper(col('payments_payment_method'))
                                                            .isin('FAKEPAL', 'BONUS', 'INTERNAL_TRANSFER')))))
        df = pre_df.groupBy("payments_profile_id", "payments_prod_num", "payments_profile_id_compound") \
            .agg(min('payments_creation_time').alias('v_ftd_ftd_time'),
                 max('payments_creation_time').alias('v_ftd_ltd_time')
                 )
        return df

    def join_df(self, payment_df):
        ftd_df = self.ftd_df(payment_df)
        df = payment_df.alias('df_1') \
            .join(ftd_df.alias('df_2'), payment_df['payments_profile_id_compound']
                  == ftd_df['payments_profile_id_compound'], 'left') \
            .select("df_1.*", "df_2.v_ftd_ftd_time", "df_2.v_ftd_ltd_time")
        return df

    def filter_df(self):
        payment_df = self.payment_df.filter_df()
        agent_v_df = self.agent_v_df.filter_df()
        join_df = self.join_df(payment_df)
        df = join_df.alias('df_1').join(agent_v_df.alias('df_2'),
                                        join_df["payments_agent_id_compound"]
                                        == agent_v_df['agent_user_hierarchy_uuid_compound'], 'left') \
            .select('df_1.*', 'df_2.*')
        df = df.select(col('payments_login').alias('login'),
                       col('payments_payment_id').alias('payment_id'),
                       when(col('v_ftd_ftd_time') == col('payments_creation_time'), lit('Yes')).otherwise(lit('No'))
                       .alias('ftd_status'),
                       when(col('v_ftd_ftd_time') == col('payments_creation_time'), lit('1')).otherwise(lit('0'))
                       .alias('ftd'),
                       when(col('v_ftd_ftd_time') == col('payments_creation_time'), col('payments_amount'))
                       .otherwise(lit(None)).alias('ftd_deposit'),
                       when(col('v_ftd_ftd_time') == col('payments_creation_time'), col('payments_normalized_amount'))
                       .otherwise(lit(None)).
                       alias('ftd_deposit_usd'),
                       when((~(col('v_ftd_ftd_time') == col('payments_creation_time'))
                             & (col('payments_type') == 'DEPOSIT')), col('payments_amount')).
                       otherwise(lit(None)).alias('redeposit'),
                       when((~(col('v_ftd_ftd_time') == col('payments_creation_time'))
                             & (col('payments_type') == 'DEPOSIT')),
                            col('payments_normalized_amount')).
                       otherwise(lit(None)).alias('redeposit_usd'),
                       col('v_ftd_ftd_time').alias('ftd_time'),
                       col('v_ftd_ltd_time').alias('last_deposit_time'),
                       col('payments_currency').alias('currency'),
                       when(col('payments_type') == 'DEPOSIT', col('payments_amount'))
                       .otherwise(lit(0)).cast("double").alias('deposit'),
                       when(col('payments_type') == 'WITHDRAW', col('payments_amount'))
                       .otherwise(lit(0)).cast("double").alias('withdraw'),
                       (when(col('payments_type') == 'DEPOSIT', col('payments_amount')).otherwise(lit(0)).cast(
                           "double") -
                        when(col('payments_type') == 'WITHDRAW', col('payments_amount')).otherwise(lit(0)).cast(
                            "double")).
                       alias('net_deposit'),
                       when(col('payments_type') == 'TRANSFER_IN', col('payments_amount')).otherwise(lit(0)).cast(
                           "double").alias(
                           'transfer_in'),
                       when(col('payments_type') == 'TRANSFER_OUT', col('payments_amount')).otherwise(lit(0)).cast(
                           "double").alias(
                           'transfer_out'),
                       when(col('payments_type') == 'CREDIT_IN', col('payments_amount')).otherwise(lit(0)).cast(
                           "double").alias(
                           'credit_in'),
                       when(col('payments_type') == 'CREDIT_OUT', col('payments_amount')).otherwise(lit(0)).cast(
                           "double").alias(
                           'credit_out'),
                       when(col('payments_type') == 'DEPOSIT', col('payments_normalized_amount')).otherwise(
                           lit(0)).cast(
                           "double").alias(
                           'deposit_usd'),
                       when(col('payments_type') == 'WITHDRAW', col('payments_normalized_amount')).otherwise(
                           lit(0)).cast(
                           "double").alias(
                           'withdraw_usd'),
                       (when(col('payments_type') == 'DEPOSIT', col('payments_normalized_amount')).otherwise(
                           lit(0)).cast(
                           "double") -
                        when(col('payments_type') == 'WITHDRAW', col('payments_normalized_amount')).otherwise(
                            lit(0)).cast(
                            "double")).alias('net_deposit_usd'),
                       when((col('payments_normalized_amount') > 0)
                            & (col('payments_normalized_amount').isNotNull()),
                            round(col('payments_amount') / col('payments_normalized_amount'), 2)).otherwise(
                           lit(1)).cast("double").alias('rate'),
                       col('payments_creation_time').alias('creation_time'),
                       col('payments_country').alias('country'),
                       col('payments_payment_method').alias('payment_method'),
                       col('payments_agent_id').alias('agent_id'),
                       coalesce('name', lit('Null')).alias('agent_name'),
                       coalesce('agent_desk_name', lit('Null')).alias('agent_desk_name'),
                       col('agent_language'),
                       coalesce('user_type', lit('Null')).alias('user_type'),
                       when(col('payments_payment_method').isin(['CASHIER', 'PAYRETAILERS', 'PAYTRIO']), 'CREDIT_CARD') \
                       .otherwise(col('payments_payment_method')).alias('group_payment_method'),
                       col('payments_user_agent').alias('user_agent'),
                       lit(1).alias('index'),
                       col('payments_amount').alias('amount'),
                       col('payments_normalized_amount').alias('amount_usd'),
                       col('payments_type').alias('type'),
                       col('payments_profile_id').alias('profile_id'),
                       col('payments_status').alias('status'),
                       when((~(col('v_ftd_ftd_time') == col('payments_creation_time'))
                             & (col('payments_type') == 'DEPOSIT')), col('payments_profile_id')).
                       otherwise(lit(None)).alias('noftd'),
                       col('payments_is_published').alias('is_published'),
                       col('payments_client_ip').alias('client_ip'),
                       col('payments_is_mobile').alias('is_mobile'),
                       col('payments_payment_aggregator').alias('payment_aggregator'),
                       col('payments_brand_id').alias('brand_id'),
                       col('payments_prod_num').alias('prod_num'),
                       col('payments_id').alias('id'),
                       col('payments_external_reference').alias('external_reference'),
                       col('payments_payment_transaction_id').alias('payment_transaction_id'),
                       col('payments_expiration_date').alias('expiration_date'),
                       col('payments_version').alias('version'),
                       col('payments_created_by').alias('created_by'),
                       col('payments_status_changed_at').alias('status_changed_at'),
                       col('payments_normalized_amount').alias('normalized_amount'),
                       col('payments_decline_reason').alias('decline_reason'),
                       col('payments_deleted').alias('deleted'),
                       col('name'),
                       col('payments_bank_name').alias('bank_name'),
                       col('payments_masked_pan').alias('masked_pan'),
                       col('payments_profile_id_compound').alias('profile_id_compound'),
                       col('payments_payment_id_compound').alias('payment_id_compound'),
                       col('payments_agent_id_compound').alias('agent_id_compound'),
                       col('payments_moto').alias('moto'),
                       col('payments_updated_at').alias('payment_updated_at')
                       ) \
            .filter((~col('payment_method').isin(['BONUS', 'INTERNAL_TRANSFER'])) | (col('payment_method').isNull()))
        return df

    def get_incomplete_df(self):
        df = self.payment_df.get_incomplete_df()
        return df

    def get_rejected_df(self, base_dataframe_list: list):
        self.payment_df.get_rejected_df(base_dataframe_list)
