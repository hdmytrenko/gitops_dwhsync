--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

--
-- Name: releases; Type: TABLE; Schema: public; Owner: data_sync
--

CREATE TABLE public.releases (
    id integer NOT NULL,
    prod_id integer NOT NULL,
    release_date date,
    version character varying(32)
);


ALTER TABLE public.releases OWNER TO data_sync;

--
-- Name: releases_id_seq; Type: SEQUENCE; Schema: public; Owner: data_sync
--

CREATE SEQUENCE public.releases_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.releases_id_seq OWNER TO data_sync;

--
-- Name: releases_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: data_sync
--

ALTER SEQUENCE public.releases_id_seq OWNED BY public.releases.id;


--
-- Name: releases id; Type: DEFAULT; Schema: public; Owner: data_sync
--

ALTER TABLE ONLY public.releases ALTER COLUMN id SET DEFAULT nextval('public.releases_id_seq'::regclass);


--
-- Name: releases releases_pkey; Type: CONSTRAINT; Schema: public; Owner: data_sync
--

ALTER TABLE ONLY public.releases
    ADD CONSTRAINT releases_pkey PRIMARY KEY (id);


--
-- Name: releases releases_prod_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: data_sync
--

ALTER TABLE ONLY public.releases
    ADD CONSTRAINT releases_prod_id_fkey FOREIGN KEY (prod_id) REFERENCES public.products(id);


--
-- Name: TABLE releases; Type: ACL; Schema: public; Owner: data_sync
--

REVOKE ALL ON TABLE public.releases FROM data_sync;


--
-- PostgreSQL database dump complete
--

