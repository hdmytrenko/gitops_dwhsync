import uuid

from pyspark.sql.functions import col, coalesce, lit, when, upper, trim, concat, datediff, dayofweek, date_format, \
    regexp_replace
from pyspark.sql.types import TimestampType
from sqlalchemy import Table
from sqlalchemy.orm import close_all_sessions

from agent_v_sync import AgentVSync
from mongo_spark_sync import ElasticProfileView
from sync_entities.spark_sync_entities import CountrySync, BrandCountrySync, AffiliateSync, SparkRuntimeCheckpoint
from utils.common import create_spark_session, create_config_dictionary, create_session_new, obtain_lock, write_log, \
    make_dataframe_folding, write_exception, log_layer_rows_count
from utils.constants import SynchronizationState, LOCK_QUERY, DbType, EngineType
from utils.writers import save_dataframe_flat
from view_payment_sync import ViewPaymentSync

app_name = 'es_profile_trading_payment'
config_path = "settings_spark_es_profile_prod02.ini"
config_dict_source = create_config_dictionary(config_path,
                                              DbType.SOURCE.value,
                                              engine_type=EngineType.postgres.value)
config_dict_dwh = create_config_dictionary(config_path,
                                           DbType.DWH.value,
                                           engine_type=EngineType.postgres.value)
log_session = create_session_new(config_dict_dwh)
lock_session = create_session_new(config_dict_dwh)
dwh_session = create_session_new(create_config_dictionary(config_path,
                                                          DbType.DWH.value,
                                                          schema_translate_map=True,
                                                          engine_type=EngineType.postgres.value))
log_state = SynchronizationState(log_session,
                                 uuid.uuid1(),
                                 source_engine_type=EngineType.postgres.value,
                                 prod_number=config_dict_dwh['prod_num'],
                                 dwh_session=dwh_session,
                                 sync_name=__file__)


class EsProfileTradingPayment:
    def __init__(self, filters_dict: dict = None, date_filter=None):
        self.spark_elastic_search = create_spark_session(app_name)
        self.log_state = log_state
        self.elastic_profile_df = ElasticProfileView(config_path, filters_dict=filters_dict, date_filter=date_filter)
        self.view_payment_df = ViewPaymentSync(app_name, log_state, config_dict_source, filters_dict=filters_dict,
                                               date_filter=date_filter)
        self.country_df = CountrySync(app_name, log_state, config_dict_dwh)
        self.brand_country_df = BrandCountrySync(app_name, log_state, config_dict_dwh)
        self.agent_v_df = AgentVSync(app_name, log_state, config_dict_source, date_filter=date_filter)
        self.affiliate_df = AffiliateSync(app_name, log_state, config_dict_source, filters_dict=filters_dict,
                                          date_filter=date_filter)

    def pre_join_df(self):
        elastic_profile_df = self.elastic_profile_df.filter_df().withColumnRenamed('country', 'profile_country') \
            .drop('login', 'ftd_time', 'currency')
        view_payment_df = self.view_payment_df.filter_df().filter(
            col('payments_status') == 'PAYMENT_COMPLETED').withColumnRenamed('brand_id', 'payment_brand_id').drop(
            'prod_num')
        df = elastic_profile_df.alias("df_1") \
            .join(view_payment_df.alias("df_2"),
                  elastic_profile_df['player_uuid_compound'] == view_payment_df['profile_id_compound'], 'left') \
            .select('df_1.*', 'df_2.*')
        return df

    def country_pre_join_df(self):
        country_df = self.country_df.filter_df()
        pre_join_df = self.pre_join_df()
        return pre_join_df.alias('df_1').join(country_df.alias('df_2'),
                                              pre_join_df['profile_country'] == country_df['country_alpha_2'],
                                              'left').select('df_1.*', 'df_2.*')

    def brand_country_pre_join_df(self):
        brand_country_df = self.brand_country_df.filter_df()
        country_pre_join_df = self.country_pre_join_df()
        return country_pre_join_df.alias('df_1').join(brand_country_df.alias('df_2'),
                                                      country_pre_join_df['brand_id'] == brand_country_df[
                                                          'brand_country_brand_id'], 'left').select('df_1.*',
                                                                                                    'df_2.*')

    @staticmethod
    def sales_pre_join_df(agent_v_df, brand_country_pre_join_df):
        sales_df = agent_v_df.select(col('name').alias('sales_name'), col('user_type').alias('sales_user_type'),
                                     coalesce(col('agent_desk_name'), lit(None)).alias('sales_desk_name'),
                                     coalesce(col('agent_language'), lit(None)).alias('sales_language'),
                                     'agent_user_hierarchy_uuid_compound')
        return brand_country_pre_join_df.alias('df_1') \
            .join(sales_df.alias('df_2'), sales_df['agent_user_hierarchy_uuid_compound'] ==
                  brand_country_pre_join_df['sales_rep_compound'], 'left').select('df_1.*', 'df_2.*') \
            .drop('agent_user_hierarchy_uuid_compound')

    def retention_pre_join_df(self):
        agent_v_df = self.agent_v_df.filter_df()
        brand_country_pre_join_df = self.brand_country_pre_join_df()
        sales_pre_join_df = self.sales_pre_join_df(agent_v_df, brand_country_pre_join_df)
        retention_df = agent_v_df.select(col('name').alias('retention_name'),
                                         col('user_type').alias('reten_user_type'),
                                         coalesce(col('agent_desk_name'), lit(None)).alias('reten_desk_name'),
                                         coalesce(col('agent_language'), lit(None)).alias('reten_language'),
                                         'agent_user_hierarchy_uuid_compound')
        return sales_pre_join_df.alias('df_1') \
            .join(retention_df.alias('df_2'), retention_df['agent_user_hierarchy_uuid_compound'] ==
                  brand_country_pre_join_df['retention_rep_compound'], 'left').select('df_1.*', 'df_2.*')

    def join_df(self):
        retention_pre_join_df = self.retention_pre_join_df()
        affiliate_df = self.affiliate_df.filter_df().drop('affiliate_uuid')
        return retention_pre_join_df.alias('df_1') \
            .join(affiliate_df.alias('df_2'),
                  retention_pre_join_df['affiliate_uuid_compound']
                  == affiliate_df['affiliates_uuid_compound'], 'left') \
            .select('df_1.*', 'df_2.*')

    def filter_df(self):
        join_df = self.join_df()
        return join_df.select(col('player_uuid').alias('profile_id'),
                              col('email'),
                              col('login'),
                              col('acquisition_status'),
                              when(col('acquisition_status') == 'SALES', col('sales_status'))
                              .otherwise(col('retention_status')).alias('player_status'),
                              col('sales_status'),
                              col('sales_rep'),
                              col('sales_name'),
                              col('sales_user_type'),
                              col('sales_desk_name'),
                              col('sales_language'),
                              col('retention_status'),
                              col('retention_rep'),
                              col('retention_name'),
                              col('reten_user_type'),
                              col('reten_desk_name'),
                              col('reten_language'),
                              col('registration_date').alias('creation_time_profile'),
                              when(dayofweek(col('registration_date')) == 1, lit(7)).otherwise(
                                  dayofweek(col('registration_date')) - 1).alias(
                                  'day_of_week_creation_time_profile'),
                              date_format(col('registration_date'), "E").alias('day_name_creation_time_profile'),
                              col('kyc_status'),
                              col('mt_group'),
                              col('payment_id'),
                              col('type'),
                              col('status_changed_at').alias('modification_time'),
                              col('ftd_status'),
                              col('ftd'),
                              col('ftd_deposit'),
                              col('ftd_deposit_usd'),
                              col('noftd'),
                              col('redeposit'),
                              col('redeposit_usd'),
                              col('ftd_time'),
                              datediff(col('ftd_time'), col('registration_date')).alias('day_to_ftd'),
                              col('last_deposit_time'),
                              col('currency'),
                              col('deposit'),
                              col('withdraw'),
                              col('transfer_in'),
                              col('transfer_out'),
                              col('credit_in'),
                              col('credit_out'),
                              col('deposit_usd'),
                              col('withdraw_usd'),
                              col('mt_balance'),
                              col('rate'),
                              col('creation_time').alias('paymant_creation_time'),
                              upper(col('profile_country')).alias('profile_country'),
                              upper(col('country')).alias('country'),
                              col('country_name').alias('profile_country_name'),
                              upper(col('language_code')).alias('language'),
                              col('brand_id'),
                              col('payment_method'),
                              col('is_published'),
                              regexp_replace(concat(trim(col('first_name')), lit(' '), trim(col('last_name'))), '  ',
                                             ' ').alias('profile_name'),
                              col('client_ip'),
                              col('is_mobile'),
                              col('payment_aggregator'),
                              col('agent_id'),
                              col('agent_name'),
                              col('agent_desk_name'),
                              col('agent_language'),
                              col('user_type'),
                              col('group_payment_method'),
                              col('user_agent'),
                              coalesce(col('affiliate_uuid'), lit('Null')).alias('affiliate_uuid'),
                              coalesce(
                                  concat(col('affiliates_first_name'), lit(' '), col('affiliates_last_name')),
                                  lit(' '))
                              .alias('affiliate_name'),
                              coalesce(col('affiliate_referral'), lit('Null')).alias(
                                  'affiliate_referral'),
                              coalesce(col('affiliate_source'), lit('Null')).alias(
                                  'affiliate_source'),
                              when(col('type') == 'WITHDRAW', col('status_changed_at')).otherwise(
                                  col('creation_time')).alias(
                                  'execution_time').cast(TimestampType()),
                              col('index'),
                              lit('').alias('modified_by'),
                              col('net_deposit'),
                              col('net_deposit_usd'),
                              col('last_note'),
                              col('last_note_date'),
                              col('questionnaire_status'),
                              col('last_login'),
                              col('last_trade_date'),
                              col('deposit_count'),
                              lit('Null').alias('affiliate_type'),
                              col('fsa_migration_status'),
                              col('mt4_balance'),
                              col('mt4_balance_currency'),
                              col('first_note'),
                              col('first_note_date'),
                              col('ftd_amount'),
                              col('ftd_currency'),
                              col('ltd_time'),
                              col('migration_id'),
                              col('kyc_changed_at'),
                              col('external_reference'),
                              when(col('payment_aggregator') == 'MANUAL', col('created_by')).otherwise(lit('')).alias(
                                  'operator_manual'),
                              coalesce(col('payment_id'), lit('Null')).alias('payment_id_not_null')
                              )

    def get_first_layer_frame(self):
        payment_df = self.view_payment_df.get_incomplete_df()
        elastic_profile_df = self.elastic_profile_df.get_incomplete_df()
        affiliate_df = self.affiliate_df.get_incomplete_df()
        agent_v_df = self.agent_v_df.get_incomplete_df()

        view_payment_df = payment_df.join(
            agent_v_df,
            payment_df['operatorUUID_payment'] == agent_v_df['operatorUUID_agentV'],
            'full_outer'
        )
        df = view_payment_df.join(
            elastic_profile_df,
            elastic_profile_df['playerUUID_elasticProfile'] == view_payment_df['playerUUID_payment'],
            'full_outer'
        ).join(
            affiliate_df,
            affiliate_df['affiliateUUID_affiliate'] == elastic_profile_df['affiliateUUID_elasticProfile'],
            'full_outer'
        )
        return df

    def get_second_layer_frame(self):
        payment_df = self.view_payment_df.get_incomplete_df()
        elastic_profile_df = self.elastic_profile_df.get_incomplete_df()

        df = payment_df.join(
            elastic_profile_df,
            elastic_profile_df['playerUUID_elasticProfile'] == payment_df['playerUUID_payment'],
            'full_outer'
        )
        return df

    def get_rejected_df(self, base_dataframe_list: list):
        self.affiliate_df.get_rejected_frame(base_dataframe_list)
        self.view_payment_df.get_rejected_df(base_dataframe_list)
        self.elastic_profile_df.get_rejected_df(base_dataframe_list)
        base_dataframe = base_dataframe_list[0].withColumn(
            'profile_idFolded',
            lit(concat(col('profile_idFolded'), lit('-'), lit(config_dict_dwh['prod_num'])))
        ).select(
            col('profile_idFolded'),
            col('reg_dateFolded'),
            col('payment_id'),
            coalesce(
                col('affiliate_condition'),
                col('payment_condition'),
                col('profile_condition'),
                col('customer_condition'),
                lit(None)
            ).alias('condition')
        )
        return base_dataframe.filter(
            col('profile_idFolded').isNotNull() & col('condition').isNotNull()
        ).select(
            col('profile_idFolded'),
            col('reg_dateFolded'),
            coalesce(col('payment_id'), lit('Null')).alias('payment_id_not_null')
        ).distinct().withColumnRenamed('profile_idFolded', 'profile_id') \
            .withColumnRenamed('reg_dateFolded', 'creation_time_profile')


def main():
    base_table_name = app_name + '_prod0' + config_dict_dwh['prod_num']
    log_state.table_name = Table()
    log_state.table_name.__tablename__ = base_table_name
    if not obtain_lock(lock_session, LOCK_QUERY, log_state):
        close_all_sessions()
        exit(1)

    date_from, date_to = SparkRuntimeCheckpoint(app_name, log_state, config_dict_dwh).get_sync_date_range()
    write_log(log_state, message=f'Starting delta assembling with date range: ({date_from} - {date_to})')

    first_layer_frame = EsProfileTradingPayment(
        date_filter=(date_from, date_to)
    ).get_first_layer_frame()
    id_fields_dict_first = make_dataframe_folding(first_layer_frame)
    first_layer_frame.unpersist()

    if not id_fields_dict_first:
        log_message = 'There is no updated data in the source db. Sync job has been terminated'
        write_log(log_state, log_message, table_name=base_table_name)
        exit(1)
    log_layer_rows_count(1, log_state, id_fields_dict_first, base_table_name)

    second_layer_frame = EsProfileTradingPayment(
        filters_dict=id_fields_dict_first
    ).get_second_layer_frame()
    del id_fields_dict_first
    id_fields_dict_second = make_dataframe_folding(second_layer_frame)
    second_layer_frame.unpersist()
    log_layer_rows_count(2, log_state, id_fields_dict_second, base_table_name)

    third_layer_frame = EsProfileTradingPayment(
        filters_dict=id_fields_dict_second
    ).get_second_layer_frame()
    id_fields_dict_third = make_dataframe_folding(third_layer_frame)
    log_layer_rows_count(3, log_state, id_fields_dict_second, base_table_name)

    trading_payment_all_status_v = EsProfileTradingPayment(
        filters_dict=id_fields_dict_third
    ).filter_df()
    timestamp = save_dataframe_flat(config_dict_dwh, log_state, app_name, trading_payment_all_status_v)
    trading_payment_all_status_v.unpersist()

    base_dataframe_list = [third_layer_frame]
    rejected_df = EsProfileTradingPayment(
        filters_dict=id_fields_dict_third
    ).get_rejected_df(base_dataframe_list)
    del id_fields_dict_third
    third_layer_frame.unpersist()

    save_dataframe_flat(
        config_dict_dwh, log_state, app_name, rejected_df,
        timestamp + '_delete'
    )
    rejected_df.unpersist()

    SparkRuntimeCheckpoint(app_name, log_state, config_dict_dwh).save_last_runtime(date_to)
    write_log(log_state, 'Delta assembly completed')

    lock_session.rollback()


if __name__ == '__main__':
    try:
        main()
    except Exception as ex:
        print(f'\n{"-" * 40}\nException: {str(ex)}\n{"-" * 40}\n')
        log_state.message = f"DWH PROD0{log_state.prod_number} Exception: {str(ex)}"
        write_exception(log_state)
