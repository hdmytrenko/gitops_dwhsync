from pyspark.sql.types import StructType, StructField, StringType, TimestampType, BooleanType, ArrayType, LongType

profile_mongo_schema = StructType(
    [StructField("_class", StringType(), True),
     StructField("_id", StringType(), True),
     StructField("address", StructType([
         StructField("address", StringType(), True),
         StructField("city", StringType(), True),
         StructField("countryCode", StringType(), True),
         StructField("postCode", StringType(), True),
     ]),
                 True),
     StructField("affiliate", StructType([
         StructField("externalId", StringType(), True),
         StructField("referral", StringType(), True),
         StructField("source", StringType(), True),
         StructField("uuid", StringType(), True),
     ]),
                 True),
     StructField("birthDate", TimestampType(), True),
     StructField("brandId", StringType(), True),
     StructField("configuration", StructType([
         StructField("internalTransfer", BooleanType(), True),
         StructField("crs", BooleanType(), True),
         StructField("fatca", BooleanType(), True),
         StructField("gdpr", StructType([
             StructField("sms", BooleanType(), True),
             StructField("email", StringType(), True),
             StructField("phone", BooleanType(), True),
             StructField("socialMedia", BooleanType(), True),
         ])),
         StructField("subscription", StructType([
             StructField("marketNews", BooleanType(), True),
             StructField("promosAndOffers", BooleanType(), True),
             StructField("information", BooleanType(), True),
             StructField("statisticsAndSummary", BooleanType(), True),
             StructField("educational", BooleanType(), True)
         ])),
         StructField("webCookiesEnabled", BooleanType(), True)])),
     StructField("contacts", StructType([
         StructField("additionalEmail", StringType(), True),
         StructField("additionalPhone", StringType(), True),
         StructField("email", StringType(), True),
         StructField("phone", StringType(), True),
     ])),
     StructField("convertedFromLeadUuid", StringType(), True),
     StructField("firstName", StringType(), True),
     StructField("gender", StringType(), True),
     StructField("identificationNumber", StringType(), True),
     StructField("kyc", StructType([
         # StructField("changedAt", TimestampType(), True),
         StructField("status", StringType(), True),
         StructField("uuid", StringType(), True)
     ])),
     StructField("languageCode", StringType(), True),
     StructField("lastName", StringType(), True),
     StructField("lastUpdatedBy", StringType(), True),
     StructField("lastUpdatedDate", TimestampType(), True),
     StructField("migrationId", StringType(), True),
     StructField("passport", StructType([
         StructField("countrySpecificIdentifier", StringType(), True),
         StructField("countrySpecificIdentifierType", StringType(), True),
         StructField("passportNumber", StringType(), True)
     ])),
     StructField("registrationDetails", StructType([
         StructField("deviceDetails", StructType([
             StructField("deviceType", StringType(), True),
             StructField("operatingSystem", StringType(), True)
         ])),
         StructField("inetDetails", StructType([
             StructField("host", StringType(), True),
             StructField("ipAddress", StringType(), True),
             StructField("referer", StringType(), True)
         ])),
         StructField("locationDetails", StructType([
             StructField("city", StringType(), True),
             StructField("countryCode", StringType(), True),
             StructField("region", StringType(), True)
         ])),
         StructField("registeredBy", StringType(), True),
         StructField("registrationDate", TimestampType(), True),
         StructField("userAgent", StringType(), True),
         StructField("verificationToken", StructType([
             StructField("value", StringType(), True),
             StructField("expirationTime", TimestampType(), True)
         ]))
     ])),
     StructField("status", StructType([
         StructField("changedAt", TimestampType(), True),
         StructField("changedBy", StringType(), True),
         StructField("comment", StringType(), True),
         StructField("reason", StringType(), True),
         StructField("type", StringType(), True),
     ])),
     StructField("verifications", ArrayType(StringType()), True),
     StructField("version", LongType(), True),
     StructField("warnings", ArrayType(StringType()), True),
     ])
