--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: first_reten_redeposit; Type: VIEW; Schema: frx03_terra_finax_fxnob; Owner: postgres
--

CREATE VIEW frx03_terra_finax_fxnob.first_reten_redeposit AS
 SELECT t.brand_id,
    t.profile_id,
    t.creation_time,
    t.profile_country_name,
    t.country,
    t.agent_name,
    t.agent_desk_name,
    t.user_type,
    t.redeposit AS redeposit_amount
   FROM ( SELECT es_profile_trading_payment.ftd_amount,
            row_number() OVER (PARTITION BY es_profile_trading_payment.profile_id ORDER BY es_profile_trading_payment.paymant_creation_time) AS rn,
            es_profile_trading_payment.redeposit,
            es_profile_trading_payment.brand_id,
            es_profile_trading_payment.profile_id,
            es_profile_trading_payment.paymant_creation_time AS creation_time,
            es_profile_trading_payment.profile_country_name,
            es_profile_trading_payment.country,
            es_profile_trading_payment.agent_name,
            es_profile_trading_payment.agent_desk_name,
            es_profile_trading_payment.user_type
           FROM public.es_profile_trading_payment
          WHERE (((es_profile_trading_payment.brand_id)::text = ANY (ARRAY['bid-broker-stocks'::text, ('fxnobels'::character varying)::text, ('finaxis'::character varying)::text, ('terrafinance'::character varying)::text, ('energy-markets'::character varying)::text])) AND ((es_profile_trading_payment.type)::text = 'DEPOSIT'::text) AND ((es_profile_trading_payment.user_type)::text ~~ 'RETENTION%'::text) AND (es_profile_trading_payment.ftd = 0))) t
  WHERE (t.rn = 1);


ALTER TABLE frx03_terra_finax_fxnob.first_reten_redeposit OWNER TO postgres;

--
-- Name: TABLE first_reten_redeposit; Type: ACL; Schema: frx03_terra_finax_fxnob; Owner: postgres
--

GRANT SELECT ON TABLE frx03_terra_finax_fxnob.first_reten_redeposit TO prod01_metabase;
GRANT SELECT ON TABLE frx03_terra_finax_fxnob.first_reten_redeposit TO ybashkatov;


--
-- PostgreSQL database dump complete
--

