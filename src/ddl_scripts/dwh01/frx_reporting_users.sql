--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: users; Type: VIEW; Schema: frx_reporting; Owner: postgres
--

CREATE VIEW frx_reporting.users AS
 SELECT users.server_id,
    users.enable,
    users.enable_change_password,
    users.enable_read_only,
    users.enable_otp,
    users.send_reports,
    users.manager_login,
    users.login,
    users.mq_id,
    users.registration_date,
    users.last_date,
    users.leverage,
    users.agent_account,
    users.last_ip,
    users."timestamp",
    users."group",
    users.password,
    users.password_investor,
    users.password_phone,
    users.name,
    users.country,
    users.city,
    users.state,
    users.zip_code,
    users.address,
    users.lead_source,
    NULL::character varying(32) AS phone,
    users.email,
    users.comment,
    users.id,
    users.status,
    users.otp_secret,
    users.balance,
    users.previous_month_balance,
    users.previous_balance,
    users.credit,
    users.interest_rate,
    users.taxes,
    users.previous_month_equity,
    users.previous_equity,
    users.margin,
    users.proxy_timestamp,
    users.deleted_at,
    users.prod_num
   FROM stg_tables.users
  WHERE ((users.deleted_at IS NULL) AND ((users.email IS NULL) OR (upper((users.email)::text) !~~ 'UAT\_%@NEWAGESOL.COM'::text)));


ALTER TABLE frx_reporting.users OWNER TO postgres;

--
-- Name: TABLE users; Type: ACL; Schema: frx_reporting; Owner: postgres
--

GRANT SELECT ON TABLE frx_reporting.users TO prod01_metabase;
GRANT SELECT ON TABLE frx_reporting.users TO ybashkatov;


--
-- PostgreSQL database dump complete
--

