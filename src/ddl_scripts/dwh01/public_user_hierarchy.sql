--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

--
-- Name: user_hierarchy; Type: TABLE; Schema: public; Owner: ybashkatov
--

CREATE TABLE public.user_hierarchy (
    id bigint NOT NULL,
    created_at timestamp without time zone NOT NULL,
    deleted_at timestamp without time zone,
    user_type character varying(255) NOT NULL,
    uuid character varying(60) NOT NULL,
    brand_ids text[] DEFAULT '{}'::text[] NOT NULL,
    version integer DEFAULT 0 NOT NULL,
    observer_for text[],
    observable_from text[],
    updated_at timestamp without time zone
);


ALTER TABLE public.user_hierarchy OWNER TO ybashkatov;

--
-- Name: user_hierarchy_id_seq; Type: SEQUENCE; Schema: public; Owner: ybashkatov
--

CREATE SEQUENCE public.user_hierarchy_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_hierarchy_id_seq OWNER TO ybashkatov;

--
-- Name: user_hierarchy_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ybashkatov
--

ALTER SEQUENCE public.user_hierarchy_id_seq OWNED BY public.user_hierarchy.id;


--
-- Name: user_hierarchy id; Type: DEFAULT; Schema: public; Owner: ybashkatov
--

ALTER TABLE ONLY public.user_hierarchy ALTER COLUMN id SET DEFAULT nextval('public.user_hierarchy_id_seq'::regclass);


--
-- Name: user_hierarchy user_hierarchy_pkey; Type: CONSTRAINT; Schema: public; Owner: ybashkatov
--

ALTER TABLE ONLY public.user_hierarchy
    ADD CONSTRAINT user_hierarchy_pkey PRIMARY KEY (id);


--
-- Name: user_hierarchy user_hierarchy_uuid_key; Type: CONSTRAINT; Schema: public; Owner: ybashkatov
--

ALTER TABLE ONLY public.user_hierarchy
    ADD CONSTRAINT user_hierarchy_uuid_key UNIQUE (uuid);


--
-- Name: idx_user_hierarchy_user_type; Type: INDEX; Schema: public; Owner: ybashkatov
--

CREATE INDEX idx_user_hierarchy_user_type ON public.user_hierarchy USING btree (user_type);


--
-- PostgreSQL database dump complete
--

