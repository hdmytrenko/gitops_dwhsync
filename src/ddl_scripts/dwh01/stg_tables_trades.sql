--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

--
-- Name: trades; Type: TABLE; Schema: stg_tables; Owner: data_sync
--

CREATE TABLE stg_tables.trades (
    server_id smallint NOT NULL,
    cmd smallint NOT NULL,
    manager_login integer NOT NULL,
    "order" integer NOT NULL,
    login integer NOT NULL,
    digits integer NOT NULL,
    volume integer NOT NULL,
    opening_time integer NOT NULL,
    magic integer NOT NULL,
    activation integer NOT NULL,
    state integer NOT NULL,
    gw_order integer NOT NULL,
    gw_volume integer NOT NULL,
    gw_open_price integer NOT NULL,
    gw_close_price integer NOT NULL,
    closing_time integer NOT NULL,
    expiration_time integer NOT NULL,
    "timestamp" integer NOT NULL,
    symbol character varying(12) NOT NULL,
    comment character varying(32) NOT NULL,
    reason character varying(1) NOT NULL,
    opening_price double precision NOT NULL,
    stop_loss double precision NOT NULL,
    take_profit double precision NOT NULL,
    commission double precision NOT NULL,
    commission_agent double precision NOT NULL,
    storage double precision NOT NULL,
    closing_price double precision NOT NULL,
    profit double precision NOT NULL,
    taxes double precision NOT NULL,
    margin_rate double precision NOT NULL,
    opening_rate double precision NOT NULL,
    closing_rate double precision NOT NULL,
    proxy_timestamp bigint NOT NULL,
    deleted_at bigint,
    prod_num smallint
);


ALTER TABLE stg_tables.trades OWNER TO data_sync;

--
-- Name: trades trades_pk; Type: CONSTRAINT; Schema: stg_tables; Owner: data_sync
--

ALTER TABLE ONLY stg_tables.trades
    ADD CONSTRAINT trades_pk PRIMARY KEY (server_id, "order");


--
-- Name: trades_order_idx; Type: INDEX; Schema: stg_tables; Owner: data_sync
--

CREATE INDEX trades_order_idx ON stg_tables.trades USING btree ("order");


--
-- Name: TABLE trades; Type: ACL; Schema: stg_tables; Owner: data_sync
--

GRANT SELECT ON TABLE stg_tables.trades TO prod01_metabase;
GRANT SELECT ON TABLE stg_tables.trades TO ybashkatov;


--
-- PostgreSQL database dump complete
--

