import sys
import uuid

from pymongo import MongoClient

from dwh_sync.dwh.mapping_writer_classes_ecosales import ReferralsHistoryDWH, TradingAccountDWH, ProfileDWH, ReferrerDWH
from utils.common import create_session_new, create_config_dictionary, obtain_lock, write_exception, write_log
from utils.constants import SynchronizationState, LOCK_QUERY, DbType, ProdNumber, EngineType
from utils.transformers import create_cursor_mongo_with_prod_num
from utils.writers import write_rows_from_mongo

ROWS_LIMIT = 2000
LAST_MINUTES = 10

tables = [
    {'dbname': 'referral-db', 'table_name': ReferralsHistoryDWH},
    {'dbname': 'trading-account-db', 'table_name': TradingAccountDWH},
    {'dbname': 'profile-db', 'table_name': ProfileDWH},
    {'dbname': 'referral-db', 'table_name': ReferrerDWH}
]
lock_record = LOCK_QUERY
config_path = "settings_mongo_prod04.ini"

log_session = create_session_new(create_config_dictionary(config_path,
                                                          DbType.DWH.value,
                                                          engine_type=EngineType.postgres.value))
dwh_session = create_session_new(create_config_dictionary(config_path,
                                                          DbType.DWH.value,
                                                          schema_translate_map=True,
                                                          engine_type=EngineType.postgres.value))
lock_session = create_session_new(create_config_dictionary(config_path,
                                                           DbType.DWH.value,
                                                           engine_type=EngineType.postgres.value))
log_state = SynchronizationState(log_session,
                                 uuid.uuid1(),
                                 dwh_session=dwh_session,
                                 lock_session=lock_session,
                                 prod_number=ProdNumber.prod04.value,
                                 sync_name=__file__)


def main():
    for table in tables:
        try:
            log_state.source_session = MongoClient(
                host=create_config_dictionary(config_path, DbType.SOURCE.value)['mongo_uri'],
                tz_aware=False)[table['dbname']]
            log_state.table_name = table['table_name']
            if not obtain_lock(log_state.lock_session, LOCK_QUERY, log_state):
                continue
            log_state.message = f"size of sync is at least  " \
                                f"{create_cursor_mongo_with_prod_num(log_state, table['table_name']).count()} rows for " \
                                f"{log_state.table_name.__tablename__}"
            write_log(log_state)
            write_rows_from_mongo(create_cursor_mongo_with_prod_num(log_state, table['table_name']), log_state)
            log_state.message = f"table {table['table_name'].__tablename__} synchronized"
            write_log(log_state)
            log_state.lock_session.rollback()
        except Exception as ex:
            print(f'\n{"-" * 40}\nException: {str(ex)}\n{"-" * 40}\n')
            log_state.lock_session.rollback()
            log_state.message = f"DWH PROD04 Exception: {str(ex)}"
            write_exception(log_state)


if __name__ == "__main__":
    sys.exit(main())
