import sys
import uuid
from os import environ as env

from sqlalchemy import Table

from utils.common import create_session_new, write_exception
from utils.constants import SynchronizationState, LOCK_QUERY, EngineType, ProdNumber
from utils.writers import refresh_mat_views, refresh_custom_reports

# POPULATE PARAMS
db_host = env.get('DB_HOST')
db_name = env.get('DB_NAME')
db_usr = env.get('DB_USER')
db_pwd = env.get('DB_PASSWORD')

config_dictionary = {
    'user': db_usr,
    'password': db_pwd,
    'host': db_host,
    'port': 5432,
    'dbname': db_name,
    'engine_type': EngineType.postgres.value,
    'schema': 'stg_tables'
}
lock_session = create_session_new(config_dictionary)
log_session = create_session_new(config_dictionary)
config_dictionary['schema_translate_map'] = True
dwh_session = create_session_new(config_dictionary)

log_state = SynchronizationState(log_session=log_session,
                                 session_uid=uuid.uuid1(),
                                 dwh_session=dwh_session,
                                 lock_session=lock_session,
                                 source_engine_type=EngineType.postgres.value,
                                 prod_number=ProdNumber.prod01.value,
                                 sync_name=__file__)

default_views = {'ticket_system_casino.time_in_pending'}
default_functions = {'ticket_system_casino.refresh_tab_keywords_tickets'}


def main():
    try:
        log_state.table_name = Table()
        log_state.table_name.__tablename__ = 'ticket system casino matviews'
        refresh_mat_views(LOCK_QUERY, default_views, log_state)
        refresh_custom_reports(LOCK_QUERY, default_functions, log_state)
    except Exception as ex:
        print(f'\n{"-" * 40}\nException: {str(ex)}\n{"-" * 40}\n')
        log_state.message = f"DWH PROD01 Exception: {str(ex)}"
        write_exception(log_state)


if __name__ == "__main__":
    sys.exit(main())
