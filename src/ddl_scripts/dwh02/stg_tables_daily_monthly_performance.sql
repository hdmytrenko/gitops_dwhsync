--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

--
-- Name: daily_monthly_performance; Type: TABLE; Schema: stg_tables; Owner: postgres
--

CREATE TABLE stg_tables.daily_monthly_performance (
    payment_id character varying(50) NOT NULL,
    brand_id character varying(20),
    creation_time timestamp without time zone,
    deposit_usd double precision,
    execution_time timestamp without time zone,
    ftd numeric,
    withdraw_usd double precision
);


ALTER TABLE stg_tables.daily_monthly_performance OWNER TO postgres;

--
-- Name: daily_monthly_performance pk_daily_monthly_performance; Type: CONSTRAINT; Schema: stg_tables; Owner: postgres
--

ALTER TABLE ONLY stg_tables.daily_monthly_performance
    ADD CONSTRAINT pk_daily_monthly_performance PRIMARY KEY (payment_id);


--
-- Name: idx_daily_monthly_performance_create_time; Type: INDEX; Schema: stg_tables; Owner: postgres
--

CREATE INDEX idx_daily_monthly_performance_create_time ON stg_tables.daily_monthly_performance USING btree (creation_time);


--
-- Name: idx_daily_monthly_performance_execution_time; Type: INDEX; Schema: stg_tables; Owner: postgres
--

CREATE INDEX idx_daily_monthly_performance_execution_time ON stg_tables.daily_monthly_performance USING btree (execution_time);


--
-- Name: TABLE daily_monthly_performance; Type: ACL; Schema: stg_tables; Owner: postgres
--

GRANT ALL ON TABLE stg_tables.daily_monthly_performance TO data_sync;
GRANT SELECT ON TABLE stg_tables.daily_monthly_performance TO ybashkatov;


--
-- PostgreSQL database dump complete
--

