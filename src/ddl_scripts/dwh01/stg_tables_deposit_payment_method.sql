--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

--
-- Name: deposit_payment_method; Type: TABLE; Schema: stg_tables; Owner: postgres
--

CREATE TABLE stg_tables.deposit_payment_method (
    method_id character varying(64) NOT NULL,
    method_name character varying(64) NOT NULL
);


ALTER TABLE stg_tables.deposit_payment_method OWNER TO postgres;

--
-- Name: deposit_payment_method hrzn10_prod_directories_deposit_payment_method_pkey; Type: CONSTRAINT; Schema: stg_tables; Owner: postgres
--

ALTER TABLE ONLY stg_tables.deposit_payment_method
    ADD CONSTRAINT hrzn10_prod_directories_deposit_payment_method_pkey PRIMARY KEY (method_id);


--
-- Name: TABLE deposit_payment_method; Type: ACL; Schema: stg_tables; Owner: postgres
--

GRANT ALL ON TABLE stg_tables.deposit_payment_method TO data_sync;
GRANT SELECT ON TABLE stg_tables.deposit_payment_method TO ybashkatov;


--
-- PostgreSQL database dump complete
--

