--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: profile_info; Type: VIEW; Schema: hrzn07_brand_cmarket; Owner: postgres
--

CREATE VIEW hrzn07_brand_cmarket.profile_info AS
 SELECT ep.player_uuid,
    ep.username,
    ep.last_name,
    ep.first_name,
    ep.profile_status,
    ep.profile_status_reason,
    ep.login,
    ep.country,
    ep.currency,
    ep.address,
    ep.city,
    ep.phone,
    ep.email_verified,
    ep.marketing_mail,
    ep.marketing_sms,
    ep.language_code,
    ep.completed,
    ep.migrated,
    ep.creation_date,
    ep.created_at,
    ep.send_mail,
    ep.registration_date,
    ep.registration_ip,
    ep.ip,
    ep.gender,
    ep.updated_date,
    ep.post_code,
    ep.last_note,
    ep.last_note_date,
    ep.birth_date,
    ep.withdrawable_amount_amount,
    ep.withdrawable_amount_currency,
    ep.real_money_balance_amount,
    ep.real_money_balance_currency,
    ep.bonus_balance_amount,
    ep.bonus_balance_currency,
    ep.total_balance_amount,
    ep.total_balance_currency,
    ep.first_deposit,
    ep.tailor_made_email,
    ep.tailor_made_sms,
    ep.author_uuid,
    ep.kyc_rep_name,
    ep.fns_status,
    ep.last_trade_date,
    ep.affiliate_uuid,
    ep.affiliate_source,
    ep.is_test_user,
    ep.acquisition_status,
    ep.sales_status,
    ep.retention_status,
    ep.equity,
    ep.balance,
    ep.kyc_status,
    ep.base_currency_equity,
    ep.base_currency_credit,
    ep.mt_balance,
    ep.base_currency_margin,
    ep.credit,
    ep.margin,
    ep.brand_id,
    ep.country_specific_identifier,
    ep.country_specific_identifier_type,
    ep.first_deposit_date,
    ep.last_deposit_date,
    ep.retention_rep,
    ep.sales_rep,
    ep.deposit_count,
    ep.passport_number,
    ep.mt_group,
    ep.affiliate_referral,
    ep.questionnaire_status,
    ep.passport_expiration_date,
    ep.crs,
    ep.last_login,
    ep.withdrawal_count,
    ep.status_changed_at
   FROM ecosales_reporting.elastic_profile ep
  WHERE ((ep.brand_id)::text = 'cmarket'::text);


ALTER TABLE hrzn07_brand_cmarket.profile_info OWNER TO postgres;

--
-- Name: TABLE profile_info; Type: ACL; Schema: hrzn07_brand_cmarket; Owner: postgres
--

GRANT ALL ON TABLE hrzn07_brand_cmarket.profile_info TO data_sync;
GRANT SELECT ON TABLE hrzn07_brand_cmarket.profile_info TO rezo;
GRANT SELECT ON TABLE hrzn07_brand_cmarket.profile_info TO ybashkatov;


--
-- PostgreSQL database dump complete
--

