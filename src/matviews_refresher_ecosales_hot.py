import sys
import uuid

from sqlalchemy import func, Table

from utils.common import create_session_new, create_config_dictionary, write_exception, \
    generate_refreshing_plan_query, enrich_matviews_set, write_log, group_matviews_by_month, create_session_autocommit
from utils.constants import DbType, LOCK_QUERY, EngineType, ProdNumber, SynchronizationState
from utils.writers import refresh_mat_views_partitioned
from dwh_sync.dwh.mapping_writer_classes import RefreshingPlan

config_path = "settings_prod02.ini"
dwh_config_dict = create_config_dictionary(config_path, DbType.DWH.value, engine_type=EngineType.postgres.value)
log_session = create_session_new(dwh_config_dict)
dwh_session = create_session_new(create_config_dictionary(config_path,
                                                          DbType.DWH.value,
                                                          schema_translate_map=True,
                                                          engine_type=EngineType.postgres.value))
lock_session = create_session_new(dwh_config_dict)
log_state = SynchronizationState(log_session,
                                 uuid.uuid1(),
                                 dwh_session=dwh_session,
                                 lock_session=lock_session,
                                 source_engine_type=EngineType.postgres.value,
                                 prod_number=ProdNumber.prod02.value,
                                 postgres_statement_timeout=1800000,
                                 sync_name=__file__)
log_state.refresh_mat_views_session = create_session_autocommit(dwh_config_dict, log_state.postgres_statement_timeout)


def main():
    try:
        log_state.table_name = Table()
        log_state.table_name.__tablename__ = 'materialized views'
        max_id = log_state.dwh_session.query(func.max(RefreshingPlan.id)).first()[0]
        log_state.table_name.__tablename__ = 'materialized views hot'
        views = enrich_matviews_set(generate_refreshing_plan_query(log_state, hot=True))
        grouped_views = group_matviews_by_month(views)
        for views_group in grouped_views:
            refresh_mat_views_partitioned(LOCK_QUERY, views_group, log_state, max_id)
        log_state.message = "mat views hot refreshed finished"
        write_log(log_state)
    except Exception as ex:
        print(f'\n{"-" * 40}\nException: {str(ex)}\n{"-" * 40}\n')
        log_state.message = f"DWH PROD02 Exception: {str(ex)}"
        write_exception(log_state)


if __name__ == "__main__":
    sys.exit(main())
