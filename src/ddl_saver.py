import configparser
import sys

from utils.common import create_session_new, create_config_dictionary
from utils.constants import DbType, METADATA_QUERY, EngineType
from utils.readers import get_ddl

config_path = 'settings.ini'
config = configparser.ConfigParser()
config.read(config_path)


def main():
    try:
        for object_info in create_session_new(
                create_config_dictionary(config_path,
                                         DbType.DWH.value,
                                         schema_translate_map=True,
                                         engine_type=EngineType.postgres.value))\
                .execute(METADATA_QUERY):
            folder = f"dwh{config[DbType.DWH.value]['dbname'].split('prod')[1][:2]}"
            file_name = f"{object_info[0]}_{object_info[1]}"
            with open(f"ddl_scripts/{folder}/{file_name}.sql", "w") as text_file:
                text_file.write(get_ddl(config[DbType.DWH.value], object_info[0], object_info[1]))
    except Exception as ex:
        print(f'\n{"-" * 40}\nException: {str(ex)}\n{"-" * 40}\n')


if __name__ == "__main__":
    sys.exit(main())
