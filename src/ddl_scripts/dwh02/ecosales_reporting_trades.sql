--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: trades; Type: VIEW; Schema: ecosales_reporting; Owner: postgres
--

CREATE VIEW ecosales_reporting.trades AS
 SELECT trades.server_id,
    trades.cmd,
    trades.manager_login,
    trades."order",
    trades.login,
    trades.digits,
    trades.volume,
    trades.opening_time,
    trades.magic,
    trades.activation,
    trades.state,
    trades.gw_order,
    trades.gw_volume,
    trades.gw_open_price,
    trades.gw_close_price,
    trades.closing_time,
    trades.expiration_time,
    trades."timestamp",
    trades.symbol,
    trades.comment,
    trades.reason,
    trades.opening_price,
    trades.stop_loss,
    trades.take_profit,
    trades.commission,
    trades.commission_agent,
    trades.storage,
    trades.closing_price,
    trades.profit,
    trades.taxes,
    trades.margin_rate,
    trades.opening_rate,
    trades.closing_rate,
    trades.proxy_timestamp,
    trades.deleted_at,
    trades.prod_num
   FROM stg_tables.trades;


ALTER TABLE ecosales_reporting.trades OWNER TO postgres;

--
-- Name: TABLE trades; Type: ACL; Schema: ecosales_reporting; Owner: postgres
--

GRANT SELECT ON TABLE ecosales_reporting.trades TO ybashkatov;


--
-- PostgreSQL database dump complete
--

