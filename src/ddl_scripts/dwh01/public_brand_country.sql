--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

--
-- Name: brand_country; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.brand_country (
    brand_id character varying(50) NOT NULL,
    brand_country character varying(50)
);


ALTER TABLE public.brand_country OWNER TO postgres;

--
-- Name: brand_country_brand_id_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX brand_country_brand_id_idx ON public.brand_country USING btree (brand_id);


--
-- PostgreSQL database dump complete
--

