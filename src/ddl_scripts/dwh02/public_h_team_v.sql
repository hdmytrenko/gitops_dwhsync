--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: h_team_v; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.h_team_v AS
 SELECT t.parent_id AS desk_id,
    t.id AS team_id,
    t.dtype AS team_dtype,
    t.name AS team_name,
    d.desk_language,
    t.assign_index AS assign_agents
   FROM (ecosales_reporting.branch_hierarchy t
     LEFT JOIN public.h_desk_v d ON ((t.parent_id = d.desk_id)))
  WHERE ((t.deleted_at IS NULL) AND ((t.dtype)::text = 'TEAM'::text) AND ((t.name)::text !~~ '%UAT+%'::text) AND (t.parent_id IS NOT NULL));


ALTER TABLE public.h_team_v OWNER TO postgres;

--
-- Name: TABLE h_team_v; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.h_team_v TO data_sync;
GRANT SELECT ON TABLE public.h_team_v TO ybashkatov;


--
-- PostgreSQL database dump complete
--

