--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: mt4_login_first_open_trade; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.mt4_login_first_open_trade AS
 SELECT t.rn,
    t.profile_id,
    t.login,
    t.brand,
    t.first_open_trade,
    t.first_close_trade,
    t.last_open_trade,
    t.last_close_trade,
    t.profit,
    t.balance
   FROM ( SELECT row_number() OVER (PARTITION BY m.profile_id ORDER BY m.first_open_trade) AS rn,
            m.profile_id,
            m.login,
            m.brand_id AS brand,
            m.first_open_trade,
            m.first_close_trade,
            m.last_open_trade,
            m.last_close_trade,
            m.profit,
            m.balance
           FROM public.mt4_login1 m) t
  WHERE (t.rn = 1);


ALTER TABLE public.mt4_login_first_open_trade OWNER TO postgres;

--
-- Name: TABLE mt4_login_first_open_trade; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public.mt4_login_first_open_trade TO prod01_metabase;
GRANT SELECT ON TABLE public.mt4_login_first_open_trade TO ybashkatov;


--
-- PostgreSQL database dump complete
--

