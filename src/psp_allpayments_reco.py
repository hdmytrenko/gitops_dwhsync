import datetime
import re
import io
import uuid
from webdav3.client import Client
from sqlalchemy import Table
import pandas as pd
from sqlalchemy.types import Text, DateTime, Float
import sys
from src.utils.common import create_config_dictionary, create_session_new, obtain_lock, write_log, write_exception
from src.utils.constants import DbType, LOCK_QUERY, EngineType, SynchronizationState, ProdNumber

CONFIG_FILE_PATH = 'settings_psp_reco.ini'

conf_dict_dwh = create_config_dictionary(CONFIG_FILE_PATH, DbType.DWH.value, engine_type=EngineType.postgres.value)
conf_dict_source = create_config_dictionary(CONFIG_FILE_PATH, DbType.SOURCE.value)


lock_session = create_session_new(conf_dict_dwh)

dwh_session = create_session_new(conf_dict_dwh)

log_session = create_session_new(conf_dict_dwh)

log_state = SynchronizationState(log_session,
                                 uuid.uuid1(),
                                 dwh_session=dwh_session,
                                 lock_session=lock_session,
                                 prod_number=ProdNumber.prod01.value,
                                 sync_name=__file__,
                                 verbose=False)
log_state.prod_number = conf_dict_dwh['prod_number']
log_state.table_name = Table()
log_state.table_name.__tablename__  = 'psp_allpayments_reco'



pd.options.mode.chained_assignment = None  # default='warn'

REMOTE_FILENAME = datetime.datetime.now().strftime('allpayments_%Y-%m-%d-%H-%M-%S.csv')

remote_proceeded_path= "/Proceeded PSP/Allpayments/"
allpayments_path = "/PSP transaction history/Not cashier integrated psp/Allpayments/"

brand_list = ['qicmarket','galaxytrade','investflow','24funds','euromts','eurofx','luxinvestment','onyxprofit','nvestpro','traderking','influxfinance','meteortrade','octofinance','paragonfinance',
         'solutionsmarkets','luxistrade','stockfx','hashtrade','stocklux','assetgroup','igfb','platinex','majortrade','equitybroker','rubyfinance','finocapital','fxactiv','finlay','unionstock','simpleway','winmarket',
            'citadelex','adamantfx','bsb-global','proactivebrokers','toptrade','axedo','bid-broker-stocks','neoomatic','capitalfunds','tetrainvest','keyfunders','absystem','flexcapital','hexally','sensetrade','24fintime',
            'emextrade','maxiplus','finansa','energy-markets','origininvest','gravitytrade','agm','advinvesment','leveltrade','spacemarket','fxcore','solidinvesting','cointrade','primecap','fxrevolution','profitassist',
            'unitestock','primefunder','neotrade','orbitrade','investboost','cryptoliquidityx','optimarket','lionstock','trustfx','ecosales','capitalindexfund','tradingnetwork','geniusinvest','solidstocks','gmxmarkets',
            'axiomtrade','horizoninvest','advancestox','uppertrade','assistinvest','primotrade','royalbanc','fantex','investcore','marketltd','terrafinance','hqtrade']


options = {
 'webdav_hostname': f"https://{conf_dict_source['host']}/remote.php/webdav",
 'webdav_login':    conf_dict_source['user'],
 'webdav_password': conf_dict_source['password'],
}
client = Client(options)
list_of_files = client.list(allpayments_path)
list_of_files.pop(0)


def generateBrand_list(src):
    brand_name = ''
    for brand in brand_list:
        if brand in src:
            brand_name += (brand + ' ')
    brand_name = brand_name[:-1]
    return (brand_name)

def datCnv(src):
    return pd.to_datetime(src)

def swapDate(src):
    if src.month not in (datetime.datetime.now().month, datetime.datetime.now().month - 1, datetime.datetime.now().month - 2):
        try:
            date = f'{src.year}-{src.day}-{src.month} {src.hour}:{src.minute}:{src.second}'
            return pd.to_datetime(date)
        except Exception as ex:
            write_log(log_state, message=f"error format date : {src}. {str(ex)}")
            return src
    else:
        return src

def fixAmount(src):
    if pd.isna(src):
        return 0
    dat_to_str = str(src).split('-')
    if len(dat_to_str) > 2:
        dat_to_str.pop()
        dat_to_str = '.'.join(dat_to_str)
        return dat_to_str
    if ',' in str(src) and '.' in str(src):
         src = str(src).replace(',','')
    src = str(src).replace(',','.')
    src = re.sub('[^\d\.]', '', src)
    return src

def allpayments_parsing(list_of_files):
    excel = {}
    try:
        for i, k in zip(range(0, len(list_of_files)), list_of_files):
            buffer = io.BytesIO()
            client.resource(allpayments_path + k).write_to(buffer)
            excel['df_' + str(i + 1)] = pd.DataFrame(pd.read_excel(buffer))
    except Exception as ex:
        write_log(log_state, message=f"Fail read files. {str(ex)}")

    for i in range(0, len(list_of_files)):
        temp = excel[str('df_' + str(i + 1))]
        excel[str('df_' + str(i + 1))].loc[:, 'brand_name'] = excel[str('df_' + str(i + 1))]. \
                                                                  loc[:, 'Added By'].apply(generateBrand_list)
        del excel[str('df_' + str(i + 1))]
        excel[str('df_' + str(i + 1))] = temp[['TID', 'brand_name', 'Time', 'Amount', 'Currency', 'Status']]
        excel[str('df_' + str(i + 1))].loc[:, 'psp_name'] = list_of_files[i].split('-')[0]
        excel[str('df_' + str(i + 1))].loc[:, 'backoffice'] = list_of_files[i].split('-')[1].split('.')[0]
        excel[str('df_' + str(i + 1))].loc[:, 'Status'] = excel[str('df_' + str(i + 1))].loc[:, 'Status'].str.upper()
        # rename columns
        excel[str('df_' + str(i + 1))] = excel[str('df_' + str(i + 1))] \
            .rename(columns={"TID": "id",
                             "Time": "psp_date",
                             "Amount": "amount",
                             'Status': 'status',
                             "Currency": "currency"})

    final_csv = excel['df_1']
    for i in range(len(list_of_files)):
        temp = excel[str('df_' + str(i + 1))]
        final_csv = pd.concat([final_csv, temp], axis=0, ignore_index=True)
    final_csv = final_csv.loc[final_csv['status'] == 'SUCCEEDED']
    final_csv.drop('status', axis=1, inplace=True)
    final_csv.psp_date = final_csv.psp_date.apply(datCnv).apply(swapDate)
    final_csv.amount = final_csv.amount.apply(fixAmount)
    final_csv = final_csv[['id', 'psp_name', 'brand_name', 'psp_date', 'amount', 'currency', 'backoffice']]
    final_csv = final_csv.drop_duplicates()
    return final_csv



def cloud_upload(csv):
    client.execute_request('upload', path=remote_proceeded_path + REMOTE_FILENAME, data=csv.to_csv(sep=';', index=False))

def cloud_remove(path):
    for item in list_of_files:
        client.clean(path + '/' +item)



def toDWH(final_csv):

    table_name = 'psp_crm_csv'
    schema_name = conf_dict_dwh['schema']
    if not dwh_session.bind.has_table(table_name, schema_name):
        final_csv.to_sql(
            table_name,
            dwh_session.bind,
            schema_name,
            if_exists='replace',
            index=False,
            chunksize=500,
            dtype={
                "id": Text,
                "psp_name": Text,
                "brand_name": Text,
                "psp_date": DateTime,
                "amount": Float,
                "currency": Text,
                "backoffice": Text
            }
        )
        try:
            dwh_session.execute(f'delete from {schema_name}.{table_name}  a '
                           f'where a.ctid <> (SELECT min(b.ctid) '
                           f'from {schema_name}.{table_name}  b '
                           f'where a.id = b.id)')
            dwh_session.execute(f'alter table {schema_name}.{table_name} add constraint idx unique (id);')
            return True
        except Exception as ex:
            log_state.lock_session.rollback()
            write_log(log_state, message=f"Error upload new file: {str(ex)}")

    else:
        try:
            final_csv.to_sql(
                'temp_table',
                dwh_session.bind,
                schema_name,
                if_exists='replace',
                index=False,
                chunksize=500,
                dtype={
                    "id": Text,
                    "psp_name": Text,
                    "brand_name": Text,
                    "psp_date": DateTime,
                    "amount": Float,
                    "currency": Text,
                    "backoffice": Text
                }
            )
        except Exception as ex:
            write_log(log_state, message= f"creation error temp_table: {str(ex)}")
        try:
            dwh_session.execute(f'delete from {schema_name}.temp_table  a '
                           f'where a.ctid <> (SELECT min(b.ctid) '
                           f'from {schema_name}.temp_table  b '
                           f'where a.id = b.id)')
            dwh_session.execute(f'alter table {schema_name}.temp_table add constraint idx unique (id);')
        except Exception as ex:
            write_log(log_state, message= f"error creating uniqueness constraint: {str(ex)}")
            log_state.lock_session.rollback()
            return
        try:
            dwh_session.execute(f'insert into {schema_name}.{table_name} '
                           f'select * from {schema_name}.temp_table '
                           f'on conflict (id) do update '
                           f'set psp_name = excluded.psp_name, '
                           f'brand_name = excluded.brand_name, '
                           f'psp_date = excluded.psp_date, '
                           f'amount = excluded.amount, '
                           f'currency = excluded.currency, '
                           f'backoffice = excluded.backoffice;')
            dwh_session.execute(f'drop table {schema_name}.temp_table;')
            dwh_session.commit()
            return True
        except Exception as ex:
            write_log(log_state, message= f"Fail upsert : {str(ex)}")
            log_state.lock_session.rollback()
            return

def file_checker():
    client = Client(options)
    list_of_files = client.list(allpayments_path)
    list_of_files.pop(0)
    if list_of_files:
        for file in list_of_files:
            if re.match(r"([^.]+).xlsx",file) == None:
                write_log(log_state, message= f"Something wrong with names or types of files on cloud! : {str(file)}")
                return
            else:
                continue
    else:
        write_log(log_state, message="There's no files on cloud!")
        return
    return True

def main():
    if not obtain_lock(log_state.lock_session, LOCK_QUERY, log_state):
        lock_session.rollback()
        exit(1)
    if file_checker():
        write_log(log_state, message="Start file uploading")
        allpayments_csv = allpayments_parsing(list_of_files)
        if toDWH(allpayments_csv):
            cloud_upload(allpayments_csv)
            cloud_remove(allpayments_path)
    client.session.close()
    log_state.lock_session.rollback()
    write_log(log_state, message="File upload!")



if __name__ == "__main__":
    try:
        sys.exit(main())
    except Exception as ex:
        print(f'\n{"-" * 40}\nException: {str(ex)}\n{"-" * 40}\n')
        log_state.message = f"DWH PROD0{log_state.prod_number} Exception: {str(ex)}"
        write_exception(log_state)


