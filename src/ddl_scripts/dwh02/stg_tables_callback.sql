--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

--
-- Name: callback; Type: TABLE; Schema: stg_tables; Owner: postgres
--

CREATE TABLE stg_tables.callback (
    id bigint NOT NULL,
    user_id character varying(255) NOT NULL,
    operator_id character varying(255) NOT NULL,
    callback_time timestamp without time zone NOT NULL,
    status character varying(32) NOT NULL,
    creation_time timestamp without time zone NOT NULL,
    update_time timestamp without time zone,
    is_deleted boolean,
    callback_id character varying(50) NOT NULL,
    brand_id character varying(50),
    created_by character varying(50),
    prod_num integer
);


ALTER TABLE stg_tables.callback OWNER TO postgres;

--
-- Name: callback stg_callback_pk; Type: CONSTRAINT; Schema: stg_tables; Owner: postgres
--

ALTER TABLE ONLY stg_tables.callback
    ADD CONSTRAINT stg_callback_pk PRIMARY KEY (id);


--
-- Name: callback unique_stg_callback; Type: CONSTRAINT; Schema: stg_tables; Owner: postgres
--

ALTER TABLE ONLY stg_tables.callback
    ADD CONSTRAINT unique_stg_callback UNIQUE (user_id, operator_id, callback_time, status, is_deleted);


--
-- Name: idx_stg_callback; Type: INDEX; Schema: stg_tables; Owner: postgres
--

CREATE INDEX idx_stg_callback ON stg_tables.callback USING btree (callback_id);


--
-- Name: idx_stg_callback_brand_id; Type: INDEX; Schema: stg_tables; Owner: postgres
--

CREATE INDEX idx_stg_callback_brand_id ON stg_tables.callback USING btree (brand_id);


--
-- Name: idx_stg_callback_creation_time; Type: INDEX; Schema: stg_tables; Owner: postgres
--

CREATE INDEX idx_stg_callback_creation_time ON stg_tables.callback USING btree (creation_time);


--
-- Name: idx_stg_callback_operator; Type: INDEX; Schema: stg_tables; Owner: postgres
--

CREATE INDEX idx_stg_callback_operator ON stg_tables.callback USING btree (operator_id);


--
-- Name: idx_stg_callback_status; Type: INDEX; Schema: stg_tables; Owner: postgres
--

CREATE INDEX idx_stg_callback_status ON stg_tables.callback USING btree (status);


--
-- Name: idx_stg_callback_time; Type: INDEX; Schema: stg_tables; Owner: postgres
--

CREATE INDEX idx_stg_callback_time ON stg_tables.callback USING btree (callback_time);


--
-- Name: idx_stg_callback_update_time; Type: INDEX; Schema: stg_tables; Owner: postgres
--

CREATE INDEX idx_stg_callback_update_time ON stg_tables.callback USING btree (update_time);


--
-- Name: idx_stg_callback_user; Type: INDEX; Schema: stg_tables; Owner: postgres
--

CREATE INDEX idx_stg_callback_user ON stg_tables.callback USING btree (user_id);


--
-- Name: TABLE callback; Type: ACL; Schema: stg_tables; Owner: postgres
--

GRANT ALL ON TABLE stg_tables.callback TO data_sync;
GRANT SELECT ON TABLE stg_tables.callback TO ybashkatov;


--
-- PostgreSQL database dump complete
--

