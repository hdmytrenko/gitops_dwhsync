--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

--
-- Name: securities; Type: TABLE; Schema: stg_tables; Owner: data_sync
--

CREATE TABLE stg_tables.securities (
    server_id smallint NOT NULL,
    id smallint NOT NULL,
    name character varying(16) NOT NULL,
    description character varying(64),
    prod_num smallint
);


ALTER TABLE stg_tables.securities OWNER TO data_sync;

--
-- Name: TABLE securities; Type: ACL; Schema: stg_tables; Owner: data_sync
--

GRANT SELECT ON TABLE stg_tables.securities TO prod01_metabase;
GRANT SELECT ON TABLE stg_tables.securities TO ybashkatov;


--
-- PostgreSQL database dump complete
--

