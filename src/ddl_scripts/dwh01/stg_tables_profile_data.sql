--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

--
-- Name: profile_data; Type: TABLE; Schema: stg_tables; Owner: data_sync
--

CREATE TABLE stg_tables.profile_data (
    id bigint NOT NULL,
    uuid character varying(50) NOT NULL,
    player_uuid character varying(50) NOT NULL,
    brand_id character varying(20) NOT NULL,
    questionnaire_id bigint NOT NULL,
    total_score numeric,
    risk_category character varying(20),
    created_at timestamp without time zone NOT NULL,
    created_by character varying(50),
    prod_num integer
);


ALTER TABLE stg_tables.profile_data OWNER TO data_sync;

--
-- Name: profile_data profile_data_pkey; Type: CONSTRAINT; Schema: stg_tables; Owner: data_sync
--

ALTER TABLE ONLY stg_tables.profile_data
    ADD CONSTRAINT profile_data_pkey PRIMARY KEY (id);


--
-- Name: idx_profile_data_plyer_uuid_created; Type: INDEX; Schema: stg_tables; Owner: data_sync
--

CREATE INDEX idx_profile_data_plyer_uuid_created ON stg_tables.profile_data USING btree (player_uuid, created_at);


--
-- Name: TABLE profile_data; Type: ACL; Schema: stg_tables; Owner: data_sync
--

GRANT SELECT ON TABLE stg_tables.profile_data TO prod01_metabase;
GRANT SELECT ON TABLE stg_tables.profile_data TO ybashkatov;


--
-- PostgreSQL database dump complete
--

