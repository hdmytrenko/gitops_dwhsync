--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

--
-- Name: elastic_profile; Type: TABLE; Schema: stg_tables; Owner: postgres
--

CREATE TABLE stg_tables.elastic_profile (
    player_uuid character varying(255) NOT NULL,
    username character varying(255),
    last_name character varying(255),
    first_name character varying(255),
    profile_status character varying(127),
    profile_status_reason character varying(255),
    login integer[],
    country character varying(255),
    currency character(3),
    address character varying(1024),
    city character varying(255),
    phone character varying(255),
    email character varying(255),
    email_verified boolean,
    marketing_mail boolean,
    marketing_sms boolean,
    language_code character varying(255),
    completed boolean,
    migrated boolean,
    creation_date timestamp without time zone,
    created_at timestamp without time zone,
    send_mail boolean,
    registration_date timestamp without time zone,
    registration_ip character varying(255),
    ip character varying(255),
    gender character varying(255),
    updated_date timestamp without time zone,
    post_code character varying(255),
    last_note character varying(2048),
    last_note_date timestamp without time zone,
    birth_date date,
    withdrawable_amount_amount numeric(20,10),
    withdrawable_amount_currency character(3),
    real_money_balance_amount numeric(20,10),
    real_money_balance_currency character(3),
    bonus_balance_amount numeric(20,10),
    bonus_balance_currency character(3),
    total_balance_amount numeric(20,10),
    total_balance_currency character(3),
    first_deposit boolean,
    tailor_made_email boolean,
    tailor_made_sms boolean,
    author_uuid character varying(255),
    kyc_rep_name character varying(255),
    fns_status character varying(255),
    last_trade_date timestamp without time zone,
    affiliate_uuid character varying(255),
    affiliate_source character varying(1024),
    is_test_user boolean,
    acquisition_status character varying(255),
    sales_status character varying(255),
    retention_status character varying(255),
    equity numeric(20,10),
    balance numeric(20,10),
    kyc_status character varying(255),
    base_currency_equity numeric(20,10),
    base_currency_credit numeric(20,10),
    mt_balance numeric(20,10),
    base_currency_margin numeric(20,10),
    credit numeric(20,10),
    margin numeric(20,10),
    brand_id character varying(255),
    country_specific_identifier character varying(255),
    country_specific_identifier_type character varying(255),
    first_deposit_date timestamp without time zone,
    last_deposit_date timestamp without time zone,
    retention_rep character varying(255),
    sales_rep character varying(255),
    deposit_count integer,
    passport_number character varying(255),
    mt_group character varying(255),
    affiliate_referral character varying(1024),
    passport_expiration_date date,
    questionnaire_status character varying(20),
    crs boolean,
    last_login timestamp without time zone,
    withdrawal_count integer,
    prod_num integer,
    fsa_migration_status character varying,
    kyc_changed_at timestamp without time zone,
    internal_transfer boolean,
    fatca boolean,
    gdpr_sms boolean,
    gdpr_email boolean,
    gdpr_phone boolean,
    gdpr_social_media boolean,
    spam_market_news boolean,
    spam_promos_and_offers boolean,
    spam_information boolean,
    spam_statistics_and_summary boolean,
    spam_educational boolean,
    spam_web_cookies boolean,
    verifications text[]
);


ALTER TABLE stg_tables.elastic_profile OWNER TO postgres;

--
-- Name: elastic_profile tmp_elastic_profile_pkey; Type: CONSTRAINT; Schema: stg_tables; Owner: postgres
--

ALTER TABLE ONLY stg_tables.elastic_profile
    ADD CONSTRAINT tmp_elastic_profile_pkey PRIMARY KEY (player_uuid);


--
-- Name: idx_fsc_elastic_profile_updated_date; Type: INDEX; Schema: stg_tables; Owner: postgres
--

CREATE INDEX idx_fsc_elastic_profile_updated_date ON stg_tables.elastic_profile USING btree (updated_date);


--
-- Name: idx_stg_elastic_profile_country; Type: INDEX; Schema: stg_tables; Owner: postgres
--

CREATE INDEX idx_stg_elastic_profile_country ON stg_tables.elastic_profile USING btree (upper((country)::text));


--
-- Name: idx_stg_elastic_profile_creation_date; Type: INDEX; Schema: stg_tables; Owner: postgres
--

CREATE INDEX idx_stg_elastic_profile_creation_date ON stg_tables.elastic_profile USING btree (creation_date);


--
-- Name: idx_stg_elastic_profile_day_registration_and_country; Type: INDEX; Schema: stg_tables; Owner: postgres
--

CREATE INDEX idx_stg_elastic_profile_day_registration_and_country ON stg_tables.elastic_profile USING btree (date_trunc('day'::text, registration_date), upper((country)::text));


--
-- Name: idx_stg_elastic_profile_email_sales_status; Type: INDEX; Schema: stg_tables; Owner: postgres
--

CREATE INDEX idx_stg_elastic_profile_email_sales_status ON stg_tables.elastic_profile USING btree (email, sales_status);


--
-- Name: idx_stg_elastic_profile_email_verified; Type: INDEX; Schema: stg_tables; Owner: postgres
--

CREATE INDEX idx_stg_elastic_profile_email_verified ON stg_tables.elastic_profile USING btree (email_verified);


--
-- Name: idx_stg_elastic_profile_first_name; Type: INDEX; Schema: stg_tables; Owner: postgres
--

CREATE INDEX idx_stg_elastic_profile_first_name ON stg_tables.elastic_profile USING btree (first_name);


--
-- Name: idx_stg_elastic_profile_language_code; Type: INDEX; Schema: stg_tables; Owner: postgres
--

CREATE INDEX idx_stg_elastic_profile_language_code ON stg_tables.elastic_profile USING btree (language_code);


--
-- Name: idx_stg_elastic_profile_last_name; Type: INDEX; Schema: stg_tables; Owner: postgres
--

CREATE INDEX idx_stg_elastic_profile_last_name ON stg_tables.elastic_profile USING btree (last_name);


--
-- Name: idx_stg_elastic_profile_last_note_date; Type: INDEX; Schema: stg_tables; Owner: postgres
--

CREATE INDEX idx_stg_elastic_profile_last_note_date ON stg_tables.elastic_profile USING btree (last_note_date);


--
-- Name: idx_stg_elastic_profile_last_trade_date; Type: INDEX; Schema: stg_tables; Owner: postgres
--

CREATE INDEX idx_stg_elastic_profile_last_trade_date ON stg_tables.elastic_profile USING btree (last_trade_date);


--
-- Name: idx_stg_elastic_profile_month_registration; Type: INDEX; Schema: stg_tables; Owner: postgres
--

CREATE INDEX idx_stg_elastic_profile_month_registration ON stg_tables.elastic_profile USING btree (date_trunc('month'::text, registration_date));


--
-- Name: idx_stg_elastic_profile_month_registration_country_brand; Type: INDEX; Schema: stg_tables; Owner: postgres
--

CREATE INDEX idx_stg_elastic_profile_month_registration_country_brand ON stg_tables.elastic_profile USING btree (date_trunc('month'::text, registration_date), upper((country)::text), brand_id);


--
-- Name: idx_stg_elastic_profile_registration_date; Type: INDEX; Schema: stg_tables; Owner: postgres
--

CREATE INDEX idx_stg_elastic_profile_registration_date ON stg_tables.elastic_profile USING btree (registration_date);


--
-- Name: idx_stg_elastic_profile_tp_affiliate_uuid; Type: INDEX; Schema: stg_tables; Owner: postgres
--

CREATE INDEX idx_stg_elastic_profile_tp_affiliate_uuid ON stg_tables.elastic_profile USING btree (affiliate_uuid);


--
-- Name: idx_stg_elastic_profile_tp_brand_id; Type: INDEX; Schema: stg_tables; Owner: postgres
--

CREATE INDEX idx_stg_elastic_profile_tp_brand_id ON stg_tables.elastic_profile USING btree (brand_id);


--
-- Name: idx_stg_elastic_profile_tp_retention_rep; Type: INDEX; Schema: stg_tables; Owner: postgres
--

CREATE INDEX idx_stg_elastic_profile_tp_retention_rep ON stg_tables.elastic_profile USING btree (retention_rep);


--
-- Name: idx_stg_elastic_profile_tp_retention_status; Type: INDEX; Schema: stg_tables; Owner: postgres
--

CREATE INDEX idx_stg_elastic_profile_tp_retention_status ON stg_tables.elastic_profile USING btree (retention_status);


--
-- Name: idx_stg_elastic_profile_tp_sales_rep; Type: INDEX; Schema: stg_tables; Owner: postgres
--

CREATE INDEX idx_stg_elastic_profile_tp_sales_rep ON stg_tables.elastic_profile USING btree (sales_rep);


--
-- Name: idx_stg_elastic_profile_tp_sales_status; Type: INDEX; Schema: stg_tables; Owner: postgres
--

CREATE INDEX idx_stg_elastic_profile_tp_sales_status ON stg_tables.elastic_profile USING btree (sales_status);


--
-- Name: TABLE elastic_profile; Type: ACL; Schema: stg_tables; Owner: postgres
--

GRANT ALL ON TABLE stg_tables.elastic_profile TO data_sync;
GRANT SELECT ON TABLE stg_tables.elastic_profile TO ybashkatov;


--
-- PostgreSQL database dump complete
--

