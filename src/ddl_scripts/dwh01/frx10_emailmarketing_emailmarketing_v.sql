--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: emailmarketing_v; Type: VIEW; Schema: frx10_emailmarketing; Owner: postgres
--

CREATE VIEW frx10_emailmarketing.emailmarketing_v AS
 WITH trades AS (
         SELECT GREATEST(max(tr.open_time), max(tr.close_time)) AS last_trade_date,
            tr.profile_uuid,
            tr.prod_num
           FROM frx_reporting.trade_record tr
          WHERE ((public.brand2prod(tr.brand_id) = 10) AND ((tr.cmd)::text = ANY (ARRAY[('OP_BUY'::character varying)::text, ('OP_SELL'::character varying)::text])) AND ((tr.brand_id)::text = ANY (ARRAY[('aurumpro'::character varying)::text, ('kronosinvest'::character varying)::text, ('mycapital'::character varying)::text])))
          GROUP BY tr.profile_uuid, tr.prod_num
        ), deposits AS (
         SELECT max(p.creation_time) AS last_deposit_date,
            p.profile_id,
            p.prod_num
           FROM frx_reporting.payment p
          WHERE ((public.brand2prod(p.brand_id) = 10) AND ((p.status)::text = 'PAYMENT_COMPLETED'::text) AND ((p.type)::text = 'DEPOSIT'::text) AND ((p.brand_id)::text = ANY (ARRAY[('aurumpro'::character varying)::text, ('kronosinvest'::character varying)::text, ('mycapital'::character varying)::text])))
          GROUP BY p.profile_id, p.prod_num
        )
 SELECT ep.player_uuid,
    ep.first_name,
    ep.last_name,
    ep.email,
    ep.brand_id,
    d.last_deposit_date,
    to_timestamp((t.last_trade_date)::double precision) AS last_trade_date
   FROM ((frx_reporting.elastic_profile ep
     LEFT JOIN trades t ON (((t.profile_uuid)::text = (ep.player_uuid)::text)))
     LEFT JOIN deposits d ON (((d.profile_id)::text = (ep.player_uuid)::text)))
  WHERE ((public.brand2prod(ep.brand_id) = 10) AND ((ep.brand_id)::text = ANY (ARRAY[('aurumpro'::character varying)::text, ('kronosinvest'::character varying)::text, ('mycapital'::character varying)::text])));


ALTER TABLE frx10_emailmarketing.emailmarketing_v OWNER TO postgres;

--
-- Name: TABLE emailmarketing_v; Type: ACL; Schema: frx10_emailmarketing; Owner: postgres
--

GRANT SELECT ON TABLE frx10_emailmarketing.emailmarketing_v TO prod01_metabase;
GRANT SELECT ON TABLE frx10_emailmarketing.emailmarketing_v TO ybashkatov;


--
-- PostgreSQL database dump complete
--

