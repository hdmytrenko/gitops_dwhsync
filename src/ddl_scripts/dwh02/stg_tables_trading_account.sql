--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

--
-- Name: trading_account; Type: TABLE; Schema: stg_tables; Owner: postgres
--

CREATE TABLE stg_tables.trading_account (
    _id text NOT NULL,
    balance text,
    margin text,
    credit text,
    brand_id text,
    login bigint,
    account_type text,
    server_id bigint,
    currency text,
    leverage bigint,
    name text,
    mt4_group text,
    created_by text,
    created_at timestamp without time zone,
    last_modified_date timestamp without time zone,
    is_read_only boolean,
    archived boolean,
    profile_uuid text,
    platform_type text,
    _class text,
    read_only_update_time timestamp without time zone,
    prod_num integer,
    normalized_balance numeric(14,5),
    normalized_credit numeric(14,5)
);


ALTER TABLE stg_tables.trading_account OWNER TO postgres;

--
-- Name: trading_account stg_trading_account_pk; Type: CONSTRAINT; Schema: stg_tables; Owner: postgres
--

ALTER TABLE ONLY stg_tables.trading_account
    ADD CONSTRAINT stg_trading_account_pk PRIMARY KEY (_id);


--
-- Name: idx_stg_trading_account_created_at; Type: INDEX; Schema: stg_tables; Owner: postgres
--

CREATE INDEX idx_stg_trading_account_created_at ON stg_tables.trading_account USING btree (created_at);


--
-- Name: idx_stg_trading_account_updated_at; Type: INDEX; Schema: stg_tables; Owner: postgres
--

CREATE INDEX idx_stg_trading_account_updated_at ON stg_tables.trading_account USING btree (last_modified_date);


--
-- Name: TABLE trading_account; Type: ACL; Schema: stg_tables; Owner: postgres
--

GRANT ALL ON TABLE stg_tables.trading_account TO data_sync;
GRANT SELECT ON TABLE stg_tables.trading_account TO ybashkatov;


--
-- PostgreSQL database dump complete
--

