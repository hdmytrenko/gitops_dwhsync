--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: h_company_v; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.h_company_v AS
 SELECT branch_hierarchy.id AS company_id,
    branch_hierarchy.dtype AS company_dtype,
    branch_hierarchy.name AS company_name
   FROM frx_reporting.branch_hierarchy
  WHERE ((branch_hierarchy.deleted_at IS NULL) AND ((branch_hierarchy.dtype)::text = 'COMPANY'::text));


ALTER TABLE public.h_company_v OWNER TO postgres;

--
-- Name: TABLE h_company_v; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public.h_company_v TO prod01_metabase;
GRANT SELECT ON TABLE public.h_company_v TO ybashkatov;


--
-- PostgreSQL database dump complete
--

