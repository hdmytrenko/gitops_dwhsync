--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: affiliate; Type: VIEW; Schema: ecosales_reporting; Owner: postgres
--

CREATE VIEW ecosales_reporting.affiliate AS
 SELECT affiliate.id,
    affiliate.version,
    affiliate.uuid,
    affiliate.brand,
    affiliate.first_name,
    affiliate.last_name,
    affiliate.phone,
    affiliate.email,
    affiliate.country,
    affiliate.affiliate_type,
    affiliate.status,
    affiliate.created_by,
    affiliate.created_at,
    affiliate.updated_at,
    affiliate.last_updated_by,
    affiliate.external_affiliate_id,
    affiliate.status_change_date,
    affiliate.status_change_author,
    affiliate.status_reason,
    affiliate.show_notes,
    affiliate.show_sales_status,
    affiliate.show_ftd_amount,
    affiliate.allowed_ip_addresses,
    affiliate.forbidden_countries,
    affiliate.deleted,
    affiliate.prod_num
   FROM stg_tables.affiliate
  WHERE ((NOT affiliate.deleted) AND ((affiliate.email IS NULL) OR (upper((affiliate.email)::text) !~~ '%NEWAGESOL.COM'::text)) AND ((affiliate.first_name IS NULL) OR (upper((affiliate.first_name)::text) !~ similar_escape('%(TEST|TRASH)%'::text, NULL::text))) AND ((affiliate.last_name IS NULL) OR (upper((affiliate.last_name)::text) !~ similar_escape('%(TEST|TRASH)%'::text, NULL::text))));


ALTER TABLE ecosales_reporting.affiliate OWNER TO postgres;

--
-- Name: TABLE affiliate; Type: ACL; Schema: ecosales_reporting; Owner: postgres
--

GRANT ALL ON TABLE ecosales_reporting.affiliate TO data_sync;
GRANT SELECT ON TABLE ecosales_reporting.affiliate TO ybashkatov;


--
-- PostgreSQL database dump complete
--

