--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: es_profile_trading_payment; Type: VIEW; Schema: hrzn07_brand_topinvestuspsp; Owner: postgres
--

CREATE VIEW hrzn07_brand_topinvestuspsp.es_profile_trading_payment AS
 SELECT es_profile_trading_payment.profile_id,
    es_profile_trading_payment.email,
    es_profile_trading_payment.login,
    es_profile_trading_payment.acquisition_status,
    es_profile_trading_payment.player_status,
    es_profile_trading_payment.sales_status,
    es_profile_trading_payment.sales_rep,
    es_profile_trading_payment.sales_name,
    es_profile_trading_payment.sales_user_type,
    es_profile_trading_payment.sales_desk_name,
    es_profile_trading_payment.sales_language,
    es_profile_trading_payment.retention_status,
    es_profile_trading_payment.retention_rep,
    es_profile_trading_payment.retention_name,
    es_profile_trading_payment.reten_user_type,
    es_profile_trading_payment.reten_desk_name,
    es_profile_trading_payment.reten_language,
    es_profile_trading_payment.creation_time_profile,
    es_profile_trading_payment.day_of_week_creation_time_profile,
    es_profile_trading_payment.day_name_creation_time_profile,
    es_profile_trading_payment.kyc_status,
    es_profile_trading_payment.mt_group,
    es_profile_trading_payment.payment_id,
    es_profile_trading_payment.type,
    es_profile_trading_payment.modification_time,
    es_profile_trading_payment.ftd_status,
    es_profile_trading_payment.ftd,
    es_profile_trading_payment.ftd_deposit,
    es_profile_trading_payment.ftd_deposit_usd,
    es_profile_trading_payment.noftd,
    es_profile_trading_payment.redeposit,
    es_profile_trading_payment.redeposit_usd,
    es_profile_trading_payment.ftd_time,
    es_profile_trading_payment.day_to_ftd,
    es_profile_trading_payment.last_deposit_time,
    es_profile_trading_payment.currency,
    es_profile_trading_payment.deposit,
    es_profile_trading_payment.withdraw,
    es_profile_trading_payment.transfer_in,
    es_profile_trading_payment.transfer_out,
    es_profile_trading_payment.credit_in,
    es_profile_trading_payment.credit_out,
    es_profile_trading_payment.deposit_usd,
    es_profile_trading_payment.withdraw_usd,
    es_profile_trading_payment.mt_balance,
    es_profile_trading_payment.rate,
    es_profile_trading_payment.paymant_creation_time,
    es_profile_trading_payment.profile_country,
    es_profile_trading_payment.country,
    es_profile_trading_payment.profile_country_name,
    es_profile_trading_payment.language,
    es_profile_trading_payment.brand_id,
    es_profile_trading_payment.payment_method,
    es_profile_trading_payment.is_published,
    es_profile_trading_payment.profile_name,
    es_profile_trading_payment.client_ip,
    es_profile_trading_payment.is_mobile,
    es_profile_trading_payment.payment_aggregator,
    es_profile_trading_payment.agent_id,
    es_profile_trading_payment.agent_name,
    es_profile_trading_payment.agent_desk_name,
    es_profile_trading_payment.agent_language,
    es_profile_trading_payment.user_type,
    es_profile_trading_payment.group_payment_method,
    es_profile_trading_payment.user_agent,
    es_profile_trading_payment.affiliate_uuid,
    es_profile_trading_payment.affiliate_name,
    es_profile_trading_payment.affiliate_referral,
    es_profile_trading_payment.affiliate_source,
    es_profile_trading_payment.execution_time,
    es_profile_trading_payment.index,
    es_profile_trading_payment.modified_by,
    es_profile_trading_payment.net_deposit,
    es_profile_trading_payment.net_deposit_usd,
    es_profile_trading_payment.last_note,
    es_profile_trading_payment.last_note_date,
    es_profile_trading_payment.questionnaire_status,
    es_profile_trading_payment.last_login,
    es_profile_trading_payment.last_trade_date,
    es_profile_trading_payment.deposit_count,
    es_profile_trading_payment.affiliate_type,
    es_profile_trading_payment.fsa_migration_status,
    es_profile_trading_payment.mt4_balance,
    es_profile_trading_payment.mt4_balance_currency,
    es_profile_trading_payment.first_note,
    es_profile_trading_payment.first_note_date,
    es_profile_trading_payment.ftd_amount,
    es_profile_trading_payment.ftd_currency,
    es_profile_trading_payment.ltd_time,
    es_profile_trading_payment.migration_id,
    es_profile_trading_payment.kyc_changed_at,
    es_profile_trading_payment.external_reference
   FROM public.es_profile_trading_payment
  WHERE ((es_profile_trading_payment.brand_id)::text = 'topinvestuspsp'::text);


ALTER TABLE hrzn07_brand_topinvestuspsp.es_profile_trading_payment OWNER TO postgres;

--
-- Name: TABLE es_profile_trading_payment; Type: ACL; Schema: hrzn07_brand_topinvestuspsp; Owner: postgres
--

GRANT SELECT ON TABLE hrzn07_brand_topinvestuspsp.es_profile_trading_payment TO rezo;
GRANT SELECT ON TABLE hrzn07_brand_topinvestuspsp.es_profile_trading_payment TO ybashkatov;


--
-- PostgreSQL database dump complete
--

