--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

--
-- Name: group_securities; Type: TABLE; Schema: stg_tables; Owner: data_sync
--

CREATE TABLE stg_tables.group_securities (
    server_id smallint NOT NULL,
    "group" character varying(16) NOT NULL,
    security_id smallint NOT NULL,
    show boolean NOT NULL,
    trade boolean NOT NULL,
    execution smallint NOT NULL,
    commission_base double precision NOT NULL,
    commission_type smallint NOT NULL,
    commission_lots smallint NOT NULL,
    commission_agent double precision NOT NULL,
    commission_agent_type integer NOT NULL,
    spread_diff integer NOT NULL,
    lot_min integer NOT NULL,
    lot_max integer NOT NULL,
    lot_step integer NOT NULL,
    ie_deviation integer NOT NULL,
    confirmation integer NOT NULL,
    trade_rights smallint NOT NULL,
    ie_quick_mode integer NOT NULL,
    auto_closeout_mode smallint NOT NULL,
    commission_tax double precision NOT NULL,
    commission_agents_lots smallint NOT NULL,
    free_margin_mode integer NOT NULL,
    prod_num smallint
);


ALTER TABLE stg_tables.group_securities OWNER TO data_sync;

--
-- Name: group_securities group_securities_pkey; Type: CONSTRAINT; Schema: stg_tables; Owner: data_sync
--

ALTER TABLE ONLY stg_tables.group_securities
    ADD CONSTRAINT group_securities_pkey PRIMARY KEY (server_id, "group", security_id);


--
-- Name: TABLE group_securities; Type: ACL; Schema: stg_tables; Owner: data_sync
--

GRANT SELECT ON TABLE stg_tables.group_securities TO ybashkatov;


--
-- PostgreSQL database dump complete
--

