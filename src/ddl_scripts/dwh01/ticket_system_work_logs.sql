--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

--
-- Name: work_logs; Type: TABLE; Schema: ticket_system; Owner: aleleko
--

CREATE TABLE ticket_system.work_logs (
    id integer NOT NULL,
    users_id integer NOT NULL,
    tickets_id integer NOT NULL,
    time_spent integer NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    comment character varying(255)
);


ALTER TABLE ticket_system.work_logs OWNER TO aleleko;

--
-- Name: work_logs work_logs_pkey; Type: CONSTRAINT; Schema: ticket_system; Owner: aleleko
--

ALTER TABLE ONLY ticket_system.work_logs
    ADD CONSTRAINT work_logs_pkey PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--

