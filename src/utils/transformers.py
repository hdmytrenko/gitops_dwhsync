import collections
from datetime import datetime
from pymongo import cursor
from pyspark.sql.functions import col, concat, lit
from sqlalchemy import Table, inspect

from utils.common import write_log, enrich_year_month
from utils.constants import SynchronizationState
from utils.readers import create_filter, get_max_date, create_mongo_filter, get_max_date_by_prod_num
from typing import Union, Callable

from dwh_sync.dwh.entities import entities_for_matviews_creation
from utils.readers import get_max_id_by_prod_num


def get_filter_or_backfill(log_state: SynchronizationState) -> Union[str, bool]:
    """
    Check if we need to always do backfill or just to sync last one copies
    :param log_state: instance of SynchronizationState class which consists of log arguments
    :return: filter expression for sync
    """
    if isinstance(log_state.table_name.filter_column, collections.Callable):
        f = True
    else:
        f = create_filter(log_state, get_max_date(log_state))
    return f


def create_cursor_mongo_reconciliation(log_state: SynchronizationState, filter_expression: dict) -> cursor:
    """
    :param filter_expression: filter expression for getting required date range
    :param log_state: instance of SynchronizationState class which consists of log arguments
    :return: pymongo cursor for future merge with mongo
    """
    return log_state.source_session[log_state.table_name.mongo_collection] \
        .find(filter_expression, no_cursor_timeout=True) \
        .sort([(log_state.table_name.mongo_filter_field_reconciliation, 1)])


def compare_metrics(log_state: SynchronizationState, src_metrics: list, dwh_metrics: list) -> list:
    """
    Compare two list of data from dwh and source adn returns lists
    :param log_state: instance of SynchronizationState class which consists of log arguments
    :param src_metrics: list of objects from source database
    :param dwh_metrics: list of objects from dwh database
    :return: list of datetime objects
    """
    bad_hours = []
    src_dict = {str(src_metric.group_key) + ':' + str(src_metric.hour): src_metric.row_cnt
                for src_metric in src_metrics}
    dwh_dict = {str(dwh_metric.group_key) + ':' + str(dwh_metric.hour): dwh_metric.row_cnt
                for dwh_metric in dwh_metrics}
    for key, value in src_dict.items():
        if key not in dwh_dict or dwh_dict.get(key, 0) != value:
            bad_hours.append(datetime.strptime(key.split(':')[1], '%Y-%m-%d %H'))
            log_state.message = f"Found bad hour source row count: {value} vs DWH row count {dwh_dict.get(key, 0)}. " \
                                f"Bad hour is {key.split(':')[1]}"
            write_log(log_state)
    if len(bad_hours) > 0:
        log_state.message = f'Reconciliation for {log_state.table_name.__tablename__} needed'
        write_log(log_state)
    return bad_hours


def modify_df_for_writing(df: list, dwh_table: Table, log_state: SynchronizationState) -> tuple:
    """
    Adds prod num to each row of row set
    :param df: list rows
    :param dwh_table: entity of dwh table
    :param log_state: instance of SynchronizationState class which consists of log arguments
    :return: list of modified rows
    """
    df_with_prod_num = []
    postgres_months = set()
    postgres_ids = dict()
    for row in df:
        row_to_dict = inspect(row).dict
        del row_to_dict['_sa_instance_state']
        for key in dwh_table.uuids():
            if row_to_dict.get(key) is None:
                row_to_dict[key + '_compound'] = None
            else:
                row_to_dict[key + '_compound'] = str(row_to_dict.get(key, '')) + '-' + str(log_state.prod_number)
        if log_state.table_name.__tablename__ in entities_for_matviews_creation.values():
            postgres_ids = {dwh_table.uuid_for_matview(): set()}
            postgres_ids[dwh_table.uuid_for_matview()].add(row_to_dict.get(dwh_table.uuid_for_matview(), ''))
            postgres_months = enrich_year_month(log_state, row_to_dict, postgres_months)
        modified_row = dwh_table(**row_to_dict)
        modified_row.prod_num = log_state.prod_number
        df_with_prod_num.append(modified_row)
    return df_with_prod_num, postgres_ids, postgres_months


def create_cursor_mongo_with_prod_num(log_state: SynchronizationState, dwh_table: Table) -> cursor:
    """
    :param log_state: instance of SynchronizationState class which consists of log arguments
    :param dwh_table: instance of dwh table with prod_num column
    :return: pymongo cursor for future merge with mongo
    """
    return log_state.source_session[log_state.table_name.mongo_collection] \
        .find(create_mongo_filter(log_state, get_max_date_by_prod_num(log_state, dwh_table)),
              no_cursor_timeout=True) \
        .sort([(log_state.table_name.mongo_filter_field, 1)])


def get_filter_or_backfill_by_prod_num(
        log_state: SynchronizationState,
        dwh_table: Table,
        filter_func: Callable = get_max_date_by_prod_num
) -> Union[str, bool]:
    """
    Check if we need to always do backfill or just to sync last one copies
    :param filter_func: function for filter creation
    :param log_state: instance of SynchronizationState class which consists of log arguments
    :param dwh_table: instance of dwh table with prod_num column
    :return: filter expression for sync
    """

    if isinstance(log_state.table_name.filter_column, collections.Callable):
        f = True
    else:
        f = create_filter(log_state, filter_func(log_state, dwh_table))
    return f


def get_ids_for_months(log_state: SynchronizationState, entity_for_ids: Table, ids: dict):
    """
    Write ids into required table
    :param log_state: instance of SynchronizationState class which consists of log arguments
    :param entity_for_ids: entity for table with ids for sync table
    :param ids: dictionary of ids
    :return:
    """
    log_state.dwh_session.execute(f"TRUNCATE TABLE stg_tables_partitioned.{entity_for_ids.__tablename__};")
    log_state.dwh_session.commit()
    for id_key, id_value in ids.items():
        for value in id_value:
            setattr(entity_for_ids, id_key, value)
            entity_for_ids.prod_num = log_state.prod_number
            log_state.dwh_session.add(entity_for_ids)
        log_state.dwh_session.commit()


def flatten_df_second_layer(df):
    """
    Flattens dataframe up to second layer
    :param df: unflatted dataframe
    :return: flattede dataframe
    """
    # TODO rewrite using explode
    for field in df.schema:
        if field.dataType.typeName() == 'struct':
            for subfield in field.jsonValue()['type']['fields']:
                df = df.withColumn(f"{field.name}_{subfield['name']}", col(f"{field.name}.{subfield['name']}"))
                if type(subfield['type']) == dict:
                    for subsubfield in subfield['type']['fields']:
                        df = df.withColumn(f"{field.name}_{subfield['name']}_{subsubfield['name']}",
                                           col(f"{field.name}.{subfield['name']}.{subsubfield['name']}"))
            df = df.drop(f"{field.name}")
    return df


def add_compound_columns(df, log_state):
    for column in log_state.table_name.uuids:
        df = df.withColumn(column+'_compound', concat(column, lit('-'), lit(log_state.prod_number)))
    return df

