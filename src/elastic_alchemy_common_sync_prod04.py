import sys
import uuid

from elasticsearch import Elasticsearch

from dwh_sync.dwh.mapping_writer_classes_ecosales import ProfileExtDWH
from utils.common import create_session_new, create_config_dictionary, obtain_lock, write_log, write_exception
from utils.constants import SynchronizationState, LOCK_QUERY, DbType, ProdNumber, EngineType
from utils.readers import create_elasticsearch_query, get_elastic_sync_size, get_max_date_by_prod_num
from utils.writers import write_rows_from_elasticsearch

tables = [
    {'dbname': 'profile', 'table_name': ProfileExtDWH, 'index': 'profile'}

]
lock_record = LOCK_QUERY
config_path = "settings_elastic_prod04.ini"

log_session = create_session_new(create_config_dictionary(config_path,
                                                          DbType.DWH.value,
                                                          engine_type=EngineType.postgres.value))
dwh_session = create_session_new(create_config_dictionary(config_path,
                                                          DbType.DWH.value,
                                                          schema_translate_map=True,
                                                          engine_type=EngineType.postgres.value))
lock_session = create_session_new(create_config_dictionary(config_path,
                                                           DbType.DWH.value,
                                                           engine_type=EngineType.postgres.value))
log_state = SynchronizationState(log_session,
                                 uuid.uuid1(),
                                 dwh_session=dwh_session,
                                 lock_session=lock_session,
                                 prod_number=ProdNumber.prod04.value,
                                 sync_name=__file__)


def main():
    for table in tables:
        try:
            log_state.source_session = Elasticsearch(
                hosts=create_config_dictionary(config_path, DbType.SOURCE.value)
                ["elastic_host"])
            log_state.table_name = table['table_name']
            if not obtain_lock(log_state.lock_session, LOCK_QUERY, log_state):
                continue
            log_state.message = f"size of sync is at least  " \
                                f"{get_elastic_sync_size(log_state)} rows for " \
                                f"{log_state.table_name.__tablename__}"
            write_log(log_state)
            write_rows_from_elasticsearch(
                create_elasticsearch_query(log_state, get_max_date_by_prod_num(log_state, log_state.table_name)),
                log_state,
                table['index'])
            log_state.message = f"table {table['table_name'].__tablename__} synchronized"
            write_log(log_state)
            log_state.lock_session.rollback()
        except Exception as ex:
            print(f'\n{"-" * 40}\nException: {str(ex)}\n{"-" * 40}\n')
            log_state.message = f"DWH PROD04 Exception: {str(ex)}"
            log_state.lock_session.rollback()
            if 'NotFoundError' in str(ex):
                print('Oh still that uncaught')
                write_log(log_state)
            else:
                write_exception(log_state)


if __name__ == "__main__":
    sys.exit(main())
