from datetime import timedelta

from bson import Code
from elasticsearch_dsl import Search
from sqlalchemy import func, or_, cast, Numeric, literal_column
from sqlalchemy.dialects.postgresql import aggregate_order_by
from sqlalchemy.orm import query

from utils.common import get_datetime
from utils.constants import Result


class AffiliateDetails:
    @staticmethod
    def generate_query(log_state, session) -> query:
        return session.query(func.date_trunc('hour', log_state.table_name.filter_column_reconciliation).label("hour"),
                             log_state.table_name.brand.label("group_key"),
                             func.md5(func.string_agg(func.concat(log_state.table_name.first_name,
                                                                  log_state.table_name.last_name,
                                                                  log_state.table_name.email,
                                                                  log_state.table_name.phone,
                                                                  log_state.table_name.uuid,
                                                                  log_state.table_name.status,
                                                                  log_state.table_name.status_reason),
                                                      aggregate_order_by(":", log_state.table_name.id)))
                             .label("metric_sum"),
                             func.count(log_state.table_name.id).label("row_cnt"))

    def generate_base_filter(self, log_state, session) -> query:
        return self.generate_query(log_state, session).filter(log_state.table_name.deleted.is_(False),
                                                              log_state.table_name.email.notilike('%newagesol.com'),
                                                              or_(log_state.table_name.first_name.is_(None),
                                                                  log_state.table_name.first_name.notilike('test'),
                                                                  log_state.table_name.first_name.notilike('trash')),
                                                              or_(log_state.table_name.last_name.is_(None),
                                                                  log_state.table_name.last_name.notilike('test'),
                                                                  log_state.table_name.last_name.notilike('trash')))

    def generate_date_filter(self, log_state, session, date_start, date_stop):
        return self.generate_base_filter(log_state, session) \
            .filter(log_state.table_name.filter_column_reconciliation >= date_start,
                    log_state.table_name.filter_column_reconciliation < date_stop)

    def generate_group_by(self, log_state, session, date_start, date_stop):
        return self.generate_date_filter(log_state, session, date_start, date_stop).group_by(
            log_state.table_name.brand,
            func.date_trunc('hour', log_state.table_name.filter_column_reconciliation))

    def get_all(self, log_state, session, date_start, date_stop):
        return self.generate_group_by(log_state, session, date_start, date_stop).all()


class PaymentDetails:
    @staticmethod
    def generate_query(log_state, session) -> query:
        return session.query(func.date_trunc('hour', log_state.table_name.filter_column_reconciliation).label("hour"),
                             func.concat(log_state.table_name.brand_id, log_state.table_name.status).label("group_key"),
                             func.count(log_state.table_name.id).label("row_cnt"),
                             func.concat(cast(func.sum(log_state.table_name.normalized_amount), Numeric(20, 5)),
                                         func.md5(func.string_agg(func.concat(log_state.table_name.account_type,
                                                                              log_state.table_name.account_uuid,
                                                                              log_state.table_name.affiliate_uuid,
                                                                              log_state.table_name.agent_id,
                                                                              log_state.table_name.country,
                                                                              log_state.table_name.language,
                                                                              log_state.table_name.type),
                                                                  aggregate_order_by("':'", log_state.table_name.id))))
                             .label("metric_sum"))

    def generate_base_filter(self, log_state, session) -> query:
        return self.generate_query(log_state, session) \
            .filter(log_state.table_name.deleted.is_(False),
                    or_(log_state.table_name.payment_method.is_(None),
                        log_state.table_name.payment_method.notilike('fakepal')),
                    or_(log_state.table_name.profile_first_name.is_(None),
                        log_state.table_name.profile_first_name.notilike('test')),
                    or_(log_state.table_name.profile_last_name.is_(None),
                        log_state.table_name.profile_last_name.notilike('test')),
                    or_(log_state.table_name.brand_id.is_(None),
                        log_state.table_name.brand_id.notilike('dobby')))

    def generate_date_filter(self, log_state, session, date_start, date_stop):
        return self.generate_base_filter(log_state, session) \
            .filter(log_state.table_name.filter_column_reconciliation >= date_start,
                    log_state.table_name.filter_column_reconciliation < date_stop)

    def generate_group_by(self, log_state, session, date_start, date_stop):
        return self.generate_date_filter(log_state, session, date_start, date_stop).group_by(
            func.concat(log_state.table_name.brand_id, log_state.table_name.status),
            func.date_trunc('hour', log_state.table_name.filter_column_reconciliation))

    def get_all(self, log_state, session, date_start, date_stop):
        return self.generate_group_by(log_state, session, date_start, date_stop).all()


class OperatorDetails:
    @staticmethod
    def generate_query(log_state, session) -> query:
        return session.query(func.date_trunc('hour', log_state.table_name.filter_column_reconciliation).label("hour"),
                             log_state.table_name.status.label("group_key"),
                             func.md5(func.string_agg(func.concat(
                                 log_state.table_name.first_name,
                                 log_state.table_name.last_name,
                                 log_state.table_name.email,
                                 log_state.table_name.uuid,
                                 log_state.table_name.operator_role),
                                 aggregate_order_by("':'", log_state.table_name.id))).label("metric_sum"),
                             func.count(log_state.table_name.id).label("row_cnt"))

    def generate_base_filter(self, log_state, session) -> query:
        return self.generate_query(log_state, session) \
            .filter(log_state.table_name.email.notilike('%newagesol.com'),
                    or_(log_state.table_name.first_name.is_(None),
                        log_state.table_name.first_name.notilike('test'),
                        log_state.table_name.first_name.notilike('trash')),
                    or_(log_state.table_name.last_name.is_(None),
                        log_state.table_name.last_name.notilike('test'),
                        log_state.table_name.last_name.notilike('trash')),
                    log_state.table_name.uuid.notilike('test%'),
                    log_state.table_name.uuid.notilike('uat%'))

    def generate_date_filter(self, log_state, session, date_start, date_stop):
        return self.generate_base_filter(log_state, session) \
            .filter(log_state.table_name.filter_column_reconciliation >= date_start,
                    log_state.table_name.filter_column_reconciliation < date_stop)

    def generate_group_by(self, log_state, session, date_start, date_stop):
        return self.generate_date_filter(log_state, session, date_start, date_stop).group_by(
            log_state.table_name.status,
            func.date_trunc('hour', log_state.table_name.filter_column_reconciliation))

    def get_all(self, log_state, session, date_start, date_stop):
        return self.generate_group_by(log_state, session, date_start, date_stop).all()


class CallbackDetails:
    @staticmethod
    def generate_query(log_state, session) -> query:
        return session.query(func.date_trunc('hour', log_state.table_name.filter_column_reconciliation).label("hour"),
                             log_state.table_name.brand_id.label("group_key"),
                             func.md5(func.string_agg(func.concat(log_state.table_name.user_id,
                                                                  log_state.table_name.operator_id,
                                                                  log_state.table_name.status,
                                                                  log_state.table_name.callback_id,
                                                                  log_state.table_name.created_by),
                                                      aggregate_order_by("':'", log_state.table_name.id)))
                             .label("metric_sum"),
                             func.count(log_state.table_name.id).label("row_cnt"))

    def generate_base_filter(self, log_state, session) -> query:
        return self.generate_query(log_state, session) \
            .filter(log_state.table_name.brand_id != 'dobby')

    def generate_date_filter(self, log_state, session, date_start, date_stop):
        return self.generate_base_filter(log_state, session) \
            .filter(log_state.table_name.filter_column_reconciliation >= date_start,
                    log_state.table_name.filter_column_reconciliation < date_stop)

    def generate_group_by(self, log_state, session, date_start, date_stop):
        return self.generate_date_filter(log_state, session, date_start, date_stop).group_by(
            log_state.table_name.brand_id,
            func.date_trunc('hour', log_state.table_name.filter_column_reconciliation))

    def get_all(self, log_state, session, date_start, date_stop):
        return self.generate_group_by(log_state, session, date_start, date_stop).all()


class TradeRecordDetails:
    @staticmethod
    def generate_query(log_state, session) -> query:
        return session.query(func.date_trunc('hour', log_state.table_name.filter_column_reconciliation).label("hour"),
                             log_state.table_name.symbol.label("group_key"),
                             func.count(log_state.table_name.id).label("row_cnt"),
                             func.sum(log_state.table_name.profit).label("metric_sum"))

    def generate_base_filter(self, log_state, session) -> query:
        return self.generate_query(log_state, session).filter(log_state.table_name.trade_type.notilike('demo'))

    def generate_date_filter(self, log_state, session, date_start, date_stop):
        return self.generate_base_filter(log_state, session) \
            .filter(log_state.table_name.filter_column_reconciliation >= date_start,
                    log_state.table_name.filter_column_reconciliation < date_stop)

    def generate_group_by(self, log_state, session, date_start, date_stop):
        return self.generate_date_filter(log_state, session, date_start, date_stop).group_by(
            log_state.table_name.symbol,
            func.date_trunc('hour', log_state.table_name.filter_column_reconciliation))

    def get_all(self, log_state, session, date_start, date_stop):
        return self.generate_group_by(log_state, session, date_start, date_stop).all()


class LeadDetails:
    @staticmethod
    def generate_query(log_state, session) -> query:
        return session.query(func.date_trunc('hour', log_state.table_name.filter_column_reconciliation).label("hour"),
                             log_state.table_name.brand_id.label("group_key"),
                             func.md5(func.string_agg(func.concat(log_state.table_name.name,
                                                                  log_state.table_name.surname,
                                                                  log_state.table_name.email,
                                                                  log_state.table_name.phone,
                                                                  log_state.table_name.country,
                                                                  log_state.table_name.source,
                                                                  log_state.table_name.status),
                                                      aggregate_order_by("':'", log_state.table_name.id)))
                             .label("metric_sum"),
                             func.count(log_state.table_name.id).label("row_cnt"))

    def generate_base_filter(self, log_state, session) -> query:
        return self.generate_query(log_state, session).filter(log_state.table_name.email.notilike('%newagesol.com'),
                                                              or_(log_state.table_name.name.is_(None),
                                                                  log_state.table_name.name.notilike('test'),
                                                                  log_state.table_name.name.notilike('trash')),
                                                              or_(log_state.table_name.surname.is_(None),
                                                                  log_state.table_name.surname.notilike('test'),
                                                                  log_state.table_name.surname.notilike('trash')))

    def generate_date_filter(self, log_state, session, date_start, date_stop):
        return self.generate_base_filter(log_state, session) \
            .filter(log_state.table_name.filter_column_reconciliation >= date_start,
                    log_state.table_name.filter_column_reconciliation < date_stop)

    def generate_group_by(self, log_state, session, date_start, date_stop):
        return self.generate_date_filter(log_state, session, date_start, date_stop).group_by(
            log_state.table_name.brand_id,
            func.date_trunc('hour', log_state.table_name.filter_column_reconciliation))

    def get_all(self, log_state, session, date_start, date_stop):
        return self.generate_group_by(log_state, session, date_start, date_stop).all()


class TradesDetails:
    @staticmethod
    def generate_query(log_state, session) -> query:
        return session.query(func.date_trunc('hour', log_state.table_name.filter_column_reconciliation)
                             .label("hour"),
                             log_state.table_name.symbol.label("group_key"),
                             func.md5(func.string_agg(func.concat(log_state.table_name.opening_time,
                                                                  log_state.table_name.closing_time,
                                                                  log_state.table_name.cmd,
                                                                  log_state.table_name.comment,
                                                                  log_state.table_name.login),
                                                      aggregate_order_by("':'", log_state.table_name.order)))
                             .label("metric_sum"),
                             func.count(log_state.table_name.order).label("row_cnt"))

    def generate_date_filter(self, log_state, session, date_start, date_stop):
        return self.generate_query(log_state, session) \
            .filter(log_state.table_name.filter_column_reconciliation >= date_start,
                    log_state.table_name.filter_column_reconciliation < date_stop)

    def generate_group_by(self, log_state, session, date_start, date_stop):
        return self.generate_date_filter(log_state, session, date_start, date_stop).group_by(
            log_state.table_name.symbol,
            func.date_trunc('hour', log_state.table_name.filter_column_reconciliation))

    def get_all(self, log_state, session, date_start, date_stop):
        return self.generate_group_by(log_state, session, date_start, date_stop).all()


class UsersDetails:
    @staticmethod
    def generate_query(log_state, session) -> query:
        return session.query(func.date_trunc('hour', log_state.table_name.filter_column_reconciliation).label("hour"),
                             log_state.table_name.server_id.label("group_key"),
                             func.md5(func.string_agg(func.concat(log_state.table_name.login,
                                                                  log_state.table_name.name,
                                                                  log_state.table_name.email,
                                                                  log_state.table_name.group,
                                                                  log_state.table_name.country,
                                                                  log_state.table_name.city,
                                                                  log_state.table_name.state,
                                                                  log_state.table_name.zip_code,
                                                                  log_state.table_name.address,
                                                                  log_state.table_name.leverage,
                                                                  log_state.table_name.previous_month_balance,
                                                                  log_state.table_name.previous_balance),
                                                      aggregate_order_by("':'", log_state.table_name.login)))
                             .label("metric_sum"),
                             func.count(log_state.table_name.login).label("row_cnt"))

    def generate_base_filter(self, log_state, session) -> query:
        return self.generate_query(log_state, session).filter(
            log_state.table_name.deleted_at.is_(None), or_(log_state.table_name.email.is_(None),
                                                           log_state.table_name.email.notilike('%newagesol.com')))

    def generate_date_filter(self, log_state, session, date_start, date_stop):
        return self.generate_base_filter(log_state, session) \
            .filter(log_state.table_name.filter_column_reconciliation >= date_start,
                    log_state.table_name.filter_column_reconciliation < date_stop)

    def generate_group_by(self, log_state, session, date_start, date_stop):
        return self.generate_date_filter(log_state, session, date_start, date_stop).group_by(
            log_state.table_name.server_id,
            func.date_trunc('hour', log_state.table_name.filter_column_reconciliation))

    def get_all(self, log_state, session, date_start, date_stop):
        return self.generate_group_by(log_state, session, date_start, date_stop).all()


class ProfileDetails:
    @staticmethod
    def generate_query(log_state, session) -> query:
        return session.query(func.date_trunc('hour', log_state.table_name.filter_column_reconciliation).label("hour"),
                             literal_column("'empty'").label("group_key"),
                             literal_column("0").label("metric_sum"),
                             func.count(log_state.table_name.player_uuid).label("row_cnt"))

    def generate_base_filter(self, log_state, session) -> query:
        return self.generate_query(log_state, session).filter(
            or_(log_state.table_name.brand_id.is_(None), log_state.table_name.brand_id != 'dobby'),
            or_(log_state.table_name.first_name.is_(None), log_state.table_name.first_name.notilike('test')),
            or_(log_state.table_name.last_name.is_(None), log_state.table_name.last_name.notilike('test')))

    def generate_date_filter(self, log_state, session, date_start, date_stop):
        return self.generate_base_filter(log_state, session) \
            .filter(log_state.table_name.filter_column_reconciliation >= date_start,
                    log_state.table_name.filter_column_reconciliation < date_stop)

    def generate_group_by(self, log_state, session, date_start, date_stop):
        return self.generate_date_filter(log_state, session, date_start, date_stop) \
            .group_by(func.date_trunc('hour', log_state.table_name.filter_column_reconciliation))

    def get_all(self, log_state, session, date_start, date_stop):
        return self.generate_group_by(log_state, session, date_start, date_stop).all()

    @staticmethod
    def generate_map_mongo(log_state):
        return Code("function() "
                    "{emit(this." + log_state.table_name.mongo_filter_field_reconciliation + ".getHours(), 1);}")

    @staticmethod
    def generate_reduce_mongo():
        return Code("function(keyId, valuesCount) {"
                    "   return Array.sum(valuesCount);}")

    @staticmethod
    def generate_date_filter_mongo(log_state, date_start, date_stop):
        return {log_state.table_name.mongo_filter_field_reconciliation: {'$gte': date_start, '$lt': date_stop}}

    def generate_map_reduce(self, log_state, date_start, date_stop):
        return log_state.source_session[log_state.table_name.mongo_collection].map_reduce(
            self.generate_map_mongo(log_state),
            self.generate_reduce_mongo(),
            out={"inline": 1},
            query=self.generate_date_filter_mongo(log_state, date_start, date_stop))

    @staticmethod
    def generate_result(list_of_dict, current_day):
        resulting_list = []
        for dicts in list_of_dict:
            resulting_list.append(Result(current_day, dicts['_id'], dicts['value']))
        return resulting_list

    def get_all_mongo(self, log_state, date_start):
        results = []
        for x in range(log_state.reconciliation_interval_days):
            results.extend(self.generate_result(self.generate_map_reduce(log_state, date_start + timedelta(days=x - 1),
                                                                         date_start + timedelta(days=x))['results'],
                                                date_start + timedelta(days=x)))
        return results


class TradingAccountDetails:
    @staticmethod
    def generate_query(log_state, session) -> query:
        return session.query(func.date_trunc('hour', log_state.table_name.filter_column_reconciliation).label("hour"),
                             literal_column("'empty'").label("group_key"),
                             literal_column("0").label("metric_sum"),
                             func.count(log_state.table_name._id).label("row_cnt"))

    def generate_base_filter(self, log_state, session) -> query:
        return self.generate_query(log_state, session)

    def generate_date_filter(self, log_state, session, date_start, date_stop):
        return self.generate_base_filter(log_state, session) \
            .filter(log_state.table_name.filter_column_reconciliation >= date_start,
                    log_state.table_name.filter_column_reconciliation < date_stop)

    def generate_group_by(self, log_state, session, date_start, date_stop):
        return self.generate_date_filter(log_state, session, date_start, date_stop) \
            .group_by(func.date_trunc('hour', log_state.table_name.filter_column_reconciliation))

    def get_all(self, log_state, session, date_start, date_stop):
        return self.generate_group_by(log_state, session, date_start, date_stop).all()

    @staticmethod
    def generate_map_mongo(log_state):
        return Code("function() "
                    "{emit(this." + log_state.table_name.mongo_filter_field_reconciliation + ".getHours(), 1);}")

    @staticmethod
    def generate_reduce_mongo():
        return Code("function(keyId, valuesCount) {"
                    "   return Array.sum(valuesCount);}")

    @staticmethod
    def generate_date_filter_mongo(log_state, date_start, date_stop):
        return {log_state.table_name.mongo_filter_field_reconciliation: {'$gte': date_start, '$lt': date_stop}}

    def generate_map_reduce(self, log_state, date_start, date_stop):
        return log_state.source_session[log_state.table_name.mongo_collection].map_reduce(
            self.generate_map_mongo(log_state),
            self.generate_reduce_mongo(),
            out={"inline": 1},
            query=self.generate_date_filter_mongo(log_state, date_start, date_stop))

    @staticmethod
    def generate_result(list_of_dict, current_day):
        resulting_list = []
        for dicts in list_of_dict:
            resulting_list.append(Result(current_day, dicts['_id'], dicts['value']))
        return resulting_list

    def get_all_mongo(self, log_state, date_start):
        results = []
        for x in range(log_state.reconciliation_interval_days):
            results.extend(self.generate_result(self.generate_map_reduce(log_state, date_start + timedelta(days=x - 1),
                                                                         date_start + timedelta(days=x))['results'],
                                                date_start + timedelta(days=x)))
        return results


class ProfileExtDetails:
    @staticmethod
    def generate_query(log_state, session) -> query:
        return session.query(func.date_trunc('hour', log_state.table_name.filter_column_reconciliation).label("hour"),
                             literal_column("'empty'").label("group_key"),
                             literal_column("0").label("metric_sum"),
                             func.count(log_state.table_name.player_uuid).label("row_cnt"))

    def generate_base_filter(self, log_state, session) -> query:
        return self.generate_query(log_state, session).filter(
            or_(log_state.table_name.brand_id.is_(None), log_state.table_name.brand_id != 'dobby'),
            or_(log_state.table_name.first_name.is_(None), log_state.table_name.first_name.notilike('test')),
            or_(log_state.table_name.last_name.is_(None), log_state.table_name.last_name.notilike('test')))

    def generate_date_filter(self, log_state, session, date_start, date_stop):
        return self.generate_base_filter(log_state, session) \
            .filter(log_state.table_name.filter_column_reconciliation >= date_start,
                    log_state.table_name.filter_column_reconciliation < date_stop)

    def generate_group_by(self, log_state, session, date_start, date_stop):
        return self.generate_date_filter(log_state, session, date_start, date_stop) \
            .group_by(func.date_trunc('hour', log_state.table_name.filter_column_reconciliation))

    def get_all(self, log_state, session, date_start, date_stop):
        return self.generate_group_by(log_state, session, date_start, date_stop).all()

    @staticmethod
    def get_search_elasticsearch(log_state):
        return Search(using=log_state.source_session, index='profile')

    @staticmethod
    def generate_date_range_elasticsearch(date_start, date_stop):
        return {"lastUpdatedDate": {'gte': date_start, 'lt': date_stop}}

    def generate_query_elasticsearch(self, log_state, date_start, date_stop):
        return {"query": {"bool": {"must": {"exists": {"field": "uuid"}},
                                   "filter": {"range": self.generate_date_range_elasticsearch(date_start, date_stop)}}},
                "size": 0,
                "aggs": {
                    "profile_by_hour": {
                        "date_histogram": {
                            "field": log_state.table_name.elastic_filter_field,
                            "fixed_interval": "1h"}}}
                }

    def get_all_elasticsearch(self, log_state, date_start, date_stop):
        class Result:
            def __init__(self, key_as_string, row_cnt):
                self.hour = str(get_datetime(key_as_string))
                self.group_key = 'empty'
                self.metric_sum = '0'
                self.row_cnt = int(row_cnt)

            def __repr__(self):
                return f"{self.hour}, {self.group_key}, {self.metric_sum}, {self.row_cnt}"

        s = self.get_search_elasticsearch(log_state).update_from_dict(self.generate_query_elasticsearch(log_state,
                                                                                                        date_start,
                                                                                                        date_stop))
        return [Result(result['key_as_string'], result['doc_count'])
                for result in s.execute().aggregations.profile_by_hour.buckets]
