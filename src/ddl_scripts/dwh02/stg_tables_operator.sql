--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

--
-- Name: operator; Type: TABLE; Schema: stg_tables; Owner: postgres
--

CREATE TABLE stg_tables.operator (
    id bigint NOT NULL,
    uuid character varying(255) NOT NULL,
    first_name character varying(255) NOT NULL,
    last_name character varying(255) NOT NULL,
    phone_number character varying(255),
    email character varying(255) NOT NULL,
    country character varying(255),
    registration_date timestamp without time zone NOT NULL,
    status character varying(255) NOT NULL,
    status_change_date timestamp without time zone,
    status_change_author character varying(255),
    registered_by character varying(255) NOT NULL,
    status_reason character varying(255) NOT NULL,
    operator_role character varying(15),
    didlogic_password character varying(100),
    external_affiliate_id bigint,
    updated_at timestamp without time zone,
    deleted boolean DEFAULT false NOT NULL,
    created_at timestamp without time zone,
    prod_num integer
);


ALTER TABLE stg_tables.operator OWNER TO postgres;

--
-- Name: operator stg_operator_pk; Type: CONSTRAINT; Schema: stg_tables; Owner: postgres
--

ALTER TABLE ONLY stg_tables.operator
    ADD CONSTRAINT stg_operator_pk PRIMARY KEY (id);


--
-- Name: operator stg_operator_uk_email; Type: CONSTRAINT; Schema: stg_tables; Owner: postgres
--

ALTER TABLE ONLY stg_tables.operator
    ADD CONSTRAINT stg_operator_uk_email UNIQUE (email);


--
-- Name: operator stg_operator_uk_uuid; Type: CONSTRAINT; Schema: stg_tables; Owner: postgres
--

ALTER TABLE ONLY stg_tables.operator
    ADD CONSTRAINT stg_operator_uk_uuid UNIQUE (uuid);


--
-- Name: operator uk_stg_operator_email; Type: CONSTRAINT; Schema: stg_tables; Owner: postgres
--

ALTER TABLE ONLY stg_tables.operator
    ADD CONSTRAINT uk_stg_operator_email UNIQUE (email);


--
-- Name: operator uk_stg_operator_uuid; Type: CONSTRAINT; Schema: stg_tables; Owner: postgres
--

ALTER TABLE ONLY stg_tables.operator
    ADD CONSTRAINT uk_stg_operator_uuid UNIQUE (uuid);


--
-- Name: idx_stg_operator_created_at; Type: INDEX; Schema: stg_tables; Owner: postgres
--

CREATE INDEX idx_stg_operator_created_at ON stg_tables.operator USING btree (created_at);


--
-- Name: idx_stg_operator_updated_at; Type: INDEX; Schema: stg_tables; Owner: postgres
--

CREATE INDEX idx_stg_operator_updated_at ON stg_tables.operator USING btree (updated_at);


--
-- Name: TABLE operator; Type: ACL; Schema: stg_tables; Owner: postgres
--

GRANT ALL ON TABLE stg_tables.operator TO data_sync;
GRANT SELECT ON TABLE stg_tables.operator TO ybashkatov;


--
-- PostgreSQL database dump complete
--

