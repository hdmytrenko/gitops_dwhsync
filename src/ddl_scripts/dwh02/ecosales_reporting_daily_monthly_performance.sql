--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: daily_monthly_performance; Type: VIEW; Schema: ecosales_reporting; Owner: postgres
--

CREATE VIEW ecosales_reporting.daily_monthly_performance AS
 SELECT dmp.payment_id,
    dmp.brand_id,
    dmp.creation_time,
    dmp.deposit_usd,
    dmp.execution_time,
    dmp.ftd,
    dmp.withdraw_usd,
    bo.owner_name,
    (((bo.owner_name)::text || ' - '::text) || (dmp.brand_id)::text) AS owner_brand,
    (((dmp.brand_id)::text || ' - '::text) || (bo.owner_name)::text) AS brand_owner
   FROM (stg_tables.daily_monthly_performance dmp
     LEFT JOIN stg_tables.brand_owner bo ON (((bo.brand_id)::text = (dmp.brand_id)::text)))
UNION ALL
 SELECT pv.payment_id,
    pv.brand_id,
    pv.creation_time,
    pv.deposit_usd,
    pv.execution_time,
    pv.ftd,
    pv.withdraw_usd,
    bo.owner_name,
    (((bo.owner_name)::text || ' - '::text) || (pv.brand_id)::text) AS owner_brand,
    (((pv.brand_id)::text || ' - '::text) || (bo.owner_name)::text) AS brand_owner
   FROM (public.payments_v pv
     LEFT JOIN stg_tables.brand_owner bo ON (((bo.brand_id)::text = (pv.brand_id)::text)))
  WHERE (((pv.brand_id)::text = ANY (ARRAY[('24newstrade'::character varying)::text, ('topinvestus'::character varying)::text, ('finansa'::character varying)::text, ('stockfx'::character varying)::text, ('fxactiv'::character varying)::text, ('finocapital'::character varying)::text, ('skycapital'::character varying)::text, ('cmarket'::character varying)::text, ('bridgefund'::character varying)::text])) AND ((date_trunc('month'::text, pv.execution_time) >= date_trunc('month'::text, (now() + '-3 mons'::interval))) OR (date_trunc('month'::text, pv.creation_time) >= date_trunc('month'::text, (now() + '-1 mons'::interval)))));


ALTER TABLE ecosales_reporting.daily_monthly_performance OWNER TO postgres;

--
-- Name: TABLE daily_monthly_performance; Type: ACL; Schema: ecosales_reporting; Owner: postgres
--

GRANT SELECT ON TABLE ecosales_reporting.daily_monthly_performance TO ybashkatov;


--
-- PostgreSQL database dump complete
--

