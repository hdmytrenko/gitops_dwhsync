--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

--
-- Name: symbols; Type: TABLE; Schema: stg_tables; Owner: data_sync
--

CREATE TABLE stg_tables.symbols (
    server_id smallint NOT NULL,
    swap_enable boolean NOT NULL,
    manager_login integer NOT NULL,
    security_id integer NOT NULL,
    digits integer NOT NULL,
    trade integer NOT NULL,
    background_color integer NOT NULL,
    count integer NOT NULL,
    count_original integer NOT NULL,
    realtime integer NOT NULL,
    starting integer NOT NULL,
    profit_mode integer NOT NULL,
    filter integer NOT NULL,
    filter_counter integer NOT NULL,
    filter_smoothing integer NOT NULL,
    logging integer NOT NULL,
    spread integer NOT NULL,
    spread_balance integer NOT NULL,
    execution_mode integer NOT NULL,
    swap_type integer NOT NULL,
    swap_rollover_3_days integer NOT NULL,
    stops_level integer NOT NULL,
    gtc_pendings integer NOT NULL,
    margin_mode integer NOT NULL,
    long_only integer NOT NULL,
    instant_max_volume integer NOT NULL,
    freeze_level integer NOT NULL,
    margin_hedged_strong integer NOT NULL,
    value_date integer NOT NULL,
    quotes_delay integer NOT NULL,
    swap_open_price integer NOT NULL,
    swap_variation_margin integer NOT NULL,
    expiration_time integer NOT NULL,
    symbol character varying(12) NOT NULL,
    description character varying(64),
    source character varying(12),
    currency character varying(12) NOT NULL,
    margin_currency character varying(12) NOT NULL,
    filter_limit double precision NOT NULL,
    swap_long double precision NOT NULL,
    swap_short double precision NOT NULL,
    contract_size double precision NOT NULL,
    tick_value double precision NOT NULL,
    tick_size double precision NOT NULL,
    margin_initial double precision NOT NULL,
    margin_maintenance double precision NOT NULL,
    margin_hedged double precision NOT NULL,
    margin_divider double precision NOT NULL,
    point double precision NOT NULL,
    multiply double precision NOT NULL,
    bid_tick_value double precision NOT NULL,
    ask_tick_value double precision NOT NULL,
    prod_num smallint
);


ALTER TABLE stg_tables.symbols OWNER TO data_sync;

--
-- Name: symbols symbols_pkey; Type: CONSTRAINT; Schema: stg_tables; Owner: data_sync
--

ALTER TABLE ONLY stg_tables.symbols
    ADD CONSTRAINT symbols_pkey PRIMARY KEY (server_id, symbol);


--
-- Name: TABLE symbols; Type: ACL; Schema: stg_tables; Owner: data_sync
--

GRANT SELECT ON TABLE stg_tables.symbols TO ybashkatov;


--
-- PostgreSQL database dump complete
--

