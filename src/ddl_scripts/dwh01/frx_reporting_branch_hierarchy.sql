--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: branch_hierarchy; Type: VIEW; Schema: frx_reporting; Owner: postgres
--

CREATE VIEW frx_reporting.branch_hierarchy AS
 SELECT branch_hierarchy.dtype,
    branch_hierarchy.id,
    branch_hierarchy.created_at,
    branch_hierarchy.deleted_at,
    branch_hierarchy.name,
    branch_hierarchy.uuid,
    branch_hierarchy.desk_type,
    branch_hierarchy.language,
    branch_hierarchy.country,
    branch_hierarchy.default_branch_id,
    branch_hierarchy.default_branch_user_id,
    branch_hierarchy.parent_id,
    branch_hierarchy.brand_id,
    branch_hierarchy.assign_index,
    branch_hierarchy.version,
    branch_hierarchy.updated_at,
    branch_hierarchy.manager_id,
    branch_hierarchy.prod_num
   FROM stg_tables.branch_hierarchy
  WHERE ((branch_hierarchy.deleted_at IS NULL) AND ((branch_hierarchy.brand_id IS NULL) OR ((branch_hierarchy.brand_id)::text <> 'dobby'::text)) AND ((branch_hierarchy.name IS NULL) OR (upper((branch_hierarchy.name)::text) !~ similar_escape('%(TEST|UAT+)%'::text, NULL::text))));


ALTER TABLE frx_reporting.branch_hierarchy OWNER TO postgres;

--
-- Name: TABLE branch_hierarchy; Type: ACL; Schema: frx_reporting; Owner: postgres
--

GRANT SELECT ON TABLE frx_reporting.branch_hierarchy TO prod01_metabase;
GRANT SELECT ON TABLE frx_reporting.branch_hierarchy TO ybashkatov;


--
-- PostgreSQL database dump complete
--

