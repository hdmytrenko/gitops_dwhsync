--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: groups; Type: VIEW; Schema: ecosales_reporting; Owner: postgres
--

CREATE VIEW ecosales_reporting.groups AS
 SELECT groups.server_id,
    groups.enable,
    groups.use_swap,
    groups.hedge_prohibited,
    groups.stopout_skip_hedged,
    groups.manager_login,
    groups.timeout,
    groups.otp_mode,
    groups.copies,
    groups.reports,
    groups.default_leverage,
    groups.margin_call,
    groups.margin_mode,
    groups.margin_stopout,
    groups.news,
    groups.check_ie_prices,
    groups.max_positions,
    groups.close_reopen,
    groups.close_fifo,
    groups.hedge_large_leg,
    groups.margin_type,
    groups.archive_period,
    groups.archive_max_balance,
    groups.archive_pending_period,
    groups."group" AS group_name,
    groups.company,
    groups.signature,
    groups.support_page,
    groups.smtp_server,
    groups.smtp_login,
    groups.smtp_password,
    groups.support_email,
    groups.templates,
    groups.currency,
    groups.default_deposit,
    groups.credit,
    groups.interest_rate,
    groups.prod_num
   FROM stg_tables.groups;


ALTER TABLE ecosales_reporting.groups OWNER TO postgres;

--
-- Name: TABLE groups; Type: ACL; Schema: ecosales_reporting; Owner: postgres
--

GRANT SELECT ON TABLE ecosales_reporting.groups TO ybashkatov;


--
-- PostgreSQL database dump complete
--

