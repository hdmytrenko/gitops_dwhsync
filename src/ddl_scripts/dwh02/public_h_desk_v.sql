--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: h_desk_v; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.h_desk_v AS
 SELECT branch_hierarchy.parent_id AS office_id,
    branch_hierarchy.id AS desk_id,
    branch_hierarchy.dtype AS desk_dtype,
    branch_hierarchy.name AS desk_name,
    branch_hierarchy.desk_type,
    upper((branch_hierarchy.language)::text) AS desk_language
   FROM ecosales_reporting.branch_hierarchy
  WHERE ((branch_hierarchy.deleted_at IS NULL) AND ((branch_hierarchy.dtype)::text = 'DESK'::text) AND ((branch_hierarchy.name)::text !~~ '%UAT+%'::text));


ALTER TABLE public.h_desk_v OWNER TO postgres;

--
-- Name: TABLE h_desk_v; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.h_desk_v TO data_sync;
GRANT SELECT ON TABLE public.h_desk_v TO ybashkatov;


--
-- PostgreSQL database dump complete
--

