import collections
import sys
import uuid

from dwh_sync.dwh.mapping_classes_ecosales import Affiliate, Payment, Operator, \
    Callback, TradeRecord, Lead, Trades, Users, Securities, Groups, Symbols
from dwh_sync.dwh.mapping_writer_classes_ecosales import AffiliateDWH, PaymentDWH, OperatorDWH, \
    CallbackDWH,  TradeRecordDWH, LeadDWH, TradesDWH, UsersDWH, \
    SecuritiesDWH, GroupsDWH, SymbolsDWH
from utils.common import write_log, create_session_new, create_config_dictionary, obtain_lock, \
    get_sync_length, write_exception
from utils.constants import DbType, SynchronizationState, LOCK_QUERY, EngineType, ProdNumber
from utils.transformers import get_filter_or_backfill_by_prod_num
from utils.writers import write_modified_rows, refresh_mat_views, remove_prefetched_rows

tables = [
    {
        'table': Affiliate,
        'dbname': 'affiliate',
        'dwh_table': AffiliateDWH
    },
    {
        'table': Payment,
        'dbname': 'payment',
        'dwh_table': PaymentDWH
    },
    {
        'table': Operator,
        'dbname': 'operator',
        'dwh_table': OperatorDWH
    },
    {
        'table': Callback,
        'dbname': 'callback',
        'dwh_table': CallbackDWH
    },
    {
        'table': TradeRecord,
        'dbname': 'trading-activity',
        'dwh_table': TradeRecordDWH
    },
    {
        'table': Lead,
        'dbname': 'lead-updater',
        'dwh_table': LeadDWH
    },
    {
        'table': Trades,
        'dbname': 'mt4-connector-adapter',
        'dwh_table': TradesDWH
    },
    {
        'table': Users,
        'dbname': 'mt4-connector-adapter',
        'dwh_table': UsersDWH
    },
    {
        'table': Securities,
        'dbname': 'mt4-connector-adapter',
        'dwh_table': SecuritiesDWH
    },
    {
        'table': Groups,
        'dbname': 'mt4-connector-adapter',
        'dwh_table': GroupsDWH
    },
    {
        'table': Symbols,
        'dbname': 'mt4-connector-adapter',
        'dwh_table': SymbolsDWH
    }
]

config_path = "settings_ecosales.ini"
dwh_session = create_session_new(create_config_dictionary(config_path,
                                                          DbType.DWH.value,
                                                          schema_translate_map=True,
                                                          engine_type=EngineType.postgres.value))
log_session = create_session_new(create_config_dictionary(config_path,
                                                          DbType.DWH.value,
                                                          engine_type=EngineType.postgres.value))
lock_session = create_session_new(create_config_dictionary(config_path,
                                                           DbType.DWH.value,
                                                           engine_type=EngineType.postgres.value))
log_state = SynchronizationState(log_session,
                                 uuid.uuid1(),
                                 dwh_session=dwh_session,
                                 lock_session=lock_session,
                                 source_engine_type=EngineType.postgres.value,
                                 prod_number=ProdNumber.prod02.value,
                                 sync_name=__file__)
views = ["public.agent_v", "public.v_ftd", "public.profile_data_mv"]


def main():
    for table in tables:
        try:
            log_state.table_name = table['table']
            log_state.source_session = create_session_new(
                create_config_dictionary(config_path,
                                         DbType.SOURCE.value,
                                         dbname=table["dbname"],
                                         engine_type=EngineType.postgres.value))
            if not obtain_lock(log_state.lock_session, LOCK_QUERY, log_state):
                continue
            f = get_filter_or_backfill_by_prod_num(log_state, table['dwh_table'])
            if isinstance(log_state.table_name.filter_column, collections.Callable):
                remove_prefetched_rows(log_state, table['table'], table['dwh_table'])
            log_state.message = f"size of sync is no more than " \
                                f"{len(get_sync_length(log_state, f)) * log_state.batch_size} rows for " \
                                f"{log_state.table_name.__tablename__}"
            write_log(log_state)
            write_modified_rows(log_state, get_sync_length(log_state, f), f, table['dwh_table'])
            log_state.message = f"table {table['table'].__tablename__} synchronized"
            write_log(log_state)
            log_state.lock_session.rollback()
        except Exception as ex:
            print(f'\n{"-" * 40}\nException: {str(ex)}\n{"-" * 40}\n')
            log_state.message = f"DWH PROD02 Exception: {str(ex)}"
            write_exception(log_state)
            continue
    refresh_mat_views(LOCK_QUERY, views, log_state)


if __name__ == "__main__":
    sys.exit(main())
