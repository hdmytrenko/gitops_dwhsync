--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

--
-- Name: payment; Type: TABLE; Schema: stg_tables; Owner: postgres
--

CREATE TABLE stg_tables.payment (
    id bigint NOT NULL,
    login character varying(50),
    profile_id character varying(50),
    payment_id character varying(50),
    amount numeric,
    currency character varying(20),
    external_reference character varying(250),
    type character varying(30),
    status character varying(30),
    creation_time timestamp without time zone NOT NULL,
    country character varying(20),
    language character varying(20),
    brand_id character varying(20),
    payment_method character varying(55),
    is_published boolean DEFAULT false,
    payment_transaction_id character varying(50),
    expiration_date timestamp without time zone,
    version bigint DEFAULT 0 NOT NULL,
    profile_first_name character varying(100) DEFAULT NULL::character varying,
    profile_last_name character varying(100) DEFAULT NULL::character varying,
    client_ip character varying(50) DEFAULT NULL::character varying,
    created_by character varying(50) DEFAULT NULL::character varying,
    is_mobile boolean,
    user_agent character varying(100) DEFAULT NULL::character varying,
    profile_country character varying(20),
    payment_aggregator character varying(20),
    agent_name character varying(100),
    agent_id character varying(50),
    payment_migration_id character varying(30),
    user_migration_id character varying(30),
    normalized_amount numeric,
    decline_reason character varying(100),
    modification_time timestamp without time zone,
    modified_by character varying(50),
    ex_rate numeric,
    linked_transaction_id character varying(50),
    moto boolean DEFAULT false NOT NULL,
    last_operation_type character varying(55) DEFAULT 'CREATION'::character varying NOT NULL,
    updated_at timestamp without time zone,
    account_uuid character varying(128),
    deleted boolean DEFAULT false,
    prod_num integer,
    status_changed_at timestamp without time zone,
    first_time_deposit boolean,
    affiliate_uuid character varying(50),
    agent_branches text[],
    platform_type character varying(128) DEFAULT 'MT4'::character varying,
    account_type character varying(128) DEFAULT 'LIVE'::character varying,
    comment character varying(256),
    sequence_position integer
);


ALTER TABLE stg_tables.payment OWNER TO postgres;

--
-- Name: payment stg_payment_pk; Type: CONSTRAINT; Schema: stg_tables; Owner: postgres
--

ALTER TABLE ONLY stg_tables.payment
    ADD CONSTRAINT stg_payment_pk PRIMARY KEY (id);


--
-- Name: payment unique_stg_payment; Type: CONSTRAINT; Schema: stg_tables; Owner: postgres
--

ALTER TABLE ONLY stg_tables.payment
    ADD CONSTRAINT unique_stg_payment UNIQUE (payment_id);


--
-- Name: payment_creation_time_idx; Type: INDEX; Schema: stg_tables; Owner: postgres
--

CREATE INDEX payment_creation_time_idx ON stg_tables.payment USING btree (creation_time);


--
-- Name: payment_creation_time_month_idx; Type: INDEX; Schema: stg_tables; Owner: postgres
--

CREATE INDEX payment_creation_time_month_idx ON stg_tables.payment USING btree (date_trunc('month'::text, creation_time));


--
-- Name: payment_updated_at_idx; Type: INDEX; Schema: stg_tables; Owner: postgres
--

CREATE INDEX payment_updated_at_idx ON stg_tables.payment USING btree (updated_at);


--
-- Name: stg_payment_agent_id_idx; Type: INDEX; Schema: stg_tables; Owner: postgres
--

CREATE INDEX stg_payment_agent_id_idx ON stg_tables.payment USING btree (agent_id);


--
-- Name: stg_payment_brand_id_idx; Type: INDEX; Schema: stg_tables; Owner: postgres
--

CREATE INDEX stg_payment_brand_id_idx ON stg_tables.payment USING btree (brand_id);


--
-- Name: stg_payment_creation_time_idx; Type: INDEX; Schema: stg_tables; Owner: postgres
--

CREATE INDEX stg_payment_creation_time_idx ON stg_tables.payment USING gist (creation_time);


--
-- Name: stg_payment_external_reference_idx; Type: INDEX; Schema: stg_tables; Owner: postgres
--

CREATE INDEX stg_payment_external_reference_idx ON stg_tables.payment USING btree (external_reference);


--
-- Name: stg_payment_profile_country_idx; Type: INDEX; Schema: stg_tables; Owner: postgres
--

CREATE INDEX stg_payment_profile_country_idx ON stg_tables.payment USING btree (profile_country);


--
-- Name: stg_payment_profile_id_idx; Type: INDEX; Schema: stg_tables; Owner: postgres
--

CREATE INDEX stg_payment_profile_id_idx ON stg_tables.payment USING btree (profile_id);


--
-- Name: stg_payment_status_changed_at_idx; Type: INDEX; Schema: stg_tables; Owner: postgres
--

CREATE INDEX stg_payment_status_changed_at_idx ON stg_tables.payment USING btree (status_changed_at);


--
-- Name: stg_payment_status_idx; Type: INDEX; Schema: stg_tables; Owner: postgres
--

CREATE INDEX stg_payment_status_idx ON stg_tables.payment USING btree (((status)::text)) WHERE ((status)::text = 'MT4_COMPLETED'::text);


--
-- Name: stg_payment_updated_at_idx; Type: INDEX; Schema: stg_tables; Owner: postgres
--

CREATE INDEX stg_payment_updated_at_idx ON stg_tables.payment USING btree (updated_at);


--
-- Name: stg_payment_user_agent_idx; Type: INDEX; Schema: stg_tables; Owner: postgres
--

CREATE INDEX stg_payment_user_agent_idx ON stg_tables.payment USING btree (user_agent);


--
-- Name: TABLE payment; Type: ACL; Schema: stg_tables; Owner: postgres
--

GRANT ALL ON TABLE stg_tables.payment TO data_sync;
GRANT SELECT ON TABLE stg_tables.payment TO ybashkatov;


--
-- PostgreSQL database dump complete
--

