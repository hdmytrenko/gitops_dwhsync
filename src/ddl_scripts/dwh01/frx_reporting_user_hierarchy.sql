--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: user_hierarchy; Type: VIEW; Schema: frx_reporting; Owner: postgres
--

CREATE VIEW frx_reporting.user_hierarchy AS
 SELECT user_hierarchy.id,
    user_hierarchy.created_at,
    user_hierarchy.deleted_at,
    user_hierarchy.user_type,
    user_hierarchy.uuid,
    user_hierarchy.brand_ids,
    user_hierarchy.version,
    user_hierarchy.observer_for,
    user_hierarchy.observable_from,
    user_hierarchy.updated_at,
    user_hierarchy.prod_num
   FROM stg_tables.user_hierarchy
  WHERE (((user_hierarchy.uuid)::text !~ similar_escape('%(TEST|UAT)%'::text, NULL::text)) AND (user_hierarchy.brand_ids <> ARRAY['dobby'::text]));


ALTER TABLE frx_reporting.user_hierarchy OWNER TO postgres;

--
-- Name: TABLE user_hierarchy; Type: ACL; Schema: frx_reporting; Owner: postgres
--

GRANT SELECT ON TABLE frx_reporting.user_hierarchy TO prod01_metabase;
GRANT SELECT ON TABLE frx_reporting.user_hierarchy TO ybashkatov;


--
-- PostgreSQL database dump complete
--

