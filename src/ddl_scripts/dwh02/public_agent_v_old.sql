--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: agent_v_old; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.agent_v_old AS
 WITH op AS (
         SELECT operator.uuid,
            public.normalize_space((((operator.first_name)::text || ' '::text) || (operator.last_name)::text)) AS name,
            operator.email,
            operator.registration_date,
            operator.status
           FROM ecosales_reporting.operator
          WHERE (operator.deleted = false)
        ), lan AS (
         SELECT lan_1.user_uuid,
            string_agg(lan_1.desk_language, ', '::text) AS lan
           FROM ( SELECT DISTINCT u.user_uuid,
                    upper(u.desk_language) AS desk_language
                   FROM public.h_user_v u
                  WHERE (u.desk_language IS NOT NULL)) lan_1
          GROUP BY lan_1.user_uuid
        ), desk AS (
         SELECT desk_1.agent_user_hierarchy_uuid,
            string_agg(desk_1.agent_desk_name, ', '::text) AS desk
           FROM ( SELECT DISTINCT user_hierarchy_v.agent_user_hierarchy_uuid,
                    upper((user_hierarchy_v.agent_desk_name)::text) AS agent_desk_name
                   FROM public.user_hierarchy_v
                  WHERE ((user_hierarchy_v.agent_desk_name IS NOT NULL) AND ((user_hierarchy_v.agent_dtype)::text = ANY (ARRAY['DESK'::text, 'TEAM'::text])))) desk_1
          GROUP BY desk_1.agent_user_hierarchy_uuid
        )
 SELECT DISTINCT uh.user_uuid AS agent_user_hierarchy_uuid,
    op.name,
    uh.user_type,
    COALESCE(desk.desk, 'Null'::text) AS agent_desk_name,
    lan.lan AS agent_language,
    op.email,
    op.registration_date,
    op.status,
    (uh.user_brand_id)::text AS user_brand_id
   FROM (((public.h_user_v uh
     LEFT JOIN op ON (((uh.user_uuid)::text = (op.uuid)::text)))
     LEFT JOIN lan ON (((uh.user_uuid)::text = (lan.user_uuid)::text)))
     LEFT JOIN desk ON (((uh.user_uuid)::text = (desk.agent_user_hierarchy_uuid)::text)));


ALTER TABLE public.agent_v_old OWNER TO postgres;

--
-- Name: TABLE agent_v_old; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public.agent_v_old TO ybashkatov;


--
-- PostgreSQL database dump complete
--

