--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: elastic_profile; Type: VIEW; Schema: frx_reporting; Owner: data_sync
--

CREATE VIEW frx_reporting.elastic_profile AS
 SELECT e.player_uuid,
    e.username,
    e.last_name,
    e.first_name,
    e.profile_status,
    e.profile_status_reason,
    e.login,
    e.country,
    e.currency,
    e.address,
    e.city,
    NULL::character varying(255) AS phone,
    e.email,
    e.email_verified,
    e.marketing_mail,
    e.marketing_sms,
    e.language_code,
    e.completed,
    e.migrated,
    e.creation_date,
    e.created_at,
    e.send_mail,
    e.registration_date,
    e.registration_ip,
    e.ip,
    e.gender,
    e.updated_date,
    e.post_code,
    ex.last_note,
    ex.last_note_changed_at AS last_note_date,
    e.birth_date,
    e.withdrawable_amount_amount,
    e.withdrawable_amount_currency,
    e.real_money_balance_amount,
    e.real_money_balance_currency,
    e.bonus_balance_amount,
    e.bonus_balance_currency,
    e.total_balance_amount,
    e.total_balance_currency,
    e.first_deposit,
    e.tailor_made_email,
    e.tailor_made_sms,
    e.author_uuid,
    e.kyc_rep_name,
    e.fns_status,
    ex.last_trade_created_at AS last_trade_date,
    e.affiliate_uuid,
    e.affiliate_source,
    e.is_test_user,
    e.acquisition_status,
    e.sales_status,
    e.retention_status,
    e.equity,
    e.balance,
    ex.kyc_status,
    e.base_currency_equity,
    e.base_currency_credit,
    e.mt_balance,
    e.base_currency_margin,
    e.credit,
    e.margin,
    e.brand_id,
    e.country_specific_identifier,
    e.country_specific_identifier_type,
    e.first_deposit_date,
    e.last_deposit_date,
    e.retention_rep,
    e.sales_rep,
    ex.payment_details_deposits_count AS deposit_count,
    e.passport_number,
    e.mt_group,
    e.affiliate_referral,
    e.questionnaire_status,
    e.passport_expiration_date,
    e.crs,
    e.last_login,
    e.withdrawal_count,
    e.prod_num,
    e.fsa_migration_status,
    ex.balance_amount AS mt4_balance,
    ex.balance_currency AS mt4_balance_currency,
    ex.first_note,
    ex.first_note_changed_at AS first_note_date,
    ex.ftd_amount,
    ex.ftd_currency,
    ex.ftd_time,
    ex.ltd_time,
    ex.migration_id,
    ex.kyc_changed_at
   FROM (stg_tables.elastic_profile e
     LEFT JOIN stg_tables.elastic_profile_ext ex ON (((e.player_uuid)::text = (ex.player_uuid)::text)))
  WHERE (((e.first_name IS NULL) OR (upper((e.first_name)::text) <> 'TEST'::text) OR (e.last_name IS NULL) OR (upper((e.last_name)::text) <> 'TEST'::text)) AND ((e.brand_id IS NULL) OR ((e.brand_id)::text <> 'dobby'::text)));


ALTER TABLE frx_reporting.elastic_profile OWNER TO data_sync;

--
-- Name: TABLE elastic_profile; Type: ACL; Schema: frx_reporting; Owner: data_sync
--

REVOKE ALL ON TABLE frx_reporting.elastic_profile FROM data_sync;
GRANT SELECT ON TABLE frx_reporting.elastic_profile TO prod01_metabase;
GRANT SELECT ON TABLE frx_reporting.elastic_profile TO ybashkatov;


--
-- PostgreSQL database dump complete
--

