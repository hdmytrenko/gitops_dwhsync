from sqlalchemy import String, Integer, Column
from sqlalchemy.ext.declarative import declarative_base

base = declarative_base()


class AffiliateIds(base):
    __tablename__ = 'affiliate_ids'
    __table_args__ = {'schema': 'stg_tables_partitioned'}
    uuid = Column(String, primary_key=True)
    prod_num = Column(Integer)


class PaymentIds(base):
    __tablename__ = 'payment_ids'
    __table_args__ = {'schema': 'stg_tables_partitioned'}
    id = Column(String, primary_key=True)
    prod_num = Column(Integer)


class OperatorIds(base):
    __tablename__ = 'operator_ids'
    __table_args__ = {'schema': 'stg_tables_partitioned'}
    id = Column(Integer, primary_key=True)
    prod_num = Column(Integer)


class ElasticProfileIds(base):
    __tablename__ = "elastic_profile_ids"
    __table_args__ = {'schema': 'stg_tables_partitioned'}
    player_uuid = Column(String, primary_key=True)
    prod_num = Column(Integer)


class ElasticProfileExtIds(base):
    __tablename__ = "elastic_profile_ext_ids"
    __table_args__ = {'schema': 'stg_tables_partitioned'}
    player_uuid = Column(String, primary_key=True)
    prod_num = Column(Integer)


class HierarchyCustomerIds(base):
    __tablename__ = 'hierarchy_customer_ids'
    __table_args__ = {'schema': 'stg_tables_partitioned'}
    uuid = Column(String, primary_key=True)
    prod_num = Column(Integer)
