--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

--
-- Name: elastic_profile_ext; Type: TABLE; Schema: stg_tables; Owner: postgres
--

CREATE TABLE stg_tables.elastic_profile_ext (
    brand_id text,
    first_name text,
    last_name text,
    language_code text,
    last_updated_date timestamp without time zone,
    affiliate double precision,
    migration_id text,
    payment_details double precision,
    last_trade double precision,
    questionnaire_passed text,
    player_uuid character varying(255) NOT NULL,
    kyc_status text,
    kyc_changed_at timestamp without time zone,
    sales_rep text,
    acquisition_sales_status text,
    retention_rep text,
    acquisition_retention_status text,
    acquisition_acquisition_status text,
    email text,
    contacts_additional_email text,
    phone text,
    address_country_code text,
    registration_date timestamp without time zone,
    registration_details_registered_by text,
    status_type text,
    status_changed_at timestamp without time zone,
    balance_amount double precision,
    balance_credit double precision,
    balance_currency text,
    affiliate_uuid text,
    affiliate_affiliate_source text,
    affiliate_affiliate_referral text,
    affiliate_external_id character varying,
    affiliate_sms character varying,
    affiliate_affiliate_type text,
    last_note_uuid text,
    last_note_changed_at timestamp without time zone,
    last_note text,
    first_note_changed_at timestamp without time zone,
    first_note_uuid text,
    first_note text,
    ftd_time timestamp without time zone,
    ftd_amount double precision,
    ftd_currency text,
    ltd_time timestamp without time zone,
    payment_details_deposits_count double precision,
    last_trade_created_at timestamp without time zone,
    affiliate_campaign_id text,
    fsa_migration_info double precision,
    prod_num integer
);


ALTER TABLE stg_tables.elastic_profile_ext OWNER TO postgres;

--
-- Name: elastic_profile_ext stgt_elastic_profile_ext_pkey; Type: CONSTRAINT; Schema: stg_tables; Owner: postgres
--

ALTER TABLE ONLY stg_tables.elastic_profile_ext
    ADD CONSTRAINT stgt_elastic_profile_ext_pkey PRIMARY KEY (player_uuid);


--
-- Name: idx_stgt_elastic_profile_ext_kyc_changed_at; Type: INDEX; Schema: stg_tables; Owner: postgres
--

CREATE INDEX idx_stgt_elastic_profile_ext_kyc_changed_at ON stg_tables.elastic_profile_ext USING btree (kyc_changed_at);


--
-- Name: idx_stgt_elastic_profile_ext_ltd_time; Type: INDEX; Schema: stg_tables; Owner: postgres
--

CREATE INDEX idx_stgt_elastic_profile_ext_ltd_time ON stg_tables.elastic_profile_ext USING btree (ltd_time);


--
-- Name: TABLE elastic_profile_ext; Type: ACL; Schema: stg_tables; Owner: postgres
--

GRANT ALL ON TABLE stg_tables.elastic_profile_ext TO data_sync;


--
-- PostgreSQL database dump complete
--

