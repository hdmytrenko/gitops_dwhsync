--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: trading_account; Type: VIEW; Schema: frx_reporting; Owner: postgres
--

CREATE VIEW frx_reporting.trading_account AS
 SELECT trading_account._id,
    trading_account.balance,
    trading_account.margin,
    trading_account.credit,
    trading_account.brand_id,
    trading_account.login,
    trading_account.account_type,
    trading_account.server_id,
    trading_account.currency,
    trading_account.leverage,
    trading_account.name,
    trading_account.mt4_group,
    trading_account.created_by,
    trading_account.created_at,
    trading_account.last_modified_date,
    trading_account.is_read_only,
    trading_account.archived,
    trading_account.profile_uuid,
    trading_account.platform_type,
    trading_account._class,
    trading_account.read_only_update_time,
    trading_account.prod_num,
    trading_account.normalized_balance,
    trading_account.normalized_credit
   FROM stg_tables.trading_account;


ALTER TABLE frx_reporting.trading_account OWNER TO postgres;

--
-- Name: TABLE trading_account; Type: ACL; Schema: frx_reporting; Owner: postgres
--

GRANT SELECT ON TABLE frx_reporting.trading_account TO prod01_metabase;
GRANT SELECT ON TABLE frx_reporting.trading_account TO ybashkatov;


--
-- PostgreSQL database dump complete
--

