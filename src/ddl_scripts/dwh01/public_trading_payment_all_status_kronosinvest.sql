--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: trading_payment_all_status_kronosinvest; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.trading_payment_all_status_kronosinvest AS
 SELECT tp10.company,
    tp10.index,
    tp10.id,
    tp10.brand_country,
    tp10.login,
    tp10.profile_id,
    tp10.payment_id,
    tp10.currency,
    tp10.external_reference,
    tp10.deposit,
    tp10.withdraw,
    tp10.net_deposit,
    tp10.transfer_in,
    tp10.transfer_out,
    tp10.credit_in,
    tp10.credit_out,
    tp10.deposit_usd,
    tp10.withdraw_usd,
    tp10.net_deposit_usd,
    tp10.type,
    tp10.rate,
    tp10.status,
    tp10.kyc_status,
    tp10.acquisition_status,
    tp10.creation_time_profile,
    tp10.creation_time,
    tp10.day_payment,
    tp10.month_payment,
    tp10.year_payment,
    tp10.week_payment,
    tp10.day_of_week_creation_time,
    tp10.day_name_creation_time,
    tp10.country,
    tp10.language,
    tp10.brand_id,
    tp10.payment_method,
    tp10.group_payment_method,
    tp10.is_published,
    tp10.payment_transaction_id,
    tp10.expiration_date,
    tp10.version,
    tp10.profile_first_name,
    tp10.profile_last_name,
    tp10.email,
    tp10.client_ip,
    tp10.created_by,
    tp10.is_mobile,
    tp10.user_agent,
    tp10.profile_country,
    tp10.payment_aggregator,
    tp10.agent_id,
    tp10.agent_name,
    tp10.sales_status,
    tp10.sales_rep,
    tp10.sales_name,
    tp10.sales_user_type,
    tp10.sales_desk_name,
    tp10.sales_language,
    tp10.retention_status,
    tp10.retention_rep,
    tp10.retention_name,
    tp10.reten_user_type,
    tp10.reten_desk_name,
    tp10.reten_language,
    tp10.affiliate_uuid,
    tp10.affiliate_name,
    tp10.affiliate_referral,
    tp10.affiliate_source,
    tp10.profile_country_name,
    tp10.user_type,
    tp10.agent_desk_name,
    tp10.agent_language,
    tp10.ftd_status,
    tp10.ftd_time,
    tp10.day_to_ftd,
    tp10.ftd,
    tp10.ftd_deposit,
    tp10.ftd_deposit_usd,
    tp10.noftd,
    tp10.redeposit,
    tp10.redeposit_usd,
    tp10.last_deposit_time,
    tp10.last_note,
    tp10.last_note_date
   FROM public.trading_payment_all_status_v tp10
  WHERE ((public.brand2prod(tp10.brand_id) = 10) AND ((tp10.brand_id)::text = ANY (ARRAY[('kronosinvest'::character varying)::text, ('thecapitalstocks'::character varying)::text])));


ALTER TABLE public.trading_payment_all_status_kronosinvest OWNER TO postgres;

--
-- Name: TABLE trading_payment_all_status_kronosinvest; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public.trading_payment_all_status_kronosinvest TO prod01_metabase;
GRANT SELECT ON TABLE public.trading_payment_all_status_kronosinvest TO ybashkatov;


--
-- PostgreSQL database dump complete
--

