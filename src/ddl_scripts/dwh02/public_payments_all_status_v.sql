--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: payments_all_status_v; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.payments_all_status_v AS
 SELECT trading_payment_all_status_v.login,
    trading_payment_all_status_v.profile_id,
    trading_payment_all_status_v.email,
    trading_payment_all_status_v.payment_id,
    trading_payment_all_status_v.currency,
    trading_payment_all_status_v.external_reference,
    trading_payment_all_status_v.type,
    trading_payment_all_status_v.status,
    trading_payment_all_status_v.deposit,
    trading_payment_all_status_v.withdraw,
    trading_payment_all_status_v.net_deposit,
    trading_payment_all_status_v.transfer_in,
    trading_payment_all_status_v.transfer_out,
    trading_payment_all_status_v.credit_in,
    trading_payment_all_status_v.credit_out,
    trading_payment_all_status_v.deposit_usd,
    trading_payment_all_status_v.withdraw_usd,
    trading_payment_all_status_v.net_deposit_usd,
    trading_payment_all_status_v.rate,
    trading_payment_all_status_v.kyc_status,
    trading_payment_all_status_v.acquisition_status,
    trading_payment_all_status_v.creation_time_profile,
    trading_payment_all_status_v.creation_time,
    trading_payment_all_status_v.day_payment,
    trading_payment_all_status_v.month_payment,
    trading_payment_all_status_v.year_payment,
    trading_payment_all_status_v.week_payment,
    trading_payment_all_status_v.day_of_week_creation_time,
    trading_payment_all_status_v.day_name_creation_time,
    trading_payment_all_status_v.country,
    trading_payment_all_status_v.language,
    trading_payment_all_status_v.brand_id,
    trading_payment_all_status_v.payment_method,
    trading_payment_all_status_v.group_payment_method,
    trading_payment_all_status_v.is_published,
    trading_payment_all_status_v.payment_transaction_id,
    trading_payment_all_status_v.expiration_date,
    trading_payment_all_status_v.version,
    trading_payment_all_status_v.profile_first_name,
    trading_payment_all_status_v.profile_last_name,
    trading_payment_all_status_v.client_ip,
    trading_payment_all_status_v.created_by,
    trading_payment_all_status_v.is_mobile,
    trading_payment_all_status_v.user_agent,
    trading_payment_all_status_v.profile_country,
    trading_payment_all_status_v.payment_aggregator,
    trading_payment_all_status_v.agent_id,
    trading_payment_all_status_v.agent_name,
    trading_payment_all_status_v.sales_status,
    trading_payment_all_status_v.sales_rep,
    trading_payment_all_status_v.sales_name,
    trading_payment_all_status_v.sales_user_type,
    trading_payment_all_status_v.sales_desk_name,
    trading_payment_all_status_v.sales_language,
    trading_payment_all_status_v.retention_status,
    trading_payment_all_status_v.retention_rep,
    trading_payment_all_status_v.retention_name,
    trading_payment_all_status_v.reten_user_type,
    trading_payment_all_status_v.reten_desk_name,
    trading_payment_all_status_v.reten_language,
    trading_payment_all_status_v.affiliate_uuid,
    trading_payment_all_status_v.affiliate_name,
    trading_payment_all_status_v.affiliate_referral,
    trading_payment_all_status_v.affiliate_source,
    trading_payment_all_status_v.profile_country_name,
    trading_payment_all_status_v.user_type,
    trading_payment_all_status_v.agent_desk_name,
    trading_payment_all_status_v.agent_language,
    trading_payment_all_status_v.ftd_status,
    trading_payment_all_status_v.ftd_time,
    trading_payment_all_status_v.day_to_ftd,
    trading_payment_all_status_v.ftd,
    trading_payment_all_status_v.ftd_deposit,
    trading_payment_all_status_v.ftd_deposit_usd,
    trading_payment_all_status_v.noftd,
    trading_payment_all_status_v.redeposit,
    trading_payment_all_status_v.redeposit_usd,
    trading_payment_all_status_v.last_deposit_time,
    trading_payment_all_status_v.modification_time,
    trading_payment_all_status_v.execution_time,
    trading_payment_all_status_v.amount,
    trading_payment_all_status_v.amount_usd,
    trading_payment_all_status_v.last_note,
    trading_payment_all_status_v.last_note_date
   FROM public.trading_payment_all_status_v;


ALTER TABLE public.payments_all_status_v OWNER TO postgres;

--
-- Name: TABLE payments_all_status_v; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public.payments_all_status_v TO ybashkatov;


--
-- PostgreSQL database dump complete
--

