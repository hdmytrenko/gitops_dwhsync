--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: payment; Type: VIEW; Schema: frx_reporting; Owner: postgres
--

CREATE VIEW frx_reporting.payment AS
 SELECT payment.id,
    payment.login,
    payment.profile_id,
    payment.payment_id,
    payment.amount,
    payment.currency,
    payment.external_reference,
    payment.type,
    payment.status,
    payment.creation_time,
    payment.country,
    payment.language,
    payment.brand_id,
    payment.payment_method,
    payment.is_published,
    payment.payment_transaction_id,
    payment.expiration_date,
    payment.version,
    payment.profile_first_name,
    payment.profile_last_name,
    payment.client_ip,
    payment.created_by,
    payment.is_mobile,
    payment.user_agent,
    payment.profile_country,
    payment.payment_aggregator,
    payment.agent_name,
    payment.agent_id,
    payment.payment_migration_id,
    payment.user_migration_id,
    payment.normalized_amount,
    payment.decline_reason,
    payment.modification_time,
    payment.modified_by,
    payment.ex_rate,
    payment.linked_transaction_id,
    payment.moto,
    payment.last_operation_type,
    payment.updated_at,
    payment.deleted,
    payment.fee,
    payment.initial_transaction,
    public.brand2prod(payment.brand_id) AS prod_num,
    COALESCE(payment.status_changed_at, payment.updated_at) AS status_changed_at,
    payment.account_uuid,
    payment.first_time_deposit,
    payment.affiliate_uuid,
    payment.agent_branches
   FROM stg_tables.payment
  WHERE ((NOT payment.deleted) AND ((payment.payment_method IS NULL) OR (upper((payment.payment_method)::text) <> 'FAKEPAL'::text)) AND ((payment.profile_first_name IS NULL) OR (upper((payment.profile_first_name)::text) <> 'TEST'::text) OR (payment.profile_last_name IS NULL) OR (upper((payment.profile_last_name)::text) <> 'TEST'::text)) AND ((payment.brand_id IS NULL) OR ((payment.brand_id)::text <> 'dobby'::text)));


ALTER TABLE frx_reporting.payment OWNER TO postgres;

--
-- Name: TABLE payment; Type: ACL; Schema: frx_reporting; Owner: postgres
--

GRANT SELECT ON TABLE frx_reporting.payment TO prod01_metabase;
GRANT ALL ON TABLE frx_reporting.payment TO data_sync;
GRANT SELECT ON TABLE frx_reporting.payment TO ybashkatov;


--
-- PostgreSQL database dump complete
--

