--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: trading_payment_all_status; Type: VIEW; Schema: hrzn07_brand_consulting_alfa; Owner: postgres
--

CREATE VIEW hrzn07_brand_consulting_alfa.trading_payment_all_status AS
 SELECT tpas.login,
    tpas.profile_id,
    tpas.email,
    tpas.payment_id,
    tpas.currency,
    tpas.external_reference,
    tpas.type,
    tpas.status,
    tpas.deposit,
    tpas.withdraw,
    tpas.net_deposit,
    tpas.transfer_in,
    tpas.transfer_out,
    tpas.credit_in,
    tpas.credit_out,
    tpas.deposit_usd,
    tpas.withdraw_usd,
    tpas.net_deposit_usd,
    tpas.rate,
    tpas.kyc_status,
    tpas.acquisition_status,
    tpas.creation_time_profile,
    tpas.creation_time,
    tpas.day_payment,
    tpas.month_payment,
    tpas.year_payment,
    tpas.week_payment,
    tpas.day_of_week_creation_time,
    tpas.day_name_creation_time,
    tpas.country,
    tpas.language,
    tpas.brand_id,
    tpas.payment_method,
    tpas.group_payment_method,
    tpas.is_published,
    tpas.payment_transaction_id,
    tpas.expiration_date,
    tpas.version,
    tpas.profile_first_name,
    tpas.profile_last_name,
    tpas.client_ip,
    tpas.created_by,
    tpas.is_mobile,
    tpas.user_agent,
    tpas.profile_country,
    tpas.payment_aggregator,
    tpas.agent_id,
    tpas.agent_name,
    tpas.sales_status,
    tpas.sales_rep,
    tpas.sales_name,
    tpas.sales_user_type,
    tpas.sales_desk_name,
    tpas.sales_language,
    tpas.retention_status,
    tpas.retention_rep,
    tpas.retention_name,
    tpas.reten_user_type,
    tpas.reten_desk_name,
    tpas.reten_language,
    tpas.affiliate_uuid,
    tpas.affiliate_name,
    tpas.affiliate_referral,
    tpas.affiliate_source,
    tpas.profile_country_name,
    tpas.user_type,
    tpas.agent_desk_name,
    tpas.agent_language,
    tpas.ftd_status,
    tpas.ftd_time,
    tpas.day_to_ftd,
    tpas.ftd,
    tpas.ftd_deposit,
    tpas.ftd_deposit_usd,
    tpas.noftd,
    tpas.redeposit,
    tpas.redeposit_usd,
    tpas.last_deposit_time,
    tpas.modification_time,
    tpas.execution_time,
    tpas.amount,
    tpas.amount_usd,
    tpas.last_note,
    tpas.last_note_date
   FROM public.trading_payment_all_status_v tpas
  WHERE ((tpas.brand_id)::text = 'consulting-alfa'::text);


ALTER TABLE hrzn07_brand_consulting_alfa.trading_payment_all_status OWNER TO postgres;

--
-- Name: TABLE trading_payment_all_status; Type: ACL; Schema: hrzn07_brand_consulting_alfa; Owner: postgres
--

GRANT ALL ON TABLE hrzn07_brand_consulting_alfa.trading_payment_all_status TO data_sync;


--
-- PostgreSQL database dump complete
--

