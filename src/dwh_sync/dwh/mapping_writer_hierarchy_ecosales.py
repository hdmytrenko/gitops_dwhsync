from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, String, Integer, DATETIME, ARRAY
from sqlalchemy.ext.hybrid import hybrid_property

base = declarative_base()


# hierarchy-updater.public.operator
class HierarchyOperatorDWH(base):
    __tablename__ = "operator"
    __table_args__ = {'schema': 'public'}
    id = Column(Integer, primary_key=True)
    uuid = Column(String(60))
    brand_ids = Column(ARRAY(String))
    type = Column(String(20))
    observer_for = Column(ARRAY(String))
    observable_from = Column(ARRAY(String))
    created_at = Column(DATETIME(timezone=False))
    updated_at = Column(DATETIME(timezone=False))
    version = Column(Integer)
    prod_num = Column(Integer, primary_key=True)

    @staticmethod
    def uuids():
        return tuple()

    @hybrid_property
    def filter_column(self):
        return self.updated_at

    @staticmethod
    def uuid_for_matview():
        return ''

    matview_refresh_date_filter = None

    def __repr__(self):
        return '<HierarchyOperator((id="{}", uuid={}, created_at="{}", updated_at="{}")'. \
            format(self.id, self.uuid, self.created_at, self.updated_at)


# hierarchy-updater.public.customer
class HierarchyCustomerDWH(base):
    __tablename__ = "customer"
    __table_args__ = {'schema': 'public'}
    id = Column(Integer, primary_key=True)
    uuid = Column(String(60))
    brand_id = Column(String(32))
    observable_from = Column(ARRAY(String))
    parent_operator_id = Column(Integer)
    sales_rep = Column(String)
    sales_status = Column(String)
    retention_rep = Column(String)
    retention_status = Column(String)
    acquisition_status = Column(String)
    created_at = Column(DATETIME(timezone=False))
    updated_at = Column(DATETIME(timezone=False))
    deleted_at = Column(DATETIME(timezone=False))
    sales_status_update_date = Column(DATETIME(timezone=False))
    version = Column(Integer)
    prod_num = Column(Integer, primary_key=True)

    @staticmethod
    def uuids():
        return tuple()

    @staticmethod
    def uuid_for_matview():
        return ''

    @hybrid_property
    def filter_column(self):
        return self.updated_at

    matview_refresh_date_filter = None

    def __repr__(self):
        return '<HierarchyCustomer((id="{}", uuid={}, created_at="{}", updated_at="{}")'. \
            format(self.id, self.uuid, self.created_at, self.updated_at)


# hierarchy-updater.public.lead
class HierarchyLeadDWH(base):
    __tablename__ = "lead"
    __table_args__ = {'schema': 'public'}
    id = Column(Integer, primary_key=True)
    uuid = Column(String(60))
    brand_id = Column(String(32))
    observable_from = Column(ARRAY(String))
    parent_operator_id = Column(Integer)
    sales_status = Column(String)
    created_at = Column(DATETIME(timezone=False))
    updated_at = Column(DATETIME(timezone=False))
    deleted_at = Column(DATETIME(timezone=False))
    version = Column(Integer)
    prod_num = Column(Integer, primary_key=True)

    @staticmethod
    def uuids():
        return tuple()

    @staticmethod
    def uuid_for_matview():
        return ''

    @hybrid_property
    def filter_column(self):
        return self.updated_at

    matview_refresh_date_filter = None

    def __repr__(self):
        return '<HierarchyLead((id="{}", uuid={}, created_at="{}", updated_at="{}")'. \
            format(self.id, self.uuid, self.created_at, self.updated_at)


# hierarchy-updater.public.branch
class HierarchyBranchDWH(base):
    __tablename__ = "branch"
    __table_args__ = {'schema': 'public'}
    id = Column(Integer, primary_key=True)
    uuid = Column(String(60))
    type = Column(String(31))
    name = Column(String(255))
    brand_id = Column(String(32))
    parent_id = Column(Integer)
    manager_id = Column(Integer)
    default_branch_id = Column(Integer)
    default_branch_operator_id = Column(Integer)
    desk_type = Column(String(10))
    language = Column(String(2))
    country = Column(String(2))
    created_at = Column(DATETIME(timezone=False))
    updated_at = Column(DATETIME(timezone=False))
    deleted_at = Column(DATETIME(timezone=False))
    assign_index = Column(Integer)
    version = Column(Integer)
    prod_num = Column(Integer, primary_key=True)

    @staticmethod
    def uuids():
        return tuple()

    @staticmethod
    def uuid_for_matview():
        return ''

    @hybrid_property
    def filter_column(self):
        return self.updated_at

    matview_refresh_date_filter = None

    def __repr__(self):
        return '<HierarchyBranch((id="{}", uuid={}, created_at="{}", updated_at="{}")'. \
            format(self.id, self.uuid, self.created_at, self.updated_at)


# hierarchy-updater.public.operator_branch
class HierarchyOperatorBranchDWH(base):
    __tablename__ = 'operator_branch'
    __table_args__ = {'schema': 'public'}
    operator_id = Column(Integer, primary_key=True)
    branch_id = Column(Integer, primary_key=True)
    prod_num = Column(Integer, primary_key=True)

    @staticmethod
    def uuids():
        return tuple()

    @staticmethod
    def uuid_for_matview():
        return ''

    @staticmethod
    def filter_column():
        return None

    matview_refresh_date_filter = None

    def __repr__(self):
        return '<UserHierarchyBranch(operator_id={}, branch_id={})'. \
            format(self.operator_id, self.branch_id)
