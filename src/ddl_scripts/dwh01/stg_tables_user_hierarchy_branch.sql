--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

--
-- Name: user_hierarchy_branch; Type: TABLE; Schema: stg_tables; Owner: postgres
--

CREATE TABLE stg_tables.user_hierarchy_branch (
    user_hierarchy_id bigint NOT NULL,
    branch_id bigint NOT NULL,
    prod_num integer
);


ALTER TABLE stg_tables.user_hierarchy_branch OWNER TO postgres;

--
-- Name: user_hierarchy_branch stg_user_hierarchy_branch_pk; Type: CONSTRAINT; Schema: stg_tables; Owner: postgres
--

ALTER TABLE ONLY stg_tables.user_hierarchy_branch
    ADD CONSTRAINT stg_user_hierarchy_branch_pk PRIMARY KEY (user_hierarchy_id, branch_id);


--
-- Name: idx_stg_user_hierarchy_branch_branch_id; Type: INDEX; Schema: stg_tables; Owner: postgres
--

CREATE INDEX idx_stg_user_hierarchy_branch_branch_id ON stg_tables.user_hierarchy_branch USING btree (branch_id);


--
-- Name: idx_stg_user_hierarchy_branch_user_hierarchy_id; Type: INDEX; Schema: stg_tables; Owner: postgres
--

CREATE INDEX idx_stg_user_hierarchy_branch_user_hierarchy_id ON stg_tables.user_hierarchy_branch USING btree (user_hierarchy_id);


--
-- Name: TABLE user_hierarchy_branch; Type: ACL; Schema: stg_tables; Owner: postgres
--

GRANT ALL ON TABLE stg_tables.user_hierarchy_branch TO data_sync;
GRANT SELECT ON TABLE stg_tables.user_hierarchy_branch TO prod01_metabase;
GRANT SELECT ON TABLE stg_tables.user_hierarchy_branch TO ybashkatov;


--
-- PostgreSQL database dump complete
--

