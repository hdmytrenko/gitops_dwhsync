--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: agent_v; Type: VIEW; Schema: hrzn07_brand_skycapital; Owner: postgres
--

CREATE VIEW hrzn07_brand_skycapital.agent_v AS
 SELECT av.agent_user_hierarchy_uuid,
    av.name,
    av.user_type,
    av.agent_desk_name,
    av.agent_language,
    av.email,
    av.registration_date,
    av.status,
    av.user_brand_id
   FROM public.agent_v av;


ALTER TABLE hrzn07_brand_skycapital.agent_v OWNER TO postgres;

--
-- Name: TABLE agent_v; Type: ACL; Schema: hrzn07_brand_skycapital; Owner: postgres
--

GRANT ALL ON TABLE hrzn07_brand_skycapital.agent_v TO data_sync;
GRANT SELECT ON TABLE hrzn07_brand_skycapital.agent_v TO ybashkatov;


--
-- PostgreSQL database dump complete
--

