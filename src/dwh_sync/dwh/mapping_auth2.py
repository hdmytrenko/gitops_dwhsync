from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, String, Integer, and_, not_
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import Session

base = declarative_base()


class Users(base):
    __tablename__ = "users"
    __table_args__ = {'schema': 'public'}
    id = Column(Integer, primary_key=True)
    uuid = Column(String(64))

    @hybrid_property
    def filter_column(self):
        return self.id

    @staticmethod
    def get_prefiltered_dataset(session: Session):
        return session.query(Users).filter(and_(Users.uuid.like('OPERATOR%'), not_(Users.uuid.like('%test%'))))

    def __repr__(self):
        return '<Users((id="{}", uuid={})'. \
            format(self.id, self.uuid)


class UserAuthority(base):
    __tablename__ = "user_authority"
    user_id = Column(Integer, primary_key=True)
    authority_id = Column(Integer, primary_key=True)

    @staticmethod
    def uuids():
        return tuple()

    @staticmethod
    def filter_column():
        return None

    @staticmethod
    def get_prefiltered_dataset(session: Session):
        return session.query(UserAuthority).join(Users, UserAuthority.user_id == Users.id).filter(
            and_(Users.uuid.like('OPERATOR%'), not_(Users.uuid.like('%test%'))))

    def __repr__(self):
        return '<UserAuthority(user_id: {}, authority_id: {})>'.format(self.user_id, self.authority_id)


class Authorities(base):
    __tablename__ = "authorities"
    __table_args__ = {'schema': 'public'}
    id = Column(Integer, primary_key=True)
    brand = Column(String)
    department = Column(String)
    role = Column(String)

    @hybrid_property
    def filter_column(self):
        return self.id

    @staticmethod
    def uuids():
        return tuple()

    @staticmethod
    def get_prefiltered_dataset(session: Session):
        return session.query(Authorities).filter(
            and_(
                Authorities.department.notin_(['AFFILIATE', 'PLAYER']),
                Authorities.brand != 'dobby'
            )
        )

    def __repr__(self):
        return '<Authorities((id="{}", brand={}, department={}, role={})'. \
            format(self.id, self.brand, self.department, self.role)


class AccessPhoneAuthorities(base):
    __tablename__ = "authority_action"
    __table_args__ = {'schema': 'public'}
    authority_id = Column(Integer, primary_key=True)
    action_id = Column(Integer, primary_key=True)

    @staticmethod
    def filter_column():
        return None

    @staticmethod
    def uuids():
        return tuple()

    @staticmethod
    def get_prefiltered_dataset(session: Session):
        return session.query(AccessPhoneAuthorities) \
            .join(Actions, AccessPhoneAuthorities.action_id == Actions.id)\
            .filter(Actions.action == 'backoffice-graphql.profile.field.phone')

    def __repr__(self):
        return '<AccessPhoneAuthorities((authority_id="{}")'.format(self.authority_id)


class Actions(base):
    __tablename__ = "actions"
    __table_args__ = {'schema': 'public'}
    id = Column(Integer, primary_key=True)
    action = Column(String)

    def __repr__(self):
        return '<AccessPhoneAuthorities((id="{}", action="{}")'.format(self.id, self.action)
