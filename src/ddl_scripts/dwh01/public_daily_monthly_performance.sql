--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: daily_monthly_performance; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.daily_monthly_performance AS
 SELECT tp.payment_id,
    tp.brand_id,
    tp.creation_time,
    tp.deposit_usd,
    tp.execution_time,
    tp.ftd,
    tp.withdraw_usd
   FROM public.trading_payment_all_status_v tp
  WHERE (((tp.status)::text = 'PAYMENT_COMPLETED'::text) AND ((tp.brand_id)::text = ANY (ARRAY[('bsb-global'::character varying)::text, ('bid-broker-stocks'::character varying)::text, ('bycrypto'::character varying)::text, ('gladtotrade'::character varying)::text])) AND ((date_trunc('month'::text, tp.execution_time) >= date_trunc('month'::text, (now() + '-3 mons'::interval))) OR (date_trunc('month'::text, tp.creation_time) >= date_trunc('month'::text, (now() + '-1 mons'::interval)))));


ALTER TABLE public.daily_monthly_performance OWNER TO postgres;

--
-- Name: TABLE daily_monthly_performance; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public.daily_monthly_performance TO data_sync;
GRANT SELECT ON TABLE public.daily_monthly_performance TO ybashkatov;


--
-- PostgreSQL database dump complete
--

