--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: es_profile_trading_payment; Type: VIEW; Schema: hrzn07_brand_stockfx; Owner: postgres
--

CREATE VIEW hrzn07_brand_stockfx.es_profile_trading_payment AS
 SELECT f_es_profile_trading_payment.profile_id,
    f_es_profile_trading_payment.email,
    f_es_profile_trading_payment.login,
    f_es_profile_trading_payment.acquisition_status,
    f_es_profile_trading_payment.player_status,
    f_es_profile_trading_payment.sales_status,
    f_es_profile_trading_payment.sales_rep,
    f_es_profile_trading_payment.sales_name,
    f_es_profile_trading_payment.sales_user_type,
    f_es_profile_trading_payment.sales_desk_name,
    f_es_profile_trading_payment.sales_language,
    f_es_profile_trading_payment.retention_status,
    f_es_profile_trading_payment.retention_rep,
    f_es_profile_trading_payment.retention_name,
    f_es_profile_trading_payment.reten_user_type,
    f_es_profile_trading_payment.reten_desk_name,
    f_es_profile_trading_payment.reten_language,
    f_es_profile_trading_payment.creation_time_profile,
    f_es_profile_trading_payment.day_of_week_creation_time_profile,
    f_es_profile_trading_payment.day_name_creation_time_profile,
    f_es_profile_trading_payment.kyc_status,
    f_es_profile_trading_payment.mt_group,
    f_es_profile_trading_payment.payment_id,
    f_es_profile_trading_payment.type,
    f_es_profile_trading_payment.modification_time,
    f_es_profile_trading_payment.ftd_status,
    f_es_profile_trading_payment.ftd,
    f_es_profile_trading_payment.ftd_deposit,
    f_es_profile_trading_payment.ftd_deposit_usd,
    f_es_profile_trading_payment.noftd,
    f_es_profile_trading_payment.redeposit,
    f_es_profile_trading_payment.redeposit_usd,
    f_es_profile_trading_payment.ftd_time,
    f_es_profile_trading_payment.day_to_ftd,
    f_es_profile_trading_payment.last_deposit_time,
    f_es_profile_trading_payment.currency,
    f_es_profile_trading_payment.deposit,
    f_es_profile_trading_payment.withdraw,
    f_es_profile_trading_payment.transfer_in,
    f_es_profile_trading_payment.transfer_out,
    f_es_profile_trading_payment.credit_in,
    f_es_profile_trading_payment.credit_out,
    f_es_profile_trading_payment.deposit_usd,
    f_es_profile_trading_payment.withdraw_usd,
    f_es_profile_trading_payment.mt_balance,
    f_es_profile_trading_payment.rate,
    f_es_profile_trading_payment.paymant_creation_time,
    f_es_profile_trading_payment.profile_country,
    f_es_profile_trading_payment.country,
    f_es_profile_trading_payment.profile_country_name,
    f_es_profile_trading_payment.language,
    f_es_profile_trading_payment.brand_id,
    f_es_profile_trading_payment.payment_method,
    f_es_profile_trading_payment.is_published,
    f_es_profile_trading_payment.profile_name,
    f_es_profile_trading_payment.client_ip,
    f_es_profile_trading_payment.is_mobile,
    f_es_profile_trading_payment.payment_aggregator,
    f_es_profile_trading_payment.agent_id,
    f_es_profile_trading_payment.agent_name,
    f_es_profile_trading_payment.agent_desk_name,
    f_es_profile_trading_payment.agent_language,
    f_es_profile_trading_payment.user_type,
    f_es_profile_trading_payment.group_payment_method,
    f_es_profile_trading_payment.user_agent,
    f_es_profile_trading_payment.affiliate_uuid,
    f_es_profile_trading_payment.affiliate_name,
    f_es_profile_trading_payment.affiliate_referral,
    f_es_profile_trading_payment.affiliate_source,
    f_es_profile_trading_payment.execution_time,
    f_es_profile_trading_payment.index,
    f_es_profile_trading_payment.modified_by,
    f_es_profile_trading_payment.net_deposit,
    f_es_profile_trading_payment.net_deposit_usd,
    f_es_profile_trading_payment.last_note,
    f_es_profile_trading_payment.last_note_date,
    f_es_profile_trading_payment.questionnaire_status,
    f_es_profile_trading_payment.last_login,
    f_es_profile_trading_payment.last_trade_date,
    f_es_profile_trading_payment.deposit_count,
    f_es_profile_trading_payment.affiliate_type,
    f_es_profile_trading_payment.fsa_migration_status,
    f_es_profile_trading_payment.mt4_balance,
    f_es_profile_trading_payment.mt4_balance_currency,
    f_es_profile_trading_payment.first_note,
    f_es_profile_trading_payment.first_note_date,
    f_es_profile_trading_payment.ftd_amount,
    f_es_profile_trading_payment.ftd_currency,
    f_es_profile_trading_payment.ltd_time,
    f_es_profile_trading_payment.migration_id,
    f_es_profile_trading_payment.kyc_changed_at,
    f_es_profile_trading_payment.external_reference
   FROM public.f_es_profile_trading_payment('stockfx'::text) f_es_profile_trading_payment(profile_id, email, login, acquisition_status, player_status, sales_status, sales_rep, sales_name, sales_user_type, sales_desk_name, sales_language, retention_status, retention_rep, retention_name, reten_user_type, reten_desk_name, reten_language, creation_time_profile, day_of_week_creation_time_profile, day_name_creation_time_profile, kyc_status, mt_group, payment_id, type, modification_time, ftd_status, ftd, ftd_deposit, ftd_deposit_usd, noftd, redeposit, redeposit_usd, ftd_time, day_to_ftd, last_deposit_time, currency, deposit, withdraw, transfer_in, transfer_out, credit_in, credit_out, deposit_usd, withdraw_usd, mt_balance, rate, paymant_creation_time, profile_country, country, profile_country_name, language, brand_id, payment_method, is_published, profile_name, client_ip, is_mobile, payment_aggregator, agent_id, agent_name, agent_desk_name, agent_language, user_type, group_payment_method, user_agent, affiliate_uuid, affiliate_name, affiliate_referral, affiliate_source, execution_time, index, modified_by, net_deposit, net_deposit_usd, last_note, last_note_date, questionnaire_status, last_login, last_trade_date, deposit_count, affiliate_type, fsa_migration_status, mt4_balance, mt4_balance_currency, first_note, first_note_date, ftd_amount, ftd_currency, ltd_time, migration_id, kyc_changed_at, external_reference);


ALTER TABLE hrzn07_brand_stockfx.es_profile_trading_payment OWNER TO postgres;

--
-- Name: TABLE es_profile_trading_payment; Type: ACL; Schema: hrzn07_brand_stockfx; Owner: postgres
--

GRANT SELECT ON TABLE hrzn07_brand_stockfx.es_profile_trading_payment TO ybashkatov;


--
-- PostgreSQL database dump complete
--

