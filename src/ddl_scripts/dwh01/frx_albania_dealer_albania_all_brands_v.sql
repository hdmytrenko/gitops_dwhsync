--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: dealer_albania_all_brands_v; Type: VIEW; Schema: frx_albania; Owner: postgres
--

CREATE VIEW frx_albania.dealer_albania_all_brands_v AS
 WITH trades AS (
         SELECT ta.profile_uuid,
            ta._id,
            ta.prod_num,
            count(tr_1.trade_id) AS cnt_trades,
            sum(tr_1.profit) AS pnl,
            to_timestamp((max(tr_1.open_time))::double precision) AS last_open_trade,
            to_timestamp((min(tr_1.open_time))::double precision) AS first_open_trade
           FROM (frx_reporting.trade_record tr_1
             JOIN frx_reporting.trading_account ta ON ((ta.login = tr_1.login)))
          WHERE ((public.brand2prod(tr_1.brand_id) = 10) AND ((tr_1.cmd)::text = ANY (ARRAY[('OP_BUY'::character varying)::text, ('OP_SELL'::character varying)::text])))
          GROUP BY ta.profile_uuid, ta._id, ta.prod_num
        ), payments AS (
         SELECT p_1.profile_id,
            p_1.prod_num,
            max(f.ftd_time) AS first_time_deposit,
            max(f.ltd_time) AS last_deposit_date,
            max(
                CASE
                    WHEN (p_1.creation_time = f.ftd_time) THEN p_1.normalized_amount
                    ELSE (0)::numeric
                END) AS ftd_sum_usd,
            max(
                CASE
                    WHEN (p_1.creation_time = f.ltd_time) THEN p_1.normalized_amount
                    ELSE (0)::numeric
                END) AS ltd_sum_usd,
            sum(
                CASE
                    WHEN ((p_1.type)::text = 'DEPOSIT'::text) THEN p_1.normalized_amount
                    ELSE NULL::numeric
                END) AS deposits_usd,
            sum(
                CASE
                    WHEN ((p_1.type)::text = 'DEPOSIT'::text) THEN 1
                    ELSE NULL::integer
                END) AS deposits_cnt,
            sum(
                CASE
                    WHEN ((p_1.type)::text = 'WITHDRAW'::text) THEN p_1.normalized_amount
                    ELSE NULL::numeric
                END) AS withdraw_usd,
            sum(
                CASE
                    WHEN ((p_1.type)::text = 'WITHDRAW'::text) THEN 1
                    ELSE NULL::integer
                END) AS withdraw_cnt
           FROM (frx_reporting.payment p_1
             JOIN public.v_ftd f ON (((p_1.profile_id)::text = (f.profile_id)::text)))
          WHERE ((public.brand2prod(p_1.brand_id) = 10) AND ((p_1.status)::text = 'PAYMENT_COMPLETED'::text) AND ((p_1.type)::text = ANY (ARRAY[('DEPOSIT'::character varying)::text, ('WITHDRAW'::character varying)::text])) AND (((p_1.payment_method)::text <> ALL (ARRAY['FAKEPAL'::text, 'INTERNAL_TRANSFER'::text, 'BONUS'::text])) OR (p_1.payment_method IS NULL)))
          GROUP BY p_1.profile_id, p_1.prod_num
        )
 SELECT ep.brand_id AS "Brand",
    ep.player_uuid AS "Client ID",
    tr._id AS "MT4 ID",
    ep.email,
    COALESCE(sales.name, reten.name, 'null'::text) AS "Agent",
    COALESCE(sales.agent_desk_name, reten.agent_desk_name, 'null'::text) AS "Agent Desk Name",
    tr.last_open_trade AS "First Open Trade",
    tr.last_open_trade AS "Last Open Trade",
    p.ftd_sum_usd AS "First Deposit",
    p.ltd_sum_usd AS "Last Deposit",
    p.first_time_deposit AS "First Deposit Date",
    p.last_deposit_date AS "Last Deposit Date",
    ep.last_note AS "Last Comment",
    ep.last_note_date AS "Last Comment Date",
    p.deposits_cnt AS "Deposits Count",
    p.deposits_usd AS "Deposits USD",
    p.withdraw_cnt AS "WD Count",
    p.withdraw_usd AS "WD USD",
    (p.deposits_usd - p.withdraw_usd) AS "Net USD",
    ep.mt_balance AS "Balance",
    tr.pnl AS "PnL",
    tr.cnt_trades AS "Number Of Trades",
        CASE
            WHEN ((tr.cnt_trades IS NULL) OR (tr.cnt_trades = 0)) THEN 'Yes'::text
            ELSE 'No'::text
        END AS "Never Trade"
   FROM (((((stg_tables.elastic_profile_v ep
     LEFT JOIN payments p ON (((p.profile_id)::text = (ep.player_uuid)::text)))
     LEFT JOIN trades tr ON ((tr.profile_uuid = (ep.player_uuid)::text)))
     LEFT JOIN public.agent_v sales ON (((sales.agent_user_hierarchy_uuid)::text = (ep.sales_rep)::text)))
     LEFT JOIN public.agent_v reten ON (((reten.agent_user_hierarchy_uuid)::text = (ep.retention_rep)::text)))
     LEFT JOIN public.brand_country bc ON (((bc.brand_id)::text = (ep.brand_id)::text)))
  WHERE ((bc.brand_country)::text = 'Albania'::text)
  ORDER BY p.deposits_cnt DESC NULLS LAST, p.withdraw_cnt DESC NULLS LAST, tr.cnt_trades DESC NULLS LAST;


ALTER TABLE frx_albania.dealer_albania_all_brands_v OWNER TO postgres;

--
-- Name: TABLE dealer_albania_all_brands_v; Type: ACL; Schema: frx_albania; Owner: postgres
--

GRANT SELECT ON TABLE frx_albania.dealer_albania_all_brands_v TO prod01_metabase;
GRANT SELECT ON TABLE frx_albania.dealer_albania_all_brands_v TO ybashkatov;


--
-- PostgreSQL database dump complete
--

