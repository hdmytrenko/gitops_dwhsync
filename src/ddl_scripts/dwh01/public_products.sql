--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

--
-- Name: products; Type: TABLE; Schema: public; Owner: data_sync
--

CREATE TABLE public.products (
    id integer NOT NULL,
    prod_name character varying(512) NOT NULL,
    prod_description text
);


ALTER TABLE public.products OWNER TO data_sync;

--
-- Name: products_id_seq; Type: SEQUENCE; Schema: public; Owner: data_sync
--

CREATE SEQUENCE public.products_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.products_id_seq OWNER TO data_sync;

--
-- Name: products_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: data_sync
--

ALTER SEQUENCE public.products_id_seq OWNED BY public.products.id;


--
-- Name: products id; Type: DEFAULT; Schema: public; Owner: data_sync
--

ALTER TABLE ONLY public.products ALTER COLUMN id SET DEFAULT nextval('public.products_id_seq'::regclass);


--
-- Name: products products_pkey; Type: CONSTRAINT; Schema: public; Owner: data_sync
--

ALTER TABLE ONLY public.products
    ADD CONSTRAINT products_pkey PRIMARY KEY (id);


--
-- Name: TABLE products; Type: ACL; Schema: public; Owner: data_sync
--

REVOKE ALL ON TABLE public.products FROM data_sync;


--
-- PostgreSQL database dump complete
--

