--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

--
-- Name: country_subdivision; Type: TABLE; Schema: stg_tables; Owner: postgres
--

CREATE TABLE stg_tables.country_subdivision (
    code character varying(255) NOT NULL,
    country_code character(2),
    name character varying(255),
    parent_code character varying(255),
    type character varying(255)
);


ALTER TABLE stg_tables.country_subdivision OWNER TO postgres;

--
-- Name: country_subdivision hrzn10_prod_dictionaries_country_subdivision_pkey; Type: CONSTRAINT; Schema: stg_tables; Owner: postgres
--

ALTER TABLE ONLY stg_tables.country_subdivision
    ADD CONSTRAINT hrzn10_prod_dictionaries_country_subdivision_pkey PRIMARY KEY (code);


--
-- Name: TABLE country_subdivision; Type: ACL; Schema: stg_tables; Owner: postgres
--

GRANT ALL ON TABLE stg_tables.country_subdivision TO data_sync;
GRANT SELECT ON TABLE stg_tables.country_subdivision TO prod01_metabase;
GRANT SELECT ON TABLE stg_tables.country_subdivision TO ybashkatov;


--
-- PostgreSQL database dump complete
--

