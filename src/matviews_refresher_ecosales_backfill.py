import sys
import uuid
from pprint import pprint

from sqlalchemy import func, Table

from utils.common import create_session_new, create_config_dictionary, write_exception,\
    generate_refreshing_plan_query, enrich_matviews_set, create_session_autocommit, write_log
from utils.constants import DbType, SynchronizationState, LOCK_QUERY_RECONCILE, EngineType, ProdNumber
from utils.writers import refresh_mat_views, refresh_mat_views_partitioned
from dwh_sync.dwh.mapping_writer_classes import RefreshingPlan

config_path = "settings_prod02.ini"
log_session = create_session_new(create_config_dictionary(config_path,
                                                          DbType.DWH.value,
                                                          engine_type=EngineType.postgres.value))
dwh_session = create_session_new(create_config_dictionary(config_path,
                                                          DbType.DWH.value,
                                                          schema_translate_map=True,
                                                          engine_type=EngineType.postgres.value),
                                 statement_timeout=1200000)
lock_session = create_session_new(create_config_dictionary(config_path,
                                                           DbType.DWH.value,
                                                           engine_type=EngineType.postgres.value))
refresh_mat_views_session = create_session_autocommit(create_config_dictionary(config_path,
                                                                               DbType.DWH.value,
                                                                               engine_type=EngineType.postgres.value),
                                                      statement_timeout=1200000)
log_state = SynchronizationState(log_session,
                                 uuid.uuid1(),
                                 dwh_session=dwh_session,
                                 lock_session=lock_session,
                                 source_engine_type=EngineType.postgres.value,
                                 prod_number=ProdNumber.prod02.value,
                                 refresh_mat_views_session=refresh_mat_views_session,
                                 sync_name=__file__)

default_views = ["public.v_ftd", "public.profile_data_mv", "public.agent_v"]


def main():
    try:
        log_state.table_name = Table()
        log_state.table_name.__tablename__ = 'materialized views'
        refresh_mat_views(LOCK_QUERY_RECONCILE, set(default_views), log_state)
        max_id = log_state.dwh_session.query(func.max(RefreshingPlan.id)).first()[0]
        views = enrich_matviews_set(generate_refreshing_plan_query(log_state, hot=False))
        log_state.table_name.__tablename__ = 'materialized views backfill'
        refresh_mat_views_partitioned(LOCK_QUERY_RECONCILE, views, log_state, max_id)
        log_state.message = "mat views backfill refreshed finished"
        write_log(log_state)
    except Exception as ex:
        print(f'\n{"-" * 40}\nException: {str(ex)}\n{"-" * 40}\n')
        log_state.message = f"DWH PROD02 Exception: {str(ex)}"
        write_exception(log_state)


if __name__ == "__main__":
    sys.exit(main())
