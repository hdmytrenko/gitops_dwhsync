FROM python:3.9.5-slim

ADD src/ /opt

RUN apt update && mkdir -p /usr/share/man/man1 && apt install default-jre-headless build-essential make gcc locales libgdal20 libgdal-dev -y && pip install -r /opt/requirements.txt

ADD drivers/ /usr/local/lib/python3.9/site-packages/pyspark/jars

RUN dpkg-reconfigure locales && locale-gen C.UTF-8 && /usr/sbin/update-locale LANG=C.UTF-8

ENV LC_ALL C.UTF-8
ENV JAVA_HOME /usr/lib/jvm/java-11-openjdk-amd64

WORKDIR /opt
