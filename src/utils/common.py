import configparser
import uuid
from datetime import datetime
from json import dumps
from operator import or_, and_
from typing import Union, Optional

from pyspark.sql import SparkSession
from pyspark.sql.types import StructType, StructField, StringType
from requests import post
from sqlalchemy import create_engine
from sqlalchemy.exc import OperationalError, ResourceClosedError
from sqlalchemy.orm import sessionmaker, Session

import dwh_sync.dwh.mapping_classes as mapping
from dwh_sync.dwh.mapping_writer_classes import RefreshingPlan
from utils.constants import SlackMessageType, SynchronizationState
from utils.constants import slack_args


def send_slack_message(message: str) -> int:
    """
    Send message to Slack channel
    :param message: input message
    :return: status of Slack API response
    """

    body = dumps({
        'channel': slack_args["channel"],
        'username': slack_args["username"],
        'text': f"*BI Sync Bot*\n> {message}"
    })
    headers = {'Content-type': 'application/json'}
    r = post(f"https://hooks.slack.com/services/{slack_args['workspace']}",
             data=body,
             headers=headers)
    print(f'slack status: {r.status_code}')
    return r.status_code


def get_datetime(str_timestamp: str) -> datetime:
    """
    Function converts str to datetime (with or without fractional secs)
    :param str_timestamp: string with datetime, e.g. 2020-03-27 24:02:00 or 2020-03-27 24:02:00.998
    :return: datetime object
    """
    if str_timestamp.find('Z') > 0:
        return datetime.strptime(str_timestamp, "%Y-%m-%dT%H:%M:%S.%fZ")
    elif str_timestamp.find('.') > 0:
        return datetime.strptime(str_timestamp, "%Y-%m-%dT%H:%M:%S.%f")
    else:
        return datetime.strptime(str_timestamp, "%Y-%m-%dT%H:%M:%S")


def create_config_dictionary(config_path: str,
                             db_type: str,
                             **session_options) -> dict:
    """
    Create config dictionary from config file and input arguments
    :param config_path: *.ini file relative path
    :param db_type: key for database type inside of *.ini file
    :param session_options: number of session options for session creation
    :return: dictionary of input session creation parameters
    """
    config = configparser.ConfigParser()
    config.read(config_path)
    conf = config._sections[db_type]
    for key, value in session_options.items():
        conf[key] = value
    return conf


def create_session_new(conf: dict, statement_timeout: int = None) -> Session:
    """
    Function create engine's session for different configuration
    :param statement_timeout: Maximum session timeout state in millis
    :param conf: input parameters for session creation
    :return: Session object
    """
    if statement_timeout:
        connect_args = {"options": f"-c statement_timeout={statement_timeout}"}
    else:
        connect_args = {}
    engine = create_engine('{}://{}:{}@{}:{}/{}'.format(
        conf['engine_type'],
        conf['user'],
        conf['password'],
        conf['host'],
        conf['port'],
        conf['dbname']),
        echo=False, connect_args=connect_args)
    if 'schema_translate_map' in conf:
        engine = engine.execution_options(schema_translate_map={None: conf['schema'],
                                                                "public": conf['schema']})
    return sessionmaker(bind=engine)()


def create_session_autocommit(conf: dict, statement_timeout: int = None) -> Session:
    """
    Function create engine's session for different configuration
    :param statement_timeout: Maximum session timeout state in millis
    :param conf: input parameters for session creation
    :return: Session object
    """
    if statement_timeout:
        connect_args = {"options": f"-c statement_timeout={statement_timeout}"}
    else:
        connect_args = {}
    engine = create_engine('{}://{}:{}@{}:{}/{}'.format(
        conf['engine_type'],
        conf['user'],
        conf['password'],
        conf['host'],
        conf['port'],
        conf['dbname']),
        echo=False, isolation_level='AUTOCOMMIT', connect_args=connect_args)
    if 'schema_translate_map' in conf:
        engine = engine.execution_options(schema_translate_map={None: conf['schema'],
                                                                "public": conf['schema']})
    return sessionmaker(bind=engine)()


def create_clickhouse_session_new(conf: dict) -> Session:
    """
    Function create engine's session for different configuration
    :param conf: input parameters for session creation
    :return: Session object
    """
    engine = create_engine('{}://{}:@{}/{}'.format(
        conf['engine_type'],
        conf['user'],
        conf['host'],
        conf['dbname']),
        echo=False)
    if 'schema_translate_map' in conf:
        engine = engine.execution_options(schema_translate_map={None: conf['schema'],
                                                                "public": conf['schema']})
    return sessionmaker(bind=engine)()


def write_log(log_state: SynchronizationState, message: Optional[str] = None,
              table_name: Optional[str] = None) -> uuid:
    """
    Put info about ETL health and issues into database
    :param table_name: set certain tablename to log row
    :param message: message for log writing
    :param log_state: instance of SynchronizationState class which consists of log arguments
    :return: log session uid
    """
    if message:
        log_state.message = message
    if table_name:
        log_state.table_name.__tablename__ = table_name
    if log_state.verbose:
        print(f"{datetime.now()},  {log_state.table_name.__tablename__}, {log_state.prod_number}, {log_state.sync_name}"
              f"\n{log_state.message}")
        return log_state.session_uid
    log_state.log_session.add(mapping.LoggingClass(session_uuid=str(log_state.session_uid),
                                                   execution_time=datetime.now(),
                                                   table_name=log_state.table_name.__tablename__,
                                                   error_message=log_state.message,
                                                   message_type=SlackMessageType.NOTIFY.value,
                                                   prod_number=log_state.prod_number,
                                                   sync_name=log_state.sync_name))
    log_state.log_session.commit()
    return log_state.session_uid


def write_exception(log_state: SynchronizationState) -> uuid:
    """
    Put exception and notifies Slack
    :param log_state: instance of SynchronizationState class which consists of log arguments
    :return: log session uid
    """
    if log_state.verbose:
        print(f"{datetime.now()},  {log_state.table_name.__tablename__}, {log_state.prod_number}, {log_state.sync_name}"
              f"\n{log_state.message}")
        return log_state.session_uid
    send_slack_message(f"{datetime.now()},  {log_state.table_name.__tablename__}, {log_state.prod_number}, "
                       f"{log_state.sync_name}"
                       f"\n{log_state.message}")
    log_state.log_session.add(mapping.LoggingClass(session_uuid=str(log_state.session_uid),
                                                   execution_time=datetime.now(),
                                                   table_name=log_state.table_name.__tablename__,
                                                   error_message=log_state.message,
                                                   message_type=SlackMessageType.EXCEPTION.value,
                                                   prod_number=log_state.prod_number,
                                                   sync_name=log_state.sync_name))
    log_state.log_session.commit()
    return log_state.session_uid


def commit_session(session: Session, log_state: SynchronizationState) -> bool:
    """
    Commit each object in session or rollback changes
    :param session: session to commit
    :param log_state: instance of SynchronizationState class which consists of log arguments
    :return: status of commit
    """
    try:
        session.commit()
        return True
    except Exception as ex:
        # TODO add different exception types
        log_state.message = f'Exception while committing: {str(ex)}! Rolling back'
        write_log(log_state)
        session.rollback()
        return False


def obtain_lock(session: Session, lock_record: str, log_state: SynchronizationState) -> bool:
    try:
        result = session.execute(lock_record.format(table=log_state.table_name.__tablename__))
    except OperationalError as e:
        if e.__dict__.get('code') == 'e3q8':
            log_state.message = f"lock for {log_state.table_name.__tablename__} is not obtained"
            log_state.message_type = SlackMessageType.NOTIFY.value
            write_log(log_state)
            session.rollback()
            return False
        raise
    if result.rowcount != 1:
        log_state.message = f"flag for {log_state.table_name.__tablename__} is not found"
        write_log(log_state)
        session.rollback()
        return False
    return True


def get_sync_length(log_state: SynchronizationState, filter_expression: str) -> range:
    """
    Gets length list of rows to fetch
    :param log_state: instance of SynchronizationState class which consists of log arguments
    :param filter_expression: filter to remove existing in destination table rows
    :return:  range for offset creation
    """
    if 'get_prefiltered_dataset' in log_state.table_name.__dict__.keys():
        query = log_state.table_name.get_prefiltered_dataset(log_state.source_session)
    else:
        query = log_state.source_session.query(log_state.table_name)
    return range(query.filter(filter_expression).count() // log_state.batch_size + 1)


def flatten(dictionary: dict, separator: str = '.', prefix: str = '') -> dict:
    """
    Recursively flattens input dictionary with nested dictionaries inside of values
    :param dictionary: input dictionary for flattening
    :param separator: separator for keys concatenating
    :param prefix: prefix for keys concatenating
    :return: flat dictionary without nested dictionary inside
    """
    return {prefix + separator + k if prefix else k: v
            for kk, vv in dictionary.items()
            for k, v in flatten(vv, separator, kk).items()
            } if isinstance(dictionary, dict) else {prefix: dictionary}


def handle_datetime_type(various_datetime: Union[datetime, str]) -> datetime:
    """
    Handles possibility of different types inside of date
    :param various_datetime: string or datetime object
    :return: datetime object
    """
    if type(various_datetime) == str:
        row_datetime = get_datetime(various_datetime)
    else:
        row_datetime = various_datetime
    return row_datetime


def enrich_year_month(log_state: SynchronizationState, object_to_enrich: dict, months: set) -> set:
    """
    Gets year and months from rows represented as dictionary
    :param log_state: instance of SynchronizationState class which consists of log arguments
    :param object_to_enrich: dictionary object read from
    :param months: initial set of year months for enrichment
    :return: set of year and months
    """
    if log_state.table_name.matview_refresh_date_filter:
        row_datetime = handle_datetime_type(
            object_to_enrich.get(log_state.table_name.matview_refresh_date_filter))
        months.add(remove_old_dates(row_datetime))
    return months


def remove_old_dates(old_date: datetime) -> str:
    """
    Changes date if it is lower than 2018-12-01
    :param old_date: old date which needed to be fixed
    :return: fixed date
    """
    if old_date.year <= 2018:
        return '201812'
    else:
        return str(old_date.year) + '{:02d}'.format(old_date.month)


def generate_refreshing_plan_query(log_state: SynchronizationState, hot: bool) -> list:
    """
    Generates set of matviews names from refreshing plan
    :param log_state: instance of SynchronizationState class which consists of log arguments
    :param hot: flag for flow of sync
    :return: list of refreshing plan matviews names
    """
    current_month_string = datetime.now().strftime('%Y%m')
    last_month_string = datetime(
        year=datetime.now().year,
        month=datetime.now().month - 1,
        day=1
    ).strftime('%Y%m')

    initial_query = log_state.dwh_session.query(RefreshingPlan.matview_name) \
        .group_by(RefreshingPlan.matview_name)

    if hot:
        result_query = initial_query.filter(or_(
            RefreshingPlan.matview_name.contains(current_month_string),
            RefreshingPlan.matview_name.contains(last_month_string)))
    else:
        result_query = initial_query.filter(and_(RefreshingPlan.refresh_time.is_(None), ~(or_(
            RefreshingPlan.matview_name.contains(current_month_string),
            RefreshingPlan.matview_name.contains(last_month_string)))))
    return result_query.all()


def group_matviews_by_month(views: set) -> list:
    month_groups = dict()
    for view_name in views:
        date_string = view_name[-6:]
        if date_string not in month_groups:
            month_groups[date_string] = set()
        month_groups[date_string].add(view_name)
    return list(month_groups.values())


def enrich_matviews_set(refreshing_list: list) -> set:
    """
    Enriches initial set of matviews by refreshing plan data
    :param views: set of initial views
    :param refreshing_list: list of matviews from refreshing plan
    :return: full set of  matview names
    """
    views = set()
    for x in refreshing_list:
        views.add('stg_tables_partitioned.' + str(x[0]))
    return views


def run_function(session: Session, log_state: SynchronizationState, function_name: str, parameter: str = None):
    try:
        query = f"SELECT * FROM {function_name}(%s)" % (f"'{parameter}'" if parameter else "")
        result = session.execute(query)
        response = result.fetchall()[0][0]
        commit_session(session, log_state)
    except (IndexError, ResourceClosedError):
        response = ''
    except Exception as ex:
        raise ex
    return f'Function {function_name} work finished' + f' with result: {response}' if response else ''


def get_sync_length_for_string_query(log_state: SynchronizationState, query: str) -> range:
    """
    Gets length list of rows to fetch
    :param log_state: instance of SynchronizationState class which consists of log arguments
    :param query: query for count
    :return:  range for offset creation
    """
    return range(log_state
                 .source_session
                 .execute(f"SELECT count(*) from ({query})").first()[0] // log_state.batch_size + 1)


def add_prefix(df, prefix):
    for field in df.schema:
        df = df.withColumnRenamed(field.name, f"{prefix}_{field.name}")
    return df


def add_unflattened_suffix(df, columns_to_select, field_name):
    df = df.withColumnRenamed(f"{field_name}", f"{field_name}_unflattened")
    columns_to_select.append(f"{field_name}_unflattened.*")
    columns_to_select.remove(f"{field_name}")
    return df, columns_to_select


def create_spark_session(app_name):
    spark = SparkSession.builder \
        .master("local[1]") \
        .appName(app_name) \
        .config("spark.driver.extraJavaOptions", "-Duser.timezone=GMT") \
        .config("spark.executor.extraJavaOptions", "-Duser.timezone=GMT") \
        .config("spark.sql.session.timeZone", "GMT") \
        .config("spark.executor.cores", "4") \
        .config("spark.executor.memory", "3g") \
        .config("spark.driver.memory", "12g") \
        .config("spark.cores.max", "4") \
        .config('spark.sql.legacy.parquet.int96RebaseModeInWrite', 'CORRECTED') \
        .config("spark.mongodb.input.sampleSize", 50000) \
        .getOrCreate()
    return spark


def select_filters_data_by_id_field(filter_data: dict, id_field_mapping: list):
    result_dict = dict()
    if filter_data:
        result_dict[id_field_mapping[1]] = set()
        for field_name in filter_data:
            field_part = field_name.split('_')[0]
            if field_part == id_field_mapping[0]:
                result_dict[id_field_mapping[1]].update(set(filter_data[field_name]))
        for key in result_dict:
            result_dict[key] = list(result_dict[key])
    return result_dict if any(result_dict.values()) else None


def select_filters_data_by_allowed_fields(filter_data: dict, table_name: str, fields_mapping: dict,
                                          allowed_columns: list):
    result_dict = dict()
    for field_name in filter_data:
        field_part, table_part = field_name.split('_')
        if field_part in list(fields_mapping) and (field_part in allowed_columns or table_part != table_name):
            mapped_field = fields_mapping[field_part]
            if mapped_field not in result_dict:
                result_dict[mapped_field] = set()
            result_dict[mapped_field].update(set(filter_data[field_name]))
    return result_dict


def create_date_filter_dictionary(date_field, date_range_tuple):
    return {
        date_field: date_range_tuple
    }


def create_empty_spark_dataframe(spark_session: SparkSession, table_name: str, fields: list):
    return spark_session.createDataFrame(
        data=[],
        schema=StructType(list(map(
            lambda x: StructField(x + '_' + table_name, StringType(), True),
            fields
        )))
    )


def make_dataframe_folding(df):
    fields_filter = dict()
    fields = df.schema.fields
    loaded_df = df.collect()
    for row in loaded_df:
        for i, value in enumerate(row):
            field_name = fields[i].name
            if field_name not in fields_filter:
                fields_filter[field_name] = set()
            if value:
                fields_filter[field_name].add(value)

    for field in fields_filter:
        fields_filter[field] = list(fields_filter[field])
    return fields_filter


def log_layer_rows_count(layer_num: int, log_state: SynchronizationState, items_dict: dict, table_name: str = str()):
    count_list = [f'{key}({len(items_dict[key])})' for key in items_dict.keys()]
    write_log(log_state, message=f'Layer {layer_num} rows count: {", ".join(count_list)}', table_name=table_name)