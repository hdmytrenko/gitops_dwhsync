--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

--
-- Name: sync_logs; Type: TABLE; Schema: full_sync_copies; Owner: postgres
--

CREATE TABLE full_sync_copies.sync_logs (
    id integer NOT NULL,
    session_uuid uuid,
    execution_time timestamp without time zone,
    table_name character varying(256),
    pk_val character varying(512),
    error_msg text,
    msg_type character varying(32)
);


ALTER TABLE full_sync_copies.sync_logs OWNER TO postgres;

--
-- Name: sync_logs_id_seq; Type: SEQUENCE; Schema: full_sync_copies; Owner: postgres
--

CREATE SEQUENCE full_sync_copies.sync_logs_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE full_sync_copies.sync_logs_id_seq OWNER TO postgres;

--
-- Name: sync_logs_id_seq; Type: SEQUENCE OWNED BY; Schema: full_sync_copies; Owner: postgres
--

ALTER SEQUENCE full_sync_copies.sync_logs_id_seq OWNED BY full_sync_copies.sync_logs.id;


--
-- Name: sync_logs id; Type: DEFAULT; Schema: full_sync_copies; Owner: postgres
--

ALTER TABLE ONLY full_sync_copies.sync_logs ALTER COLUMN id SET DEFAULT nextval('full_sync_copies.sync_logs_id_seq'::regclass);


--
-- Name: sync_logs sync_logs_pkey; Type: CONSTRAINT; Schema: full_sync_copies; Owner: postgres
--

ALTER TABLE ONLY full_sync_copies.sync_logs
    ADD CONSTRAINT sync_logs_pkey PRIMARY KEY (id);


--
-- Name: TABLE sync_logs; Type: ACL; Schema: full_sync_copies; Owner: postgres
--

GRANT SELECT ON TABLE full_sync_copies.sync_logs TO ybashkatov;


--
-- PostgreSQL database dump complete
--

