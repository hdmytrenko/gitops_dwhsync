--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

--
-- Name: affiliate; Type: TABLE; Schema: stg_tables; Owner: postgres
--

CREATE TABLE stg_tables.affiliate (
    id bigint NOT NULL,
    version bigint DEFAULT 0 NOT NULL,
    uuid character varying(60) NOT NULL,
    brand character varying(30) NOT NULL,
    first_name character varying(255) NOT NULL,
    last_name character varying(255) NOT NULL,
    phone character varying(255),
    email character varying(255) NOT NULL,
    country character varying(255),
    affiliate_type character varying(30),
    status character varying(255) NOT NULL,
    created_by character varying(255) NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    last_updated_by character varying(255),
    external_affiliate_id character varying(255),
    status_change_date timestamp without time zone,
    status_change_author character varying(255),
    status_reason character varying(255) NOT NULL,
    show_notes boolean DEFAULT false NOT NULL,
    show_sales_status boolean DEFAULT false NOT NULL,
    show_ftd_amount boolean DEFAULT false NOT NULL,
    allowed_ip_addresses text[],
    forbidden_countries text[],
    deleted boolean DEFAULT false NOT NULL,
    prod_num integer,
    public boolean DEFAULT false,
    cellexpert boolean DEFAULT false
);


ALTER TABLE stg_tables.affiliate OWNER TO postgres;

--
-- Name: affiliate affiliate_pkey; Type: CONSTRAINT; Schema: stg_tables; Owner: postgres
--

ALTER TABLE ONLY stg_tables.affiliate
    ADD CONSTRAINT affiliate_pkey PRIMARY KEY (id);


--
-- Name: affiliate stg_affiliate_uuid_key; Type: CONSTRAINT; Schema: stg_tables; Owner: postgres
--

ALTER TABLE ONLY stg_tables.affiliate
    ADD CONSTRAINT stg_affiliate_uuid_key UNIQUE (uuid);


--
-- Name: idx_stg_affiliate_created_at; Type: INDEX; Schema: stg_tables; Owner: postgres
--

CREATE INDEX idx_stg_affiliate_created_at ON stg_tables.affiliate USING btree (created_at);


--
-- Name: idx_stg_affiliate_email_brand; Type: INDEX; Schema: stg_tables; Owner: postgres
--

CREATE INDEX idx_stg_affiliate_email_brand ON stg_tables.affiliate USING btree (email, brand);


--
-- Name: idx_stg_affiliate_updated_at; Type: INDEX; Schema: stg_tables; Owner: postgres
--

CREATE INDEX idx_stg_affiliate_updated_at ON stg_tables.affiliate USING btree (updated_at);


--
-- Name: TABLE affiliate; Type: ACL; Schema: stg_tables; Owner: postgres
--

GRANT ALL ON TABLE stg_tables.affiliate TO data_sync;
GRANT SELECT ON TABLE stg_tables.affiliate TO ybashkatov;


--
-- PostgreSQL database dump complete
--

