--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

--
-- Name: country; Type: TABLE; Schema: stg_tables; Owner: data_sync
--

CREATE TABLE stg_tables.country (
    code integer NOT NULL,
    alpha_2 character(2),
    alpha_3 character(3),
    name character varying(255),
    official_name character varying(255)
);


ALTER TABLE stg_tables.country OWNER TO data_sync;

--
-- Name: country country_pkey; Type: CONSTRAINT; Schema: stg_tables; Owner: data_sync
--

ALTER TABLE ONLY stg_tables.country
    ADD CONSTRAINT country_pkey PRIMARY KEY (code);


--
-- Name: TABLE country; Type: ACL; Schema: stg_tables; Owner: data_sync
--

GRANT SELECT ON TABLE stg_tables.country TO ybashkatov;


--
-- PostgreSQL database dump complete
--

