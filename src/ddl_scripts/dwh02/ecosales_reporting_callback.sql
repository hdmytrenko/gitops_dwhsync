--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: callback; Type: VIEW; Schema: ecosales_reporting; Owner: postgres
--

CREATE VIEW ecosales_reporting.callback AS
 SELECT callback.id,
    callback.user_id,
    callback.operator_id,
    callback.callback_time,
    callback.status,
    callback.creation_time,
    callback.update_time,
    callback.is_deleted,
    callback.callback_id,
    callback.brand_id,
    callback.created_by,
    callback.prod_num
   FROM stg_tables.callback;


ALTER TABLE ecosales_reporting.callback OWNER TO postgres;

--
-- Name: TABLE callback; Type: ACL; Schema: ecosales_reporting; Owner: postgres
--

GRANT ALL ON TABLE ecosales_reporting.callback TO data_sync;
GRANT SELECT ON TABLE ecosales_reporting.callback TO ybashkatov;


--
-- PostgreSQL database dump complete
--

