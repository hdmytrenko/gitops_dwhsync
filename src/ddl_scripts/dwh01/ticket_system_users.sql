--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

--
-- Name: users; Type: TABLE; Schema: ticket_system; Owner: aleleko
--

CREATE TABLE ticket_system.users (
    id integer NOT NULL,
    user_name character varying(255) NOT NULL,
    first_name character varying(255) NOT NULL,
    last_name character varying(255) NOT NULL,
    gender integer NOT NULL,
    email character varying(255),
    ban integer NOT NULL,
    password character varying(60) NOT NULL,
    active integer NOT NULL,
    is_delete integer DEFAULT 0 NOT NULL,
    ext character varying(255),
    country_code integer,
    phone_number character varying(255),
    mobile character varying(255),
    agent_sign text NOT NULL,
    account_type character varying(255) NOT NULL,
    account_status character varying(255) NOT NULL,
    assign_group integer,
    primary_dpt integer,
    agent_tzone character varying(255) NOT NULL,
    daylight_save character varying(255) NOT NULL,
    limit_access character varying(255) NOT NULL,
    directory_listing character varying(255) NOT NULL,
    vacation_mode character varying(255) NOT NULL,
    company character varying(255) NOT NULL,
    role character varying(255) NOT NULL,
    internal_note character varying(255) NOT NULL,
    profile_pic character varying(255) NOT NULL,
    remember_token character varying(100),
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    user_language character varying(10),
    ib_name character varying(255),
    is_ib_manager integer DEFAULT 0 NOT NULL,
    can_receive_notifications_manager integer DEFAULT 1 NOT NULL,
    is_permitted_news integer DEFAULT 0 NOT NULL,
    organization_id integer,
    tickets_count integer DEFAULT 0 NOT NULL,
    last_login timestamp without time zone,
    is_permitted_reports integer DEFAULT 0 NOT NULL,
    slack_user_id character varying(255),
    can_track_time integer
);


ALTER TABLE ticket_system.users OWNER TO aleleko;

--
-- Name: users users_email_key; Type: CONSTRAINT; Schema: ticket_system; Owner: aleleko
--

ALTER TABLE ONLY ticket_system.users
    ADD CONSTRAINT users_email_key UNIQUE (email);


--
-- Name: users users_mobile_key; Type: CONSTRAINT; Schema: ticket_system; Owner: aleleko
--

ALTER TABLE ONLY ticket_system.users
    ADD CONSTRAINT users_mobile_key UNIQUE (mobile);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: ticket_system; Owner: aleleko
--

ALTER TABLE ONLY ticket_system.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: users users_user_name_key; Type: CONSTRAINT; Schema: ticket_system; Owner: aleleko
--

ALTER TABLE ONLY ticket_system.users
    ADD CONSTRAINT users_user_name_key UNIQUE (user_name);


--
-- PostgreSQL database dump complete
--

