from datetime import datetime
from typing import Optional

from pyspark.sql import DataFrame
from pyspark.sql.functions import upper, col, lit, lower, array_sort, array, concat, regexp_replace
from sqlalchemy import Table

from dwh_sync.structures.elastic_search_structures import profile_elastic_search_schema
from dwh_sync.structures.mongo_structures import profile_mongo_schema
from sync_entities.sync_output_columns import profile_map, profile_ext_map
from utils.common import create_config_dictionary, create_spark_session, add_prefix, select_filters_data_by_id_field, \
    create_date_filter_dictionary, create_empty_spark_dataframe, select_filters_data_by_allowed_fields, \
    create_session_new, commit_session
from utils.constants import SynchronizationState, DbType, EngineType
from utils.readers import read_postgresql_with_schema, read_mongo_with_schema, read_elastic_search_with_schema, \
    generate_initial_query, get_max_value_from_string_query
from utils.transformers import flatten_df_second_layer, add_compound_columns

ELASTICSEARCH_MAX_TERMS_COUNT = 65535
MONGODB_MAX_PARAMETERS_COUNT = 100000


class SparkPostgresSync:
    config_dict: dict
    sync_state: SynchronizationState
    db: str = None

    def __init__(self, sync_state: SynchronizationState, config_path):
        self.config_dict = create_config_dictionary(config_path,
                                                    DbType.SOURCE.value,
                                                    engine_type=EngineType.postgres.value)
        self.sync_state = sync_state
        self.sync_state.source_session = create_spark_session(self.__class__.__name__)

    def read_table(self, table_name: str):
        return read_postgresql_with_schema(
            spark=self.sync_state.source_session,
            postgresql_uri="jdbc:postgresql://{}:{}/{}".format(
                self.config_dict.get(f'prod0{self.sync_state.prod_number}_host'),
                self.config_dict.get('port'),
                self.db
            ),
            query=table_name,
            user=self.config_dict.get('user'),
            password=self.config_dict.get('password')
        )


class ProfileSync:
    def __init__(self, app_name, profile_uri, profile_db, profile_collection, log_state, config_dict,
                 filters_dict: dict = None, date_filter: datetime = None):
        self.spark_mongo = create_spark_session(app_name)
        self.profile_uri = profile_uri
        self.profile_db = profile_db
        self.profile_collection = profile_collection
        self.id_field_mapping = ('playerUUID', '_id')
        self.fields_mapping = {
            'playerUUID': '_id',
            'affiliateUUID': 'affiliate.uuid'
        }
        self.filters_dict = select_filters_data_by_allowed_fields(
            filters_dict,
            'elasticProfile',
            self.fields_mapping,
            ['playerUUID']
        ) if filters_dict else None
        self.log_state = log_state
        self.config_dict = config_dict
        self.date_filter = create_date_filter_dictionary(self.init_table_name().table_name.filter_column,
                                                         date_filter) if date_filter else None

    def init_table_name(self):
        self.log_state.table_name = Table()
        self.log_state.table_name.__tablename__ = 'elastic_profile'
        self.log_state.table_name.filter_column = 'lastUpdatedDate'
        self.log_state.schema = self.config_dict.get("schema")
        self.log_state.table_name.uuids = ['profile__id', 'profile_affiliate_uuid']
        return self.log_state

    def read_data(self, select_fields: tuple = tuple('*')):
        if self.filters_dict and len(self.filters_dict.get(self.id_field_mapping[1])) > MONGODB_MAX_PARAMETERS_COUNT:

            ids_list = list(self.filters_dict.get(self.id_field_mapping[1]))
            union_list = list()
            while ids_list:
                self.filters_dict[self.id_field_mapping[1]] = ids_list[:MONGODB_MAX_PARAMETERS_COUNT]
                partial_df = read_mongo_with_schema(self.spark_mongo,
                                                    self.log_state,
                                                    self.profile_uri,
                                                    self.profile_db,
                                                    self.profile_collection,
                                                    profile_mongo_schema,
                                                    fields_filter_dict=self.filters_dict,
                                                    date_range_filter=self.date_filter,
                                                    select_fields=select_fields
                                                    )
                union_list.append(partial_df)
                del ids_list[:MONGODB_MAX_PARAMETERS_COUNT]
            result_df = union_list.pop(0)
            for df in union_list:
                result_df.union(df)
            return result_df
        else:
            df = read_mongo_with_schema(self.spark_mongo,
                                        self.log_state,
                                        self.profile_uri,
                                        self.profile_db,
                                        self.profile_collection,
                                        profile_mongo_schema,
                                        fields_filter_dict=self.filters_dict,
                                        date_range_filter=self.date_filter,
                                        select_fields=select_fields
                                        )
            return df

    def flatten_df(self):
        df = add_prefix(self.read_data(), "profile")
        return flatten_df_second_layer(df)

    def map_df(self):
        return self.flatten_df().select(list(profile_map.keys()))

    def add_compound_df(self):
        return add_compound_columns(self.map_df(), self.init_table_name())

    def filter_df(self, invert_filters: bool = False) -> DataFrame:
        if not invert_filters:
            return self.add_compound_df().filter(
                ((col('profile_firstName').isNull()) | (upper(col('profile_firstName')) != 'TEST')
                 | (col('profile_lastName').isNull()) | (upper(col('profile_lastName')) != 'TEST'))
                & ((col('profile_brandId').isNull()) | (col('profile_brandId') != 'dobby')))
        else:
            return self.add_compound_df().filter(
                ~((col('profile_firstName').isNull()) | (upper(col('profile_firstName')) != 'TEST')
                  | (col('profile_lastName').isNull()) | (upper(col('profile_lastName')) != 'TEST')
                  & (col('profile_brandId').isNull()) | (col('profile_brandId') != 'dobby')))

    def get_incomplete_df(self):
        if not any([self.date_filter, self.filters_dict]):
            return create_empty_spark_dataframe(self.spark_mongo, 'elasticProfile',
                                                ['playerUUID', 'affiliateUUID', 'regDate'])
        else:
            return self.read_data(select_fields=['_id', 'affiliate.uuid',
                                                 'registrationDetails.registrationDate']
                                  ).withColumnRenamed("_id", "playerUUID_elasticProfile") \
                .withColumnRenamed("uuid", "affiliateUUID_elasticProfile").withColumnRenamed(
                'registrationDate', 'regDate_elasticProfile')

    def get_rejected_df(self, base_dataframe_list: list):
        base_dataframe = base_dataframe_list[0]
        rejected_df = self.filter_df(invert_filters=True)
        base_dataframe_list[0] = base_dataframe.join(
            rejected_df,
            rejected_df['profile__id'] == base_dataframe['playerUUID_elasticProfile'],
            'left'
        ).withColumnRenamed('profile__id', 'profile_condition')
        return base_dataframe_list


class ProfileExtSync:
    as_array = "existInBrands, latestSignInSessions, observableFrom, warnings"
    index = "profile"

    def __init__(self, app_name, elastic_profile_nodes, log_state, config_dict, filters_dict: dict = None,
                 date_filter: datetime = None):
        self.spark_elastic_search = create_spark_session(app_name)
        self.elastic_profile_nodes = elastic_profile_nodes
        self.id_field_mapping = ('playerUUID', '_id')
        self.log_state = log_state
        self.config_dict = config_dict
        self.filters_dict = select_filters_data_by_id_field(filters_dict, self.id_field_mapping)
        self.date_filter = create_date_filter_dictionary(self.init_table_name().table_name.filter_column,
                                                         date_filter) if date_filter else None

    def init_table_name(self):
        self.log_state.table_name = Table()
        self.log_state.table_name.__tablename__ = 'elastic_profile_ext'
        self.log_state.table_name.filter_column = 'lastUpdatedDate'
        self.log_state.schema = self.config_dict.get("schema")
        self.log_state.table_name.uuids = ['elastic_profile_uuid']
        return self.log_state

    def read_data(self, select_fields: list = None):
        if self.filters_dict and len(self.filters_dict.get(self.id_field_mapping[1])) > ELASTICSEARCH_MAX_TERMS_COUNT:
            ids_list = self.filters_dict.get(self.id_field_mapping[1])
            union_list = list()
            while ids_list:
                partial_df = read_elastic_search_with_schema(self.spark_elastic_search,
                                                             self.elastic_profile_nodes,
                                                             self.as_array,
                                                             profile_elastic_search_schema,
                                                             self.index,
                                                             fields_filter_dict={
                                                                 self.id_field_mapping[1]: ids_list[
                                                                                           :ELASTICSEARCH_MAX_TERMS_COUNT]
                                                             },
                                                             date_range_filter=self.date_filter,
                                                             select_fields=select_fields)
                union_list.append(partial_df)
                del ids_list[:ELASTICSEARCH_MAX_TERMS_COUNT]
            result_df = union_list.pop(0)
            for df in union_list:
                result_df.union(df)
            return result_df
        else:
            return read_elastic_search_with_schema(self.spark_elastic_search,
                                                   self.elastic_profile_nodes,
                                                   self.as_array,
                                                   profile_elastic_search_schema,
                                                   self.index,
                                                   fields_filter_dict=self.filters_dict,
                                                   date_range_filter=self.date_filter,
                                                   select_fields=select_fields)

    def flatten_df(self):
        df = add_prefix(self.read_data(), "elastic_profile")
        return flatten_df_second_layer(df)

    def map_df(self):
        return self.flatten_df().select(list(profile_ext_map.keys()))

    def add_compound_df(self):
        return add_compound_columns(self.map_df(), self.init_table_name())

    def filter_df(self):
        return self.add_compound_df()

    def get_incomplete_df(self):
        if not any([self.date_filter, self.filters_dict]):
            return create_empty_spark_dataframe(self.spark_elastic_search, 'elasticProfileExt',
                                                ['playerUUID', 'regDate'])
        else:
            return self.read_data(select_fields=['uuid', 'registrationDetails.registrationDate']).withColumnRenamed(
                "uuid", "playerUUID_elasticProfileExt").withColumnRenamed("registrationDate",
                                                                          "regDate_elasticProfileExt")


class HierarchyCustomerSync:
    def __init__(self, app_name, log_state, config_dict, filters_dict: dict = None,
                 date_filter: datetime = None):
        self.spark_postgres_sql = create_spark_session(app_name)
        self.uri = f"jdbc:postgresql://{config_dict['host']}:5432/hierarchy-updater"
        self.log_state = log_state
        self.config_dict = config_dict
        self.id_field_mapping = ('playerUUID', 'uuid')
        self.filters_dict = select_filters_data_by_id_field(filters_dict, self.id_field_mapping)
        self.date_filter = create_date_filter_dictionary(
            self.init_table_name().table_name.filter_column,
            date_filter) if date_filter else None

    def init_table_name(self):
        self.log_state.table_name = Table()
        self.log_state.table_name.__tablename__ = 'customer'
        self.log_state.table_name.filter_column = 'updated_at'
        self.log_state.schema = self.config_dict.get("hierarchy_schema")
        self.log_state.table_name.uuids = ['hierarchy_customer_uuid',
                                           'hierarchy_customer_sales_rep',
                                           'hierarchy_customer_retention_rep']
        return self.log_state

    def generate_query(self, select_fields=None):
        return generate_initial_query(self.init_table_name(),
                                      date_range_filter=self.date_filter,
                                      fields_filter_dict=self.filters_dict,
                                      select_fields=select_fields)

    def read_df(self):
        df = add_prefix(read_postgresql_with_schema(self.spark_postgres_sql,
                                                    self.uri,
                                                    self.config_dict["user"],
                                                    self.config_dict["password"],
                                                    self.generate_query()
                                                    ), "hierarchy_customer")
        return df

    def max_date(self):
        return get_max_value_from_string_query(self.init_table_name())

    def add_compound_df(self):
        return add_compound_columns(self.read_df(), self.init_table_name())

    def filter_df(self, invert_filters=False) -> DataFrame:
        tilde = '~' if invert_filters else ''
        filter_df_string = f"""df = self.add_compound_df().filter({tilde}((col("hierarchy_customer_deleted_at").isNull())
                                             & (col("hierarchy_customer_brand_id").isNull()
                                                | (col("hierarchy_customer_brand_id") != 'dobby'))))"""
        loc = locals()
        loc.update({'self': self, 'col': col, 'upper': upper, 'df': None})
        exec(filter_df_string, globals(), loc)
        return loc['df']

    def get_incomplete_df(self):
        if any([self.filters_dict, self.date_filter]):
            query = self.generate_query(select_fields=tuple(['uuid', 'created_at']))
            df = read_postgresql_with_schema(self.spark_postgres_sql,
                                             self.uri,
                                             self.config_dict["user"],
                                             self.config_dict["password"],
                                             query
                                             ).withColumnRenamed("uuid", "playerUUID_customer").withColumnRenamed(
                "created_at", "regDate_customer")
        else:
            table_name = 'customer'
            df = create_empty_spark_dataframe(
                create_spark_session(table_name),
                table_name,
                ["playerUUID", 'regDate']
            )
        return df

    def get_rejected_df(self, base_dataframe_list: list):
        base_dataframe = base_dataframe_list[0]
        rejected_df = self.filter_df(invert_filters=True)
        base_dataframe_list[0] = base_dataframe.join(
            rejected_df,
            base_dataframe['playerUUID_customer'] == rejected_df['hierarchy_customer_uuid'],
            'left'
        ).withColumnRenamed('hierarchy_customer_uuid', 'customer_condition')
        return base_dataframe_list


class PaymentSync:
    def __init__(self, app_name, log_state, config_dict, filters_dict: dict = None, date_filter=None):
        self.spark_postgres_sql = create_spark_session(app_name)
        self.uri = f"jdbc:postgresql://{config_dict['host']}:5432/payment"
        self.log_state = log_state
        self.config_dict = config_dict
        self.id_field_mapping = ('playerUUID', 'profile_id')
        self.filters_dict = filters_dict
        self.fields_mapping = {
            'playerUUID': 'profile_id',
            'operatorUUID': 'agent_id'
        }
        self.filters_dict = select_filters_data_by_allowed_fields(
            filter_data=filters_dict,
            table_name='payment',
            fields_mapping=self.fields_mapping,
            allowed_columns=['playerUUID']
        ) if filters_dict else None
        self.date_filter = create_date_filter_dictionary(self.init_table_name().table_name.filter_column,
                                                         date_filter) if date_filter else None

    def init_table_name(self):
        self.log_state.table_name = Table()
        self.log_state.table_name.__tablename__ = 'payment'
        self.log_state.table_name.filter_column = 'updated_at'
        self.log_state.schema = self.config_dict.get("schema")
        self.log_state.table_name.uuids = ['payments_profile_id', 'payments_agent_id', 'payments_payment_id']
        return self.log_state

    def generate_query(self, select_fields: tuple = None):
        return generate_initial_query(
            self.init_table_name(),
            fields_filter_dict=self.filters_dict,
            date_range_filter=self.date_filter,
            select_fields=select_fields
        )

    def read_df(self):
        df = add_prefix(read_postgresql_with_schema(self.spark_postgres_sql,
                                                    self.uri,
                                                    self.config_dict["user"],
                                                    self.config_dict["password"],
                                                    self.generate_query()
                                                    ), "payments")
        return df

    def max_date(self):
        return get_max_value_from_string_query(self.init_table_name())

    def add_compound_df(self):
        return add_compound_columns(self.read_df(), self.init_table_name()) \
            .withColumn("payments_prod_num", lit(self.log_state.prod_number))

    def filter_df(self, invert_filters: bool = False) -> DataFrame:
        tilde = '~' if invert_filters else ''
        filter_df_string = f"""df = self.add_compound_df().filter({tilde}(~(col('payments_deleted')) &
                                             ((col('payments_payment_method').isNull()) |
                                              ~(upper(col('payments_payment_method')).isin('FAKEPAL', 'BONUS',
                                                                                           'INTERNAL_TRANSFER'))) &
                                             ((col('payments_profile_first_name').isNull()) |
                                              (upper(col('payments_profile_first_name')) != 'TEST') |
                                              (col('payments_profile_last_name').isNull()) |
                                              (upper(col('payments_profile_last_name')) != 'TEST')) &
                                             ((col('payments_brand_id').isNull()) | (
                                                     col('payments_brand_id') != 'dobby'))))"""
        loc = locals()
        loc.update({'self': self, 'col': col, 'upper': upper, 'df': None})
        exec(filter_df_string, globals(), loc)
        return loc['df']

    def get_incomplete_df(self):
        if any([self.filters_dict, self.date_filter]):
            query = self.generate_query(select_fields=tuple(['profile_id', 'agent_id', 'payment_id', 'creation_time']))
            df = read_postgresql_with_schema(self.spark_postgres_sql,
                                             self.uri,
                                             self.config_dict["user"],
                                             self.config_dict["password"],
                                             query
                                             ).withColumnRenamed("profile_id", "playerUUID_payment").withColumnRenamed(
                "agent_id", "operatorUUID_payment")
        else:
            table_name = self.init_table_name().table_name.__tablename__
            df = create_empty_spark_dataframe(
                create_spark_session(table_name),
                table_name,
                ["playerUUID", "operatorUUID"]
            )
        return df

    def get_rejected_df(self, base_dataframe_list: list):
        rejected_df = self.filter_df(invert_filters=True)
        base_dataframe = base_dataframe_list[0]
        base_dataframe_list[0] = base_dataframe.join(
            rejected_df,
            rejected_df['payments_payment_id'] == base_dataframe['payment_id'],
            'left'
        ).withColumnRenamed('payments_payment_id', 'payment_condition')
        return base_dataframe_list


class HierarchyBranchSync:
    def __init__(self, app_name, log_state, config_dict, date_filter=None):
        self.spark_postgres_sql = create_spark_session(app_name)
        self.uri = f"jdbc:postgresql://{config_dict['host']}:5432/hierarchy-updater"
        self.log_state = log_state
        self.config_dict = config_dict
        self.date_filter = create_date_filter_dictionary(self.init_table_name().table_name.filter_column,
                                                         date_filter) if date_filter else None

    def init_table_name(self):
        self.log_state.table_name = Table()
        self.log_state.table_name.__tablename__ = 'branch'
        self.log_state.table_name.filter_column = 'updated_at'
        self.log_state.schema = self.config_dict.get("hierarchy_schema")
        self.log_state.table_name.uuids = ['hierarchy_branch_uuid']
        return self.log_state

    def generate_query(self, select_fields: tuple = None):
        return generate_initial_query(self.init_table_name(), date_range_filter=self.date_filter,
                                      select_fields=select_fields)

    def read_df(self):
        df = add_prefix(read_postgresql_with_schema(self.spark_postgres_sql,
                                                    self.uri,
                                                    self.config_dict["user"],
                                                    self.config_dict["password"],
                                                    self.generate_query(),
                                                    ), "hierarchy_branch")
        return df

    def max_date(self):
        return get_max_value_from_string_query(self.init_table_name())

    def add_compound_df(self):
        return add_compound_columns(self.read_df(), self.init_table_name()) \
            .withColumn("hierarchy_branch_prod_num", lit(self.log_state.prod_number))

    def filter_df(self):
        return self.add_compound_df().filter(
            (col("hierarchy_branch_deleted_at").isNull()) &
            (col("hierarchy_branch_brand_id").isNull() | (col('hierarchy_branch_brand_id') != 'dobby')) &
            (col('hierarchy_branch_name').isNotNull() & (col('hierarchy_branch_name') != 'unitestock') &
             ~(lower(col('hierarchy_branch_name')).like('%test%') | lower(col('hierarchy_branch_name')).like(
                 '%uat%')))
        )

    def get_incomplete_df(self):
        query = self.generate_query(select_fields=tuple(['uuid', 'id']))
        df = read_postgresql_with_schema(self.spark_postgres_sql,
                                         self.uri,
                                         self.config_dict["user"],
                                         self.config_dict["password"],
                                         query
                                         ).withColumnRenamed("uuid", "branchUUID_branch") \
            .withColumnRenamed("id", "branchID_branch")
        return df


class HierarchyOperatorSync:
    def __init__(self, app_name, log_state, config_dict, date_filter=None):
        self.spark_postgres_sql = create_spark_session(app_name)
        self.uri = f"jdbc:postgresql://{config_dict['host']}:5432/hierarchy-updater"
        self.log_state = log_state
        self.config_dict = config_dict
        self.date_filter = create_date_filter_dictionary(self.init_table_name().table_name.filter_column,
                                                         date_filter) if date_filter else None

    def init_table_name(self):
        self.log_state.table_name = Table()
        self.log_state.table_name.__tablename__ = 'operator'
        self.log_state.table_name.filter_column = 'updated_at'
        self.log_state.schema = self.config_dict.get("hierarchy_schema")
        self.log_state.table_name.uuids = ['hierarchy_operator_uuid']
        return self.log_state

    def generate_query(self, select_fields: tuple = None):
        return generate_initial_query(self.init_table_name(), date_range_filter=self.date_filter,
                                      select_fields=select_fields)

    def read_df(self):
        df = add_prefix(read_postgresql_with_schema(self.spark_postgres_sql,
                                                    self.uri,
                                                    self.config_dict["user"],
                                                    self.config_dict["password"],
                                                    self.generate_query()
                                                    ), "hierarchy_operator")
        return df

    def max_date(self):
        return get_max_value_from_string_query(self.init_table_name())

    def add_compound_df(self):
        return add_compound_columns(self.read_df(), self.init_table_name()) \
            .withColumn("hierarchy_operator_prod_num", lit(self.log_state.prod_number))

    def filter_df(self):
        return self.add_compound_df() \
            .filter(col('hierarchy_operator_brand_ids') != array_sort(array(*[lit(e) for e in ['dobby']])))

    def get_incomplete_df(self):
        query = self.generate_query(select_fields=tuple(['uuid', 'id']))
        df = read_postgresql_with_schema(self.spark_postgres_sql,
                                         self.uri,
                                         self.config_dict["user"],
                                         self.config_dict["password"],
                                         query
                                         ).withColumnRenamed("uuid", "operatorUUID_operatorH") \
            .withColumnRenamed("id", "operatorID_operatorH")
        return df


class HierarchyOperatorBranchSync:
    def __init__(self, app_name, log_state, config_dict):
        self.spark_postgres_sql = create_spark_session(app_name)
        self.uri = f"jdbc:postgresql://{config_dict['host']}:5432/hierarchy-updater"
        self.log_state = log_state
        self.config_dict = config_dict

    def init_table_name(self):
        self.log_state.table_name = Table()
        self.log_state.table_name.__tablename__ = 'operator_branch'
        self.log_state.table_name.filter_column = None
        self.log_state.schema = self.config_dict.get("hierarchy_schema")
        self.log_state.table_name.uuids = []
        return self.log_state

    def generate_query(self):
        return generate_initial_query(self.init_table_name())

    def read_df(self):
        df = add_prefix(read_postgresql_with_schema(self.spark_postgres_sql,
                                                    self.uri,
                                                    self.config_dict["user"],
                                                    self.config_dict["password"],
                                                    self.generate_query()
                                                    ), "hierarchy_operator_branch")
        return df

    def max_date(self):
        return get_max_value_from_string_query(self.init_table_name())

    def add_compound_df(self):
        return add_compound_columns(self.read_df(), self.init_table_name()) \
            .withColumn("hierarchy_operator_branch_prod_num", lit(self.log_state.prod_number))

    def filter_df(self):
        return self.add_compound_df()


class OperatorSync:
    def __init__(self, app_name, log_state, config_dict, date_filter=None, invert_filters: bool = False):
        self.spark_postgres_sql = create_spark_session(app_name)
        self.uri = f"jdbc:postgresql://{config_dict['host']}:5432/operator"
        self.log_state = log_state
        self.config_dict = config_dict
        self.date_filter = create_date_filter_dictionary(self.init_table_name().table_name.filter_column,
                                                         date_filter) if date_filter else None
        self.invert_filters = invert_filters

    def init_table_name(self):
        self.log_state.table_name = Table()
        self.log_state.table_name.__tablename__ = 'operator'
        self.log_state.table_name.filter_column = 'updated_at'
        self.log_state.schema = self.config_dict.get("operator")
        self.log_state.table_name.uuids = ['operator_uuid']
        return self.log_state

    def generate_query(self, select_fields: tuple = None):
        return generate_initial_query(self.init_table_name(), date_range_filter=self.date_filter,
                                      select_fields=select_fields)

    def read_df(self):
        df = add_prefix(read_postgresql_with_schema(self.spark_postgres_sql,
                                                    self.uri,
                                                    self.config_dict["user"],
                                                    self.config_dict["password"],
                                                    self.generate_query()
                                                    ), "operator")
        return df

    def max_date(self):
        return get_max_value_from_string_query(self.init_table_name())

    def add_compound_df(self):
        return add_compound_columns(self.read_df(), self.init_table_name()) \
            .withColumn("operator_prod_num", lit(self.log_state.prod_number)) \
            .withColumn('operator_first_name', regexp_replace('operator_first_name', '  ', ' ')) \
            .withColumn('operator_last_name', regexp_replace('operator_last_name', '  ', ' '))

    def filter_df(self):
        return self.add_compound_df().filter(~upper(col('operator_email')).like('%NEWAGESOL.COM')
                                             & ~upper(concat(col('operator_first_name'), lit(' '),
                                                             col('operator_last_name'))).like('%(TEST|TRASH)%') &
                                             ~upper(col('operator_uuid')).like('%(TEST|UAT+)%'))

    def get_incomplete_df(self):
        query = self.generate_query(select_fields=tuple(['uuid']))
        df = read_postgresql_with_schema(self.spark_postgres_sql,
                                         self.uri,
                                         self.config_dict["user"],
                                         self.config_dict["password"],
                                         query
                                         ).withColumnRenamed("uuid", "operatorUUID_operator")
        return df


class CountrySync:
    def __init__(self, app_name, log_state, config_dict):
        self.spark_postgres_sql = create_spark_session(app_name)
        self.uri = f"jdbc:postgresql://{config_dict['host']}:5432/{config_dict['dbname']}"
        self.log_state = log_state
        self.config_dict = config_dict

    def init_table_name(self):
        self.log_state.table_name = Table()
        self.log_state.table_name.__tablename__ = 'country'
        self.log_state.table_name.filter_column = None
        self.log_state.schema = 'stg_tables'
        self.log_state.table_name.uuids = []
        return self.log_state

    def generate_query(self):
        return generate_initial_query(self.init_table_name())

    def read_df(self):
        df = add_prefix(read_postgresql_with_schema(self.spark_postgres_sql,
                                                    self.uri,
                                                    self.config_dict["user"],
                                                    self.config_dict["password"],
                                                    f"select * from stg_tables.country"
                                                    ), "country")
        return df

    def max_date(self):
        return get_max_value_from_string_query(self.init_table_name())

    def add_compound_df(self):
        return add_compound_columns(self.read_df(), self.init_table_name())

    def filter_df(self):
        return self.add_compound_df()


class BrandCountrySync:
    def __init__(self, app_name, log_state, config_dict):
        self.spark_postgres_sql = create_spark_session(app_name)
        self.uri = f"jdbc:postgresql://{config_dict['host']}:5432/{config_dict['dbname']}"
        self.log_state = log_state
        self.config_dict = config_dict

    def init_table_name(self):
        self.log_state.table_name = Table()
        self.log_state.table_name.__tablename__ = 'brand_country'
        self.log_state.table_name.filter_column = None
        self.log_state.schema = 'public'
        self.log_state.table_name.uuids = []
        return self.log_state

    def generate_query(self):
        return generate_initial_query(self.init_table_name())

    def read_df(self):
        df = add_prefix(read_postgresql_with_schema(self.spark_postgres_sql,
                                                    self.uri,
                                                    self.config_dict["user"],
                                                    self.config_dict["password"],
                                                    f"select * from public.brand_country"
                                                    ), "brand_country")
        return df

    def max_date(self):
        return get_max_value_from_string_query(self.init_table_name())

    def add_compound_df(self):
        return add_compound_columns(self.read_df(), self.init_table_name())

    def filter_df(self):
        return self.add_compound_df()


class AffiliateSync:
    def __init__(self, app_name, log_state, config_dict, filters_dict: dict = None, date_filter=None):
        self.spark_postgres_sql = create_spark_session(app_name)
        self.uri = f"jdbc:postgresql://{config_dict['host']}:5432/affiliate"
        self.log_state = log_state
        self.config_dict = config_dict
        self.id_field_mapping = ('affiliateUUID', 'uuid')
        self.filters_dict = select_filters_data_by_id_field(
            filter_data=filters_dict,
            id_field_mapping=self.id_field_mapping
        ) if filters_dict else None
        self.date_filter = create_date_filter_dictionary(self.init_table_name().table_name.filter_column,
                                                         date_filter) if date_filter else None

    def init_table_name(self):
        self.log_state.table_name = Table()
        self.log_state.table_name.__tablename__ = 'affiliate'
        self.log_state.table_name.filter_column = 'updated_at'
        self.log_state.schema = self.config_dict.get("affiliate")
        self.log_state.table_name.uuids = ['affiliates_uuid']
        return self.log_state

    def generate_query(self, select_fields=None):
        return generate_initial_query(self.init_table_name(),
                                      fields_filter_dict=self.filters_dict,
                                      date_range_filter=self.date_filter,
                                      select_fields=select_fields)

    def read_df(self):
        df = add_prefix(read_postgresql_with_schema(self.spark_postgres_sql,
                                                    self.uri,
                                                    self.config_dict["user"],
                                                    self.config_dict["password"],
                                                    self.generate_query()
                                                    ), "affiliates")
        return df

    def max_date(self):
        return get_max_value_from_string_query(self.init_table_name())

    def add_compound_df(self):
        return add_compound_columns(self.read_df(), self.init_table_name()) \
            .withColumn("affiliate_prod_num", lit(self.log_state.prod_number))

    def filter_df(self, invert_filters: bool = False) -> DataFrame:
        tilde = '~' if invert_filters else ''
        filter_df_string = f"""df = self.add_compound_df().filter({tilde}((~col('affiliates_deleted'))
                                             & ((col('affiliates_email').isNull())
                                                | (~upper(col('affiliates_email')).like('%NEWAGESOL.COM%')))
                                             & ((upper(col('affiliates_first_name')).isNull()) |
                                                (~upper(col('affiliates_first_name')).like('%UNITESTOCK%')) |
                                                (~upper(col('affiliates_first_name')).like('%(TEST|TRASH)%'))) &
                                             ((upper(col('affiliates_last_name')).isNull()) |
                                              (~upper(col('affiliates_last_name')).like('%UNITESTOCK%')) |
                                              (~upper(col('affiliates_last_name')).like('%(TEST|TRASH)%')))))"""
        loc = locals()
        loc.update({'self': self, 'col': col, 'upper': upper, 'df': None})
        exec(filter_df_string, globals(), loc)
        return loc['df']

    def get_incomplete_df(self):
        if any([self.filters_dict, self.date_filter]):
            query = self.generate_query(select_fields=tuple(['uuid']))
            df = read_postgresql_with_schema(self.spark_postgres_sql,
                                             self.uri,
                                             self.config_dict["user"],
                                             self.config_dict["password"],
                                             query
                                             ).withColumnRenamed("uuid", "affiliateUUID_affiliate")
        else:
            table_name = self.init_table_name().table_name.__tablename__
            df = create_empty_spark_dataframe(
                create_spark_session(table_name),
                table_name,
                ["affiliateUUID"]
            )
        return df

    def get_rejected_frame(self, base_dataframe_list: list):
        rejected_df = self.filter_df(invert_filters=True)
        base_dataframe = base_dataframe_list[0]
        base_dataframe_list[0] = base_dataframe.join(
            rejected_df,
            rejected_df['affiliates_uuid'] == base_dataframe['affiliateUUID_elasticProfile'],
            'left'
        ).withColumnRenamed('affiliates_uuid', 'affiliate_condition')
        return base_dataframe_list


class SparkRuntimeCheckpoint:
    def __init__(self, app_name: str, log_state: SynchronizationState, config_dict: dict):
        self.table_name = 'spark_runtime_checkpoint'
        self.schema_name = 'full_sync_copies'
        self.app_name = app_name
        self.log_state = log_state
        self.config_dict = config_dict
        self.dwh_session = create_session_new(config_dict)
        self.record_name = f'{self.app_name}_prod0{self.config_dict["prod_num"]}'

    def get_sync_date_range(self):
        last_runtime = self.dwh_session.execute(
            f"SELECT last_runtime FROM {self.schema_name}.{self.table_name} WHERE table_name = '{self.record_name}'"
        ).fetchall()[0][0]
        if not last_runtime:
            raise Exception("Can't define last spark job runtime")
        else:
            last_runtime = last_runtime.replace(microsecond=0)
            return last_runtime, datetime.utcnow().replace(microsecond=0)

    def save_last_runtime(self, last_run_time: datetime):
        last_run_time = last_run_time
        self.dwh_session.execute(f"""UPDATE {self.schema_name}.{self.table_name}
             SET last_runtime = '{last_run_time}'::timestamp
             WHERE table_name = '{self.record_name}'""")
        commit_session(self.dwh_session, self.log_state)
