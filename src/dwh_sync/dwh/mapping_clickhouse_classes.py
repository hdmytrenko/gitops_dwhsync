
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, String, Integer, DATETIME, ARRAY, BOOLEAN, Numeric, Float, Text, Date, func
from sqlalchemy.ext.hybrid import hybrid_property

base = declarative_base()


class Activity(base):
    __tablename__ = "activity"
    # __table_args__ = {'schema': 'public'}
    profileUuid = Column(String, primary_key=True)
    profileType = Column(String)
    brandId = Column(String, primary_key=True)
    createdAt = Column(DATETIME(timezone=False), primary_key=True)
    eventType = Column(String, primary_key=True)
    eventAction = Column(String)
    eventValue = Column(String)
    eventLabel = Column(String)
    device = Column(String)
    ip = Column(String)
    dimension = Column(String)
    version = Column(String)
    location = Column(String)
    referer = Column(String)
    application = Column(String)

    @hybrid_property
    def filter_column(self):
        return self.createdAt

    @hybrid_property
    def filter_column_reconciliation(self):
        return self.createdAt

    def __repr__(self):
        return '<Activity(profileUuid={}, createdAt="{}")'. \
            format(self.profileUuid, self.createdAt)

