--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: h_user_v; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.h_user_v AS
 SELECT hb.branch_id AS parent_id,
    uh.id AS user_id,
    uh.user_type,
    uh.uuid AS user_uuid,
    uh.brand_ids AS user_brand_id,
    t.desk_language
   FROM ((ecosales_reporting.user_hierarchy uh
     LEFT JOIN ecosales_reporting.user_hierarchy_branch hb ON ((uh.id = hb.user_hierarchy_id)))
     LEFT JOIN public.h_team_v t ON ((hb.branch_id = t.team_id)))
  WHERE ((uh.deleted_at IS NULL) AND (hb.branch_id IS NOT NULL) AND ((uh.uuid)::text !~~ '%test%'::text) AND ((uh.uuid)::text !~~ '%+%'::text));


ALTER TABLE public.h_user_v OWNER TO postgres;

--
-- Name: TABLE h_user_v; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.h_user_v TO data_sync;
GRANT SELECT ON TABLE public.h_user_v TO ybashkatov;


--
-- PostgreSQL database dump complete
--

