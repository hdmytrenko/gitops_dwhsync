--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: trading_payment_all_status_v; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.trading_payment_all_status_v AS
 SELECT tp.login,
    tp.profile_id,
    ep.email,
    tp.payment_id,
    tp.currency,
    tp.external_reference,
    tp.type,
    tp.status,
        CASE tp.type
            WHEN 'DEPOSIT'::text THEN (tp.amount)::double precision
            ELSE (0)::double precision
        END AS deposit,
        CASE tp.type
            WHEN 'WITHDRAW'::text THEN (tp.amount)::double precision
            ELSE (0)::double precision
        END AS withdraw,
    (
        CASE tp.type
            WHEN 'DEPOSIT'::text THEN (tp.amount)::double precision
            ELSE (0)::double precision
        END -
        CASE tp.type
            WHEN 'WITHDRAW'::text THEN (tp.amount)::double precision
            ELSE (0)::double precision
        END) AS net_deposit,
        CASE tp.type
            WHEN 'TRANSFER_IN'::text THEN (tp.amount)::double precision
            ELSE (0)::double precision
        END AS transfer_in,
        CASE tp.type
            WHEN 'TRANSFER_OUT'::text THEN (tp.amount)::double precision
            ELSE (0)::double precision
        END AS transfer_out,
        CASE tp.type
            WHEN 'CREDIT_IN'::text THEN (tp.amount)::double precision
            ELSE (0)::double precision
        END AS credit_in,
        CASE tp.type
            WHEN 'CREDIT_OUT'::text THEN (tp.amount)::double precision
            ELSE (0)::double precision
        END AS credit_out,
        CASE tp.type
            WHEN 'DEPOSIT'::text THEN (tp.normalized_amount)::double precision
            ELSE (0)::double precision
        END AS deposit_usd,
        CASE tp.type
            WHEN 'WITHDRAW'::text THEN (tp.normalized_amount)::double precision
            ELSE (0)::double precision
        END AS withdraw_usd,
    (
        CASE tp.type
            WHEN 'DEPOSIT'::text THEN (tp.normalized_amount)::double precision
            ELSE (0)::double precision
        END -
        CASE tp.type
            WHEN 'WITHDRAW'::text THEN (tp.normalized_amount)::double precision
            ELSE (0)::double precision
        END) AS net_deposit_usd,
        CASE
            WHEN (((tp.normalized_amount)::double precision > (0)::double precision) AND (tp.normalized_amount IS NOT NULL)) THEN (round((tp.amount / tp.normalized_amount), 2))::numeric(14,4)
            ELSE (1)::numeric(14,4)
        END AS rate,
    (ep.kyc_status)::character varying(255) AS kyc_status,
    ep.acquisition_status,
    ep.registration_date AS creation_time_profile,
    tp.creation_time,
    (date_part('day'::text, tp.creation_time))::integer AS day_payment,
    (date_part('month'::text, tp.creation_time))::integer AS month_payment,
    (date_part('year'::text, tp.creation_time))::text AS year_payment,
    (date_part('week'::text, tp.creation_time))::integer AS week_payment,
    (date_part('isodow'::text, tp.creation_time))::integer AS day_of_week_creation_time,
    to_char(ep.registration_date, 'Day'::text) AS day_name_creation_time,
    upper((ep.country)::text) AS country,
    upper((tp.language)::text) AS language,
    tp.brand_id,
    (tp.payment_method)::character varying(80) AS payment_method,
        CASE tp.payment_method
            WHEN 'CASHIER'::text THEN 'CREDIT_CARD'::character varying
            WHEN 'PAYRETAILERS'::text THEN 'CREDIT_CARD'::character varying
            WHEN 'PAYTRIO'::text THEN 'CREDIT_CARD'::character varying
            ELSE tp.payment_method
        END AS group_payment_method,
    tp.is_published,
    tp.payment_transaction_id,
    tp.expiration_date,
    tp.version,
    ep.first_name AS profile_first_name,
    ep.last_name AS profile_last_name,
    tp.client_ip,
    tp.created_by,
    tp.is_mobile,
    tp.user_agent,
    upper((ep.country)::text) AS profile_country,
    tp.payment_aggregator,
    tp.agent_id,
    agent.name AS agent_name,
    ep.sales_status,
    ep.sales_rep,
    sales.name AS sales_name,
    (COALESCE(sales.user_type, 'Null'::character varying))::character varying(1020) AS sales_user_type,
    COALESCE(sales.agent_desk_name, 'Null'::text) AS sales_desk_name,
    sales.agent_language AS sales_language,
    ep.retention_status,
    ep.retention_rep,
    COALESCE(reten.name, 'Null'::text) AS retention_name,
    (COALESCE(reten.user_type, 'Null'::character varying))::character varying(1020) AS reten_user_type,
    COALESCE(reten.agent_desk_name, 'Null'::text) AS reten_desk_name,
    COALESCE(reten.agent_language, 'Null'::text) AS reten_language,
    ep.affiliate_uuid,
    COALESCE(concat(aff.first_name, ' ', aff.last_name), 'Null'::text) AS affiliate_name,
    ep.affiliate_referral,
    ep.affiliate_source,
    pc.name AS profile_country_name,
    (COALESCE(agent.user_type, 'Null'::character varying))::character varying(1020) AS user_type,
    COALESCE(agent.agent_desk_name, 'Null'::text) AS agent_desk_name,
    agent.agent_language,
        CASE
            WHEN (time_deposit.ftd_time = tp.creation_time) THEN 'Yes'::text
            ELSE 'No'::text
        END AS ftd_status,
    time_deposit.ftd_time,
    ((time_deposit.ftd_time)::date - (ep.registration_date)::date) AS day_to_ftd,
        CASE
            WHEN (time_deposit.ftd_time = tp.creation_time) THEN (1)::numeric
            ELSE (0)::numeric
        END AS ftd,
        CASE
            WHEN (time_deposit.ftd_time = tp.creation_time) THEN tp.amount
            ELSE NULL::numeric
        END AS ftd_deposit,
        CASE
            WHEN (time_deposit.ftd_time = tp.creation_time) THEN tp.normalized_amount
            ELSE NULL::numeric
        END AS ftd_deposit_usd,
        CASE
            WHEN ((time_deposit.ftd_time <> tp.creation_time) AND ((tp.type)::text = 'DEPOSIT'::text)) THEN (tp.profile_id)::text
            ELSE NULL::text
        END AS noftd,
        CASE
            WHEN ((time_deposit.ftd_time <> tp.creation_time) AND ((tp.type)::text = 'DEPOSIT'::text)) THEN tp.amount
            ELSE NULL::numeric
        END AS redeposit,
        CASE
            WHEN ((time_deposit.ftd_time <> tp.creation_time) AND ((tp.type)::text = 'DEPOSIT'::text)) THEN tp.normalized_amount
            ELSE NULL::numeric
        END AS redeposit_usd,
    ep.last_deposit_date AS last_deposit_time,
    tp.modification_time,
        CASE tp.type
            WHEN 'WITHDRAW'::text THEN tp.status_changed_at
            ELSE tp.creation_time
        END AS execution_time,
    tp.amount,
    tp.normalized_amount AS amount_usd,
    ep.last_note,
    ep.last_note_date
   FROM (((((((ecosales_reporting.payment tp
     LEFT JOIN ecosales_reporting.elastic_profile ep ON (((tp.profile_id)::text = (ep.player_uuid)::text)))
     LEFT JOIN public.country_v pc ON ((upper(((ep.country)::bpchar)::text) = (pc.alpha_2)::text)))
     LEFT JOIN public.v_ftd time_deposit ON (((tp.profile_id)::text = (time_deposit.profile_id)::text)))
     LEFT JOIN public.agent_v agent ON ((((tp.agent_id)::text = (agent.agent_user_hierarchy_uuid)::text) AND ((agent.user_type)::text = ANY (ARRAY['BRAND_ADMIN'::text, 'RETENTION_AGENT'::text, 'RETENTION_HOD'::text, 'RETENTION_LEAD'::text, 'RETENTION_MANAGER'::text, 'SALES_AGENT'::text, 'SALES_HOD'::text, 'SALES_LEAD'::text, 'SALES_MANAGER'::text])))))
     LEFT JOIN ecosales_reporting.affiliate aff ON ((((ep.affiliate_uuid)::text = (aff.uuid)::text) AND (aff.deleted = false))))
     LEFT JOIN public.agent_v sales ON ((((ep.sales_rep)::text = (sales.agent_user_hierarchy_uuid)::text) AND ((sales.user_type)::text = ANY (ARRAY[('SALES_AGENT'::character varying)::text, ('SALES_HOD'::character varying)::text, ('SALES_LEAD'::character varying)::text, ('SALES_MANAGER'::character varying)::text])))))
     LEFT JOIN public.agent_v reten ON ((((ep.retention_rep)::text = (reten.agent_user_hierarchy_uuid)::text) AND ((reten.user_type)::text = ANY (ARRAY[('RETENTION_AGENT'::character varying)::text, ('RETENTION_HOD'::character varying)::text, ('RETENTION_LEAD'::character varying)::text, ('RETENTION_MANAGER'::character varying)::text])))))
  WHERE ((((tp.payment_method)::text <> ALL (ARRAY['BONUS'::text, 'INTERNAL_TRANSFER'::text])) OR (tp.payment_method IS NULL)) AND (tp.deleted = false));


ALTER TABLE public.trading_payment_all_status_v OWNER TO postgres;

--
-- Name: TABLE trading_payment_all_status_v; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public.trading_payment_all_status_v TO ybashkatov;


--
-- PostgreSQL database dump complete
--

