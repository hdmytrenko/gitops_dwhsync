--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: v_ftd_old; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.v_ftd_old AS
 SELECT payment.profile_id,
    min(payment.creation_time) AS ftd_time,
    max(payment.creation_time) AS ltd_time,
    payment.prod_num
   FROM frx_reporting.payment
  WHERE ((payment.deleted = false) AND ((payment.status)::text = 'PAYMENT_COMPLETED'::text) AND ((payment.type)::text = 'DEPOSIT'::text) AND (((payment.payment_method)::text <> ALL (ARRAY['FAKEPAL'::text, 'INTERNAL_TRANSFER'::text, 'BONUS'::text])) OR (payment.payment_method IS NULL)))
  GROUP BY payment.profile_id, payment.prod_num;


ALTER TABLE public.v_ftd_old OWNER TO postgres;

--
-- Name: TABLE v_ftd_old; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public.v_ftd_old TO ybashkatov;


--
-- PostgreSQL database dump complete
--

