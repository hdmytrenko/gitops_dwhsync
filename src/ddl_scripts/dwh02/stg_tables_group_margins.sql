--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

--
-- Name: group_margins; Type: TABLE; Schema: stg_tables; Owner: data_sync
--

CREATE TABLE stg_tables.group_margins (
    server_id smallint NOT NULL,
    "group" character varying(16) NOT NULL,
    symbol character varying(12) NOT NULL,
    swap_short double precision,
    swap_long double precision,
    margin_divider double precision,
    prod_num smallint
);


ALTER TABLE stg_tables.group_margins OWNER TO data_sync;

--
-- Name: group_margins group_margins_pkey; Type: CONSTRAINT; Schema: stg_tables; Owner: data_sync
--

ALTER TABLE ONLY stg_tables.group_margins
    ADD CONSTRAINT group_margins_pkey PRIMARY KEY (server_id, "group", symbol);


--
-- Name: TABLE group_margins; Type: ACL; Schema: stg_tables; Owner: data_sync
--

GRANT SELECT ON TABLE stg_tables.group_margins TO ybashkatov;


--
-- PostgreSQL database dump complete
--

