import collections
import sys
import uuid

from dwh_sync.dwh.mapping_hierarchy import HierarchyOperator, HierarchyCustomer, HierarchyLead, HierarchyBranch, \
    HierarchyOperatorBranch
from dwh_sync.dwh.mapping_writer_hierarchy import HierarchyOperatorDWH, HierarchyCustomerDWH, HierarchyLeadDWH, \
    HierarchyBranchDWH, HierarchyOperatorBranchDWH
from dwh_sync.dwh.mapping_writer_ids_classes import HierarchyCustomerIds
from utils.common import write_log, create_session_new, create_config_dictionary, write_exception, \
    obtain_lock, get_sync_length
from utils.constants import DbType, SynchronizationState, LOCK_HIERARCHY_QUERY, EngineType, ProdNumber, LOCK_QUERY
from utils.transformers import get_filter_or_backfill_by_prod_num
from utils.writers import updating_dates_for_es_profile_trading_payment, \
    updating_dates_for_trading_payment_all_status_v, write_modified_rows, remove_prefetched_rows, refresh_mat_views

from dwh_sync.dwh.entities import entities_for_matviews_creation as entities


tables = [
    {
        'table': HierarchyOperator,
        'dbname': 'hierarchy-updater',
        'dwh_table': HierarchyOperatorDWH
    },
    {
        'table': HierarchyCustomer,
        'dbname': 'hierarchy-updater',
        'dwh_table': HierarchyCustomerDWH,
        'entity_for_ids': HierarchyCustomerIds
    },
    {
        'table': HierarchyLead,
        'dbname': 'hierarchy-updater',
        'dwh_table': HierarchyLeadDWH
    },
    {
        'table': HierarchyBranch,
        'dbname': 'hierarchy-updater',
        'dwh_table': HierarchyBranchDWH
    },
    {
        'table': HierarchyOperatorBranch,
        'dbname': 'hierarchy-updater',
        'dwh_table': HierarchyOperatorBranchDWH
    }
]

config_path = "settings_hierarchy_prod01.ini"
log_session = create_session_new(create_config_dictionary(config_path,
                                                          DbType.DWH.value,
                                                          engine_type=EngineType.postgres.value))
dwh_session = create_session_new(create_config_dictionary(config_path,
                                                          DbType.DWH.value,
                                                          schema_translate_map=True,
                                                          engine_type=EngineType.postgres.value))
lock_session = create_session_new(create_config_dictionary(config_path,
                                                           DbType.DWH.value,
                                                           engine_type=EngineType.postgres.value))
refresh_mat_views_session = create_session_new((create_config_dictionary(config_path,
                                                                         DbType.DWH.value,
                                                                         engine_type=EngineType.postgres.value)))
log_state = SynchronizationState(log_session,
                                 uuid.uuid1(),
                                 dwh_session=dwh_session,
                                 lock_session=lock_session,
                                 source_engine_type=EngineType.postgres.value,
                                 prod_number=ProdNumber.prod01.value,
                                 refresh_mat_views_session=refresh_mat_views_session,
                                 sync_name=__file__)

views = ["public.agent_v"]


def main():
    for table in tables:
        try:
            log_state.table_name = table['table']
            log_state.source_session = create_session_new(
                create_config_dictionary(config_path,
                                         DbType.SOURCE.value,
                                         dbname=table["dbname"],
                                         engine_type=EngineType.postgres.value))
            if not obtain_lock(log_state.lock_session, LOCK_HIERARCHY_QUERY, log_state):
                continue
            f = get_filter_or_backfill_by_prod_num(log_state, table['dwh_table'])
            if isinstance(log_state.table_name.filter_column, collections.Callable):
                remove_prefetched_rows(log_state, table['table'], table['dwh_table'])
            log_state.message = f"size of sync is no more than " \
                                f"{len(get_sync_length(log_state, f)) * log_state.batch_size} rows for " \
                                f"{log_state.table_name.__tablename__}"
            write_log(log_state)
            postgres_ids, postgres_months = write_modified_rows(log_state,
                                                                get_sync_length(log_state, f),
                                                                f,
                                                                table['dwh_table'])
            log_state.message = f"table {table['table'].__tablename__} synchronized"
            write_log(log_state)
            if table['dwh_table'] in entities.values():
                updating_dates_for_trading_payment_all_status_v(log_state,
                                                                postgres_months,
                                                                postgres_ids,
                                                                table['entity_for_ids'])
                updating_dates_for_es_profile_trading_payment(log_state,
                                                              postgres_months,
                                                              postgres_ids,
                                                              table['entity_for_ids'])
            log_state.lock_session.rollback()
        except Exception as ex:
            print(f'\n{"-" * 40}\nHierarchy Exception: {str(ex)}\n{"-" * 40}\n')
            log_state.message = f"DWH PROD01 Hierarchy Exception: {str(ex)}"
            write_exception(log_state)
            continue
    log_state.table_name.__tablename__ = 'agent_v'
    refresh_mat_views(LOCK_QUERY, views, log_state)


if __name__ == "__main__":
    sys.exit(main())
