--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

--
-- Name: v_ftd; Type: MATERIALIZED VIEW; Schema: public; Owner: postgres
--

CREATE MATERIALIZED VIEW public.v_ftd AS
 SELECT payment.profile_id,
    min(payment.creation_time) AS ftd_time,
    max(payment.creation_time) AS ltd_time
   FROM ecosales_reporting.payment
  WHERE ((payment.deleted = false) AND ((payment.status)::text = 'PAYMENT_COMPLETED'::text) AND ((payment.type)::text = 'DEPOSIT'::text) AND (((payment.payment_method)::text <> ALL (ARRAY['FAKEPAL'::text, 'INTERNAL_TRANSFER'::text, 'BONUS'::text])) OR (payment.payment_method IS NULL)))
  GROUP BY payment.profile_id
  WITH NO DATA;


ALTER TABLE public.v_ftd OWNER TO postgres;

--
-- Name: idx_mv_ftd_ftd_time; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_mv_ftd_ftd_time ON public.v_ftd USING btree (ftd_time);


--
-- Name: idx_mv_ftd_ltd_time; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_mv_ftd_ltd_time ON public.v_ftd USING btree (ltd_time);


--
-- Name: idx_mv_ftd_profile_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX idx_mv_ftd_profile_id ON public.v_ftd USING btree (profile_id);


--
-- PostgreSQL database dump complete
--

