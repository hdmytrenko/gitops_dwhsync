import sys
import uuid

from sqlalchemy import Table

from utils.common import create_session_new, create_config_dictionary, write_exception, write_log
from utils.constants import DbType, SynchronizationState, LOCK_QUERY, EngineType, ProdNumber
from utils.writers import refresh_custom_reports

config_path = "settings_prod01.ini"
log_session = create_session_new(create_config_dictionary(config_path,
                                                          DbType.DWH.value,
                                                          engine_type=EngineType.postgres.value))
dwh_session = create_session_new(create_config_dictionary(config_path,
                                                          DbType.DWH.value,
                                                          schema_translate_map=True,
                                                          engine_type=EngineType.postgres.value))
lock_session = create_session_new(create_config_dictionary(config_path,
                                                           DbType.DWH.value,
                                                           engine_type=EngineType.postgres.value))

log_state = SynchronizationState(log_session,
                                 uuid.uuid1(),
                                 dwh_session=dwh_session,
                                 lock_session=lock_session,
                                 source_engine_type=EngineType.postgres.value,
                                 prod_number=ProdNumber.prod01.value,
                                 sync_name=__file__)

report_refreshers = {"stg_tables.refresh_tab_165", "stg_cashir_tables.refresh_txv_customer"}


def main():
    try:
        log_state.table_name = Table()
        log_state.table_name.__tablename__ = 'custom reports'
        refresh_custom_reports(LOCK_QUERY, report_refreshers, log_state)
    except Exception as ex:
        print(f'\n{"-" * 40}\nException: {str(ex)}\n{"-" * 40}\n')
        log_state.message = f"DWH PROD01 Exception: {str(ex)}"
        write_exception(log_state)


if __name__ == "__main__":
    sys.exit(main())
