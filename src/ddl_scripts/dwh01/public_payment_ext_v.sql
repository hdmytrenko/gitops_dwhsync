--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: payment_ext_v; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.payment_ext_v AS
 SELECT tp.profile_id,
    tp.prod_num,
    tp.login,
    tp.payment_id,
    tp.status,
        CASE
            WHEN (time_deposit.ftd_time = tp.creation_time) THEN 'Yes'::text
            ELSE 'No'::text
        END AS ftd_status,
        CASE
            WHEN (time_deposit.ftd_time = tp.creation_time) THEN 1
            ELSE 0
        END AS ftd,
        CASE
            WHEN (time_deposit.ftd_time = tp.creation_time) THEN (tp.amount)::double precision
            ELSE NULL::double precision
        END AS ftd_deposit,
        CASE
            WHEN (time_deposit.ftd_time = tp.creation_time) THEN (tp.normalized_amount)::double precision
            ELSE NULL::double precision
        END AS ftd_deposit_usd,
        CASE
            WHEN ((time_deposit.ftd_time <> tp.creation_time) AND ((tp.type)::text = 'DEPOSIT'::text)) THEN tp.profile_id
            ELSE NULL::character varying
        END AS noftd,
        CASE
            WHEN ((time_deposit.ftd_time <> tp.creation_time) AND ((tp.type)::text = 'DEPOSIT'::text)) THEN (tp.amount)::double precision
            ELSE NULL::double precision
        END AS redeposit,
        CASE
            WHEN ((time_deposit.ftd_time <> tp.creation_time) AND ((tp.type)::text = 'DEPOSIT'::text)) THEN (tp.normalized_amount)::double precision
            ELSE NULL::double precision
        END AS redeposit_usd,
    time_deposit.ftd_time,
    time_deposit.ltd_time AS last_deposit_time,
    tp.currency,
        CASE tp.type
            WHEN 'DEPOSIT'::text THEN (tp.amount)::double precision
            ELSE (0)::double precision
        END AS deposit,
        CASE tp.type
            WHEN 'WITHDRAW'::text THEN (tp.amount)::double precision
            ELSE (0)::double precision
        END AS withdraw,
    (
        CASE tp.type
            WHEN 'DEPOSIT'::text THEN (tp.amount)::double precision
            ELSE (0)::double precision
        END -
        CASE tp.type
            WHEN 'WITHDRAW'::text THEN (tp.amount)::double precision
            ELSE (0)::double precision
        END) AS net_deposit,
        CASE tp.type
            WHEN 'TRANSFER_IN'::text THEN (tp.amount)::double precision
            ELSE (0)::double precision
        END AS transfer_in,
        CASE tp.type
            WHEN 'TRANSFER_OUT'::text THEN (tp.amount)::double precision
            ELSE (0)::double precision
        END AS transfer_out,
        CASE tp.type
            WHEN 'CREDIT_IN'::text THEN (tp.amount)::double precision
            ELSE (0)::double precision
        END AS credit_in,
        CASE tp.type
            WHEN 'CREDIT_OUT'::text THEN (tp.amount)::double precision
            ELSE (0)::double precision
        END AS credit_out,
        CASE tp.type
            WHEN 'DEPOSIT'::text THEN (tp.normalized_amount)::double precision
            ELSE (0)::double precision
        END AS deposit_usd,
        CASE tp.type
            WHEN 'WITHDRAW'::text THEN (tp.normalized_amount)::double precision
            ELSE (0)::double precision
        END AS withdraw_usd,
    (
        CASE tp.type
            WHEN 'DEPOSIT'::text THEN (tp.normalized_amount)::double precision
            ELSE (0)::double precision
        END -
        CASE tp.type
            WHEN 'WITHDRAW'::text THEN (tp.normalized_amount)::double precision
            ELSE (0)::double precision
        END) AS net_deposit_usd,
        CASE
            WHEN (((tp.normalized_amount)::double precision > (0)::double precision) AND (tp.normalized_amount IS NOT NULL)) THEN (round((tp.amount / tp.normalized_amount), 2))::numeric(14,4)
            ELSE (1)::numeric(14,4)
        END AS rate,
    tp.creation_time AS payment_creation_time,
    upper((tp.country)::text) AS country,
    tp.payment_method,
    tp.is_published,
    tp.client_ip,
    tp.is_mobile,
    tp.payment_aggregator,
    tp.agent_id,
    COALESCE(agent.name, 'Null'::text) AS agent_name,
    COALESCE(agent.agent_desk_name, 'Null'::text) AS agent_desk_name,
    agent.agent_language,
    (COALESCE(agent.user_type, 'Null'::character varying))::character varying(1020) AS user_type,
        CASE tp.payment_method
            WHEN 'CASHIER'::text THEN 'CREDIT_CARD'::character varying
            WHEN 'PAYRETAILERS'::text THEN 'CREDIT_CARD'::character varying
            WHEN 'PAYTRIO'::text THEN 'CREDIT_CARD'::character varying
            ELSE tp.payment_method
        END AS group_payment_method,
    tp.user_agent,
    1 AS index,
    tp.amount,
    tp.normalized_amount AS amount_usd,
    tp.type
   FROM ((frx_reporting.payment tp
     LEFT JOIN public.v_ftd time_deposit ON (((tp.profile_id)::text = (time_deposit.profile_id)::text)))
     LEFT JOIN public.agent_v agent ON (((tp.agent_id)::text = (agent.agent_user_hierarchy_uuid)::text)))
  WHERE (((tp.payment_method)::text <> ALL (ARRAY['BONUS'::text, 'INTERNAL_TRANSFER'::text])) AND (tp.deleted = false));


ALTER TABLE public.payment_ext_v OWNER TO postgres;

--
-- Name: TABLE payment_ext_v; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public.payment_ext_v TO prod01_metabase;
GRANT SELECT ON TABLE public.payment_ext_v TO ybashkatov;


--
-- PostgreSQL database dump complete
--

