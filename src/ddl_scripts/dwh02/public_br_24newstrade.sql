--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

--
-- Name: br_24newstrade; Type: MATERIALIZED VIEW; Schema: public; Owner: aleleko
--

CREATE MATERIALIZED VIEW public.br_24newstrade AS
 SELECT ep.phone,
    eptp.brand_id,
    eptp.profile_name,
    eptp.email,
    eptp.creation_time_profile,
    eptp.affiliate_name,
    eptp.affiliate_source
   FROM (public.es_profile_trading_payment eptp
     LEFT JOIN stg_tables.elastic_profile ep ON (((ep.player_uuid)::text = (eptp.profile_id)::text)))
  WHERE ((eptp.brand_id)::text = '24newstrade'::text)
  WITH NO DATA;


ALTER TABLE public.br_24newstrade OWNER TO aleleko;

--
-- Name: idx_br_24newstrade_email; Type: INDEX; Schema: public; Owner: aleleko
--

CREATE INDEX idx_br_24newstrade_email ON public.br_24newstrade USING btree (email);


--
-- Name: idx_br_24newstrade_phone; Type: INDEX; Schema: public; Owner: aleleko
--

CREATE INDEX idx_br_24newstrade_phone ON public.br_24newstrade USING btree (phone);


--
-- PostgreSQL database dump complete
--

