--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

--
-- Name: profile_data_mv; Type: MATERIALIZED VIEW; Schema: public; Owner: postgres
--

CREATE MATERIALIZED VIEW public.profile_data_mv AS
 SELECT t.player_uuid,
    t.risk_category,
    t.total_score
   FROM ( SELECT row_number() OVER (PARTITION BY profile_data.player_uuid ORDER BY profile_data.created_at DESC) AS rn,
            profile_data.id,
            profile_data.uuid,
            profile_data.player_uuid,
            profile_data.brand_id,
            profile_data.questionnaire_id,
            profile_data.total_score,
            profile_data.risk_category,
            profile_data.created_at,
            profile_data.created_by,
            profile_data.prod_num
           FROM stg_tables.profile_data) t
  WHERE (t.rn = 1)
  WITH NO DATA;


ALTER TABLE public.profile_data_mv OWNER TO postgres;

--
-- Name: idx_profile_data_mv_player_uuid; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX idx_profile_data_mv_player_uuid ON public.profile_data_mv USING btree (player_uuid);


--
-- Name: TABLE profile_data_mv; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.profile_data_mv TO data_sync;


--
-- PostgreSQL database dump complete
--

