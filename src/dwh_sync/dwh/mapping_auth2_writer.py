from sqlalchemy import Column, Integer, String
from sqlalchemy.ext.hybrid import hybrid_property

from dwh_sync.dwh.mapping_auth2 import base


class UsersDWH(base):
    __tablename__ = "users"
    __table_args__ = {'schema': 'stg_auth2_tables'}
    id = Column(Integer, primary_key=True)
    uuid = Column(String(64))
    uuid_compound = Column(String(64))
    prod_num = Column(Integer, primary_key=True)

    @hybrid_property
    def filter_column(self):
        return self.id

    @staticmethod
    def uuids():
        return ['uuid']

    def __repr__(self):
        return '<Users((id="{}", uuid={})'. \
            format(self.id, self.uuid)


class UserAuthorityDWH(base):
    __tablename__ = "user_authority"
    __table_args__ = {'schema': 'stg_auth2_tables'}
    user_id = Column(Integer, primary_key=True)
    authority_id = Column(Integer, primary_key=True)
    prod_num = Column(Integer)

    @staticmethod
    def uuids():
        return tuple()

    @staticmethod
    def filter_column():
        return None

    def __repr__(self):
        return '<UserAuthority(user_id: {}, authority_id: {})>'.format(self.user_id, self.authority_id)


class AuthoritiesDWH(base):
    __tablename__ = "authorities"
    __table_args__ = {'schema': 'stg_auth2_tables'}
    id = Column(Integer, primary_key=True)
    brand = Column(String)
    department = Column(String)
    role = Column(String)
    prod_num = Column(Integer, primary_key=True)

    @hybrid_property
    def filter_column(self):
        return self.id

    @staticmethod
    def uuids():
        return tuple()

    def __repr__(self):
        return '<Authorities((id="{}", brand={}, department={}, role={})'. \
            format(self.id, self.brand, self.department, self.role)


class AccessPhoneAuthoritiesDWH(base):
    __tablename__ = "access_phone_authorities"
    __table_args__ = {'schema': 'stg_auth2_tables'}
    authority_id = Column(Integer, primary_key=True)
    action_id = Column(Integer, primary_key=True)
    prod_num = Column(Integer)

    @staticmethod
    def filter_column():
        return None

    @staticmethod
    def uuids():
        return tuple()

    def __repr__(self):
        return '<AccessPhoneAuthoritiesDWH((authority_id="{}", prod_num="{}")'.format(
            self.authority_id,
            self.prod_num
        )
