--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

--
-- Name: profile_country_name; Type: MATERIALIZED VIEW; Schema: public; Owner: data_sync
--

CREATE MATERIALIZED VIEW public.profile_country_name AS
 SELECT ep.player_uuid,
    c.alpha_2,
    c.name
   FROM (frx_reporting.elastic_profile ep
     JOIN stg_tables.country c ON (((c.alpha_2)::text = (ep.country)::text)))
  WITH NO DATA;


ALTER TABLE public.profile_country_name OWNER TO data_sync;

--
-- Name: idx_profile_country_name; Type: INDEX; Schema: public; Owner: data_sync
--

CREATE UNIQUE INDEX idx_profile_country_name ON public.profile_country_name USING btree (player_uuid, name);


--
-- Name: TABLE profile_country_name; Type: ACL; Schema: public; Owner: data_sync
--

GRANT SELECT ON TABLE public.profile_country_name TO prod01_metabase;


--
-- PostgreSQL database dump complete
--

