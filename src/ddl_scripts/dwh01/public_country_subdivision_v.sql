--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: country_subdivision_v; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.country_subdivision_v AS
 SELECT country_subdivision.code,
    country_subdivision.country_code,
    country_subdivision.name,
    country_subdivision.parent_code,
    country_subdivision.type
   FROM stg_tables.country_subdivision;


ALTER TABLE public.country_subdivision_v OWNER TO postgres;

--
-- Name: TABLE country_subdivision_v; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public.country_subdivision_v TO prod01_metabase;
GRANT SELECT ON TABLE public.country_subdivision_v TO ybashkatov;


--
-- PostgreSQL database dump complete
--

