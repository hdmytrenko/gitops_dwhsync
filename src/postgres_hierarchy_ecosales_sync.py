import collections
import sys
import uuid


from dwh_sync.dwh.mapping_hierarchy import HierarchyOperator, HierarchyCustomer, HierarchyLead, HierarchyBranch, \
    HierarchyOperatorBranch
from dwh_sync.dwh.mapping_writer_hierarchy_ecosales import HierarchyOperatorDWH, HierarchyLeadDWH, HierarchyBranchDWH, \
    HierarchyOperatorBranchDWH, HierarchyCustomerDWH
from utils.common import write_log, create_session_new, create_config_dictionary, write_exception, \
    obtain_lock, get_sync_length
from utils.constants import ProdNumber
from utils.transformers import get_filter_or_backfill_by_prod_num
from utils.writers import write_modified_rows
from utils.constants import DbType, SynchronizationState, LOCK_QUERY, LOCK_HIERARCHY_QUERY, EngineType
from utils.writers import refresh_mat_views, remove_prefetched_rows

tables = [
    {
        'table': HierarchyOperator,
        'dbname': 'hierarchy-updater',
        'dwh_table': HierarchyOperatorDWH
    },
    {
        'table': HierarchyCustomer,
        'dbname': 'hierarchy-updater',
        'dwh_table': HierarchyCustomerDWH
    },
    {
        'table': HierarchyLead,
        'dbname': 'hierarchy-updater',
        'dwh_table': HierarchyLeadDWH
    },
    {
        'table': HierarchyBranch,
        'dbname': 'hierarchy-updater',
        'dwh_table': HierarchyBranchDWH
    },
    {
        'table': HierarchyOperatorBranch,
        'dbname': 'hierarchy-updater',
        'dwh_table': HierarchyOperatorBranchDWH
    }
]

config_path = "settings_hierarchy_ecosales.ini"
log_session = create_session_new(create_config_dictionary(config_path,
                                                          DbType.DWH.value,
                                                          engine_type=EngineType.postgres.value))
dwh_session = create_session_new(create_config_dictionary(config_path,
                                                          DbType.DWH.value,
                                                          schema_translate_map=True,
                                                          engine_type=EngineType.postgres.value))
lock_session = create_session_new(create_config_dictionary(config_path,
                                                           DbType.DWH.value,
                                                           engine_type=EngineType.postgres.value))
log_state = SynchronizationState(log_session,
                                 uuid.uuid1(),
                                 dwh_session=dwh_session,
                                 lock_session=lock_session,
                                 source_engine_type=EngineType.postgres.value,
                                 prod_number=ProdNumber.prod02.value,
                                 sync_name=__file__)
views = ["public.agent_v"]


def main():
    for table in tables:
        try:
            log_state.table_name = table['table']
            log_state.source_session = create_session_new(
                create_config_dictionary(config_path,
                                         DbType.SOURCE.value,
                                         dbname=table["dbname"],
                                         engine_type=EngineType.postgres.value))
            if not obtain_lock(log_state.lock_session, LOCK_HIERARCHY_QUERY, log_state):
                continue
            f = get_filter_or_backfill_by_prod_num(log_state, table['dwh_table'])
            if isinstance(log_state.table_name.filter_column, collections.Callable):
                remove_prefetched_rows(log_state, table['table'], table['dwh_table'])
            log_state.message = f"size of sync is no more than " \
                                f"{len(get_sync_length(log_state, f)) * log_state.batch_size} rows for " \
                                f"{log_state.table_name.__tablename__}"
            write_log(log_state)
            write_modified_rows(log_state, get_sync_length(log_state, f), f, table['dwh_table'])
            log_state.message = f"table {table['table'].__tablename__} synchronized"
            write_log(log_state)
            log_state.lock_session.rollback()
        except Exception as ex:
            print(f'\n{"-" * 40}\nHierarchy Exception: {str(ex)}\n{"-" * 40}\n')
            log_state.message = f"DWH PROD02 Hierarchy Exception: {str(ex)}"
            write_exception(log_state)
            continue
    refresh_mat_views(LOCK_QUERY, views, log_state)


if __name__ == "__main__":
    sys.exit(main())
