--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

--
-- Name: lead; Type: TABLE; Schema: stg_tables; Owner: postgres
--

CREATE TABLE stg_tables.lead (
    id uuid NOT NULL,
    status character varying(50),
    name character varying(50) NOT NULL,
    surname character varying(50) NOT NULL,
    email text NOT NULL,
    phone character varying(30) NOT NULL,
    mobile character varying(30),
    country character varying(50),
    source character varying(50),
    sales_status character varying(50),
    sales_agent character varying(50),
    registration_date timestamp without time zone,
    birth_date date,
    affiliate character varying(50),
    gender character varying(10),
    city character varying(50),
    language character varying(50),
    brand_id character varying(100) NOT NULL,
    status_changed_date timestamp without time zone,
    converted_to_client_uuid character varying(50) DEFAULT NULL::character varying,
    converted_by_operator_uuid character varying(50) DEFAULT NULL::character varying,
    version bigint DEFAULT 0 NOT NULL,
    migration_id bigint,
    observable_from text[],
    updated_at timestamp without time zone,
    deleted boolean DEFAULT false NOT NULL,
    created_at timestamp without time zone,
    prod_num integer
);


ALTER TABLE stg_tables.lead OWNER TO postgres;

--
-- Name: COLUMN lead.registration_date; Type: COMMENT; Schema: stg_tables; Owner: postgres
--

COMMENT ON COLUMN stg_tables.lead.registration_date IS 'OBSOLETE';


--
-- Name: COLUMN lead.observable_from; Type: COMMENT; Schema: stg_tables; Owner: postgres
--

COMMENT ON COLUMN stg_tables.lead.observable_from IS 'OBSOLETE';


--
-- Name: lead stg_lead_converted_to_client_uuid_key; Type: CONSTRAINT; Schema: stg_tables; Owner: postgres
--

ALTER TABLE ONLY stg_tables.lead
    ADD CONSTRAINT stg_lead_converted_to_client_uuid_key UNIQUE (converted_to_client_uuid, prod_num);


--
-- Name: lead stg_lead_email_brand_id_key; Type: CONSTRAINT; Schema: stg_tables; Owner: postgres
--

ALTER TABLE ONLY stg_tables.lead
    ADD CONSTRAINT stg_lead_email_brand_id_key UNIQUE (email, brand_id);


--
-- Name: lead stg_lead_pk; Type: CONSTRAINT; Schema: stg_tables; Owner: postgres
--

ALTER TABLE ONLY stg_tables.lead
    ADD CONSTRAINT stg_lead_pk PRIMARY KEY (id);


--
-- Name: idx_stg_lead_brand_id; Type: INDEX; Schema: stg_tables; Owner: postgres
--

CREATE INDEX idx_stg_lead_brand_id ON stg_tables.lead USING btree (brand_id);


--
-- Name: idx_stg_lead_brand_id_l; Type: INDEX; Schema: stg_tables; Owner: postgres
--

CREATE INDEX idx_stg_lead_brand_id_l ON stg_tables.lead USING btree (lower((brand_id)::text));


--
-- Name: idx_stg_lead_created_at; Type: INDEX; Schema: stg_tables; Owner: postgres
--

CREATE INDEX idx_stg_lead_created_at ON stg_tables.lead USING btree (created_at);


--
-- Name: idx_stg_lead_sales_agent; Type: INDEX; Schema: stg_tables; Owner: postgres
--

CREATE INDEX idx_stg_lead_sales_agent ON stg_tables.lead USING btree (sales_agent);


--
-- Name: idx_stg_lead_updated_at; Type: INDEX; Schema: stg_tables; Owner: postgres
--

CREATE INDEX idx_stg_lead_updated_at ON stg_tables.lead USING btree (updated_at);


--
-- Name: TABLE lead; Type: ACL; Schema: stg_tables; Owner: postgres
--

GRANT ALL ON TABLE stg_tables.lead TO data_sync;
GRANT SELECT ON TABLE stg_tables.lead TO ybashkatov;


--
-- PostgreSQL database dump complete
--

