--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

--
-- Name: groups; Type: TABLE; Schema: stg_tables; Owner: data_sync
--

CREATE TABLE stg_tables.groups (
    server_id smallint NOT NULL,
    enable boolean NOT NULL,
    use_swap boolean NOT NULL,
    hedge_prohibited boolean NOT NULL,
    stopout_skip_hedged boolean NOT NULL,
    manager_login integer NOT NULL,
    timeout integer NOT NULL,
    otp_mode integer NOT NULL,
    copies integer NOT NULL,
    reports integer NOT NULL,
    default_leverage integer NOT NULL,
    margin_call integer NOT NULL,
    margin_mode integer NOT NULL,
    margin_stopout integer NOT NULL,
    news integer NOT NULL,
    check_ie_prices integer NOT NULL,
    max_positions integer NOT NULL,
    close_reopen integer NOT NULL,
    close_fifo integer NOT NULL,
    hedge_large_leg integer NOT NULL,
    margin_type integer NOT NULL,
    archive_period integer NOT NULL,
    archive_max_balance integer NOT NULL,
    archive_pending_period integer NOT NULL,
    "group" character varying(16) NOT NULL,
    company character varying(128),
    signature character varying(128),
    support_page character varying(128),
    smtp_server character varying(64),
    smtp_login character varying(32),
    smtp_password character varying(32),
    support_email character varying(64),
    templates character varying(32),
    currency character varying(12),
    default_deposit double precision NOT NULL,
    credit double precision NOT NULL,
    interest_rate double precision NOT NULL,
    prod_num smallint
);


ALTER TABLE stg_tables.groups OWNER TO data_sync;

--
-- Name: groups groups_pkey; Type: CONSTRAINT; Schema: stg_tables; Owner: data_sync
--

ALTER TABLE ONLY stg_tables.groups
    ADD CONSTRAINT groups_pkey PRIMARY KEY (server_id, "group");


--
-- Name: TABLE groups; Type: ACL; Schema: stg_tables; Owner: data_sync
--

GRANT SELECT ON TABLE stg_tables.groups TO ybashkatov;


--
-- PostgreSQL database dump complete
--

