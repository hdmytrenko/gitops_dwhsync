--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

--
-- Name: sync_locks; Type: TABLE; Schema: full_sync_copies; Owner: aleleko
--

CREATE TABLE full_sync_copies.sync_locks (
    table_name character varying(255) NOT NULL,
    flag character varying(64)
);


ALTER TABLE full_sync_copies.sync_locks OWNER TO aleleko;

--
-- Name: sync_locks sync_locks_pkey; Type: CONSTRAINT; Schema: full_sync_copies; Owner: aleleko
--

ALTER TABLE ONLY full_sync_copies.sync_locks
    ADD CONSTRAINT sync_locks_pkey PRIMARY KEY (table_name);


--
-- Name: TABLE sync_locks; Type: ACL; Schema: full_sync_copies; Owner: aleleko
--

GRANT SELECT ON TABLE full_sync_copies.sync_locks TO ybashkatov;


--
-- PostgreSQL database dump complete
--

