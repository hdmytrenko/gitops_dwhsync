import sys
import uuid

from dwh_sync.dwh.mapping_classes_ecosales import Affiliate, Payment, Operator,  \
    Callback, TradeRecord, Lead, Trades, Users, Profile, TradingAccount, ProfileExt
from dwh_sync.dwh.reconciliation_details_ecosales import AffiliateDetails, PaymentDetails, \
    OperatorDetails, CallbackDetails, TradeRecordDetails, TradesDetails, LeadDetails, \
    UsersDetails, ProfileDetails, TradingAccountDetails, ProfileExtDetails
from utils.common import create_session_new, create_config_dictionary, obtain_lock, write_exception, \
    write_log
from utils.constants import SynchronizationState, EngineType, DbType,  ReconciliationType, \
    LOCK_QUERY_RECONCILE
from utils.writers import reconcile_postgres, reconcile_mongo, reconcile_elasticsearch

config_path = "settings_ecosales.ini"
tables = [
    {
        'table': ProfileExt,
        'dbname': 'profile',
        'reconciliation': ProfileExtDetails,
        'reconciliation_type': 'elasticsearch'
    },
    {
        'table': Profile,
        'dbname': 'profile-db',
        'reconciliation': ProfileDetails,
        'reconciliation_type': 'mongo'
    },
    {
        'table': TradingAccount,
        'dbname': 'trading-account-db',
        'reconciliation': TradingAccountDetails,
        'reconciliation_type': 'mongo'
    },
    {
        'table': Affiliate,
        'dbname': 'affiliate',
        'reconciliation': AffiliateDetails,
        'reconciliation_type': 'postgres'
    },
    {
        'table': Payment,
        'dbname': 'payment',
        'reconciliation': PaymentDetails,
        'reconciliation_type': 'postgres'
    },
    {
        'table': Operator,
        'dbname': 'operator',
        'reconciliation': OperatorDetails,
        'reconciliation_type': 'postgres'
    },
    {
        'table': Callback,
        'dbname': 'callback',
        'reconciliation': CallbackDetails,
        'reconciliation_type': 'postgres'
    },
    {
        'table': TradeRecord,
        'dbname': 'trading-activity',
        'reconciliation': TradeRecordDetails,
        'reconciliation_type': 'postgres'
    },
    {
        'table': Lead,
        'dbname': 'lead-updater',
        'reconciliation': LeadDetails,
        'reconciliation_type': 'postgres'
    },
    {
        'table': Trades,
        'dbname': 'mt4-connector-adapter',
        'reconciliation': TradesDetails,
        'reconciliation_type': 'postgres'
    },
    {
        'table': Users,
        'dbname': 'mt4-connector-adapter',
        'reconciliation': UsersDetails,
        'reconciliation_type': 'postgres'
    },
    {
        'table': TradingAccount,
        'dbname': 'trading-account-db',
        'reconciliation': TradingAccountDetails,
        'reconciliation_type': 'mongo'
    },
    {
        'table': Profile,
        'dbname': 'profile-db',
        'reconciliation': ProfileDetails,
        'reconciliation_type': 'mongo'
    }
]

log_session = create_session_new(create_config_dictionary(config_path,
                                                          DbType.DWH.value,
                                                          engine_type=EngineType.postgres.value))
dwh_session = create_session_new(create_config_dictionary(config_path,
                                                          DbType.DWH.value,
                                                          schema_translate_map=True,
                                                          engine_type=EngineType.postgres.value))
lock_session = create_session_new(create_config_dictionary(config_path,
                                                           DbType.DWH.value,
                                                           engine_type=EngineType.postgres.value))
log_state = SynchronizationState(log_session,
                                 uuid.uuid1(),
                                 dwh_session=dwh_session,
                                 lock_session=lock_session,
                                 source_engine_type=EngineType.postgres.value,
                                 reconciliation_interval_days=30,
                                 sync_name=__file__)


def main():
    try:
        for table in tables:
            log_state.table_name = table['table']
            reconciliation = table['reconciliation']()
            log_state.fix = False
            if not obtain_lock(log_state.lock_session, LOCK_QUERY_RECONCILE, log_state):
                continue
            if table['reconciliation_type'] == ReconciliationType.postgres.value:
                reconcile_postgres(log_state, config_path, table, reconciliation)
            elif table['reconciliation_type'] == ReconciliationType.mongo.value:
                reconcile_mongo(log_state, config_path, table, reconciliation)
            elif table['reconciliation_type'] == ReconciliationType.elasticsearch.value:
                reconcile_elasticsearch(log_state, config_path, reconciliation)
            log_state.message = f"Table {table['table'].__tablename__} reconciled"
            write_log(log_state)
            log_state.lock_session.rollback()

    except Exception as ex:
        print(f'\n{"-" * 40}\nException: {str(ex)}\n{"-" * 40}\n')
        log_state.message = f"DWH PROD01 Exception: {str(ex)}"
        if 'NotFoundError' in str(ex):
            print('Oh still that uncaught')
            write_log(log_state)
        else:
            write_exception(log_state)


if __name__ == "__main__":
    sys.exit(main())
