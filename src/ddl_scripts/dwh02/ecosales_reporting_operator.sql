--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: operator; Type: VIEW; Schema: ecosales_reporting; Owner: postgres
--

CREATE VIEW ecosales_reporting.operator AS
 SELECT operator.id,
    operator.uuid,
    operator.first_name,
    operator.last_name,
    operator.phone_number,
    operator.email,
    operator.country,
    operator.registration_date,
    operator.status,
    operator.status_change_date,
    operator.status_change_author,
    operator.registered_by,
    operator.status_reason,
    operator.operator_role,
    operator.didlogic_password,
    operator.external_affiliate_id,
    operator.updated_at,
    operator.deleted,
    operator.created_at,
    operator.prod_num
   FROM stg_tables.operator
  WHERE ((NOT operator.deleted) AND ((operator.email IS NULL) OR (upper((operator.email)::text) !~~ '%NEWAGESOL.COM'::text)) AND ((operator.first_name IS NULL) OR (upper((operator.first_name)::text) !~ similar_escape('%(TEST|TRASH)%'::text, NULL::text)) OR (operator.last_name IS NULL) OR ((operator.last_name)::text !~ similar_escape('%(TEST|TRASH)%'::text, NULL::text))) AND (upper((operator.uuid)::text) !~ similar_escape('%(TEST|UAT+)%'::text, NULL::text)));


ALTER TABLE ecosales_reporting.operator OWNER TO postgres;

--
-- Name: TABLE operator; Type: ACL; Schema: ecosales_reporting; Owner: postgres
--

GRANT ALL ON TABLE ecosales_reporting.operator TO data_sync;
GRANT SELECT ON TABLE ecosales_reporting.operator TO ybashkatov;


--
-- PostgreSQL database dump complete
--

