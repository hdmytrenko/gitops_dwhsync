import datetime
import uuid

from pyspark.sql.functions import col, lit, coalesce
from pyspark.sql.types import StringType, IntegerType, BooleanType, TimestampType, FloatType

from sync_entities.spark_sync_entities import ProfileSync, ProfileExtSync, HierarchyCustomerSync
from sync_entities.sync_output_columns import profile_ext_map, profile_map, hierarchy_customer_map
from utils.common import create_config_dictionary, create_session_new
from utils.constants import DbType, EngineType, LOCK_QUERY, SynchronizationState

lock_record = LOCK_QUERY


class ElasticProfileView:
    def __init__(self, config_path, filters_dict: dict = None, date_filter: datetime = None):
        self.config_dict_source = create_config_dictionary(config_path,
                                                           DbType.SOURCE.value,
                                                           engine_type=EngineType.postgres.value)
        self.config_dict_dwh = create_config_dictionary(config_path,
                                                        DbType.DWH.value,
                                                        engine_type=EngineType.postgres.value)
        self.log_session = create_session_new(self.config_dict_dwh)
        self.dwh_session = create_session_new(create_config_dictionary(config_path,
                                                                       DbType.DWH.value,
                                                                       schema_translate_map=True,
                                                                       engine_type=EngineType.postgres.value))
        self.log_state = SynchronizationState(self.log_session,
                                              uuid.uuid1(),
                                              source_engine_type=EngineType.postgres.value,
                                              prod_number=self.config_dict_dwh['prod_num'],
                                              dwh_session=self.dwh_session,
                                              sync_name=__file__)
        app_name = f"MongoSyncProd0{self.log_state.prod_number}"
        profile_uri = self.config_dict_source['mongo_uri']
        elastic_profile_nodes = self.config_dict_source['elastic_host']
        profile_db = "profile-db"
        profile_collection = "profile"
        self.profile_df = ProfileSync(app_name, profile_uri, profile_db, profile_collection, self.log_state,
                                      self.config_dict_source, filters_dict=filters_dict, date_filter=date_filter)
        self.profile_ext_df = ProfileExtSync(app_name, elastic_profile_nodes, self.log_state, self.config_dict_source,
                                             filters_dict=filters_dict, date_filter=date_filter)
        self.hierarchy_customer_df = HierarchyCustomerSync(app_name, self.log_state, self.config_dict_source,
                                                           filters_dict=filters_dict, date_filter=date_filter)

    def create_elastic_profile_view(self):
        profile_df = self.profile_df.filter_df().select('profile__id', 'profile_firstName', 'profile_lastName',
                                                        'profile_status_type',
                                                        'profile_status_reason', "profile_address_countryCode",
                                                        "profile_address_address",
                                                        'profile_address_city', 'profile_contacts_email',
                                                        'profile_languageCode',
                                                        'profile_registrationDetails_registrationDate',
                                                        'profile_gender',
                                                        'profile_lastUpdatedBy',
                                                        'profile_address_postCode', 'profile_birthDate',
                                                        'profile_affiliate_uuid',
                                                        'profile_affiliate_source', 'profile_brandId',
                                                        'profile_passport_countrySpecificIdentifier',
                                                        'profile_passport_countrySpecificIdentifierType',
                                                        'profile_passport_passportNumber',
                                                        'profile_affiliate_referral', 'profile_configuration_crs',
                                                        'profile__id_compound',
                                                        'profile_affiliate_uuid_compound')
        profile_ext_df = self.profile_ext_df.filter_df().select('elastic_profile_uuid',
                                                                'elastic_profile_lastNote_content',
                                                                'elastic_profile_lastNote_changedAt',
                                                                'elastic_profile_lastTrade_createdAt',
                                                                'elastic_profile_kyc_status',
                                                                'elastic_profile_paymentDetails_depositsCount',
                                                                'elastic_profile_balance_amount',
                                                                'elastic_profile_balance_currency',
                                                                'elastic_profile_firstNote_content',
                                                                'elastic_profile_firstNote_changedAt',
                                                                'elastic_profile_paymentDetails_firstDepositAmount',
                                                                'elastic_profile_paymentDetails_firstDepositCurrency',
                                                                'elastic_profile_paymentDetails_firstDepositTime',
                                                                'elastic_profile_paymentDetails_lastDepositTime',
                                                                'elastic_profile_migrationId',
                                                                # 'elastic_profile_kyc_changedAt'
                                                                )
        df = profile_df \
            .alias("df_1") \
            .join(profile_ext_df.alias('df_2'),
                  on=profile_df['profile__id'] == profile_ext_df['elastic_profile_uuid'],
                  how='left').select("df_1.*", "df_2.*").where(col('profile__id').isNotNull()).drop(
            'elastic_profile_uuid')
        return df

    def create_frx_reporting_elastic_profile_view(self):
        elastic_profile_view = self.create_elastic_profile_view()
        hierarchy_customer_df = self.hierarchy_customer_df.filter_df()
        df = elastic_profile_view.alias("df_1").join(
            hierarchy_customer_df.select('hierarchy_customer_uuid', 'hierarchy_customer_acquisition_status',
                                         'hierarchy_customer_sales_status', 'hierarchy_customer_retention_status',
                                         'hierarchy_customer_retention_rep', 'hierarchy_customer_sales_rep',
                                         'hierarchy_customer_sales_rep_compound',
                                         'hierarchy_customer_retention_rep_compound',
                                         'hierarchy_customer_sales_status_update_date').alias('df_2'),
            elastic_profile_view['profile__id'] == hierarchy_customer_df['hierarchy_customer_uuid'], 'left')
        return df

    def create_redundant_columns(self):
        df = self.create_frx_reporting_elastic_profile_view() \
            .withColumn('username', lit(None).cast(StringType())).withColumn('login', lit(None).cast(IntegerType())) \
            .withColumn('currency', lit(None).cast(StringType())) \
            .withColumn('email_verified', lit(None).cast(BooleanType())) \
            .withColumn('marketing_mail', lit(None).cast(BooleanType())) \
            .withColumn('marketing_sms', lit(None).cast(BooleanType())) \
            .withColumn('completed', lit(None).cast(BooleanType())) \
            .withColumn('migrated', lit(None).cast(BooleanType())) \
            .withColumn('creation_date', lit(None).cast(TimestampType())) \
            .withColumn('created_at', lit(None).cast(TimestampType())) \
            .withColumn('send_mail', lit(None).cast(BooleanType())). \
            withColumn('registration_ip', lit(None).cast(StringType())) \
            .withColumn('ip', lit(None).cast(StringType())) \
            .withColumn('withdrawable_amount_amount', lit(None).cast(FloatType())) \
            .withColumn('withdrawable_amount_currency', lit(None).cast(StringType())) \
            .withColumn('real_money_balance_amount', lit(None).cast(FloatType())) \
            .withColumn('real_money_balance_currency', lit(None).cast(StringType())) \
            .withColumn('bonus_balance_amount', lit(None).cast(FloatType())) \
            .withColumn('bonus_balance_currency', lit(None).cast(StringType())) \
            .withColumn('total_balance_amount', lit(None).cast(FloatType())) \
            .withColumn('total_balance_currency', lit(None).cast(StringType())) \
            .withColumn('first_deposit', lit(None).cast(BooleanType())) \
            .withColumn('tailor_made_email', lit(None).cast(BooleanType())) \
            .withColumn('tailor_made_sms', lit(None).cast(BooleanType())) \
            .withColumn('author_uuid', lit(None).cast(StringType())) \
            .withColumn('kyc_rep_name', lit(None).cast(StringType())) \
            .withColumn('fns_status', lit(None).cast(StringType())) \
            .withColumn('is_test_user', lit(None).cast(BooleanType())) \
            .withColumn('equity', lit(None).cast(FloatType())).withColumn('balance', lit(None).cast(BooleanType())) \
            .withColumn('base_currency_equity', lit(None).cast(FloatType())) \
            .withColumn('base_currency_credit', lit(None).cast(FloatType())) \
            .withColumn('mt_balance', lit(None).cast(FloatType())) \
            .withColumn('base_currency_margin', lit(None).cast(FloatType())) \
            .withColumn('credit', lit(None).cast(FloatType())) \
            .withColumn('margin', lit(None).cast(FloatType())) \
            .withColumn('first_deposit_date', lit(None).cast(TimestampType())) \
            .withColumn('last_deposit_date', lit(None).cast(TimestampType())) \
            .withColumn('mt_group', lit(None).cast(StringType())) \
            .withColumn('questionnaire_status', lit(None).cast(StringType())) \
            .withColumn('last_login', lit(None).cast(TimestampType())) \
            .withColumn('withdrawal_count', lit(None).cast(IntegerType())) \
            .withColumn('fsa_migration_status', lit(None).cast(StringType())) \
            .withColumn('phone', lit(None).cast(StringType()))
        return df

    def map_columns(self):
        df = self.create_redundant_columns()
        profile_map_copy = profile_map.copy()
        profile_map_copy.update(profile_ext_map)
        profile_map_copy.update(hierarchy_customer_map)
        profile_map_copy.update({"profile__id_compound": "player_uuid_compound",
                                 "hierarchy_customer_sales_rep_compound": "sales_rep_compound",
                                 "hierarchy_customer_retention_rep_compound": "retention_rep_compound",
                                 "profile_affiliate_uuid_compound": "affiliate_uuid_compound"})
        for old, new in profile_map_copy.items():
            df = df.withColumnRenamed(old, new)
        return df

    def create_custom_columns(self):
        df = self.map_columns()
        df = df.withColumnRenamed('last_note_changed_at', 'last_note_date') \
            .withColumnRenamed('last_trade_created_at', 'last_trade_date') \
            .withColumnRenamed('payment_details_deposits_count', 'deposit_count') \
            .withColumn('passport_expiration_date', lit(None).cast(TimestampType())) \
            .withColumn('prod_num', lit(self.log_state.prod_number)) \
            .withColumnRenamed('balance_amount', 'mt4_balance') \
            .withColumnRenamed('balance_currency', 'mt4_balance_currency') \
            .withColumnRenamed('first_note_changed_at', 'first_note_date') \
            .withColumn('referrer_profile_uuid', lit(None).cast(StringType())) \
            .withColumn('referral_hash', lit(None).cast(StringType())) \
            .withColumn('referrer_full_name', lit(None).cast(StringType())) \
            .withColumn('referrer_full_name', lit(None).cast(StringType())) \
            .withColumn('kyc_changed_at', lit(None).cast(TimestampType()))
        return df

    def filter_df(self):
        return self.create_custom_columns().select('player_uuid', 'username', 'last_name', 'first_name',
                                                   'profile_status',
                                                   'profile_status_reason', 'login', 'country', 'currency', 'address',
                                                   'city', 'phone', 'email',
                                                   'email_verified', 'marketing_mail', 'marketing_sms', 'language_code',
                                                   'completed', 'migrated', 'creation_date', 'created_at', 'send_mail',
                                                   'registration_date',
                                                   'registration_ip', 'ip', 'gender', 'updated_date', 'post_code',
                                                   'last_note', 'last_note_date',
                                                   'birth_date', 'withdrawable_amount_amount',
                                                   'withdrawable_amount_currency',
                                                   'real_money_balance_amount', 'real_money_balance_currency',
                                                   'bonus_balance_amount',
                                                   'bonus_balance_currency', 'total_balance_amount',
                                                   'tailor_made_email', 'first_deposit', 'author_uuid', 'kyc_rep_name',
                                                   'fns_status',
                                                   'last_trade_date', 'affiliate_uuid', 'affiliate_source',
                                                   'is_test_user', 'acquisition_status',
                                                   'sales_status', 'retention_status', 'equity', 'balance',
                                                   'kyc_status',
                                                   'base_currency_equity',
                                                   'base_currency_credit', 'mt_balance', 'base_currency_margin',
                                                   'credit',
                                                   'margin', 'brand_id',
                                                   'country_specific_identifier', 'country_specific_identifier_type',
                                                   'first_deposit_date',
                                                   'last_deposit_date', 'retention_rep', 'sales_rep', 'deposit_count',
                                                   'passport_number',
                                                   'mt_group', 'affiliate_referral', 'questionnaire_status',
                                                   'passport_expiration_date',
                                                   'crs', 'last_login', 'withdrawal_count', 'prod_num',
                                                   'fsa_migration_status', 'mt4_balance',
                                                   'mt4_balance_currency', 'first_note', 'first_note_date',
                                                   'ftd_amount',
                                                   'ftd_currency',
                                                   'ftd_time', 'ltd_time', 'migration_id', 'kyc_changed_at',
                                                   'referrer_profile_uuid',
                                                   'referral_hash', 'referrer_full_name', 'player_uuid_compound',
                                                   'sales_rep_compound',
                                                   'retention_rep_compound', 'affiliate_uuid_compound',
                                                   'sales_status_update_date')

    def get_incomplete_df(self):
        profile_df = self.profile_df.get_incomplete_df()
        profile_ext_df = self.profile_ext_df.get_incomplete_df()
        customer_df = self.hierarchy_customer_df.get_incomplete_df()
        return profile_df.join(
            profile_ext_df,
            on=profile_df['playerUUID_elasticProfile'] == profile_ext_df['playerUUID_elasticProfileExt'],
            how='full_outer'
        ).join(customer_df, col('playerUUID_elasticProfile') == customer_df['playerUUID_customer'],
               'full_outer').select(
            'playerUUID_elasticProfile',
            'affiliateUUID_elasticProfile',
            'playerUUID_elasticProfileExt',
            'playerUUID_customer',
            coalesce('playerUUID_elasticProfile', 'playerUUID_elasticProfileExt', 'playerUUID_customer',
                     lit(None)).alias('profile_idFolded'),
            coalesce('regDate_elasticProfile', 'regDate_elasticProfileExt', 'regDate_customer', lit(None)).alias(
                'reg_dateFolded')
        )

    def get_rejected_df(self, base_dataframe_list: list):
        self.profile_df.get_rejected_df(base_dataframe_list)
        self.hierarchy_customer_df.get_rejected_df(base_dataframe_list)
