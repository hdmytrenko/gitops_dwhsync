from pyspark.sql.types import StructType, StructField, LongType, StringType, TimestampType, ArrayType, IntegerType, \
    DoubleType, BooleanType

profile_elastic_search_schema = StructType(
    [StructField("accounts", ArrayType(
        StructType([
            StructField("login", LongType(), True),
            StructField("uuid", StringType(), True),
            StructField("type", StringType(), True)]), True), True),
     StructField("acquisition", StructType(
         [
          StructField("acquisitionStatus", StringType(), True),
          StructField("retentionRepresentative", StringType(), True),
          StructField("retentionStatus", StringType(), True),
          StructField("salesRepresentative", StringType(), True),
          StructField("salesStatus", StringType(), True),
          StructField("salesStatusUpdatedDate", StringType(), True)
         ]),
                 True),
     StructField(
         "address", StructType(
             [StructField("countryCode", StringType(), True)]
         )
     ),
     StructField(
         "affiliate", StructType(
             [
              StructField("affiliateType", StringType(), True),
              StructField("campaignId", StringType(), True),
              StructField("externalId", StringType(), True),
              StructField("firstName", StringType(), True),
              #StructField("referral", StringType(), True),
              #StructField("satellite", StringType(), True),
              #StructField("sms", StringType(), True),
              #StructField("source", StringType(), True),
              #StructField("uuid", StringType(), True)
             ]
         )
     ),
     StructField(
         "balance", StructType(
             [StructField("amount", DoubleType(), True),
              StructField("credit", DoubleType(), True),
              StructField("currency", StringType(), True)]
         )
     ),
     StructField("brandId", StringType(), True),
     StructField(
         "contacts", StructType(
             [StructField("additionalEmail", StringType(), True),
              StructField("additionalPhone", StringType(), True),
              StructField("email", StringType(), True),
              StructField("phone", StringType(), True)]
         )
     ),
     StructField("existInBrands", ArrayType(StringType()), True),
     StructField("firstName", StringType(), True),
     StructField(
         "firstNote", StructType(
             [StructField("changedAt", StringType(), True),
              StructField("changedBy", StringType(), True),
              StructField("content", StringType(), True),
              StructField("uuid", StringType(), True)]
         )
     ),
     StructField(
         "fsaMigrationInfo", StructType(
             [StructField("agreedToFsaMigrationDate", StringType(), True),
              StructField("fsaMigrationStatus", StringType(), True)]
         )
     ),
     StructField(
         "kyc", StructType(
             [StructField("changedAt", StringType(), True),
              StructField("status", StringType(), True)]
         )
     ),
     StructField("languageCode", StringType(), True),
     StructField(
         "lastActivity", StructType(
             [
                 StructField("application", StringType(), True),
                 StructField("date", StringType(), True),
                 StructField("eventLabel", StringType(), True),
                 StructField("eventType", StringType(), True),
                 StructField("eventValue", StringType(), True),
                 #StructField("location", StringType(), True)
             ]
         )
     ),
     StructField("lastName", StringType(), True),
     StructField(
         "lastNote", StructType(
             [StructField("changedAt", StringType(), True),
              StructField("changedBy", StringType(), True),
              StructField("content", StringType(), True),
              StructField("uuid", StringType(), True)]
         )
     ),
     StructField(
         "lastTrade", StructType(
             [StructField("createdAt", StringType(), True)]
         )
     ),
     StructField("lastUpdatedDate", StringType(), True),
     StructField(
         "latestSignInSessions", ArrayType(StructType(
             [
              #StructField("countryCode", StringType(), True),
              #StructField("ip", StringType(), True),
              #StructField("startedAt", StringType(), True)
              ]
         ), True)
     ),
     StructField("migrationId", StringType(), True),
     StructField("observableFrom", ArrayType(StringType()), True),
     StructField(
         "paymentDetails", StructType(
             [
              StructField("depositsCount", IntegerType(), True),
              StructField("firstDepositAmount", DoubleType(), True),
              StructField("firstDepositCurrency", StringType(), True),
              StructField("firstDepositTime", StringType(), True),
              StructField("lastDepositTime", StringType(), True),
              StructField("showFtdToAffiliate", BooleanType(), True)
             ]
         )
     ),
     StructField("questionnairePassed", BooleanType(), True),
     StructField(
         "referrer", StructType(
             [StructField("fullName", StringType(), True),
              StructField("referralHash", StringType(), True),
              StructField("uuid", StringType(), True)]
         )
     ),
     StructField(
         "registrationDetails", StructType(
             [
              StructField("registeredBy", StringType(), True),
              StructField("registrationDate", StringType(), True)
              ]
         )
     ),
     StructField(
         "status", StructType(
             [StructField("changedAt", StringType(), True),
              StructField("type", StringType(), True)]
         )
     ),
     StructField("timeZone", StringType(), True),
     StructField("uuid", StringType(), True),
     ])
