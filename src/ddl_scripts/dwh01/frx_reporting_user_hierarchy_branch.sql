--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: user_hierarchy_branch; Type: VIEW; Schema: frx_reporting; Owner: postgres
--

CREATE VIEW frx_reporting.user_hierarchy_branch AS
 SELECT user_hierarchy_branch.user_hierarchy_id,
    user_hierarchy_branch.branch_id,
    user_hierarchy_branch.prod_num
   FROM stg_tables.user_hierarchy_branch;


ALTER TABLE frx_reporting.user_hierarchy_branch OWNER TO postgres;

--
-- Name: TABLE user_hierarchy_branch; Type: ACL; Schema: frx_reporting; Owner: postgres
--

GRANT SELECT ON TABLE frx_reporting.user_hierarchy_branch TO prod01_metabase;
GRANT SELECT ON TABLE frx_reporting.user_hierarchy_branch TO ybashkatov;


--
-- PostgreSQL database dump complete
--

