import uuid
from dataclasses import dataclass
from datetime import timedelta

from enum import Enum
from typing import Union

from pymongo.database import Database
from elasticsearch import Elasticsearch
from pyspark.sql import SparkSession
from sqlalchemy import Table
from sqlalchemy.orm import Session


class DbType(Enum):
    SOURCE = "DB-Source"
    DWH = "DB-DWH"


class SlackMessageType(Enum):
    EXCEPTION = "exception"
    NOTIFY = "notify"
    VERBOSE = "verbose"


class EngineType(Enum):
    mysql = 'mysql+mysqlconnector'
    postgres = 'postgresql'
    clickhouse = 'clickhouse'


class ReconciliationType(Enum):
    mongo = 'mongo'
    elasticsearch = 'elasticsearch'
    postgres = 'postgresql'
    mysql = 'mysql'


class ProdNumber(Enum):
    prod01 = 1
    prod02 = 2
    prod03 = 3
    prod04 = 4


class IndexManipulationFunc(Enum):
    drop_indexes = 'stg_tables_partitioned.drop_matview_indexes'
    create_indexes = 'stg_tables_partitioned.create_matview_indexes'


LOCK_QUERY = "SELECT * FROM full_sync_copies.sync_locks where flag='1' and table_name='{table}' FOR UPDATE NOWAIT"
LOCK_TICKET_QUERY = "SELECT * FROM full_sync_copies.sync_locks where flag='1' and table_name='ticket_system.{table}' " \
                    "FOR UPDATE NOWAIT"
LOCK_TICKET_CASINO_DEMO_QUERY = "SELECT * FROM full_sync_copies.sync_locks where flag='1' and " \
                                "table_name='ticket_system_demo.{table}' FOR UPDATE NOWAIT"
LOCK_TICKET_CASINO_QUERY = "SELECT * FROM full_sync_copies.sync_locks where flag='1' and " \
                           "table_name='ticket_system_casino.{table}' FOR UPDATE NOWAIT"
LOCK_HIERARCHY_QUERY = "SELECT * FROM full_sync_copies.sync_locks where flag='1' and " \
                       "table_name='stg_hierarchy_tables.{table}' FOR UPDATE NOWAIT"
LOCK_AUTH2_QUERY = "SELECT * FROM full_sync_copies.sync_locks where flag='1' and " \
                   "table_name='stg_auth2_tables.{table}' FOR UPDATE NOWAIT"
LOCK_QUERY_TEST = "SELECT * FROM full_sync_copies.sync_locks_test where flag='1' and table_name='{table}' " \
                  "FOR UPDATE NOWAIT"
LOCK_QUERY_RECONCILE = "SELECT * FROM full_sync_copies.sync_locks where flag='1' and table_name='{table}' FOR UPDATE"

slack_args = {
    "channel": "bi_synclog",
    "username": "dwhSync",
    "workspace": "T010Q3F3HB5/B011PHZ241Y/jJM2BQMGhe9GqUCthnyehQcC"
}

METADATA_QUERY = """select
                        nsp.nspname as object_schema, 
                        cls.relname as object_name,
                        case 
                        cls.relkind when 'r' then 'TABLE'
                        when 'm' then 'MATERIALIZED_VIEW'
                        when 'v' then 'VIEW'
                        else cls.relkind::text
                        end as object_type
                    from
                        pg_class cls
                    join pg_namespace nsp on
                        nsp.oid = cls.relnamespace
                    where
                        (nsp.nspname not in ('information_schema', 'pg_catalog', 'full_sync_copies') 
                        or cls.relname in ('sync_errors', 'sync_locks', 'sync_logs'))
                        and nsp.nspname not like 'pg_toast%'
                        and cls.relkind in ('r', 'm', 'v')
                    order by
                        nsp.nspname, 
                        cls.relname;"""

FLAT_VIEW_QUERY = """insert
    into
        custom_reports.tab_165_monthly_gross_deposit
    select
        *
    from
        custom_reports.view_165_monthly_gross_deposit on
    conflict on
        constraint pk_tab_165_monthly_gross_deposit_payment_id do
    update
    set
        creation_time = excluded.creation_time,
        deposit_usd = excluded.deposit_usd,
        withdraw_usd = excluded.withdraw_usd,
        net_deposit_usd = excluded.net_deposit_usd,
        ftd = excluded.ftd,
        brand_id = excluded.brand_id,
        "language" = excluded."language",
        profile_country_name = excluded.profile_country_name,
        brand_country = excluded.brand_country,
        group_payment_method = excluded.group_payment_method,
        agent_desk_name = excluded.agent_desk_name,
        updated_at = excluded.updated_at;"""


@dataclass
class SynchronizationState:
    log_session: Session
    session_uid: uuid.UUID
    table_name: Table = None
    message: str = 'Empty message'
    verbose: bool = False
    source_session: Union[Session, Database, Elasticsearch, SparkSession] = None
    batch_size: int = 10000
    dwh_session: Session = None
    lock_session: Session = None
    refresh_mat_views_session: Session = None
    source_engine_type: str = str()
    elasticsearch_scroll_size: str = '5m'
    reconciliation_interval_days: int = 30
    fix: bool = False
    prod_number: int = None
    sync_name: str = str()
    postgres_statement_timeout: int = None

    def __post_init__(self):
        self.sync_name = self.sync_name.split('/')[-1]


class Result:
    def __init__(self, current_day, object_id, row_cnt):
        self.hour = (current_day + timedelta(hours=object_id)).replace(minute=0, second=0, microsecond=0)
        self.group_key = 'empty'
        self.metric_sum = '0'
        self.row_cnt = int(row_cnt)

    def __repr__(self):
        return f"{self.hour}, {self.group_key}, {self.metric_sum}, {self.row_cnt}"
