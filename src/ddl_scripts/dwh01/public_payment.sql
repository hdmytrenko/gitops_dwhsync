--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

--
-- Name: payment; Type: TABLE; Schema: public; Owner: ybashkatov
--

CREATE TABLE public.payment (
    id bigint NOT NULL,
    login character varying(50),
    profile_id character varying(50),
    payment_id character varying(50),
    amount numeric,
    currency character varying(20),
    external_reference character varying(250),
    type character varying(30),
    status character varying(30),
    creation_time timestamp without time zone NOT NULL,
    country character varying(20),
    language character varying(20),
    brand_id character varying(20),
    payment_method character varying(55),
    is_published boolean DEFAULT false,
    payment_transaction_id character varying(50),
    expiration_date timestamp without time zone,
    version bigint DEFAULT 0 NOT NULL,
    profile_first_name character varying(100) DEFAULT NULL::character varying,
    profile_last_name character varying(100) DEFAULT NULL::character varying,
    client_ip character varying(50) DEFAULT NULL::character varying,
    created_by character varying(50) DEFAULT NULL::character varying,
    is_mobile boolean,
    user_agent character varying(100) DEFAULT NULL::character varying,
    profile_country character varying(20),
    payment_aggregator character varying(20),
    agent_name character varying(100),
    agent_id character varying(50),
    payment_migration_id character varying(30),
    user_migration_id character varying(30),
    normalized_amount numeric,
    decline_reason character varying(100),
    modified_by character varying(50),
    ex_rate numeric,
    linked_transaction_id character varying(50),
    moto boolean DEFAULT false NOT NULL,
    last_operation_type character varying(55) DEFAULT 'CREATION'::character varying NOT NULL,
    updated_at timestamp without time zone,
    status_changed_at timestamp without time zone,
    account_uuid character varying(128),
    first_time_deposit boolean DEFAULT false,
    affiliate_uuid character varying(50),
    deleted boolean DEFAULT false NOT NULL,
    agent_branches text[],
    warnings text[] DEFAULT ARRAY[]::text[],
    platform_type character varying(128) DEFAULT 'MT4'::character varying NOT NULL,
    account_type character varying(128) DEFAULT 'LIVE'::character varying NOT NULL,
    comment character varying(256),
    sequence_position integer
);


ALTER TABLE public.payment OWNER TO ybashkatov;

--
-- Name: payment payment_pkey; Type: CONSTRAINT; Schema: public; Owner: ybashkatov
--

ALTER TABLE ONLY public.payment
    ADD CONSTRAINT payment_pkey PRIMARY KEY (id);


--
-- Name: payment unique_payment; Type: CONSTRAINT; Schema: public; Owner: ybashkatov
--

ALTER TABLE ONLY public.payment
    ADD CONSTRAINT unique_payment UNIQUE (payment_id);


--
-- Name: agent_id_idx; Type: INDEX; Schema: public; Owner: ybashkatov
--

CREATE INDEX agent_id_idx ON public.payment USING btree (agent_id);


--
-- Name: external_reference_idx; Type: INDEX; Schema: public; Owner: ybashkatov
--

CREATE INDEX external_reference_idx ON public.payment USING btree (external_reference);


--
-- Name: payment_idx; Type: INDEX; Schema: public; Owner: ybashkatov
--

CREATE INDEX payment_idx ON public.payment USING btree (payment_id);


--
-- Name: profile_id_idx; Type: INDEX; Schema: public; Owner: ybashkatov
--

CREATE INDEX profile_id_idx ON public.payment USING btree (profile_id);


--
-- PostgreSQL database dump complete
--

